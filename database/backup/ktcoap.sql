-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 26, 2018 at 04:13 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ktcoap`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"Enter name\"}}}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, NULL, 9),
(81, 9, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(82, 9, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, '{\"validation\":{\"rule\":\"required\",\"messages\":{\"required\":\"กรุณาระบุชื่อ Form\"}}}', 2),
(83, 9, 'description', 'text_area', 'Description', 0, 1, 1, 1, 1, 1, NULL, 3),
(84, 9, 'content_heading', 'text_area', 'Content Heading', 0, 1, 1, 1, 1, 1, NULL, 4),
(85, 9, 'content_sub_heading', 'text_area', 'Content Sub Heading', 0, 1, 1, 1, 1, 1, NULL, 5),
(86, 9, 'content_condition', 'text_area', 'Content Condition', 0, 1, 1, 1, 1, 1, NULL, 6),
(87, 9, 'content_contact', 'rich_text_box', 'Content Contact', 0, 1, 1, 1, 1, 1, NULL, 7),
(88, 9, 'active', 'hidden', 'Active', 0, 0, 1, 1, 1, 0, NULL, 8),
(89, 9, 'create_by', 'hidden', 'Create By', 0, 0, 0, 1, 1, 0, NULL, 9),
(90, 9, 'modified_by', 'hidden', 'Modified By', 0, 0, 0, 1, 1, 0, NULL, 10),
(91, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 1, 1, 0, NULL, 11),
(92, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 1, 1, 0, NULL, 12),
(104, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, NULL, 6),
(105, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(106, 12, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, NULL, 2),
(107, 12, 'slug', 'text', 'Slug', 0, 1, 1, 1, 1, 1, NULL, 3),
(108, 12, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, NULL, 4),
(109, 12, 'content', 'text', 'Content', 0, 1, 1, 1, 1, 1, NULL, 5),
(110, 12, 'layout', 'text', 'Layout', 0, 1, 1, 1, 1, 1, NULL, 6),
(111, 12, 'indicator', 'text', 'Indicator', 0, 1, 1, 1, 1, 1, NULL, 7),
(112, 12, 'sub_indicator', 'text', 'Sub Indicator', 0, 1, 1, 1, 1, 1, NULL, 8),
(113, 12, 'inbound', 'text', 'Inbound', 0, 1, 1, 1, 1, 1, NULL, 9),
(114, 12, 'outbound', 'text', 'Outbound', 0, 1, 1, 1, 1, 1, NULL, 10),
(115, 12, 'active', 'text', 'Active', 0, 1, 1, 1, 1, 1, NULL, 11),
(116, 12, 'LeaveForm_id', 'text', 'LeaveForm Id', 0, 1, 1, 1, 1, 1, NULL, 12),
(117, 12, 'Product_id', 'text', 'Product Id', 0, 1, 1, 1, 1, 1, NULL, 13),
(118, 12, 'create_by', 'text', 'Create By', 0, 1, 1, 1, 1, 1, NULL, 14),
(119, 12, 'modified_by', 'text', 'Modified By', 0, 1, 1, 1, 1, 1, NULL, 15),
(120, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 16),
(121, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 17),
(133, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(134, 14, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, NULL, 2),
(135, 14, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, NULL, 3),
(136, 14, 'content_heading', 'text', 'Content Heading', 0, 1, 1, 1, 1, 1, NULL, 4),
(137, 14, 'content_sub_heading', 'text', 'Content Sub Heading', 0, 1, 1, 1, 1, 1, NULL, 5),
(138, 14, 'content_condition', 'text', 'Content Condition', 0, 1, 1, 1, 1, 1, NULL, 6),
(139, 14, 'content_contact', 'text', 'Content Contact', 0, 1, 1, 1, 1, 1, NULL, 7),
(140, 14, 'active', 'text', 'Active', 0, 1, 1, 1, 1, 1, NULL, 8),
(141, 14, 'create_by', 'text', 'Create By', 0, 1, 1, 1, 1, 1, NULL, 9),
(142, 14, 'modified_by', 'text', 'Modified By', 0, 1, 1, 1, 1, 1, NULL, 10),
(143, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 11),
(144, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 12),
(145, 15, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(146, 15, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 2),
(147, 15, 'active', 'text', 'Active', 1, 1, 1, 1, 1, 1, NULL, 3),
(148, 15, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(149, 15, 'create_by', 'text', 'Create By', 1, 1, 1, 1, 1, 1, NULL, 5),
(150, 15, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(151, 15, 'modified_by', 'text', 'Modified By', 0, 1, 1, 1, 1, 1, NULL, 7),
(152, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(153, 16, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, NULL, 2),
(154, 16, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, NULL, 3),
(155, 16, 'pic', 'text', 'Pic', 0, 1, 1, 1, 1, 1, NULL, 4),
(156, 16, 'sys_product_id', 'text', 'Sys Product Id', 0, 1, 1, 1, 1, 1, NULL, 5),
(157, 16, 'create_by', 'text', 'Create By', 0, 1, 1, 1, 1, 1, NULL, 6),
(158, 16, 'modified_by', 'text', 'Modified By', 0, 1, 1, 1, 1, 1, NULL, 7),
(159, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 8),
(160, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(161, 16, 'active', 'text', 'Active', 0, 1, 1, 1, 1, 1, NULL, 10),
(163, 18, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(164, 18, 'fname', 'text', 'Fname', 0, 1, 1, 1, 1, 1, NULL, 2),
(165, 18, 'lname', 'text', 'Lname', 0, 1, 1, 1, 1, 1, NULL, 3),
(166, 18, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, NULL, 4),
(167, 18, 'tel', 'text', 'Tel', 0, 1, 1, 1, 1, 1, NULL, 5),
(168, 18, 'active', 'text', 'Active', 0, 1, 1, 1, 1, 1, NULL, 6),
(169, 18, 'opt_Salary_id', 'text', 'Opt Salary Id', 0, 1, 1, 1, 1, 1, NULL, 7),
(170, 18, 'PageLeaveForm_id', 'text', 'PageLeaveForm Id', 0, 1, 1, 1, 1, 1, NULL, 8),
(171, 18, 'Product_id', 'text', 'Product Id', 0, 1, 1, 1, 1, 1, NULL, 9),
(172, 18, 'modified_by', 'text', 'Modified By', 1, 1, 1, 1, 1, 1, NULL, 10),
(173, 18, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 11),
(174, 18, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 12);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-04 08:21:00', '2018-10-21 12:35:04'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-10-04 08:21:00', '2018-10-04 08:21:00'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-10-04 08:21:00', '2018-10-04 08:21:00'),
(9, 'leaveform', 'form', 'Form', 'Forms', 'voyager-window-list', 'App\\Leaveform', NULL, 'Backend\\FormController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-20 20:35:02', '2018-10-22 13:31:17'),
(11, 'pageleaveform', 'pageleaveform', 'Page', 'Page', 'voyager-file-text', 'App\\Pageleaveform', NULL, 'Backend\\PageleaveformController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-22 13:57:34', '2018-10-22 13:57:34'),
(12, 'pageleaveforms', 'pageleaveforms', 'Pageleaveform', 'Pageleaveforms', NULL, 'App\\Pageleaveform', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-24 05:26:36', '2018-10-24 05:26:36'),
(14, 'leaveforms', 'leaveforms', 'Leaveform', 'Leaveforms', NULL, 'App\\Leaveform', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-24 05:27:16', '2018-10-24 05:27:16'),
(15, 'opt_salaries', 'opt-salaries', 'Opt Salary', 'Option Salary', 'voyager-list-add', 'App\\OptSalary', NULL, 'Backend\\OptsalaryController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-24 19:33:34', '2018-10-24 19:33:34'),
(16, 'products', 'products', 'Product', 'Products', 'voyager-credit-cards', 'App\\Product', NULL, 'Backend\\ProductController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-24 19:37:17', '2018-10-24 19:37:17'),
(18, 'leavefromapps', 'leads', 'Leads', 'Leads', 'voyager-people', 'App\\Leavefromapp', NULL, 'Backend\\PageleaveformController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-10-26 14:01:36', '2018-10-26 14:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `formlayouts`
--

CREATE TABLE `formlayouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `PageLeaveForm_id` int(11) DEFAULT NULL,
  `card_hero` text COLLATE utf8mb4_unicode_ci,
  `card_cover` text COLLATE utf8mb4_unicode_ci,
  `card_slide` json DEFAULT NULL,
  `heading` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_heading` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `features` json DEFAULT NULL,
  `setting` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `formlayouts`
--

INSERT INTO `formlayouts` (`id`, `PageLeaveForm_id`, `card_hero`, `card_cover`, `card_slide`, `heading`, `sub_heading`, `features`, `setting`, `created_at`, `updated_at`) VALUES
(5, 16, 'pageleaveform-card\\October2018\\OiUSbkzVmkvfliTPI7Nz.png', 'pageleaveform-cover-card\\October2018\\HJAOhZgdH4zRAmbjRLeS.png', NULL, 'aa', 'aaa', '{\"sub\": [\"aa\"], \"title\": [\"aa\"]}', NULL, '2018-10-26 14:43:28', '2018-10-26 14:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `leaveforms`
--

CREATE TABLE `leaveforms` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content_heading` text COLLATE utf8mb4_unicode_ci,
  `content_sub_heading` text COLLATE utf8mb4_unicode_ci,
  `content_condition` text COLLATE utf8mb4_unicode_ci,
  `content_contact` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT '0',
  `create_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leaveforms`
--

INSERT INTO `leaveforms` (`id`, `title`, `description`, `content_heading`, `content_sub_heading`, `content_condition`, `content_contact`, `active`, `create_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(1, 'บัตรเครดิตออนไลน์', 'สมัครบัตรเครดิตออนไลน์\r\nสมัครบัตรเครดิตออนไลน์\r\nสมัครบัตรเครดิตออนไลน์', 'สมัครบัตรเครดิตออนไลน์', 'เจ้าหน้าที่จะติดต่อกลับและแนะนำบัตรที่เหมาะสมกับคุณ ภายใน 2 ชั่วโมง ในวันและเวลาทำการ', '<p>ข้อมูลของผู้สมัครจะไม่ถูกเปิดเผยหรือใช้ร่วมกับบุคคลที่ 3 นอกเหนือจาก KTC</p>', '<p><span class=\"text-center last-notice\"><a class=\"red-notice\" href=\"#\">กรอกใบสมัครออนไลน์</a> หรือ <a class=\"red-notice\" href=\"tel:021236000\"> โทร.02 123 6000</a></span></p>', 0, 1, 1, '2018-10-20 17:00:00', '2018-10-22 13:29:05'),
(2, 'สมัครสินเชื่อเอนกประสงค์ KTC CASH', 'สมัครสินเชื่อเอนกประสงค์ KTC CASH\r\nสมัครสินเชื่อเอนกประสงค์ KTC CASH\r\nสมัครสินเชื่อเอนกประสงค์ KTC CASH', 'สมัครสินเชื่อเอนกประสงค์ KTC CASH', 'เจ้าหน้าที่จะติดต่อกลับภายใน 2 ชั่วโมง ในวันและเวลาทำการ', '<p><span style=\"    margin-bottom: 0;\">ข้อมูลของผู้สมัครจะไม่ถูกเปิดเผยหรือใช้ร่วมกับบุคคลที่ 3 นอกเหนือจาก KTC</span></p>', '<p><span class=\"text-center last-notice\"><a class=\"red-notice\" href=\"#\">กรอกใบสมัครออนไลน์</a> หรือ <a class=\"red-notice\" href=\"tel:021236000\"> โทร.02 123 6000</a></span></p>', 0, 1, 1, '2018-10-20 17:00:00', '2018-10-22 13:29:17'),
(3, 'สมัครบัตรกดเงินสด KTC PROUD', 'เงินสด KTC PROUD', 'สมัครบัตรกดเงินสด KTC PROUD', 'เจ้าหน้าที่จะติดต่อกลับภายใน 2 ชั่วโมง ในวันและเวลาทำการ', '<p><span style=\"color: #212529; font-family: Conv_SukhumvitTadmai_Thi;\">ข้อมูลของผู้สมัครจะไม่ถูกเปิดเผยหรือใช้ร่วมกับบุคคลที่ 3 นอกเหนือจาก KTC</span></p>', '<p><span class=\"text-center last-notice\"><a class=\"red-notice\" href=\"#\">กรอกใบสมัครออนไลน์</a> หรือ <a class=\"red-notice\" href=\"tel:021236000\"> โทร.02 123 6000</a></span></p>', 0, 1, 2, '2018-10-22 09:50:39', '2018-10-24 19:41:08');

-- --------------------------------------------------------

--
-- Table structure for table `leavefromapps`
--

CREATE TABLE `leavefromapps` (
  `id` int(10) UNSIGNED NOT NULL,
  `fname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) DEFAULT '0',
  `opt_Salary_id` int(11) DEFAULT NULL,
  `PageLeaveForm_id` int(11) DEFAULT NULL,
  `Product_id` int(11) DEFAULT NULL,
  `modified_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-10-04 08:21:01', '2018-10-04 08:21:01');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-dashboard', '#000000', NULL, 1, '2018-10-04 08:21:01', '2018-10-20 23:57:28', 'voyager.dashboard', 'null'),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2018-10-04 08:21:01', '2018-10-20 19:50:58', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-10-04 08:21:01', '2018-10-04 08:21:01', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-10-04 08:21:01', '2018-10-04 08:21:01', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 11, '2018-10-04 08:21:01', '2018-10-26 14:02:35', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-10-04 08:21:02', '2018-10-20 19:50:59', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-10-04 08:21:02', '2018-10-20 19:50:59', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-10-04 08:21:02', '2018-10-20 19:50:59', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-10-04 08:21:02', '2018-10-20 19:50:59', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 12, '2018-10-04 08:21:02', '2018-10-26 14:02:36', 'voyager.settings.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-10-04 08:21:14', '2018-10-20 19:50:59', 'voyager.hooks', NULL),
(16, 1, 'Forms', '', '_self', 'voyager-window-list', '#000000', NULL, 9, '2018-10-20 20:14:50', '2018-10-26 14:02:39', 'voyager.form.index', 'null'),
(19, 1, 'Pages', '', '_self', 'voyager-file-text', '#000000', NULL, 5, '2018-10-22 13:57:34', '2018-10-24 19:34:59', 'voyager.pageleaveform.index', 'null'),
(23, 1, 'Option Salary', '', '_self', 'voyager-list-add', NULL, NULL, 10, '2018-10-24 19:33:34', '2018-10-26 14:02:39', 'voyager.opt-salaries.index', NULL),
(24, 1, 'Products', '', '_self', 'voyager-credit-cards', NULL, NULL, 6, '2018-10-24 19:37:17', '2018-10-24 19:38:40', 'voyager.products.index', NULL),
(25, 1, 'Leads', '', '_self', 'voyager-people', NULL, NULL, 8, '2018-10-26 14:01:36', '2018-10-26 14:02:39', 'voyager.leads.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `opt_salaries`
--

CREATE TABLE `opt_salaries` (
  `id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `create_by` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `opt_salaries`
--

INSERT INTO `opt_salaries` (`id`, `title`, `active`, `created_at`, `create_by`, `updated_at`, `modified_by`) VALUES
(1, '500,000 บาทขึ้นไป', 0, '2018-10-14 17:00:00', 1, '2018-10-14 17:00:00', 1),
(2, '100,000 ถึง 499,999 บาท', 0, '2018-10-14 17:00:00', 1, '2018-10-14 17:00:00', 1),
(3, '50,000  ถึง 99,999 บาท', 0, '2018-10-14 17:00:00', 1, '2018-10-14 17:00:00', 1),
(4, 'ไม่เกิน 49,999 บาท', 0, '2018-10-14 17:00:00', 1, '2018-10-14 17:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pageleaveforms`
--

CREATE TABLE `pageleaveforms` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `layout` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `indicator` text COLLATE utf8mb4_unicode_ci,
  `sub_indicator` text COLLATE utf8mb4_unicode_ci,
  `inbound` text COLLATE utf8mb4_unicode_ci,
  `outbound` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) DEFAULT NULL,
  `LeaveForm_id` int(11) DEFAULT NULL,
  `Product_id` int(11) DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pageleaveforms`
--

INSERT INTO `pageleaveforms` (`id`, `title`, `slug`, `description`, `content`, `layout`, `indicator`, `sub_indicator`, `inbound`, `outbound`, `active`, `LeaveForm_id`, `Product_id`, `create_by`, `modified_by`, `created_at`, `updated_at`) VALUES
(16, 'test3456789', 'test3456789', 'tset', NULL, 'card_hero', NULL, NULL, NULL, NULL, 0, 1, 1, 1, 1, '2018-10-26 14:43:28', '2018-10-26 14:43:28');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(2, 'browse_bread', NULL, '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(3, 'browse_database', NULL, '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(4, 'browse_media', NULL, '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(5, 'browse_compass', NULL, '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(6, 'browse_menus', 'menus', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(7, 'read_menus', 'menus', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(8, 'edit_menus', 'menus', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(9, 'add_menus', 'menus', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(10, 'delete_menus', 'menus', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(11, 'browse_roles', 'roles', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(12, 'read_roles', 'roles', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(13, 'edit_roles', 'roles', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(14, 'add_roles', 'roles', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(15, 'delete_roles', 'roles', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(16, 'browse_users', 'users', '2018-10-04 08:21:02', '2018-10-04 08:21:02'),
(17, 'read_users', 'users', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(18, 'edit_users', 'users', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(19, 'add_users', 'users', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(20, 'delete_users', 'users', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(21, 'browse_settings', 'settings', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(22, 'read_settings', 'settings', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(23, 'edit_settings', 'settings', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(24, 'add_settings', 'settings', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(25, 'delete_settings', 'settings', '2018-10-04 08:21:03', '2018-10-04 08:21:03'),
(41, 'browse_hooks', NULL, '2018-10-04 08:21:14', '2018-10-04 08:21:14'),
(67, 'browse_pageleaveform', 'pageleaveforms', '2018-10-24 05:26:37', '2018-10-24 05:26:37'),
(68, 'read_pageleaveform', 'pageleaveforms', '2018-10-24 05:26:37', '2018-10-24 05:26:37'),
(69, 'edit_pageleaveform', 'pageleaveforms', '2018-10-24 05:26:37', '2018-10-24 05:26:37'),
(70, 'add_pageleaveform', 'pageleaveforms', '2018-10-24 05:26:37', '2018-10-24 05:26:37'),
(71, 'delete_pageleaveform', 'pageleaveforms', '2018-10-24 05:26:37', '2018-10-24 05:26:37'),
(77, 'browse_leaveform', 'leaveforms', '2018-10-24 05:27:16', '2018-10-24 05:27:16'),
(78, 'read_leaveform', 'leaveforms', '2018-10-24 05:27:16', '2018-10-24 05:27:16'),
(79, 'edit_leaveform', 'leaveforms', '2018-10-24 05:27:16', '2018-10-24 05:27:16'),
(80, 'add_leaveform', 'leaveforms', '2018-10-24 05:27:16', '2018-10-24 05:27:16'),
(81, 'delete_leaveform', 'leaveforms', '2018-10-24 05:27:16', '2018-10-24 05:27:16'),
(82, 'browse_opt_salaries', 'opt_salaries', '2018-10-24 19:33:34', '2018-10-24 19:33:34'),
(83, 'read_opt_salaries', 'opt_salaries', '2018-10-24 19:33:34', '2018-10-24 19:33:34'),
(84, 'edit_opt_salaries', 'opt_salaries', '2018-10-24 19:33:34', '2018-10-24 19:33:34'),
(85, 'add_opt_salaries', 'opt_salaries', '2018-10-24 19:33:34', '2018-10-24 19:33:34'),
(86, 'delete_opt_salaries', 'opt_salaries', '2018-10-24 19:33:34', '2018-10-24 19:33:34'),
(87, 'browse_products', 'products', '2018-10-24 19:37:17', '2018-10-24 19:37:17'),
(88, 'read_products', 'products', '2018-10-24 19:37:17', '2018-10-24 19:37:17'),
(89, 'edit_products', 'products', '2018-10-24 19:37:17', '2018-10-24 19:37:17'),
(90, 'add_products', 'products', '2018-10-24 19:37:17', '2018-10-24 19:37:17'),
(91, 'delete_products', 'products', '2018-10-24 19:37:17', '2018-10-24 19:37:17'),
(97, 'browse_leavefromapps', 'leavefromapps', '2018-10-26 14:01:36', '2018-10-26 14:01:36'),
(98, 'read_leavefromapps', 'leavefromapps', '2018-10-26 14:01:36', '2018-10-26 14:01:36'),
(99, 'edit_leavefromapps', 'leavefromapps', '2018-10-26 14:01:36', '2018-10-26 14:01:36'),
(100, 'add_leavefromapps', 'leavefromapps', '2018-10-26 14:01:36', '2018-10-26 14:01:36'),
(101, 'delete_leavefromapps', 'leavefromapps', '2018-10-26 14:01:36', '2018-10-26 14:01:36');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(41, 1),
(67, 1),
(67, 2),
(68, 1),
(68, 2),
(69, 1),
(69, 2),
(70, 1),
(70, 2),
(71, 1),
(71, 2),
(77, 1),
(77, 2),
(78, 1),
(78, 2),
(79, 1),
(79, 2),
(80, 1),
(80, 2),
(81, 1),
(81, 2),
(82, 1),
(82, 2),
(83, 1),
(83, 2),
(84, 1),
(84, 2),
(85, 1),
(85, 2),
(86, 1),
(86, 2),
(87, 1),
(87, 2),
(88, 2),
(89, 1),
(89, 2),
(90, 1),
(90, 2),
(91, 1),
(91, 2),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `pic` text COLLATE utf8mb4_unicode_ci,
  `sys_product_id` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `pic`, `sys_product_id`, `create_by`, `modified_by`, `created_at`, `updated_at`, `active`) VALUES
(1, 'KTC X VISA SIGNATURE', 'test upadate', 'products\\October2018\\qFgFpdkiwhmc7KfosfZ5.png', 'ktc001', 1, 1, '2018-10-21 17:00:00', '2018-10-26 02:57:44', 0),
(2, 'KTC CASH BACK VISA PLATINUM', NULL, 'product_test\\RY6j9nRJzTv3SrNvFZiEifEceeG1VFOZJXwM4O10.png', 'ktc002', 1, 1, '2018-10-21 17:00:00', '2018-10-21 17:00:00', 0),
(3, 'card name nw', 'test', 'products\\October2018\\F4iYeAwnEmkoE0gWPuNz.png', '005', 1, 1, '2018-10-26 10:23:17', '2018-10-26 10:23:17', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin MFEC', 'Super Administrator', '2018-10-04 08:21:02', '2018-10-24 19:03:04'),
(2, 'admin ktc', 'KTC Admin', '2018-10-04 08:21:02', '2018-10-24 19:08:14');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'KTC OAP', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'KTC OAP', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\October2018\\cwmAFmMdtly1Qf5shShl.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\October2018\\S1kWZknup7GcFUBNZeag.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'KTC OAP', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to KTC OAP .', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', 'settings\\October2018\\FMkhjwIWtsRDRBVql3Te.png', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\October2018\\jD0Tk2qXeePr3emIsVYU.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-10-04 08:21:12', '2018-10-04 08:21:12'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-10-04 08:21:13', '2018-10-04 08:21:13'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-10-04 08:21:13', '2018-10-04 08:21:13'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-10-04 08:21:13', '2018-10-04 08:21:13'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-10-04 08:21:13', '2018-10-04 08:21:13'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-10-04 08:21:13', '2018-10-04 08:21:13'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-10-04 08:21:13', '2018-10-04 08:21:13'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-10-04 08:21:13', '2018-10-04 08:21:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'MFEC', 'admin@mfec.com', 'users\\October2018\\XDVqEWuBgDHV1zrR0xz3.png', NULL, '$2y$10$H50CZg7dRYSYP6zm7Gn/yOfAWSW3rd6HXm3jn.D1WkFR.BlC1RfHi', '82IsKw9rdm1WLLsTrk9djv5TjYo6SnetCLxPBEB0x7Z71wemxsPOB4m3Vfxu', NULL, '2018-10-04 08:21:10', '2018-10-24 19:16:11'),
(2, 2, 'admin KTC', 'admin@ktc.com', 'users\\October2018\\jOucECv6WNVNMaG2nm1p.png', NULL, '$2y$10$GaLhbDnc7uScwWPfwV1HxORstz3GIL63GPs/vy.Sl7b0wMzc5C3UK', 'nfuPbWHTglEAFzfDGqHtHytj9BACm1sHzcO0YqFivFcKAamWsN8b10mdbx6v', NULL, '2018-10-24 19:17:09', '2018-10-26 10:46:37');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `formlayouts`
--
ALTER TABLE `formlayouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `formlayout_pageleaveform_id_index` (`PageLeaveForm_id`);

--
-- Indexes for table `leaveforms`
--
ALTER TABLE `leaveforms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leaveform_title_index` (`title`),
  ADD KEY `leaveform_active_index` (`active`);

--
-- Indexes for table `leavefromapps`
--
ALTER TABLE `leavefromapps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leavefromapp_fname_index` (`fname`),
  ADD KEY `leavefromapp_lname_index` (`lname`),
  ADD KEY `leavefromapp_opt_salary_id_index` (`opt_Salary_id`),
  ADD KEY `leavefromapp_pageleaveform_id_index` (`PageLeaveForm_id`),
  ADD KEY `leavefromapp_product_id_index` (`Product_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `opt_salaries`
--
ALTER TABLE `opt_salaries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_UNIQUE` (`id`),
  ADD UNIQUE KEY `title_UNIQUE` (`title`),
  ADD KEY `del_UNIQUE` (`active`) USING BTREE;

--
-- Indexes for table `pageleaveforms`
--
ALTER TABLE `pageleaveforms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pageleaveform_slug_index` (`slug`),
  ADD KEY `pageleaveform_active_index` (`active`),
  ADD KEY `pageleaveform_leaveform_id_index` (`LeaveForm_id`),
  ADD KEY `pageleaveform_product_id_index` (`Product_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_sys_product_id_index` (`sys_product_id`),
  ADD KEY `product_active_index` (`active`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `formlayouts`
--
ALTER TABLE `formlayouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `leaveforms`
--
ALTER TABLE `leaveforms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leavefromapps`
--
ALTER TABLE `leavefromapps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `opt_salaries`
--
ALTER TABLE `opt_salaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pageleaveforms`
--
ALTER TABLE `pageleaveforms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
