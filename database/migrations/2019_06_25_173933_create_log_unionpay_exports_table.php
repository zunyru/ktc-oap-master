<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogUnionpayExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_unionpay_exports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('last_unionpay_apps_id');
            $table->string('last_customer_id',10);
            $table->integer('row_amount');
            $table->text('file_name');
            $table->dateTime('time_stmp')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_unionpay_exports');
    }
}
