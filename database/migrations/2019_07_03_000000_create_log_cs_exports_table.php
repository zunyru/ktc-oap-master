<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogCsExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_cs_exports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('last_lead_id');
            $table->string('last_lead_unq_id',15);
            $table->integer('row_amount');
            $table->text('file_name');
            $table->dateTime('time_stmp')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_cs_exports');
    }
}
