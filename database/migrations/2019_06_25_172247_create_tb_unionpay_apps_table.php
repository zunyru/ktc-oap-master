<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbUnionpayAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_unionpay_apps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('customer_id',10);
            $table->string('fname',100);
            $table->string('lname',100);
            $table->string('product_id',5);
            $table->integer('is_duplicated',1)->nullable()->default(null);
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_unionpay_apps');
    }
}
