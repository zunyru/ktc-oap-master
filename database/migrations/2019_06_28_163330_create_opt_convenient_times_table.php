<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptConvenientTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opt_convenient_times', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',500);
            $table->string('opt_value',500);
            $table->string('opt_type',1);
            $table->integer('order');
            $table->integer('create_by');
            $table->integer('modified_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opt_convenient_times');
    }
}
