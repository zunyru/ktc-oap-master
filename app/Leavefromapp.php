<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Leavefromapp extends Model
{
    use LogsActivity;

    protected $table = 'leavefromapps';

    protected static $logAttributes = ['*'];
    protected static $logName = 'Lead';
    protected static $recordEvents = ['deleted','updated'];

    public function isDuplicated() {
        $filterDate = new Carbon(setting('leads.duplicate_filter_time_number').' '.setting('leads.duplicate_filter_time_unit').' ago');
        $data_row=$this;

        switch ($data_row->lead_dest) {
            case 'CS-Tele':

                $duplicated = Leavefromapp::whereDate('created_at', '>=', $filterDate)
                    ->where('id', '<', $this->id)
                    ->where('tel', str_replace(' ', '', $data_row->tel))
                    ->where('product_id', $data_row->Product_id)
                    ->orderBy('id', 'asc')
                    ->count();

                break;
            default:
                $duplicated = Leavefromapp::whereDate('created_at', '>=', $filterDate)
                    ->where('id', '<', $this->id)
                    ->where(function($query) use ($data_row) {
                            $query->orWhere('email', $data_row->email)
                            ->orWhere('tel', str_replace(' ', '', $data_row->tel));
                    })
                    ->orderBy('id', 'asc')
                    ->count();

                break;
        }


        return $duplicated > 0;
    }

    public function product() {
        return $this->hasOne('App\Product', 'id', 'Product_id');
    }

    public function option() {
        return $this->hasOne('App\OptSalary', 'id', 'opt_Salary_id');
    }

    public function page() {
        return $this->hasOne('App\Pageleaveform', 'id', 'PageLeaveForm_id');
    }
}
