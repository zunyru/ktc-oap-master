<?php

namespace App\Widgets;

use App\Exports\DashboardReport;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use App\Pageleaveform;
use App\Leavefromapp;
use DB;

class Pages extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
     public function run()
    {

        $page_count = DB::table('pageleaveforms')
            ->where('active', 0)
            ->get();

        $page_count_m = DB::table('pageleaveforms')
               ->where('active', 0)
               ->whereMonth('updated_at', '=', date('m'))
               ->get();

        $page_count_y = DB::table('pageleaveforms')
               ->where('active', 0)
               ->whereYear('updated_at', '=', date('Y'))
               ->get();

        $lead_count = DB::table('leavefromapps')
            ->where('active', 0)
            ->get();

        $lead_count_m = DB::table('leavefromapps')
               ->where('active', 0)
               ->whereMonth('created_at', '=', date('m'))
               ->get();

        $lead_count_y = DB::table('leavefromapps')
               ->where('active', 0)
               ->whereYear('created_at', '=', date('Y'))
               ->get();

       /*SELECT PageLeaveForm_id,pages.title,branches.title, count( PageLeaveForm_id ) AS count_id FROM leavefromapps leads INNER JOIN pageleaveforms pages ON pages.id = leads.PageLeaveForm_id LEFT JOIN branches ON pages.branch_id = branches.id GROUP BY PageLeaveForm_id order by count_page_id desc LIMIT 5*/

        $page_top_five = DB::table('leavefromapps')
            ->select('PageLeaveForm_id','pageleaveforms.title','pageleaveforms.slug','branches.title AS branches_name','branches.code', DB::raw('count( PageLeaveForm_id ) AS count_page_id'))
            ->Join('pageleaveforms', 'pageleaveforms.id', '=', 'leavefromapps.PageLeaveForm_id')
            ->leftJoin('branches', 'pageleaveforms.branch_id', '=', 'branches.id')
            ->where('pageleaveforms.active', 0)
            ->groupBy('PageLeaveForm_id')
            ->orderBy('count_page_id','desc')
            ->limit(10)
            ->get();

//        $maxValue = Leavefromapp::max('PageLeaveForm_id');

        $report = new DashboardReport();

        $data['top'] = $page_top_five;
        $data['pages'] = [
                    'count' => "{$page_count->count()}",
                    'count_month' => "{$page_count_m->count()}",
                    'count_year' => "{$page_count_y->count()}",
                    'text' => 'View all Pages',
                    'link' => route('voyager.pageleaveform.index'),
                 ];
        $data['leads'] = [
                    'count' => "{$lead_count->count()}",
                    'count_month' => "{$lead_count_m->count()}",
                    'count_year' => "{$lead_count_y->count()}",
                    'text' => 'View all Leads',
                    'link' => route('voyager.leads.index'),
                 ];
        $data['report'] = $report->query();

        return view('backend.dashboard',$data);


        /*return view('backend.dashboard' , array_merge($this->config, [
            'title'  => "{$string}",
            'data' => [
                'pages' => [
                    'count' => "{$count_page}",
                    'count_month' => "{$page_count_m}",
                    'count_year' => '',
                    'text' => 'View all Pages',
                    'link' => route('voyager.pageleaveform.index'),
                 ],
                'leads' => [
                    'count' => "{$count_leads}",
                    'count_month' => '',
                    'count_year' => '',
                    'text' => 'View all Leads',
                    'link' => route('voyager.leads.index'),
                 ],
            ],
        ]));*/
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        //return Auth::user()->can('browse', Voyager::model('User'));
        return Voyager::can('browse_admin');
    }
}
