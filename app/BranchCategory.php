<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class BranchCategory extends Model
{
    use SoftDeletes,LogsActivity;
    protected $dates = ['deleted_at'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Branch Category';

    public function branches() {
        return $this->hasMany('App\Branch', 'branch_category_id', 'id');
    }
}