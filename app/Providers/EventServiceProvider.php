<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\CheckedAppStatus' => [
            'App\Listeners\SaveLogCheckedAppStatus',
        ],
        'App\Events\AppliedCard' => [
            'App\Listeners\SaveLogAppliedCard',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
     public function boot()
    {
        parent::boot();

        Event::listen('Illuminate\Auth\Events\Login', function() {

          activity('User Auth')
                ->log('logged-in');
        });

        Event::listen('Illuminate\Auth\Events\Logout', function() {

          activity('User Auth')
                ->log('logout');
        });
    }
}
