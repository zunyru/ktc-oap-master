<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnionpayApp extends Model
{
    protected $table = 'tb_unionpay_apps';
    protected $translatable = ['customer_id', 'fname', 'lname', 'product_id', 'is_duplicated'];
}
