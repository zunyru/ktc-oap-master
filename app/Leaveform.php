<?php

namespace App;

use App\Models\ModelTranslatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Leaveform extends Model
{
    use ModelTranslatable,LogsActivity;
    public $timestamps = false;

    protected $table = 'leaveforms';
    protected $translatable = ['title', 'description', 'content_heading', 'content_sub_heading', 'content_condition', 'content_contact'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Leave Form';

    public function salaryGroup() {
        return $this->hasOne('App\OptSalaryGroup', 'id', 'opt_salary_group_id');
    }

    public function customField($index) {
        $custom_field_json = $this->{"cf_".$index};
        $custom_field_arr = json_decode($custom_field_json, true);

        return [
            "key_name" => $custom_field_arr["key_name"],
            "label_name" => (session('local') == "en" ? $custom_field_arr["label_name_en"] : $custom_field_arr["label_name"]),
            "opt" => $custom_field_arr["opt"]
        ];
    }
}