<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadsCsReportLog extends Model
{
	protected $table = 'log_cs_exports';
	public $timestamps = false;
}
