<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2018-12-06
 * Time: 23:38
 */

namespace App\Exports;


use App\Leavefromapp;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class LeadsExport implements FromView
{
    use Exportable;

    private $query;

    public function __construct( $query = null, $mode = null)
    {
        $this->query = $query;
        $this->mode = $mode;
    }

    public function view(): View
       {
            $query=$this->query;
            switch ($this->mode) {
              case 'back-office':
                $template_mode="backend.excel_exports.leads-export-back-office";
                break;
              default:
                $template_mode="backend.excel_exports.leads-export";
                break;
            }
           return view($template_mode, [
               'leads' => $query->get()
           ]);
       }
}