<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2018-12-06
 * Time: 23:38
 */

namespace App\Exports;



use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class VisaExport implements FromView
{
    use Exportable;

    private $query;

    public function __construct( $query = null)
    {
        $this->query = $query;
    }

    public function view(): View
       {
            $query=$this->query;
           return view('backend.excel_exports.visa-export', [
               'visas' => $query->get()
           ]);
       }
}