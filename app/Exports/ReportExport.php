<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2018-12-09
 * Time: 03:18
 */

namespace App\Exports;


use App\BranchCategory;
use App\Leavefromapp;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ReportExport implements FromView
{
    use Exportable;

    public function view(): View
    {

        /**
         * SELECT
        branches.id,branches.title,a.branch_code,
        COUNT(CASE WHEN MONTH(a.created_at) = 1  THEN a.created_at END) AS 'Jan',
        COUNT(CASE WHEN MONTH(a.created_at) = 2  THEN a.created_at END) AS 'Feb',
        COUNT(CASE WHEN MONTH(a.created_at) = 3  THEN a.created_at END) AS 'Mar',
        COUNT(CASE WHEN MONTH(a.created_at) = 4  THEN a.created_at END) AS 'Apr',
        COUNT(CASE WHEN MONTH(a.created_at) = 5  THEN a.created_at END) AS 'May',
        COUNT(CASE WHEN MONTH(a.created_at) = 6  THEN a.created_at END) AS 'Jun',
        COUNT(CASE WHEN MONTH(a.created_at) = 7  THEN a.created_at END) AS 'Jul',
        COUNT(CASE WHEN MONTH(a.created_at) = 8  THEN a.created_at END) AS 'Aug',
        COUNT(CASE WHEN MONTH(a.created_at) = 9  THEN a.created_at END) AS 'Sep',
        COUNT(CASE WHEN MONTH(a.created_at) = 10 THEN a.created_at END) AS 'Oct',
        COUNT(CASE WHEN MONTH(a.created_at) = 11 THEN a.created_at END) AS 'Nov',
        COUNT(CASE WHEN MONTH(a.created_at) = 12 THEN a.created_at END) AS 'Dec',
        COUNT(a.created_at)                                           AS 'TOTAL',
        COUNT(a.created_at)/12                                        AS 'AVG PER MONTH',
        COUNT(a.created_at)/(SELECT COUNT(DISTINCT MONTH(created_at)) FROM leavefromapps b  WHERE b.branch_code=a.branch_code) AS 'AVG PER AVAILABLE MONTH',
        COUNT(a.created_at)/365                                       AS 'AVG PER DAY',
        COUNT(a.created_at)/(SELECT COUNT(DISTINCT date_format(created_at,'%Y-%m-%d')) FROM leavefromapps b WHERE b.branch_code=a.branch_code) AS 'AVG PER AVAILABLE DAY'
        FROM
        leavefromapps a
        LEFT JOIN branches  on TRIM(branches.code) = TRIM(a.branch_code)
        WHERE YEAR(a.created_at) =2018 AND MONTH(a.created_at) BETWEEN 1 AND 12
        AND a.branch_code IS NOT NULL
        GROUP BY a.branch_code, branches.id
        ORDER BY branches.title ASC
         */
        $start_month = 1;
        $end_month = 12;
//        $days = 62;
        $year = 2018;

        $months = $end_month - $start_month + 1;

        $start_date = Carbon::createMidnightDate($year, $start_month, 1);
        $end_date = Carbon::createMidnightDate($year, $end_month)->endOfMonth();
        $days = $end_date->diffInDays($start_date);

        $leads_stat = DB::select('SELECT
   branches.id,branches.title,TRIM(a.branch_code) AS \'branch_code\',branch_category_id,
   COUNT(CASE WHEN MONTH(a.created_at) = 1  THEN a.created_at END) AS \'Jan\',
   COUNT(CASE WHEN MONTH(a.created_at) = 2  THEN a.created_at END) AS \'Feb\',
   COUNT(CASE WHEN MONTH(a.created_at) = 3  THEN a.created_at END) AS \'Mar\',
   COUNT(CASE WHEN MONTH(a.created_at) = 4  THEN a.created_at END) AS \'Apr\',
   COUNT(CASE WHEN MONTH(a.created_at) = 5  THEN a.created_at END) AS \'May\',
   COUNT(CASE WHEN MONTH(a.created_at) = 6  THEN a.created_at END) AS \'Jun\',
   COUNT(CASE WHEN MONTH(a.created_at) = 7  THEN a.created_at END) AS \'Jul\',
   COUNT(CASE WHEN MONTH(a.created_at) = 8  THEN a.created_at END) AS \'Aug\',
   COUNT(CASE WHEN MONTH(a.created_at) = 9  THEN a.created_at END) AS \'Sep\',
   COUNT(CASE WHEN MONTH(a.created_at) = 10 THEN a.created_at END) AS \'Oct\',
   COUNT(CASE WHEN MONTH(a.created_at) = 11 THEN a.created_at END) AS \'Nov\',
   COUNT(CASE WHEN MONTH(a.created_at) = 12 THEN a.created_at END) AS \'Dec\',
   COUNT(a.created_at)                                           AS \'TOTAL\',
   COUNT(a.created_at)/:months                                        AS \'AVG PER MONTH\',
   COUNT(a.created_at)/(SELECT COUNT(DISTINCT MONTH(created_at)) FROM leavefromapps b  WHERE b.branch_code=a.branch_code) AS \'AVG PER AVAILABLE MONTH\',
   COUNT(a.created_at)/:days                                       AS \'AVG PER DAY\',
   COUNT(a.created_at)/(SELECT COUNT(DISTINCT date_format(created_at,\'%Y-%m-%d\')) FROM leavefromapps b WHERE b.branch_code=a.branch_code) AS \'AVG PER AVAILABLE DAY\',
   (SELECT COUNT(DISTINCT MONTH(created_at)) FROM leavefromapps b WHERE b.branch_code=a.branch_code) AS \'available_months\',
   (SELECT COUNT(DISTINCT date_format(created_at,\'%Y-%m-%d\')) FROM leavefromapps b WHERE b.branch_code=a.branch_code) AS \'available_days\'
 FROM
   leavefromapps a
INNER JOIN branches  on TRIM(branches.code) = TRIM(a.branch_code)
INNER JOIN branch_categories on branches.branch_category_id = branch_categories.id
WHERE YEAR(a.created_at) = :year 
  AND MONTH(a.created_at) BETWEEN :start_month AND :end_month
  AND branch_categories.deleted_at IS NULL
  AND branches.deleted_at IS NULL
 GROUP BY a.branch_code, branches.id
 ORDER BY branch_category_id, branches.title',
            [
                "months" => $months,
                "days" => $days,
                "year" => $year,
                "start_month" => $start_month,
                "end_month" => $end_month
            ]);

        $leads_monthly = $this->exportLeadsMonthly($year);

        $leads_grand_total = $this->getYearGrandTotal($year);
        $leads_year_product_total = $this->getYearTotalByBranchProduct($year);
        $leads_year_branch_cat_total = $this->getYearTotalByBranchCategory($year);

        return view('report.export', [
            'branch_categories' => BranchCategory::with('branches')->get(),
            'leads' => collect($leads_stat),
            'leads_monthly' => collect($leads_monthly),
            'leads_grand_total' => $leads_grand_total,
            'leads_total_year_branch_product' => $leads_year_product_total,
            'leads_total_year_branch_category' => $leads_year_branch_cat_total,
            'year' => $year,
            'months' => $months,
            'days' => $days,
            'start_date' => $start_date,
            'end_date' => $end_date
        ]);
    }

    protected function exportLeadsMonthly($year) {
        $leads_monthly = DB::select('SELECT
   branches.id,branches.title,TRIM(a.branch_code) as \'branch_code\',branch_category_id,
   COUNT(DATE(a.created_at)) as \'count\',
   DATE(a.created_at) as \'date\'
 FROM
   leavefromapps a
INNER JOIN branches  on TRIM(branches.code) = TRIM(a.branch_code)
INNER JOIN branch_categories on branches.branch_category_id = branch_categories.id
WHERE YEAR(a.created_at) = :year 
  AND MONTH(a.created_at) between 1 and 12
  AND branch_categories.deleted_at IS NULL
  AND branches.deleted_at IS NULL
AND a.branch_code IS NOT NULL
 GROUP BY TRIM(a.branch_code), date, branches.id, branch_category_id
 ORDER BY branch_category_id, branches.title',
            [
                "year" => $year,
            ]);

        /**
         * data:
         * ["branch_code" => ["1" => [1,2,3,...31], "2" => [1,2,3,...,28], ..., "12" => [1,2,3,...31],
         * ...
         * "branch_title" => ["1" => [], ..., "12" => []],
         * ...
         * ]
         */
        $data = [];
        $data["grand_total"] = [];
        foreach ($leads_monthly as $item) {
            $date = new Carbon($item->date);
            if (!isset($data[$item->branch_code])) {
                $data[$item->branch_code] = [];
            }

            if (!isset($data[$item->branch_code][$date->month])) {
                $data[$item->branch_code][$date->month] = [];
            }

            $data[$item->branch_code][$date->month][$date->day] = $item->count;

            if (!isset($data[$item->title])) {
                $data[$item->title] = [];
            }
            if (!isset($data[$item->title][$date->month])) {
                $data[$item->title][$date->month] = [];
            }
            if (!isset($data[$item->title][$date->month][$date->day])) {
                $data[$item->title][$date->month][$date->day] = 0;
            }
            $data[$item->title][$date->month][$date->day] += $item->count;

            $branch_cat_key = 'branch_cat_'.$item->branch_category_id;
            if(!isset($data[$branch_cat_key])) {
                $data[$branch_cat_key] = [];
            }
            if(!isset($data[$branch_cat_key][$date->month])) {
                $data[$branch_cat_key][$date->month] = [];
            }
            if(!isset($data[$branch_cat_key][$date->month][$date->day])) {
                $data[$branch_cat_key][$date->month][$date->day] = 0;
            }
            $data[$branch_cat_key][$date->month][$date->day] += $item->count;

            if(!isset($data['grand_total'][$date->month])) {
                $data['grand_total'][$date->month] = [];
            }
            if(!isset($data['grand_total'][$date->month][$date->day])) {
                $data['grand_total'][$date->month][$date->day] = 0;
            }
            $data['grand_total'][$date->month][$date->day] += $item->count;
        }

        return $data;
    }

    protected function getYearTotalByBranchCategory($year) {
        $results = DB::select('SELECT
  c.title,
  COUNT(CASE WHEN MONTH(a.created_at) = 1  THEN a.created_at END) AS \'Jan\',
  COUNT(CASE WHEN MONTH(a.created_at) = 2  THEN a.created_at END) AS \'Feb\',
  COUNT(CASE WHEN MONTH(a.created_at) = 3  THEN a.created_at END) AS \'Mar\',
  COUNT(CASE WHEN MONTH(a.created_at) = 4  THEN a.created_at END) AS \'Apr\',
  COUNT(CASE WHEN MONTH(a.created_at) = 5  THEN a.created_at END) AS \'May\',
  COUNT(CASE WHEN MONTH(a.created_at) = 6  THEN a.created_at END) AS \'Jun\',
  COUNT(CASE WHEN MONTH(a.created_at) = 7  THEN a.created_at END) AS \'Jul\',
  COUNT(CASE WHEN MONTH(a.created_at) = 8  THEN a.created_at END) AS \'Aug\',
  COUNT(CASE WHEN MONTH(a.created_at) = 9  THEN a.created_at END) AS \'Sep\',
  COUNT(CASE WHEN MONTH(a.created_at) = 10 THEN a.created_at END) AS \'Oct\',
  COUNT(CASE WHEN MONTH(a.created_at) = 11 THEN a.created_at END) AS \'Nov\',
  COUNT(CASE WHEN MONTH(a.created_at) = 12 THEN a.created_at END) AS \'Dec\',
  COUNT(a.created_at) AS \'total\',
  COUNT(a.created_at)/12 AS \'avg_per_month\',
  COUNT(a.created_at)/(SELECT COUNT(DISTINCT MONTH(created_at)) FROM leavefromapps) AS \'avg_per_available_month\',
  COUNT(a.created_at)/365                                       AS \'avg_per_day\',
  COUNT(a.created_at)/(SELECT COUNT(DISTINCT DATE(created_at)) FROM leavefromapps) AS \'avg_per_available_day\',
  COUNT(DISTINCT MONTH(a.created_at)) AS \'available_months\',
  COUNT(DISTINCT DATE(a.created_at)) AS \'available_days\'
FROM
  leavefromapps a
    INNER JOIN branches b ON TRIM(b.code) = TRIM(a.branch_code)
    INNER JOIN branch_categories c ON b.branch_category_id = c.id
WHERE YEAR(a.created_at) = :year
  AND MONTH(a.created_at) BETWEEN 1 AND 12
  AND c.deleted_at IS NULL
  AND b.deleted_at IS NULL
GROUP BY c.title',
            [
                $year,
            ]);

        return collect($results);
    }

    protected function getYearTotalByBranchProduct($year) {
        $results = DB::select('SELECT
       b.title, c.title AS \'branch_category_title\',
  COUNT(CASE WHEN MONTH(a.created_at) = 1  THEN a.created_at END) AS \'Jan\',
  COUNT(CASE WHEN MONTH(a.created_at) = 2  THEN a.created_at END) AS \'Feb\',
  COUNT(CASE WHEN MONTH(a.created_at) = 3  THEN a.created_at END) AS \'Mar\',
  COUNT(CASE WHEN MONTH(a.created_at) = 4  THEN a.created_at END) AS \'Apr\',
  COUNT(CASE WHEN MONTH(a.created_at) = 5  THEN a.created_at END) AS \'May\',
  COUNT(CASE WHEN MONTH(a.created_at) = 6  THEN a.created_at END) AS \'Jun\',
  COUNT(CASE WHEN MONTH(a.created_at) = 7  THEN a.created_at END) AS \'Jul\',
  COUNT(CASE WHEN MONTH(a.created_at) = 8  THEN a.created_at END) AS \'Aug\',
  COUNT(CASE WHEN MONTH(a.created_at) = 9  THEN a.created_at END) AS \'Sep\',
  COUNT(CASE WHEN MONTH(a.created_at) = 10 THEN a.created_at END) AS \'Oct\',
  COUNT(CASE WHEN MONTH(a.created_at) = 11 THEN a.created_at END) AS \'Nov\',
  COUNT(CASE WHEN MONTH(a.created_at) = 12 THEN a.created_at END) AS \'Dec\',
  COUNT(a.created_at) AS \'total\',
  COUNT(a.created_at)/12 AS \'avg_per_month\',
  COUNT(a.created_at)/(SELECT COUNT(DISTINCT MONTH(created_at)) FROM leavefromapps) AS \'avg_per_available_month\',
  COUNT(a.created_at)/365                                       AS \'avg_per_day\',
  COUNT(a.created_at)/(SELECT COUNT(DISTINCT DATE(created_at)) FROM leavefromapps) AS \'avg_per_available_day\',
  COUNT(DISTINCT MONTH(a.created_at)) AS \'available_months\',
  COUNT(DISTINCT DATE(a.created_at)) AS \'available_days\'
FROM
  leavefromapps a
INNER JOIN branches b ON TRIM(b.code) = TRIM(a.branch_code)
INNER JOIN branch_categories c ON b.branch_category_id = c.id
WHERE YEAR(a.created_at) = :year
  AND MONTH(a.created_at) BETWEEN 1 AND 12
  AND c.deleted_at IS NULL
  AND b.deleted_at IS NULL
GROUP BY b.title, c.title',
            [
                $year
            ]);

        return collect($results);
    }

    protected function getYearGrandTotal($year) {
        $results = DB::select('SELECT
  COUNT(CASE WHEN MONTH(a.created_at) = 1  THEN a.created_at END) AS \'Jan\',
  COUNT(CASE WHEN MONTH(a.created_at) = 2  THEN a.created_at END) AS \'Feb\',
  COUNT(CASE WHEN MONTH(a.created_at) = 3  THEN a.created_at END) AS \'Mar\',
  COUNT(CASE WHEN MONTH(a.created_at) = 4  THEN a.created_at END) AS \'Apr\',
  COUNT(CASE WHEN MONTH(a.created_at) = 5  THEN a.created_at END) AS \'May\',
  COUNT(CASE WHEN MONTH(a.created_at) = 6  THEN a.created_at END) AS \'Jun\',
  COUNT(CASE WHEN MONTH(a.created_at) = 7  THEN a.created_at END) AS \'Jul\',
  COUNT(CASE WHEN MONTH(a.created_at) = 8  THEN a.created_at END) AS \'Aug\',
  COUNT(CASE WHEN MONTH(a.created_at) = 9  THEN a.created_at END) AS \'Sep\',
  COUNT(CASE WHEN MONTH(a.created_at) = 10 THEN a.created_at END) AS \'Oct\',
  COUNT(CASE WHEN MONTH(a.created_at) = 11 THEN a.created_at END) AS \'Nov\',
  COUNT(CASE WHEN MONTH(a.created_at) = 12 THEN a.created_at END) AS \'Dec\',
  COUNT(a.created_at) AS \'total\',
  COUNT(a.created_at)/12 AS \'avg_per_month\',
  COUNT(a.created_at)/(SELECT COUNT(DISTINCT MONTH(created_at)) FROM leavefromapps b) AS \'avg_per_available_month\',
  COUNT(a.created_at)/365                                       AS \'avg_per_day\',
  COUNT(a.created_at)/(SELECT COUNT(DISTINCT DATE(created_at)) FROM leavefromapps b) AS \'avg_per_available_day\',
  COUNT(DISTINCT MONTH(a.created_at)) AS \'available_months\',
  COUNT(DISTINCT DATE(a.created_at)) AS \'available_days\'
FROM
  leavefromapps a
INNER JOIN branches b ON TRIM(b.code) = TRIM(a.branch_code)
INNER JOIN branch_categories c ON b.branch_category_id = c.id
WHERE YEAR(a.created_at) = :year 
  AND MONTH(a.created_at) BETWEEN 1 AND 12
  AND c.deleted_at IS NULL
  AND b.deleted_at IS NULL',
            [
                $year
            ]);

        return collect($results);
    }
}