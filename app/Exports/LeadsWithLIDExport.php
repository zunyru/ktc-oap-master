<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2019-01-09
 * Time: 17:44
 */

namespace App\Exports;


use App\Leavefromapp;
use Illuminate\Database\Eloquent\Builder;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class LeadsWithLIDExport implements FromView, WithCustomCsvSettings
{
    use Exportable;

    private $query;

    public function __construct(Builder $query = null, $mode = null)
    {
        $this->query = $query;
        $this->mode = $mode;
    }
    public function view(): View
   {
        $query=$this->query;
        switch ($this->mode) {
          case 'back-office':
            $template_mode="backend.excel_exports.leads-export-back-office";
            break;
          default:
            $template_mode="backend.excel_exports.leads-export";
            break;
        }
       return view($template_mode, [
           'leads' => $query->get()
       ]);
   }

    /**
     * @return array
     */
    public function getCsvSettings(): array
    {
        return [
            'delimiter' => '|'
        ];
    }
}