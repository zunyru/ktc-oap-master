<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2018-12-27
 * Time: 03:39
 */

namespace App\Exports;


use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Branch;
use App\BranchCategory;
use App\Leavefromapp;

class DashboardReport
{
    private $branches = [];

    public function __construct(array $branches = null)
    {
        if(is_array($branches)) {
            $this->branches = $branches;
        }
    }

    function query() {

        $result = $this->getSQLQuery();

        if(count($this->branches) > 0) {
            $result->whereIn('branch_code', $this->branches);
        }

        $raw_data = $result->all();



        // $now = Carbon::now('Asia/Bangkok');
        $now = Carbon::create(2018,12,31);
        $start_month = $now->copy()->startOfYear();
        $end_month = $now->copy();
        $prev_month = $now->copy()->startOfMonth()->month($now->month - 1);

        $type_data = [];
        $grand_total = [];
        $growth = [];
        $info = [
            "as_of" => $now->day." ".$now->shortEnglishMonth,
            "work_days" => $now->copy()->startOfMonth()->diffInWeekdays($now->copy()->endOfMonth()),
            "passed_work_days" => $now->diffInWeekdays($now->copy()->startOfMonth()),
            "start_month" => $start_month,
            "current_month" => $now,
            "previous_month" => $prev_month
        ];
        $info["avail_work_days"] = $info["work_days"] - $info["passed_work_days"];

        $grand_total["total"] = 0;

        if (count($raw_data) == 0) {
            // return [
            //     "info" => $info,
            //     "type_data" => [],
            //     "today_text" => $now->format('j M')
            // ];
        }

        $loop_date = $start_month->copy();
        foreach ($raw_data as $row) {
            $type_data[$row->title]["total"] = $row->TOTAL;
            $grand_total["total"] += $row->TOTAL;
            $growth[$row->title] = [
                "max_month" => 0,
                "prev_month" => 0,
                "predict" => 0
            ];

            for ($m = $start_month->month; $m <= $end_month->month; $m++) {
                $month_name = $loop_date->month($m)->shortEnglishMonth;
                $type_data[$row->title][$month_name] = round($row->{"DAY_".$month_name} ?? 0, 2);
                if(!isset($grand_total[$month_name])) {
                    $grand_total[$month_name] = 0;
                }
                $grand_total[$month_name] += $type_data[$row->title][$month_name];
            }
        }

        foreach ($type_data as $key => $datum) {
            $end_month_num = $datum[$end_month->shortEnglishMonth];
            $start_month_num = $datum[$start_month->shortEnglishMonth];
            $growth[$key]["max_month"] = $this->findGrowth($end_month_num, $start_month_num);
            if($end_month->month == 1) {
                $growth[$key]["prev_month"] = "-";
            } else {
                $prev_month_num = $datum[$prev_month->shortEnglishMonth];
                $growth[$key]["prev_month"] = $this->findGrowth($end_month_num, $prev_month_num);
            }

            $growth[$key]["predict"] = ceil($datum["total"] + ($end_month_num * $info["avail_work_days"]));
        }

        $total_end_month_num = $grand_total[$end_month->shortEnglishMonth] ?? 0;
        $total_start_month_num = $grand_total[$start_month->shortEnglishMonth] ?? 0;
        $total_prev_month_num = $grand_total[$prev_month->shortEnglishMonth] ?? 0;
        $growth["total"] = [
            "max_month" => $this->findGrowth($total_end_month_num, $total_start_month_num),
            "prev_month" => $this->findGrowth($total_end_month_num, $total_prev_month_num),
            "predict" => ceil($grand_total["total"] + ($total_end_month_num * $info["avail_work_days"]))
        ];

        //Get product
        $forms = Leavefromapp::all();

        //Branch select
        $branches = Branch::whereIn('code', $forms->pluck('branch_code')->unique()->all())->orderBy('code', 'ASC')->get();

        //Get branch_categories
        $branch_categories = BranchCategory::orderBy('title', 'ASC')->get();

        return [
            "branches" => $branches,
            "branch_categories" => $branch_categories,
            "info" => $info,
            "type_data" => $type_data,
            "total" => $grand_total,
            "growth" => $growth,
            "today_text" => $now->format('j M')
        ];
    }

    private function getSQLQuery() {
        $query = 'SELECT
  branch_categories.title,
  COUNT(a.created_at) AS \'TOTAL\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 1  THEN DATE(a.created_at) END) AS \'DAY_Jan\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 2  THEN DATE(a.created_at) END) AS \'DAY_Feb\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 3  THEN DATE(a.created_at) END) AS \'DAY_Mar\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 4  THEN DATE(a.created_at) END) AS \'DAY_Apr\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 5  THEN DATE(a.created_at) END) AS \'DAY_May\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 6  THEN DATE(a.created_at) END) AS \'DAY_Jun\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 7  THEN DATE(a.created_at) END) AS \'DAY_Jul\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 8  THEN DATE(a.created_at) END) AS \'DAY_Aug\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 9  THEN DATE(a.created_at) END) AS \'DAY_Sep\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 10 THEN DATE(a.created_at) END) AS \'DAY_Oct\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 11 THEN DATE(a.created_at) END) AS \'DAY_Nov\',
  COUNT(a.created_at)/COUNT(DISTINCT CASE WHEN MONTH(a.created_at) = 12 THEN DATE(a.created_at) END) AS \'DAY_Dec\'
FROM
  leavefromapps a
    INNER JOIN branches  on TRIM(branches.code) = TRIM(a.branch_code)
    INNER JOIN branch_categories on branches.branch_category_id = branch_categories.id
WHERE YEAR(a.created_at) = :year
  AND MONTH(a.created_at) BETWEEN :start_month AND :end_month
  AND branch_categories.deleted_at IS NULL
  AND branches.deleted_at IS NULL
GROUP BY branch_categories.id
ORDER BY branch_categories.id';

        $now = Carbon::now('Asia/Bangkok')->startOfMonth();
        // $year = $now->year;
        // $start_month = $now->copy()->startOfYear()->month;
        // $end_month = $now->month;
        $year = 2018;
        $start_month = 1;
        $end_month = 12;

        $result = DB::select($query, [
            "year" => $year,
            "start_month" => $start_month,
            "end_month" => $end_month
        ]);
        return collect($result);
    }

    private function findGrowth($current, $start) {
        if ($start > 0) {
            return ceil(($current - $start) * 100 / $start).'%';
        }
        return "-";
    }
}