<?php

namespace App;

use App\Models\ModelTranslatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class OptSalary extends Model
{
    use SoftDeletes, ModelTranslatable;
    public $timestamps = false;
    protected $table = 'opt_salaries';
    protected $dates = ['deleted_at'];
    protected $translatable = ['title'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Option Salary';

    public function groups() {
        return $this->belongsToMany('App\OptSalaryGroup', 'opt_salary_opt_salary_group', 'option_id', 'option_group_id');
    }

    public function readableValue() {
        preg_match('/^([\<\>\=]{2}|\d+)(-)?(\d+)/', $this->opt_value, $matches);
        /**
         * if value = 10-20
         * $matches[0] = 10-20
         * $matches[1] = 10
         * $matches[2] = -
         * $matches[3] = 20
         *
         * if value = <=/>=20
         * $matches[0] = <=/>=20
         * $matches[1] = <=/>=
         * $matches[2] = (blank)
         * $matches[3] = 20
         */

        if(count($matches) == 0) {
            return "";
        }

        $last_value = number_format($matches[3]);

        if($matches[2] == "") {
            if($matches[1] == "<=") {
                return "ไม่เกิน {$last_value} บาท";
            } elseif($matches[1] == ">=") {
                return "{$last_value} บาทขึ้นไป";
            }
        } else {
            $first_value = number_format($matches[1]);
            return "{$first_value} ถึง {$last_value} บาท";
        }

        return $matches;
    }

    public function valueComponents() {
        preg_match('/^([\<\>\=]{2}|\d+)(-)?(\d+)/', $this->opt_value, $matches);

        if(count($matches) == 0) {
            return [
                "operand" => "-",
                "min_value" => "",
                "max_value" => ""
            ];
        }

        $components = [
            "operand" => ($matches[2] == "" ? $matches[1] : $matches[2]),
            "min_value" => ($matches[2] == "" ? ($matches[1] == "<=" ? $matches[3] : "") : $matches[1]),
            "max_value" => ($matches[2] == "" ? ($matches[1] == ">=" ? $matches[3] : "") : $matches[3]),
        ];

        return $components;
    }
}
