<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisaApp extends Model
{
    protected $table = 'tb_visa_apps';
    protected $translatable = ['customer_id', 'fname', 'lname', 'product_id', 'is_duplicated'];
}
