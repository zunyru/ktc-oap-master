<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class OptConvenientTime extends Model
{
    use SoftDeletes,LogsActivity ;
    public $timestamps = false;
    protected $table = 'opt_convenient_times';
    protected $dates = ['deleted_at'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Option Convenient Time';

    public function valueComponents() {
        preg_match('/^([\<\>\=]{2}|\d+)(-)?(\d+)/', $this->opt_value, $matches);
        $matchesTime = explode('-', $this->opt_value, 2);
        $matchesAfter = explode(' ', $this->opt_value, 2);
        $min_value = '';
        $max_value = '';
        if($this->opt_type == 1){
           $operand =  $this->opt_value;
        }elseif($this->opt_type == 2){
            $operand = '-';
            $min_value = $matchesTime[0];
            $max_value = $matchesTime[1];
        }elseif($this->opt_type == 3){
            $operand = 'After';
            $max_value = $matchesAfter[1];
        }
        $components = [
            "operand" =>  $operand,
            "min_value" => $min_value,
            "max_value" => $max_value,
        ];

        return $components;
    }

}
