<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisaReportLog extends Model
{
    protected $table = 'log_visa_exports';
	public $timestamps = false;
}
