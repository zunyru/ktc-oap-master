<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class Product extends Model
{
    use  LogsActivity;

    public $timestamps = false;

    protected static $logAttributes = ['*'];
    protected static $logName = 'Product';

    public function category()
    {
        return $this->belongsTo('App\ProductCategory', 'product_category_id', 'id');
    }
}
