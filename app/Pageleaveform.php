<?php

namespace App;

use App\Models\ModelTranslatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;


class Pageleaveform extends Model
{
    use SoftDeletes, ModelTranslatable ,LogsActivity;
    public $timestamps = false;
    protected $table = 'pageleaveforms';
    protected $dates = ['deleted_at'];
    protected $translatable = ['title', 'title_page', 'description', 'content'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Landing Page';

    public function product() {
        return $this->hasOne('App\Product', 'id', 'Product_id');
    }

    public function leaveForm() {
        return $this->hasOne('App\Leaveform', 'id', 'LeaveForm_id');
    }

    public function formLayout() {
        return $this->belongsTo('App\FormLayout', 'id', 'PageLeaveForm_id');
    }

    public function branch() {
        return $this->hasOne('App\Branch', 'id', 'branch_id');
    }
}