<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogsheetReportLog extends Model
{
    protected $table = 'log_sheets_exports';
	public $timestamps = false;
}
