<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class CheckSessionTransfer
{
    /**
     * Handle an incoming request.
     * Check token param to sync sso session
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {


       if($request->query('token')!==NULL){

            $this->verifyToken($request->query('token'));
            return redirect()->to(url()->current().'?'.http_build_query($request->except('token')));

       }elseif ($request->has('login')){
           $this->login();
       }elseif (session()->has('sso_aid') && session()->has('sso_token')){
           $this->generateToken();
       }

        return $next($request);
    }


    // verify token
    protected function verifyToken($token){
        $req_id = self::genData();
        $app_id = "10";
        $app_password = "oap*280";


        $data = new \stdClass();
        $data->requestID = $req_id;
        $data->appID = $app_id;
        $data->appPassword = $app_password;
        $data->token = $token;
        $data->saveLog = "Y";

        $request_data = json_encode($data);

        if(session()->has('sso_aid') && session()->has('sso_token')){
            session()->remove('sso_aid');
            session()->remove('sso_token');
            session()->remove('sso_sessionToken');
            session()->remove('sso_verify_status');
            session()->remove('sso_first_name');
        }

        try {
            $client = new Client();
            $res = $client->request('POST', 'http://172.19.10.106/ktc-uae-authorize-verifytoken/authorization/VerifyToken',
                ['headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                    'body' => $request_data]);

            $arrayResult = json_decode($res->getBody(), true);


              //  echo $arrayResult['sessionToken'];

            //echo $res->getBody();
            if($arrayResult['responseCode']==100) {
                session()->put('sso_aid', $arrayResult['AID']);
                session()->put('sso_sessionToken', $arrayResult['sessionToken']);
                session()->put('sso_verify_status', $arrayResult['responseCode']);
                session()->put('sso_first_name', $arrayResult['firstName']);
                $this->generateToken();
            }



        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    //generate token
    protected function generateToken(/*$sessionToken, $aid*/){
        $req_id = self::genData();
        $app_id = "10";
        $app_password = "oap*280";
        $aid = session()->get('sso_aid');
        $session_token = session()->get('sso_sessionToken');


        $data = new \stdClass;
        $data->requestID = $req_id;
        $data->appID = $app_id;
        $data->appPassword = $app_password;
        $data->AID = $aid;
        $data->sessionToken = $session_token;
        $data->custom2 = '10100';
        $data->sessionTimeout=300;

        $request_data = json_encode($data);


        try {
            $client = new Client();
            //check session timeout first
            $res = $client->request('POST', 'http://172.19.10.106/ktc-uae-authorize-chksession/authorization/CheckSessionTimeout',
                ['headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                    'body' => $request_data]);

                $arrayResult = json_decode($res->getBody(), true);

            //if still
            if($arrayResult['responseCode']==100) {
                 $res = $client->request('POST', 'http://172.19.10.106/ktc-uae-authorize-gentoken/authorization/GenerateToken',
                     ['headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                         'body' => $request_data]);

                    $arrayResult = json_decode($res->getBody(), true);

                    // echo $arrayResult['sessionToken'];

                // echo $res->getBody();
                 session()->put('sso_token', $arrayResult['token']);
            }else{
                //clear all auth session
                session()->remove('sso_aid');
                session()->remove('sso_token');
                session()->remove('sso_sessionToken');
                session()->remove('sso_verify_status');
                session()->remove('sso_first_name');
            }

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    protected function login(){
        $req_id = self::genData();
        $app_id = "10";
        $app_password = "oap*280";
        $username = 'bank999';
        $password = '12341234';
        /*$username = 'ball03';
        $password = '12345678';*/

        $data = new \stdClass();
        $data->requestID = $req_id;
        $data->appID = $app_id;
        $data->appPassword = $app_password;
        $data->userName = $username;
        $data->password = $password;
        $request_data = json_encode($data);


        try {
            $client = new Client();
            $res = $client->request('POST', 'http://172.31.15.237/ktc-uae-login-usernamepwd/login/LoginByUsernamePassword',
                ['headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
                    'body' => $request_data]);

             $arrayResult = json_decode($res->getBody(), true);

            if($arrayResult['responseCode']==100) {
                session()->put('sso_aid', $arrayResult['AID']);
                session()->put('sso_token', $arrayResult['sessionToken']);
                session()->put('sso_verify_status', $arrayResult['responseCode']);
               // session()->put('first_name', $arrayResult['firstName']);
                $this->generateToken();
            }




        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }

    public function strip_referrer_from_path($request)
    {
        $path = $request->path();
        $inputs = $request->input();

        $inputArray = [];
        foreach ($inputs as $key => $value)
        {
            if ($key != 'ref')
            {
                $inputArray[] = $key . "=" . $value;
            }
        }

        if(count($inputArray) > 0)
        {
            $inputString = '?';
            $inputString .= implode($inputArray,"&");
            $path .= $inputString;
        }

        return $path;
    }


    private function genData() {
        $digit = 18;
        $no1 = rand(pow(10, $digit - 1), pow(10, $digit) - 1);
        $no2 = rand(pow(10, $digit - 1), pow(10, $digit) - 1);
        $req_no = $no1 . '' . $no2;


        return $req_no;

    }
}
