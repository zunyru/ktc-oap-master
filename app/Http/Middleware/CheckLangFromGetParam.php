<?php

namespace App\Http\Middleware;

use Closure;

class CheckLangFromGetParam
{
    /**
     * Handle an incoming request.
     * Check Lang param to switch the language of the website
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has("local")) {
            app()->setLocale(session()->get("local"));
        }else{
            session()->put('local', 'th');
            app()->setLocale("th");
        }

        if (!session()->has("local") || $request->has("lang")) {
            switch ($request->query("lang",NULL)) {
                case 'th_TH':
                case 'th':
                    app()->setLocale("th");
                    session()->put('local', 'th');
                    break;
                case 'en_US':
                case 'en':
                    app()->setLocale("en");
                    session()->put('local', 'en');
                    break;
            }
        }

        return $next($request);
    }
}