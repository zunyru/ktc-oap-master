<?php

namespace App\Http\Controllers\API;


use Complex\Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ResponseController as ResponseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\Leavefromapp;
use App\Repositories\LeadRepo;


use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\OptSalary;
use App\Pageleaveform;
use App\FormLayout;
use App\Product;
use App\Leaveform;
use App\OptConvenientTime;
use Carbon\Carbon;

class AuthController extends ResponseController
{
    //create user
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|',
            'email' => 'required|string|email|unique:users',
            'password' => 'required',
            'confirm_password' => 'required|same:password'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        if($user){
            $success['token'] =  $user->createToken('token')->accessToken;
            $success['message'] = "Registration successfull..";
            return $this->sendResponse($success);
        }
        else{
            $error = "Sorry! Registration is not successfull.";
            return $this->sendError($error, 401);
        }

    }

    //login
    public function login(Request $request)
    {

        $request['email'] = $request['username'];

        $validator = Validator::make($request->all(), [
            'username' => 'required|string|email',
            'password' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError($validator->errors());
        }

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)){
            $error = "Unauthorized";
            return $this->sendError($error, 401);
        }
        $user = $request->user();
        $success['token'] =  $user->createToken('token')->accessToken;
        return $this->sendResponse($success);
    }

    //logout
    public function logout(Request $request)
    {

        $isUser = $request->user()->token()->revoke();
        if($isUser){
            $success['message'] = "Successfully logged out.";
            return $this->sendResponse($success);
        }
        else{
            $error = "Something went wrong.";
            return $this->sendResponse($error);
        }


    }

    //getuser
    public function getUser(Request $request)
    {
        //$id = $request->user()->id;
        $user = $request->user();
        if($user){
            return $this->sendResponse($user);
        }
        else{
            $error = "user not found";
            return $this->sendResponse($error);
        }
    }

    //get Page
    public function getPage(Request $request)
    {
        //$id = $request->user()->id;
        $user = $request->user();
        $slug = $request['slug'];

        if($user){

            if(!empty($slug))
            {
                if($page = $this->page($slug))
                {

                    $result = array("PageLeaveForm_id" => $page['page_model']['id']);
//                    $result = $page;

                    return $this->sendResponse($result);
                }else
                {
                    return $this->sendError('Page not found',404);
                }
            }else
            {
                return $this->sendError("Please provide slug.",400);
            }
        }
        else{

            return $this->sendError("Invalid Token.",401);
        }
    }

    //Save

    public function getSave(Request $request)
    {
        $user = $request->user();

        if($user){

            $save = $this->save($request);

            if($save['status'])
            {
                return $this->sendResponse($save['message']);
            }else
            {
                $message = $save['message'];
                return $this->sendError($message,400);
//                return $this->sendResponse(array("error" => $message[0]));
            }
        }
        else{
            $error = "user not found";
            return $this->sendResponse($error);
        }

        return $this->sendResponse("update");

    }

    public function page($slug)
    {
        //TODO check with page_id
        $page_model = Pageleaveform::where('active', 0)
            ->where('slug',$slug)
            ->first();

        if(is_null($page_model)){
            //page not found
            return false;
        }

        $preview_mode=false;


        //prev link
        $url_previous =  url()->previous();

        $custom_param_arr = json_decode($page_model->custom_param, true);
        $custom_param = collect($custom_param_arr);
        $custom_param->put('ref_url', url()->full());
        $custom_param->put('inbound', $url_previous);


        if (isset($page_model)) {
            if ($page_model->public == 1 && $preview_mode==false) {
                abort(404);
            }

            $pageForm = $page_model->leaveForm;

            if($pageForm->convenient_times === 'Y'){
                $operand_id = NULL;
                $options = OptConvenientTime::withTrashed()->whereNull('deleted_at')->orderBy('order', 'asc')->get();

                foreach ($options as $key => $item) {
                    $operand_id = $item->id;

                    if($item->opt_type == 1){
                        $matchesTime = explode(' ', $item->opt_value, 2);
                        $time_text = $matchesTime[0];
                    }elseif($item->opt_type == 2){
                        $matchesTime = explode('-', $item->opt_value, 2);
                        $time_text = $matchesTime[0].' - '.$matchesTime[1];
                    }elseif($item->opt_type == 3){
                        $matchesTime = explode(' ', $item->opt_value, 2);
                        $time_text = $matchesTime[1];
                    }
                    $components[$key] = [
                        "operand_id" =>  $operand_id,
                        "time_type" => $item->opt_type,
                        "time_text" => $time_text,
                    ];
                }
                $data['optionsTime'] = $components;
            }
            $data['method'] = "post";
            $data['slug'] = $slug;
            $data['ref_url'] = url()->full();
            $opt_group = $pageForm->salaryGroup;
            $data['salaries'] = $opt_group ? $opt_group->sortedOptions() : [];
            $data['iframe'] = false;
            $data['pcode'] = NULL;
            $data['callback'] = NULL;
            $data['custom_param'] = $custom_param;
            $data['convenient_times'] = $pageForm->convenient_times === 'Y' ? true : false;

            $data['page_model'] = $page_model;
//            return view('frontend.oap-creditcard', $data);
            return $data;
        } else {
            //page not found
            return false;
        }
    }

    public function pageById($id)
    {
        //TODO check with page_id
        $page_model = Pageleaveform::where('active', 0)
            ->where('id',$id)
            ->first();

        if(is_null($page_model)){
            //page not found
            return false;
        }

        $preview_mode=false;


        //prev link
        $url_previous =  url()->previous();

        $custom_param_arr = json_decode($page_model->custom_param, true);
        $custom_param = collect($custom_param_arr);
        $custom_param->put('ref_url', url()->full());
        $custom_param->put('inbound', $url_previous);


        if (isset($page_model)) {
            if ($page_model->public == 1 && $preview_mode==false) {
                abort(404);
            }

            $pageForm = $page_model->leaveForm;

            if($pageForm->convenient_times === 'Y'){
                $operand_id = NULL;
                $options = OptConvenientTime::withTrashed()->whereNull('deleted_at')->orderBy('order', 'asc')->get();

                foreach ($options as $key => $item) {
                    $operand_id = $item->id;

                    if($item->opt_type == 1){
                        $matchesTime = explode(' ', $item->opt_value, 2);
                        $time_text = $matchesTime[0];
                    }elseif($item->opt_type == 2){
                        $matchesTime = explode('-', $item->opt_value, 2);
                        $time_text = $matchesTime[0].' - '.$matchesTime[1];
                    }elseif($item->opt_type == 3){
                        $matchesTime = explode(' ', $item->opt_value, 2);
                        $time_text = $matchesTime[1];
                    }
                    $components[$key] = [
                        "operand_id" =>  $operand_id,
                        "time_type" => $item->opt_type,
                        "time_text" => $time_text,
                    ];
                }
                $data['optionsTime'] = $components;
            }
            $data['method'] = "post";
            $data['ref_url'] = url()->full();
            $opt_group = $pageForm->salaryGroup;
            $data['salaries'] = $opt_group ? $opt_group->sortedOptions() : [];
            $data['iframe'] = false;
            $data['pcode'] = NULL;
            $data['callback'] = NULL;
            $data['custom_param'] = $custom_param;
            $data['convenient_times'] = $pageForm->convenient_times === 'Y' ? true : false;

            $data['page_model'] = $page_model;
//            return view('frontend.oap-creditcard', $data);
            return $data;
        } else {
            //page not found
            return false;
        }
    }

    public function save(Request $request)
    {

        //validate form
        if(empty($request['PageLeaveForm_id']))
        {
            return array("status" => false,"message" => "Please provide PageLeaveForm_id.");
        }

        $page_id = $request['PageLeaveForm_id'];

        $page_model = Pageleaveform::where('active', 0)
            ->where('id',$page_id)
            ->first();

        if($this->pageById($page_id) == null)
        {
            return array("status" => false,"message" => "PageLeaveForm_id not found.");
        }

        if(!empty($request->pcode))
        {
            $check_pcode = DB::table('products')->where('sys_product_id',$request->pcode)
                ->first();

            if($check_pcode == null)
            {

                return array("status" => false,"message" => "pcode not found.");
            }
        }


        $page_info = $this->pageById($page_id);


        $slug = $page_info['page_model']['slug'];
        $product = $page_info['page_model']['Product_id'];
        $leave_form_id = $page_info['page_model']['LeaveForm_id'];
        $branch_code = $page_info['page_model']['branch_id'];
        $salaries_id = "";


        if(count($page_info["salaries"]) > 0)
        {
            foreach($page_info["salaries"] as $item)
            {

                if($item['opt_value'] == $request['opt_value'])
                {
                    $salaries_id = $item['id'];
                }

            }
        }



        if($salaries_id == "")
        {
            return array("status" => false,"message" => "Incorrect opt_value.");
        }

        $convenient_text = NULL;
        $opt_type = NULL;
        $opt_con = NULL;
        if (!is_null($request->convenient_times)) {
            $OptConvenientTime = OptConvenientTime::withTrashed()->findOrFail($request->convenient_times);
            $opt_type = $OptConvenientTime->opt_type;
            if ($OptConvenientTime->opt_type == 1) {
                $matchesTime = explode(' ', $OptConvenientTime->opt_value, 2);
                $opt_con = 'WT' . $matchesTime[0];
                $convenient_text = __('ติดต่อภายใน', ['times' => $matchesTime[0]]);
            } elseif ($OptConvenientTime->opt_type == 2) {
                $opt_con = $OptConvenientTime->opt_value;
                $convenient_text = $this->checkTimeRegisBetween($OptConvenientTime);
            } else {
                $matchesTime = explode(' ', $OptConvenientTime->opt_value, 2);
                $opt_con = 'AF' . $matchesTime[1];
                $convenient_text = $this->checkTimeRegisAfter($OptConvenientTime);
            }
        }

        #CHECK SELARY ID
        $checkSelary = $this->checkSelary($salaries_id,$product);

        ### START Validation
        $validator = Validator::make($request->all(),
            [
                'fname' => 'required',
                'lname' => 'required',
                'tel' => 'required|regex:/^[0-9]{3}[0-9]{3}[0-9]{4}$/',
                'email' => 'required|email',
                'ref_url' => 'required',
                'opt_value' => 'required'
//                ,
//                'salary' => 'required|numeric'
            ]);
        //set nicename to display
        $niceNames = array(
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'tel' => 'Phone Number',
            'email' => 'Email'
//        ,
//            'salary' => 'Salary',
        );
        $validator->setAttributeNames($niceNames);
        // go out if have any errors
        if ($validator->fails()) {
            //put default errors array messages to our noti function
            $messenges = $validator->errors()->all();
            $msg_list = array_map(function ($item) {
                return $item;
            }, $messenges);
            return array("status" => false,"message" => $messenges[0]);
        }
        ### END Validation

        ### START Custom Field
        $val_cf1 = $request->input("kcf1_" . $request->cf_1);
        $val_cf2 = $request->input("kcf2_" . $request->cf_2);

        $cf1_arr = NULL;
        if (isset($request->cf_1)) {
            $cf1_arr['key_name'] = $request->cf_1;
            $cf1_arr['value'] = $val_cf1;
            $cf1_arr = json_encode($cf1_arr);
        }
        $cf2_arr = NULL;
        if (isset($request->cf_2)) {
            $cf2_arr['key_name'] = $request->cf_2;
            $cf2_arr['value'] = $val_cf2;
            $cf2_arr = json_encode($cf2_arr);
        }
        ### END Custom Field

        if (isset($request->full_name)) {
            $full_name = explode(" ", $request->full_name);

            $fname = $full_name[0];
            $lname = $full_name[1];
        } else {
            $fname = $request->fname;
            $lname = $request->lname;
        }

        ### START find duplication record
        $filterDate = new Carbon(setting('leads.duplicate_filter_time_number') . ' ' . setting('leads.duplicate_filter_time_unit') . ' ago');
        $data_row = $request;

        switch ($checkSelary) {
            case 'CS-Tele':
                $record = Leavefromapp::where('created_at', '>=', $filterDate . " 00:00:00")
                    ->where('tel', str_replace(' ', '', $data_row->tel))
                    ->where('product_id', $product)
                    ->orderBy('id', 'asc')
                    ->first(['id', 'created_at']);
                break;
            default:
                $record = Leavefromapp::where('created_at', '>=', $filterDate . " 00:00:00")
                    ->where('lead_dest', $checkSelary)
                    ->where(function ($query) use ($data_row) {
                        $query->orWhere('email', $data_row->email)
                            ->orWhere('tel', str_replace(' ', '', $data_row->tel));
                    })
                    ->orderBy('id', 'asc')
                    ->first(['id', 'created_at']);
                break;
        }

        $is_existed = isset($record);
        ### END find duplication record


        $obj = new Leavefromapp();

        if (!$request->has('_validate')) {

            $repo = new LeadRepo();
            $uniqe_id = $repo->getNextUniqueID();
            //insert table ktc_leavefromapp
            $obj->lead_unique_id = $uniqe_id;
            $obj->fname = trim($fname);
            $obj->lname = trim($lname);
            $obj->email = $request->email;
            $obj->tel = str_replace(' ', '', $request->tel);
            $obj->cf_1 = $cf1_arr;
            $obj->cf_2 = $cf2_arr;
            $obj->custom_param = $request->custom_param;
            $obj->pcode = $request->get('pcode', NULL);
            $obj->branch_code = $branch_code;
            $obj->active = 0;
            $obj->is_first = !$is_existed;
            $obj->opt_Salary_id = $salaries_id;
            $obj->PageLeaveForm_id = $page_id;
            $obj->Product_id = $product;
            $obj->modified_by = 0;
            $obj->convenient_time = $opt_con;
            $obj->lead_dest = $checkSelary;
            $obj->save();

            $insert_id = $obj->id;

            $custom_param = json_decode($request->custom_param);

            /*   $leave = Leavefromapp::with(['option', 'product', 'page'])->orderBy('id', 'desc')->first();

            event(new AppliedCard($leave)); */

            return array("status" => true,"message" => "success");

//            if ($is_existed) {
//                //goto repetitive and display first application date
//
//                return array("repetitive" => ['namepage' => $request->segment,
//                    'dates' => $record->created_at,
//                    'url_back' => $request->segment,
//                    'callback_3rd' => (!empty($custom_param->callback) ? str_replace('{IDENTIFIER}', $uniqe_id, $custom_param->callback) : NULL),
//                    "iframe" => $request->get('iframe', NULL),
//                    "thankyou_callback" => $request->get('callback', NULL),
//                    "repetitive" => true, "time_type" => $opt_type,
//                    "convenient_text" => $convenient_text,
//                    "product_type" => $checkSelary]);
////                return redirect()->route('repetitive', ['namepage' => $request->segment])->with(['dates' => $record->created_at, 'url_back' => $request->segment, 'callback_3rd' => (!empty($custom_param->callback) ? str_replace('{IDENTIFIER}', $uniqe_id, $custom_param->callback) : NULL), "iframe" => $request->get('iframe', NULL), "thankyou_callback" => $request->get('callback', NULL), "repetitive" => true, "time_type" => $opt_type, "convenient_text" => $convenient_text, "product_type" => $checkSelary]);
//
//            } else {
//                //goto thankyou
//                return array("thankyou" => ['namepage' => $request->segment,
//                    'callback_3rd' => (!empty($custom_param->callback) ? str_replace('{IDENTIFIER}', $uniqe_id, $custom_param->callback) : NULL),
//                    "iframe" => $request->get('iframe', NULL),
//                    "thankyou_callback" => $request->get('callback', NULL),
//                    "thankyou" => true,
//                    "time_type" => $opt_type,
//                    "convenient_text" => $convenient_text,
//                    "product_type" => $checkSelary]);
////                return redirect()->route('thankyou', ['namepage' => $request->segment])->with([]);
//            }
        }

    }

    public function checkSelary($selary_id,$product_id)
    {
        $products = Product::find($product_id);
        $category_code = $products->category()->first()->category_code;
        $selaries = OptSalary::find($selary_id);
        $valueComponents = $selaries->valueComponents();
        $arrayCode = array('CC','PL','FIXED');
        //check code
        if(in_array($category_code, $arrayCode )){
            //บน
            return 'Telesales';
        }else{
            return 'CS-Tele';
            //  if($category_code == 'LCASH'){
            //       if($valueComponents['operand'] == '-'){
            //               if($valueComponents['min_value'] >= 12000){
            //                   return 'Telesales';
            //               }else{
            //               //PCL ล่าง
            //                   return 'CS-Tele';
            //               }
            //       }elseif($valueComponents['operand'] == '>='){
            //               if($valueComponents['max_value'] >= 12000){
            //                   return 'Telesales';
            //               }else{
            //               //PCL ล่าง
            //                   return 'CS-Tele';
            //               }
            //       }elseif($valueComponents['operand'] == '<='){
            //               if(($valueComponents['min_value']-1) >= 12000){
            //                   return 'Telesales';
            //               }else{
            //               //PCL ล่าง
            //                   return 'CS-Tele';
            //               }
            //       }
            // }else{
            //     //PCL ล่าง
            //     return 'CS-Tele';
            // }
        }
    }

}