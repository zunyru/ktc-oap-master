<?php

namespace App\Http\Controllers\Frontend\Visa;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\VisaApp;
use App\LogSheet;
use Carbon\Carbon;
use Redirect;

class VisaController extends Controller
{
    public function index( $offer , $slug , Request $request){
        //$url = 'http://172.19.10.137:8004/KTC2SMS/Ktc_ShortLink?wsdl'; //Production
        $url = 'http://172.30.3.128:8002/KTC2SMS/Ktc_ShortLink?wsdl';
        $slug;

        // session()->forget('user_unionpay_result');

        try {
            // code P'Yo
           $client = new \SoapClient($url,
                array(
                  "trace"      => 1,
                  "exceptions" => 0,
                  "cache_wsdl" => 0)
              );

            $params = array(
              'ShortLink_refno' => $slug,
            );

            $data = $client->ShortLink($params);
            $xml_response=$client->__getLastResponse();
            $plainXML = $this->mungXML($xml_response);
            $arrayResult = json_decode(json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        }
        catch(Exception $e) {
            echo $e->getMessage();
        }

        // $xml_response = '<ns2:ShortLinkResponse xmlns:ns2="http://nfe_ci/"> <return>{"ListRefno": [{"Cust_Name": "ปณิฐา", "Product_ID": "UPDD01", "Cust_No": "10706"} ] }</return> </ns2:ShortLinkResponse>';

        $data_json = json_decode($arrayResult['S_Body']['ns2_ShortLinkResponse']['return']);
        $data_upi['Product_ID'] = $data_json->ListRefno[0]->Product_ID;
        $data_upi['Cust_Name'] = $data_json->ListRefno[0]->Cust_Name;
        $data_upi['Cust_LName'] = $data_json->ListRefno[0]->Cust_LName;
        $data_upi['Cust_No'] = $data_json->ListRefno[0]->Cust_No;
        $data_upi['slug'] = $slug;
        $data_upi['offer'] = $offer;
        //dd($data_json);
        if($data_upi['Product_ID'] !== NULL){

          $count_dup = VisaApp::where('customer_id', $data_upi['Cust_No'])->count();
          if($count_dup == 0){
            //log sheet
            $log_sheet = new LogSheet; 
            $log_sheet->cust_name = $data_upi['Cust_Name'];
            $log_sheet->cust_lname = $data_upi['Cust_LName'];
            $log_sheet->cust_no = $data_upi['Cust_No'];
            $log_sheet->product_id = $data_upi['Product_ID'];
            $log_sheet->status = "VIEW";
            $log_sheet->url = url()->full();
            $log_sheet->save();
          }

          $data_upi['products'] = Product::select('*')->where('sys_product_id', $data_upi['Product_ID'])->first();
          if(is_null($data_upi['products'])){
            return "ไม่พบ product code นี้ ในระบบ";
          }  
          if(isset($data_upi['products']->expired_date)){
          //lt : date > expired
            if(Carbon::now()->gt(Carbon::parse($data_upi['products']->expired_date))){
            //if(Carbon::now()->lte(Carbon::parse($data_upi['products']->expired_date))){ 
              if(!is_null($data_upi['products']->redirect_url)){
                return Redirect::to($data_upi['products']->redirect_url);
              }else{
                 return Redirect::to('https://ktc.co.th/home'); 
              }
            }
         }
        }else{
           abort(404);
        }
        //put the result to session
        session()->put('user_visa_result', $data_upi);
        //check duc Zun
        $count_dup = VisaApp::where('customer_id', $data_upi['Cust_No'])
                     ->where('product_id', $data_upi['Product_ID'])
                     ->count();
         if($count_dup >= 1){

            return redirect()->route('visa-thankyou',['product' => $offer ,'namepage' => $slug]);
        }
        return view('frontend.visa.index' , $data_upi );
    }

    public function thankyou(Request $request){
        $data = session()->get('user_visa_result');
        //expired date
        if(Carbon::now()->gt(Carbon::parse($data['products']->expired_date))){
            if(!is_null($data['products']->redirect_url)){
              return Redirect::to($data['products']->redirect_url);
            }else{
               return Redirect::to('https://ktc.co.th/home'); 
            }
        }else{
            if(session()->get('user_visa_result')){

                //check duc Zun
                $count_dup = VisaApp::where('customer_id', $data['Cust_No'])
                              ->where('product_id', $data['Product_ID'])
                              ->count();

                //save data to DB ZUN
                $upi = new VisaApp;
                $upi->customer_id  = $data['Cust_No'];
                $upi->fname  = $data['Cust_Name'];
                $upi->lname  = $data['Cust_LName'];
                $upi->product_id  = $data['Product_ID'];
                $upi->product_name = str_replace(" ", "-", strtolower($data['products']->title));

                if($count_dup >= 1){
                    $data['dup'] = VisaApp::where('customer_id', $data['Cust_No'])
                                          ->where('product_id', $data['Product_ID'])
                                          ->orderByRaw('created_at ASC')->first();

                    $log_sheet = new LogSheet; 
                    $log_sheet->cust_name = $data['Cust_Name'];
                    $log_sheet->cust_lname = $data['Cust_LName'];
                    $log_sheet->cust_no = $data['Cust_No'];
                    $log_sheet->product_id = $data['Product_ID'];
                    $log_sheet->status = "DUP";
                    $log_sheet->url = url()->full();
                    $log_sheet->save();

                    //Dup VisaApp 
                    $upi->is_duplicated  =  '1';
                }else{
                  
                  $log_sheet = new LogSheet; 
                  $log_sheet->cust_name = $data['Cust_Name'];
                  $log_sheet->cust_lname = $data['Cust_LName'];
                  $log_sheet->cust_no = $data['Cust_No'];
                  $log_sheet->product_id = $data['Product_ID'];
                  $log_sheet->status = "REGISTER";
                  $log_sheet->url = url()->full();
                  $log_sheet->save();
                }
                
                $upi->save();
                // session()->forget('user_visa_result');

                $data['image'] = Product::select('pic')->where('sys_product_id', $data['Product_ID'])->first();
                $data['product'] = Product::select('*')->where('sys_product_id', $data['Product_ID'])->first();

                return view('frontend.visa.thankyou' ,$data);
            }else{
                abort(403);
            }
        }

    }

     private function mungXML($xml)
    {
        $obj = SimpleXML_Load_String($xml);
        if ($obj === FALSE) return $xml;

        // GET NAMESPACES, IF ANY
        $nss = $obj->getNamespaces(TRUE);
        if (empty($nss)) return $xml;

        // CHANGE ns: INTO ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key)
        {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
            = '#'               // REGEX DELIMITER
            . '('               // GROUP PATTERN 1
            . '\<'              // LOCATE A LEFT WICKET
            . '/?'              // MAYBE FOLLOWED BY A SLASH
            . preg_quote($key)  // THE NAMESPACE
            . ')'               // END GROUP PATTERN
            . '('               // GROUP PATTERN 2
            . ':{1}'            // A COLON (EXACTLY ONE)
            . ')'               // END GROUP PATTERN
            . '#'               // REGEX DELIMITER
            ;
            // INSERT THE UNDERSCORE INTO THE TAG NAME
            $rep
            = '$1'          // BACKREFERENCE TO GROUP 1
            . '_'           // LITERAL UNDERSCORE IN PLACE OF GROUP 2
            ;
            // PERFORM THE REPLACEMENT
            $xml =  preg_replace($rgx, $rep, $xml);
        }
        return $xml;
    }
}
