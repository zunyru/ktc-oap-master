<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OptSalary;
use App\Pageleaveform;
use App\FormLayout;
use App\Product;
use App\Leaveform;
use App\OptConvenientTime;
use Illuminate\Support\Facades\DB;


class CardpageController extends Controller
{

    //TODO Load Page from slug
    public function page($slug, Request $request)
    {
        $page_model = Pageleaveform::where('active', 0)
            ->where('slug', $slug)
            ->first();

        if(is_null($page_model)){
            //page not found
            abort(404);
            exit();
        }

        $preview_mode=false;
        if (auth()->user() && auth()->user()->can('edit', $page_model) && $request->query("preview")==1) {
            $preview_mode=true;
        }

        //prev link
        $url_previous =  url()->previous();

        $custom_param_arr = json_decode($page_model->custom_param, true);
        $custom_param = collect($custom_param_arr);
        $custom_param->put('ref_url', url()->full());
        $custom_param->put('inbound', $url_previous);


        if (isset($page_model)) {
            if ($page_model->public == 1 && $preview_mode==false) {
                abort(404);
            }

            $pageForm = $page_model->leaveForm;

            if($pageForm->convenient_times === 'Y'){
               $operand_id = NULL;
               $options = OptConvenientTime::withTrashed()->whereNull('deleted_at')->orderBy('order', 'asc')->get();

               foreach ($options as $key => $item) {
                   $operand_id = $item->id;

                   if($item->opt_type == 1){
                        $matchesTime = explode(' ', $item->opt_value, 2);
                        $time_text = $matchesTime[0];
                   }elseif($item->opt_type == 2){
                        $matchesTime = explode('-', $item->opt_value, 2);
                        $time_text = $matchesTime[0].' - '.$matchesTime[1];
                   }elseif($item->opt_type == 3){
                        $matchesTime = explode(' ', $item->opt_value, 2);
                        $time_text = $matchesTime[1];
                   }
                   $components[$key] = [
                        "operand_id" =>  $operand_id,
                        "time_type" => $item->opt_type,
                        "time_text" => $time_text,
                   ];
               }
               $data['optionsTime'] = $components;
            }
            $data['method'] = "post";
            $data['slug'] = $slug;
            $data['ref_url'] = url()->full();
            $opt_group = $pageForm->salaryGroup;
            $data['salaries'] = $opt_group ? $opt_group->sortedOptions() : [];
            $data['iframe'] = ($request->query('iframe', NULL) == 1 ? true : false);
            $data['pcode'] = $request->query('pid', NULL);
            $data['callback'] = $request->query('callback', NULL);
            $data['custom_param'] = $custom_param;
            $data['convenient_times'] = $pageForm->convenient_times === 'Y' ? true : false;

            $data['page_model'] = $page_model;
            return view('frontend.oap-creditcard', $data);
        } else {
            //page not found
            abort(404);
        }
    }

    /**
     * Link form global menu conditions
     * @var $request
     */
    public function pageGlobal(Request $request)
    {
        $dest=json_decode(setting('pages.global_dest'));
        $product=$request->query('product',NULL);
        switch ($product) {
            case 'CC':
                return redirect(($dest->CC[0]=="custom_val"?$dest->CC[1]:route("page-index",["namepage"=>$dest->CC[0],"lang"=>$request->query('lang',"th_TH")])));
                break;
            case 'PL':
                return redirect(($dest->PL[0]=="custom_val"?$dest->PL[1]:route("page-index",["namepage"=>$dest->PL[0],"lang"=>$request->query('lang',"th_TH")])));
                break;
            case 'FIXED':
                return redirect(($dest->FIXED[0]=="custom_val"?$dest->FIXED[1]:route("page-index",["namepage"=>$dest->FIXED[0],"lang"=>$request->query('lang',"th_TH")])));
                break;
            case 'PLCFC':
                return redirect(($dest->PLCFC[0]=="custom_val"?$dest->PLCFC[1]:route("page-index",["namepage"=>$dest->PLCFC[0],"lang"=>$request->query('lang',"th_TH")])));
                break;
            default:
                abort(404);
                break;
        }
    }
}