<?php

namespace App\Http\Controllers\Frontend;

use App\Events\AppliedCard;
use App\Repositories\LeadRepo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Leavefromapp;
use App\OptSalary;
use App\Product;
use App\ProductCategory;
use DB;
use Validator;
use Carbon\Carbon;
use App\OptConvenientTime;

class CreditcardController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //TODO Save data to database
    public function store(Request $request)
    {

        $convenient_text = NULL;
        $opt_type =NULL;
        $opt_con = NULL;
        if(!is_null($request->convenient_times)){
        $OptConvenientTime = OptConvenientTime::withTrashed()->findOrFail($request->convenient_times);
            $opt_type = $OptConvenientTime->opt_type;
        if($OptConvenientTime->opt_type == 1){
            $matchesTime = explode(' ', $OptConvenientTime->opt_value, 2);
            $opt_con = 'WT'.$matchesTime[0];
            $convenient_text = __('ติดต่อภายใน',['times'=>$matchesTime[0]]);
        }elseif($OptConvenientTime->opt_type == 2){
            $opt_con = $OptConvenientTime->opt_value;
            $convenient_text = $this->checkTimeRegisBetween($OptConvenientTime);
        }else{
            $matchesTime = explode(' ', $OptConvenientTime->opt_value, 2);
            $opt_con = 'AF'.$matchesTime[1];
            $convenient_text  = $this->checkTimeRegisAfter($OptConvenientTime);
        }
        }

        #CHECK SELARY ID
        $checkSelary = $this->checkSelary($request->salary,$request->product);

        ### START Validation
        $validator = Validator::make($request->all(),
            [
                'fname' => 'required',
                'lname' => 'required',
                'tel' => 'required',
                'email' => 'required|email',
                'salary' => 'required|numeric',
                'spam_lead'   => 'honeypot',
                'spam_time_lead'   => 'required|honeytime:10'
            ]);
        //set nicename to display
        $niceNames = array(
            'fname' => 'First Name',
            'lname' => 'Last Name',
            'tel' => 'Phone Number',
            'email' => 'Email',
            'salary' => 'Salary',
        );
        $validator->setAttributeNames($niceNames);
        // go out if have any errors
        if ($validator->fails()) {
            //put default errors array messages to our noti function
            $messenges = $validator->errors()->all();
            $msg_list = array_map(function ($item) {
                return $item;
            }, $messenges);
            return redirect()
                ->back()
                ->with('noti', array("valid-error"=>$msg_list));
        }
        ### END Validation

        ### START Custom Field
         $val_cf1 = $request->input("kcf1_".$request->cf1);
        $val_cf2 = $request->input("kcf2_".$request->cf2);

        $cf1_arr = NULL;
        if(isset($request->cf1)){
            $cf1_arr['key_name'] = $request->cf1;
            $cf1_arr['value'] = $val_cf1;
            $cf1_arr = json_encode($cf1_arr);
        }
        $cf2_arr = NULL;
        if(isset($request->cf2)){
            $cf2_arr['key_name'] = $request->cf2;
            $cf2_arr['value'] = $val_cf2;
            $cf2_arr = json_encode($cf2_arr);
        }
        ### END Custom Field

        if (isset($request->full_name)) {
            $full_name = explode(" ", $request->full_name);

            $fname = $full_name[0];
            $lname = $full_name[1];
        } else {
            $fname = $request->fname;
            $lname = $request->lname;
        }

        ### START find duplication record
        $filterDate = new Carbon(setting('leads.duplicate_filter_time_number').' '.setting('leads.duplicate_filter_time_unit').' ago');
        $data_row=$request;

        switch ($checkSelary) {
            case 'CS-Tele':
                $record = Leavefromapp::where('created_at', '>=', $filterDate." 00:00:00")
                    ->where('tel', str_replace(' ', '', $data_row->tel))
                    ->where('product_id', $request->product)
                    ->orderBy('id', 'asc')
                    ->first(['id','created_at']);
                break;
            default:
                $record = Leavefromapp::where('created_at', '>=', $filterDate." 00:00:00")
                    ->where('lead_dest', $checkSelary)
                    ->where(function($query) use ($data_row) {
                            $query->orWhere('email', $data_row->email)
                            ->orWhere('tel', str_replace(' ', '', $data_row->tel));
                    })
                    ->orderBy('id', 'asc')
                    ->first(['id','created_at']);
                break;
        }

        $is_existed = isset($record);
        ### END find duplication record


        $obj = new Leavefromapp();

       if(!$request->has('_validate')) {

        $repo = new LeadRepo();
        $uniqe_id=$repo->getNextUniqueID();
        //insert table ktc_leavefromapp
        $obj->lead_unique_id    = $uniqe_id;
        $obj->fname             = trim($fname);
        $obj->lname             = trim($lname);
        $obj->email             = $request->email;
        $obj->tel               = str_replace(' ', '', $request->tel);
        $obj->cf_1              = $cf1_arr;
        $obj->cf_2              = $cf2_arr;
        $obj->custom_param      = $request->custom_param;
        $obj->pcode             = $request->get('pcode',NULL);
        $obj->branch_code       = $request->branch_code;
        $obj->active            = 0;
        $obj->is_first          = !$is_existed;
        $obj->opt_Salary_id     = $request->salary;
        $obj->PageLeaveForm_id  = $request->page;
        $obj->Product_id        = $request->product;
        $obj->modified_by       = 0;
        $obj->convenient_time   = $opt_con;
        $obj->lead_dest         = $checkSelary;
        $obj->save();

        $insert_id = $obj->id;

        $custom_param=json_decode($request->custom_param);
        
            /*   $leave = Leavefromapp::with(['option', 'product', 'page'])->orderBy('id', 'desc')->first();

            event(new AppliedCard($leave)); */

        if($is_existed) {
            //goto repetitive and display first application date
            return redirect()->route('repetitive',['namepage' => $request->segment])->with(['dates' => $record->created_at,'url_back' =>$request->segment,'callback_3rd' => (!empty($custom_param->callback)? str_replace('{IDENTIFIER}', $uniqe_id , $custom_param->callback):NULL),"iframe"=>$request->get('iframe',NULL),"thankyou_callback"=>$request->get('callback',NULL),"repetitive"=>true,"time_type"=>$opt_type,"convenient_text"=>$convenient_text,"product_type"=>$checkSelary]);
        } else {
            //goto thankyou
            return redirect()->route('thankyou', ['namepage' => $request->segment])->with(['callback_3rd' => (!empty($custom_param->callback)?str_replace('{IDENTIFIER}', $uniqe_id , $custom_param->callback):NULL),"iframe"=>$request->get('iframe',NULL),"thankyou_callback"=>$request->get('callback',NULL),"thankyou"=>true,"time_type"=>$opt_type,"convenient_text"=>$convenient_text,"product_type"=>$checkSelary]);
        }
    }
}

    /**
     * Handle displaying thank you page
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function leadSubmitResult($id)
    {
        //check if journey is from submit form else go 403
        if (session()->get('thankyou')) {
            return view('frontend.oap-success');
        }elseif(session()->get('repetitive')){
            return view('frontend.oap-repetitive');
        }else{
            abort(403);
    }

    }

    public function checkTimeRegisBetween($opt)
    {
         $matchesTime = explode('-', $opt->opt_value, 2);
         $now = new Carbon();
         $dtstart = Carbon::parse($now);
         $dtend = Carbon::parse($now);
         $stattime = $dtstart->parse($matchesTime[0]);
         $endtime = $dtend->parse($matchesTime[1]);

         if ($stattime->gte($now) && $now->lte($endtime)) {
            //between
            return __('ภายในตามช่วงเวลา',['times'=>$opt->opt_value]);
         }else{
           //not between
           //return __('ติดต่อในวันถัดไป ตั้งแต่',['dates' => formatDateThat($now->addDays('1')),'times'=>$opt->opt_value]);
           return __('ติดต่อในวันถัดไป ตั้งแต่',['times'=>$opt->opt_value]);
         }
    }

    public function checkTimeRegisAfter($opt)
    {
        $matchesTime = explode(' ', $opt->opt_value, 2);
        $now = new Carbon();
        $dtstart = Carbon::parse($now);
        $stattime = $dtstart->parse($matchesTime[1]);
        if ($stattime->gte($now)){
           return  __('ติดต่อหลังวันถัดไป',['times'=> $matchesTime[1]]);
        }else{
           return  __('ติดต่อหลังเวลา',['times'=> $matchesTime[1]]);
        }
    }

    public function checkSelary($selary_id,$product_id)
    {
       $products = Product::find($product_id);
       $category_code = $products->category()->first()->category_code;
       $selaries = OptSalary::find($selary_id);
       $valueComponents = $selaries->valueComponents();
       $arrayCode = array('CC','PL','FIXED');
       //check code
       if(in_array($category_code, $arrayCode )){
           //บน
           return 'Telesales';
       }else{
             return 'CS-Tele';
          //  if($category_code == 'LCASH'){
          //       if($valueComponents['operand'] == '-'){
          //               if($valueComponents['min_value'] >= 12000){
          //                   return 'Telesales';
          //               }else{
          //               //PCL ล่าง
          //                   return 'CS-Tele';
          //               }
          //       }elseif($valueComponents['operand'] == '>='){
          //               if($valueComponents['max_value'] >= 12000){
          //                   return 'Telesales';
          //               }else{
          //               //PCL ล่าง
          //                   return 'CS-Tele';
          //               }
          //       }elseif($valueComponents['operand'] == '<='){
          //               if(($valueComponents['min_value']-1) >= 12000){
          //                   return 'Telesales';
          //               }else{
          //               //PCL ล่าง
          //                   return 'CS-Tele';
          //               }
          //       }
          // }else{
          //     //PCL ล่าง
          //     return 'CS-Tele';
          // }
       }
    }
}