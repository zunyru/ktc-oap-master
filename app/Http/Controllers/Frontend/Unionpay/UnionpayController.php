<?php

namespace App\Http\Controllers\Frontend\Unionpay;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\UnionpayApp;
use Carbon\Carbon;
use Redirect;

class UnionpayController extends Controller
{
    public function index($slug , Request $request){
        //https://www.ktc.co.th/credit-card/generic/ktc-unionpay-platinum\
        $url = 'http://172.19.10.137:8004/KTC2SMS/Ktc_ShortLink?wsdl';
        $slug;

        // session()->forget('user_unionpay_result');

        try {
            // code P'Yo
           $client = new \SoapClient($url,
                array(
                  "trace"      => 1,
                  "exceptions" => 0,
                  "cache_wsdl" => 0)
              );

            $params = array(
              'ShortLink_refno' => $slug,
            );

            $data = $client->ShortLink($params);
            $xml_response=$client->__getLastResponse();
            $plainXML = $this->mungXML($xml_response);
            $arrayResult = json_decode(json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

        }
        catch(Exception $e) {
            echo $e->getMessage();
        }

        // $xml_response = '<ns2:ShortLinkResponse xmlns:ns2="http://nfe_ci/"> <return>{"ListRefno": [{"Cust_Name": "ปณิฐา", "Product_ID": "UPDD01", "Cust_No": "10706"} ] }</return> </ns2:ShortLinkResponse>';

        $data_json = json_decode($arrayResult['S_Body']['ns2_ShortLinkResponse']['return']);

        $data_upi['Product_ID'] = $data_json->ListRefno[0]->Product_ID;
        $data_upi['Cust_Name'] = $data_json->ListRefno[0]->Cust_Name;
        $data_upi['Cust_LName'] = $data_json->ListRefno[0]->Cust_LName;
        $data_upi['Cust_No'] = $data_json->ListRefno[0]->Cust_No;
        $data_upi['slug'] = $slug;
        if($data_upi['Product_ID'] !== NULL){
          $data_upi['image'] = Product::select('pic')->where('sys_product_id', $data_upi['Product_ID'])->first();
        }else{
           abort(404);
        }
        //put the result to session
        session()->put('user_unionpay_result', $data_upi);
        //check duc Zun
        $count_dup = UnionpayApp::where('customer_id', $data_upi['Cust_No'])->count();
         if($count_dup >= 1){
            return redirect()->route('unionpay-thankyou',['namepage' => $slug]);
        }

        return view('frontend.unionpay.index' ,$data_upi);
    }

    public function thankyou(Request $request){

        if (Carbon::parse('30-7-2019')->gt(Carbon::now())){
            if(session()->get('user_unionpay_result')){

                $data = session()->get('user_unionpay_result');

                //check duc Zun
                $count_dup = UnionpayApp::where('customer_id', $data['Cust_No'])->count();


                //save data to DB ZUN
                $upi = new UnionpayApp;
                $upi->customer_id  = $data['Cust_No'];
                $upi->fname  = $data['Cust_Name'];
                $upi->lname  = $data['Cust_LName'];
                $upi->product_id  = ($data['Product_ID'] === 'UPDD01') ?  'DiA' : 'PlA';
                if($count_dup >= 1){
                    $data['dup'] = UnionpayApp::where('customer_id', $data['Cust_No'])->orderByRaw('created_at ASC')->first();
                    $upi->is_duplicated  =  '1';
                }

                $upi->save();

                // session()->forget('user_unionpay_result');

                $data['image'] = Product::select('pic')->where('sys_product_id', $data['Product_ID'])->first();

                return view('frontend.unionpay.thankyou' ,$data);
            }else{
                abort(403);
            }
        }else{
            return Redirect::to('https://www.ktc.co.th/credit-card/generic/ktc-unionpay-platinum');
        }

    }

     private function mungXML($xml)
    {
        $obj = SimpleXML_Load_String($xml);
        if ($obj === FALSE) return $xml;

        // GET NAMESPACES, IF ANY
        $nss = $obj->getNamespaces(TRUE);
        if (empty($nss)) return $xml;

        // CHANGE ns: INTO ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key)
        {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
            = '#'               // REGEX DELIMITER
            . '('               // GROUP PATTERN 1
            . '\<'              // LOCATE A LEFT WICKET
            . '/?'              // MAYBE FOLLOWED BY A SLASH
            . preg_quote($key)  // THE NAMESPACE
            . ')'               // END GROUP PATTERN
            . '('               // GROUP PATTERN 2
            . ':{1}'            // A COLON (EXACTLY ONE)
            . ')'               // END GROUP PATTERN
            . '#'               // REGEX DELIMITER
            ;
            // INSERT THE UNDERSCORE INTO THE TAG NAME
            $rep
            = '$1'          // BACKREFERENCE TO GROUP 1
            . '_'           // LITERAL UNDERSCORE IN PLACE OF GROUP 2
            ;
            // PERFORM THE REPLACEMENT
            $xml =  preg_replace($rgx, $rep, $xml);
        }
        return $xml;
    }
}
