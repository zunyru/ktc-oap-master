<?php

namespace App\Http\Controllers\Frontend;

use App\Events\CheckedAppStatus;
use App\Leavefromapp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use App\Leaveformapp;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Validator;


class AppStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session()->forget(['user_cred','user_app_result']);
        return view('frontend.app-status.index');
    }

    /**
     * Request for OAP
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function otpReq(Request $request)
    {
        ### START Validation
        $validator = Validator::make($request->all(),
            [
                'citizen_code' => 'required',
            ]);
        //set nicename to display
        $niceNames = array(
            'citizen_code' => 'Citizen Code',

        );
        $validator->setAttributeNames($niceNames);
        // go out if have any errors
        if ($validator->fails()) {
            //put default errors array messages to our noti function
            $messenges = $validator->errors()->all();
            $msg_list = array_map(function ($item) {
                return $item;
            }, $messenges);
            return redirect()
                ->back()
                ->with('noti', array("valid-error"=>$msg_list));
        }
        ### END Validation
        // $log_type = 1;
        // $citizen_code = str_replace(' ', '', $request->citizen_code);


        try {
            // code P'Yo
           $client = new \SoapClient("http://172.20.15.76/osb/AppStatusService?wsdl",
                array(
                  "trace"      => 1,
                  "exceptions" => 0,
                  "cache_wsdl" => 0)
              );

            $params = array(
              'appID' => "10",
              'appPassword' => "oap*123",
              'requestID' => "dffed16d-20e5-430a-94ca-e0bd54642c2b",
              'citizenID' => str_replace(' ', '', $request->citizen_code)
            );

            $data = $client->appStatusInquiry($params);
            $xml_response=$client->__getLastResponse();
            $plainXML = $this->mungXML($xml_response);
            $arrayResult = json_decode(json_encode(SimpleXML_Load_String($plainXML, 'SimpleXMLElement', LIBXML_NOCDATA)), true);

            //  $click001='{"SOAP-ENV_Body":{"ser_appStatusInquiryResponse":{"requestID":"dffed16d-20e5-430a-94ca-e0bd54642c2b","responseID":"4aff2f7f-9cba-4e48-9266-b309a32e5795","appStatusList":{"appNo":"1810001966","status":"8F","citizenID":"CLICK001","appCreate":"2018\/10\/24","actDate":"2018\/10\/24","productCode":"VRLP01","productDesc":"KTC PROUD","productStatusCode":"8A","reasonCode":"A10","firstName":"\u0e04\u0e25\u0e34\u0e01","lastName":"\u0e40\u0e17\u0e2a"},"responseType":{"responseCode":"000","subResponseCode":[],"description":"SUCCESS"}}}}';

            //  $click002='{"SOAP-ENV_Body":{"ser_appStatusInquiryResponse":{"requestID":"dffed16d-20e5-430a-94ca-e0bd54642c2b","responseID":"6c7a47d6-2bcc-4bc0-a288-bb04f6171f5a","appStatusList":{"appNo":"1810001967","status":"8F","citizenID":"CLICK002","appCreate":"2018\/10\/24","actDate":"2018\/10\/24","productCode":"VSPP33","productDesc":"KTC BANGCHAK VISA PLATINUM","productStatusCode":"8D","reasonCode":"D07","firstName":"\u0e04\u0e25\u0e34\u0e01-\u0e40\u0e2d","lastName":"\u0e40\u0e17\u0e2a"},"responseType":{"responseCode":"000","subResponseCode":[],"description":"SUCCESS"}}}}';

            //  $click003='{"SOAP-ENV_Body":{"ser_appStatusInquiryResponse":{"requestID":"dffed16d-20e5-430a-94ca-e0bd54642c2b","responseID":"cbda0f72-7436-4329-9fb9-e62f31a06f9e","appStatusList":[{"appNo":"1810001968","status":"4A","citizenID":"CLICK003","appCreate":"2018\/10\/24","actDate":"2018\/10\/24","productCode":"VRLP01","productDesc":"KTC PROUD","productStatusCode":[],"reasonCode":[],"firstName":"\u0e04\u0e25\u0e34\u0e01-\u0e1a\u0e35","lastName":"\u0e40\u0e17\u0e2a-\u0e1a\u0e35"},{"appNo":"1810001968","status":"4A","citizenID":"CLICK003","appCreate":"2018\/10\/24","actDate":"2018\/10\/24","productCode":"MSGT18","productDesc":"KTC X - BANGKOK AIRWAYS WORLD REWARDS MASTERCARD","productStatusCode":[],"reasonCode":[],"firstName":"\u0e04\u0e25\u0e34\u0e01-\u0e1a\u0e35","lastName":"\u0e40\u0e17\u0e2a-\u0e1a\u0e35"}],"responseType":{"responseCode":"000","subResponseCode":[],"description":"SUCCESS"}}}}';

            //  switch (str_replace(' ', '', $request->citizen_code)) {
            //     case '1111111111111':
            //          $arrayResult=json_decode($click001,true);
            //          break;
            //     case '1111111111112':
            //          $arrayResult=json_decode($click002,true);
            //          break;
            //     case '1111111111113':
            //          $arrayResult=json_decode($click003,true);
            //          break;
            //     default:
            //         return redirect()->route( 'app-status.index')->with(["noti"=>"not found"]);
            //         break;
            // }

        }
        catch(Exception $e) {
            echo $e->getMessage();
        }

        //put the result to session
        session()->put('user_app_result', $arrayResult['SOAP-ENV_Body']['ser_appStatusInquiryResponse']['return']);

        //if not found
        if (session()->get('user_app_result')['responseType']['responseCode']==002) {
            return redirect()->route( 'app-status.index')->with(["noti"=>"not found"]);
        }
        ### END Check Phone number in tele-sale system

        $log_type = 1;
        $citizen_code = str_replace(' ', '', $request->citizen_code);
        event(new CheckedAppStatus($citizen_code, $log_type));
        return redirect()->route('app-status.result');

    }

     /**
     * Get Result OTP
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function result(Request $request)
    {
        //if not normal journey
        // if (app('router')->getRoutes()->match(app('request')->create(session()->get('_previous')['url']))->getName()!=="app-status.index" ) {
        //     abort(403);
        // }

        //fetch for result
        if(session()->get('user_app_result')['responseType']['responseCode']==000){
            $appStatusList=session()->get('user_app_result')['appStatusList'];
            if ( array_key_exists("appNo",$appStatusList)){
                if ($appStatusList['productStatusCode']!=="9C") {
                        $appStatusList_fronts[]=$this->getStatusDetail($appStatusList);
                    }
            }else{
                foreach ($appStatusList as $key => $appStatusList_item) {
                    //remove not process item
                    if ($appStatusList_item['productStatusCode']!=="9C") {
                        $appStatusList_fronts[]=$this->getStatusDetail($appStatusList_item);
                    }
                }
            }
        }

        if (!isset($appStatusList_fronts) || count($appStatusList_fronts)<1) {
              return redirect()->route( 'app-status.index')->with(["noti"=>"not found"]);
        }else{
          return view('frontend.app-status.result',compact('appStatusList_fronts'));
        }

    }

    /**
     * getStatusDetail
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function getStatusDetail($appStatusList_item)
    {
        $in_processes=['1N','2C','2E1','2E2','2I','2R','3F','3P','3V','4A','5A','6P','6A','6C','6D','6W','7O'];
        $have_reasons=['8A','8C','8D'];
        $appStatusList_item_front['date']=$this->thai_date_short(strtotime(str_replace('/', '-', $appStatusList_item['appCreate'])));

        //set global for trash state
        if (session('local')=='en') {
            $global_result="On Process";
            $global_title="KTC Product";
        }else{
            $global_result="อยู่ระหว่างการดำเนินการ";
            $global_title="ผลิตภัณฑ์ KTC";
        }
        //trash state
        if (in_array($appStatusList_item['status'],["2C"])) {
            if (session('local')=='en') {
                 $appStatusList_item_front['reason']="Information not found. <br>Please contact KTC PHONE 02 123 5000.";
            }else{
                 $appStatusList_item_front['reason']="ไม่พบข้อมูลระบุสถานะการสมัครของท่าน <br>สอบถามรายละเอียดเพิ่มเติมที่ KTC PHONE 02 123 5000";
            }
            $appStatusList_item_front['result']="N/A";
            $appStatusList_item_front['app_code']=$appStatusList_item['appNo'];
            $appStatusList_item_front['p_code']="nopcode";
            $appStatusList_item_front['p_title']=$global_title;
            $appStatusList_item_front['p_pic']="";
            $appStatusList_item_front['state']="2";
        }elseif (in_array($appStatusList_item['status'],["-----"])) {
            $appStatusList_item_front['reason']="";
            $appStatusList_item_front['result']=$global_result;
            $appStatusList_item_front['app_code']=$appStatusList_item['appNo'];
            $appStatusList_item_front['p_code']="nopcode";
            $appStatusList_item_front['p_title']=$global_title;
            $appStatusList_item_front['p_pic']="";
            $appStatusList_item_front['state']="2";
        }else{
            // begin good state
            // in process
           if (in_array($appStatusList_item['status'],$in_processes) || in_array($appStatusList_item['productStatusCode'],$in_processes)) {
               $appStatusList_item_front['reason']="";
               $appStatusList_item_front['result']=$global_result;
               $appStatusList_item_front['state']="2";
               $appStatusList_item_front['app_code']=$appStatusList_item['appNo'];
               $appStatusList_item_front['p_code']=$appStatusList_item['productCode'];
               $appStatusList_item_front['p_title']=$appStatusList_item['productDesc'];
               // $appStatusList_item_front['p_pic']=DB::table('products')->select('pic')->where('sys_product_id', '=', $appStatusList_item['productCode'])->get()[0] ?? "";
               $appStatusList_item_front['p_pic']="";
            // have_reasons
           }elseif(in_array($appStatusList_item['productStatusCode'],$have_reasons)){
               $cri1=$appStatusList_item['productStatusCode'];
               // reason is generic by product
               if ($appStatusList_item['reasonCode']!=='' AND in_array($appStatusList_item['productStatusCode'],['8A','8D'])) {
                    if (is_array($appStatusList_item['productDesc'])) {
                        $appStatusList_item['productDesc']="";
                    }
                  if (in_array($appStatusList_item['reasonCode'],["D24", "D30", "D31", "D32", "D33", "D34", "D35"])) {
                      $cri2="NCB";
                  }elseif(substr_count($appStatusList_item['productCode'],"VRL") >0){
                       $cri2="loan";
                   }elseif (substr_count($appStatusList_item['productCode'],"PVL") >0) {
                       $cri2="cash";
                   }else{
                       $cri2="cr";
                   }
               }else{
                   $cri2=$appStatusList_item['reasonCode'];
               }

               // not passed let begin fetch for the reasons
               if (isset($cri1) && $cri1 !=='' && isset($cri2) && $cri2 !=='') {
                   $data=DB::select("SELECT state,display_th,display_en,mssg_th,mssg_en FROM tb_app_check_mssg WHERE product_status=? AND reason_code=?", [$cri1, $cri2])[0];
                   if (session('local')=='en') {
                       $appStatusList_item_front['reason']=$data->mssg_en;
                       $appStatusList_item_front['result']=$data->display_en;
                   }else{
                       $appStatusList_item_front['reason']=$data->mssg_th;
                       $appStatusList_item_front['result']=$data->display_th;
                   }

                   $appStatusList_item_front['app_code']=$appStatusList_item['appNo'];
                   $appStatusList_item_front['p_code']=$appStatusList_item['productCode'];
                   $appStatusList_item_front['p_title']=$appStatusList_item['productDesc'];
                   // $productCodeQuery=DB::table('products')->select('pic')->where('sys_product_id', '=', $appStatusList_item['productCode'])->get();
                   // if (count($productCodeQuery)>0) {
                   //     $appStatusList_item_front['p_pic']=$productCodeQuery[0];
                   // }else{
                   //     $appStatusList_item_front['p_pic']='';
                   // }
                   $appStatusList_item_front['p_pic']='';
                   $appStatusList_item_front['state']=$data->state;

               }
           }
        }


        // re confirm that no trash state goes
           $appStatusList_item_front['app_code']=($appStatusList_item_front['app_code']==""?"noappcode":$appStatusList_item_front['app_code']);
           $appStatusList_item_front['p_code']=($appStatusList_item_front['p_code']==""?"nopcode":$appStatusList_item_front['p_code']);
           $appStatusList_item_front['p_title']=($appStatusList_item_front['p_title']==""?$global_title:$appStatusList_item_front['p_title']);

           $appStatusList_item_front['app_code']=(is_array($appStatusList_item_front['app_code'])?"noappcode":$appStatusList_item_front['app_code']);
           $appStatusList_item_front['p_code']=(is_array($appStatusList_item_front['p_code'])?"nopcode":$appStatusList_item_front['p_code']);
           $appStatusList_item_front['p_title']=(is_array($appStatusList_item_front['p_title'])?$global_title:$appStatusList_item_front['p_title']);
        return $appStatusList_item_front;
    }
    /**
     * Helper Function for XML
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function mungXML($xml)
    {
        $obj = SimpleXML_Load_String($xml);
        if ($obj === FALSE) return $xml;

        // GET NAMESPACES, IF ANY
        $nss = $obj->getNamespaces(TRUE);
        if (empty($nss)) return $xml;

        // CHANGE ns: INTO ns_
        $nsm = array_keys($nss);
        foreach ($nsm as $key)
        {
            // A REGULAR EXPRESSION TO MUNG THE XML
            $rgx
            = '#'               // REGEX DELIMITER
            . '('               // GROUP PATTERN 1
            . '\<'              // LOCATE A LEFT WICKET
            . '/?'              // MAYBE FOLLOWED BY A SLASH
            . preg_quote($key)  // THE NAMESPACE
            . ')'               // END GROUP PATTERN
            . '('               // GROUP PATTERN 2
            . ':{1}'            // A COLON (EXACTLY ONE)
            . ')'               // END GROUP PATTERN
            . '#'               // REGEX DELIMITER
            ;
            // INSERT THE UNDERSCORE INTO THE TAG NAME
            $rep
            = '$1'          // BACKREFERENCE TO GROUP 1
            . '_'           // LITERAL UNDERSCORE IN PLACE OF GROUP 2
            ;
            // PERFORM THE REPLACEMENT
            $xml =  preg_replace($rgx, $rep, $xml);
        }
        return $xml;
    }


    /**
     * Helper Function for XML
     *
     * @return gen necessary for API
     */
    private function genData(){
        $digit = 18;
        $no1 = rand(pow(10, $digit-1), pow(10,$digit)-1);
        $no2 = rand(pow(10, $digit-1), pow(10,$digit)-1);
        $req_no = $no1.''.$no2;

        $req_date = date('Ymj'); // get current date
        $req_time = date('His').'00'; //  get current time concat 00

        $username =  'OAP.KTC';
        $password = 'Oap12345';

        $result = array( 'RequestDATE' => $req_date,
            'RequestTIME' => $req_time,
            'RequestNUMBER' => $req_no,
            'Username' => $username,
            'Password' => $password);

        return $result;

    }
    /**
     * Helper Function for thai date
     *
     * @param $time
     * @return \Illuminate\Http\Response
     */
    function thai_date_short($time){
        $thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
        $thai_month_arr=array(
            "0"=>"",
            "1"=>"มกราคม",
            "2"=>"กุมภาพันธ์",
            "3"=>"มีนาคม",
            "4"=>"เมษายน",
            "5"=>"พฤษภาคม",
            "6"=>"มิถุนายน",
            "7"=>"กรกฎาคม",
            "8"=>"สิงหาคม",
            "9"=>"กันยายน",
            "10"=>"ตุลาคม",
            "11"=>"พฤศจิกายน",
            "12"=>"ธันวาคม"
        );
        $thai_month_arr_short=array(
            "0"=>"",
            "1"=>"ม.ค.",
            "2"=>"ก.พ.",
            "3"=>"มี.ค.",
            "4"=>"เม.ย.",
            "5"=>"พ.ค.",
            "6"=>"มิ.ย.",
            "7"=>"ก.ค.",
            "8"=>"ส.ค.",
            "9"=>"ก.ย.",
            "10"=>"ต.ค.",
            "11"=>"พ.ย.",
            "12"=>"ธ.ค."
        );

        $thai_date_return = date("j",$time);
        $thai_date_return.=" &nbsp;&nbsp;".$thai_month_arr_short[date("n",$time)];
        $thai_date_return.= " ".(date("Y",$time)+543);
        return $thai_date_return;
    }


    /**
     * Decode SOAP XML String
     *
     * by Bew
     */
    private function decodeXML($xml, $service){

        try{
            //json decode 1st step
            $arrayResult = json_decode(json_encode(SimpleXML_Load_String($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
            // get array index ns_return
            // name service api
            $temp = ($arrayResult['soapenv_Body'][$service]['ns_return']);
            // decode xml in temp
            $xml_result = json_decode(json_encode(SimpleXML_Load_String($temp, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
            // get value otpp in array
            $otp = $xml_result['otp'];

            return $otp;

        }catch (Exception $exception){
            echo $exception->getMessage();
        }
    }
}
