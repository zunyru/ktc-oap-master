<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Voyager;
use Illuminate\Support\Facades\DB;
use App\OptConvenientTime;
use Illuminate\Support\Facades\Validator;

class OptconvenienttimeController extends VoyagerBaseController
{

    public function index(Request $request) {

        $dataType = $this->getCurrentDataType($request);

        $status = 0;
        $browse_state="active";
        if($request->has('status') && $request->query('status') == 'trash' || $request->segment(count($request->segments()))=='trash'){
            $status = 1;$browse_state="trash";
            $options = OptConvenientTime::onlyTrashed()->get();

        }else{
            $options = OptConvenientTime::withTrashed()->whereNull('deleted_at')->get();
        }
        $count_state=DB::select("SELECT (SELECT count(id) FROM {$dataType->name}) AS 'all', (SELECT count(id) FROM {$dataType->name} WHERE deleted_at IS NOT NULL) AS 'trashed'")[0];

        $data['options'] = $options;
        $data['browse_state'] = $browse_state;
        $data['dataType'] = $dataType;
        $data['count_state']=$count_state;

        return view('backend.opt-convenien-time-browse', $data);
    }

    public function create(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        //check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0 ? new OptConvenientTime() : null);

        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.opt-convenien-time-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request) {

        $dataType = $this->getCurrentDataType($request);

        //check permission
        $this->authorize('add', app($dataType->model_name));

        //validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        $displayName = [
            'title' => 'Option Name',
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = OptConvenientTime::withTrashed()
            ->where('title', trim($request['title']))
            ->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Option name is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        //save
         $this->saveRecord($request);

        return redirect()->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'   => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type'    => 'success'
            ]);

    }

     protected function saveRecord(Request $request, $option = null) {
        if(!isset($option)) {
            $laste_order = OptConvenientTime::orderBy('order', 'desc')->first();
            if(is_null($laste_order)){
                $laste_order = 1;
            }else{
                $laste_order = $laste_order->order + 1;
            }

            $option = new OptConvenientTime();
            $option->order = $laste_order;
            $option->create_by = auth()->user()->id;
        }

        if($request->opt_group == 'min'){
            $opt_type =1;
        }elseif($request->opt_group == 'between'){
            $opt_type =2;
        }else{
            $opt_type =3;
        }

        $option->title = $request->title;
        $option->opt_value = $this->parseOptionValue($request);
        $option->opt_type = $opt_type;
        $option->modified_by = auth()->user()->id;
        $option->updated_at= now();
        $option->save();

        // $option_en = $option->english();
        // $option_en->title = $request->title_en;
        // $option_en->save();
    }

    public function edit(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);
        $dataTypeContent = (strlen($dataType->model_name) > 0
            ? app($dataType->model_name)->withTrashed()->findOrFail($id)
            : DB::table($dataType->name)->withTrashed()->find($id));

        $this->authorize('edit', app($dataType->model_name));
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $valueComponents = $dataTypeContent->valueComponents();

        return view('backend.opt-convenien-time-edit-add',  compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'valueComponents'));
    }

     public function update(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        //validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        $displayName = [
            'title' => 'Option Name',
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = OptConvenientTime::where('title', trim($request['title']))
            ->where('id', '!=', $id)
            ->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Option is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        $option = OptConvenientTime::withTrashed()->findOrFail($id);

        $this->saveRecord($request, $option);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    protected function parseOptionValue(Request $request) {
        $group = $request->opt_group;
        $operands = [
            "min" => " minute",
            "between" => "-",
            "max" => "After "
        ];

        if($group == "between") {
            return $request->opt_between_min.$operands["between"].$request->opt_between_max;
        } else if($group == "min") {
             return $request["opt_".$group.'_'.$group].$operands[$group];
        }else{
            return $operands[$group].$request["opt_".$group.'_'.$group];
        }
    }

    public function restore(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $option = OptConvenientTime::withTrashed()->find($id);
            $option->restore();
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

     public function checkDuplicated(Request $request) {
        $value = $request->value;
        $optionId = $request->option_id;
        $count = OptConvenientTime::withTrashed()
            ->where('title', $value)
            ->where('id', '!=', $optionId)
            ->count();

        return $count;
    }

    public function destroy(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        $this->authorize('delete', app($dataType->model_name));

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        $options = OptConvenientTime::whereIn('id', $ids)->get();

        OptConvenientTime::destroy($ids);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function order(Request $request)
    {
        $dataType = $this->getCurrentDataType($request);
        $dataTypeContent = (strlen($dataType->model_name) != 0 ? new OptConvenientTime() : null);

        $status = 0;
        $browse_state="active";

        $data['browse_state'] = $browse_state;
        $data['dataType'] = $dataType;
        $data['dataTypeContent'] = $dataTypeContent;

        $data['info'] = OptConvenientTime::withTrashed()->whereNull('deleted_at')->orderBy('order', 'asc')->get();


       return view('backend.opt-convenien-time-order',$data);
    }

    public function orderlist(Request $request)
    {
       foreach ($request->position as $key => $id) {
            $option = OptConvenientTime::find($id);
            $option->order = $key+1;
            $option->modified_by = auth()->user()->id;
            $option->updated_at= now();
            $option->save();
       }

    }
}
