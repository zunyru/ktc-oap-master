<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2018-12-09
 * Time: 04:36
 */

namespace App\Http\Controllers\Backend;

use App\Exports\ReportExport;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
class ReportController extends VoyagerBaseController
{
    public function view() {
        return (new ReportExport())->view();
    }

    public function export() {
        $time = Carbon::now("Asia/Bangkok");
        $file_name = "oap-leads-report-".$time->format('Y-m-d');
        return (new ReportExport())->download("{$file_name}.xlsx");
    }

    public function getDashboardReport(Request $request) {
        // filters
        $result = $this->getDashboardReportSQL($request);
        return response()->json($result);
    }

    private function getDashboardReportSQL(Request $request) {
        $query = "SELECT
  branch_categories.title,
  COUNT(a.created_at) AS 'TOTAL'
FROM
  leavefromapps a
    INNER JOIN branches  on TRIM(branches.code) = TRIM(a.branch_code)
    INNER JOIN branch_categories on branches.branch_category_id = branch_categories.id
WHERE a.created_at BETWEEN :start_date AND :end_date
  AND branch_categories.deleted_at IS NULL
  AND branches.deleted_at IS NULL ";

        $time = Carbon::now("Asia/Bangkok");

        $date_range = $request->input('date_range');
        if(!isset($date_range)) {
            $start_date = $time->copy()->startOfYear();
            $end_date = $time;
        } else {
            $date_comp = explode('-', $date_range);
            // $start_date = Carbon::createFromFormat("d/m/Y", $date_comp[0], "Asia/Bangkok");
            // $end_date = Carbon::createFromFormat("d/m/Y", $date_comp[1], "Asia/Bangkok");
            $start_date = DateTime::createFromFormat('d/m/Y', $date_comp[0])->format('Y-m-d')." 00:00:00";
            $end_date = DateTime::createFromFormat('d/m/Y', $date_comp[1])->format('Y-m-d')." 23:59:59";
        }

        if(!empty($request->input('branches'))) {
            $branches = join(",", $request->input('branches'));
            $query .= " AND branches.id IN ({$branches})";
        }

        if(!empty($request->input('categories'))) {
            $categories = join(",", $request->input('categories'));
            $query .= " AND branch_categories.id IN ({$categories})";
        }

        $query .= " GROUP BY branch_categories.id ORDER BY branch_categories.id";

        $result = DB::select($query, [
            "start_date" => $start_date,
            "end_date" => $end_date
        ]);
        return $result;
    }
}