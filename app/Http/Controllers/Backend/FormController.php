<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Leaveform;
use Voyager;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

class FormController extends Controller
{
    use BreadRelationshipParser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Get code VoyagerBaseController
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        /*$DB = new Leaveform;
        $objs_forms = $DB::all();*/

        $status = 0;
        $browse_state="active";
        if($request->has('status') && $request->query('status') == 'trash'|| $request->segment(count($request->segments()))=='trash'){
            $status = 1;$browse_state="trash";
        }

        $objs_forms = DB::table('leaveforms')->where('active' , $status)->get();

        $count_state=DB::select("SELECT (SELECT count(id) FROM leaveforms) AS 'all', (SELECT count(id) FROM leaveforms WHERE active=1) AS 'trashed'")[0];

        //$objs_menu_item = DB::table('menu_items')->where('title' , 'Forms')->get();


       /* $canBrowseForm = Voyager::can('browse_leaveform');
        $canViewForm   = Voyager::can('read_leaveform');
        $canEditForm   = Voyager::can('edit_leaveform');
        $canAddForm    = Voyager::can('add_leaveform');
        $canDeleteForm = Voyager::can('delete_leaveform');*/


        //$data['objs_menu_item']  =  $objs_menu_item;
        // $data['canDeleteForm']   =  $canDeleteForm;

        $data['objs_forms']      =  $objs_forms;
        $data['dataType']        =  $dataType;
        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;


        return view('backend.form-browse', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;

        foreach ($dataType->addRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        //$view = 'voyager::bread.edit-add';

        /*if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }*/

        //return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
        return view('backend.form-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // set validation
        $validator = Validator::make($request->all(),
            [

             'title'       => 'required',
             'heading'     => 'required',

            ]);
        //set nicename to display
        $niceNames = array(

             'title'       => 'Title',
             'heading'     => 'Heading',
        );

        $validator->setAttributeNames($niceNames);

        if(self::checkDupTitle($request) > 0){
             return redirect()
             ->back()
             ->with([
                'message'    =>"Save Faild. This title already exists",
                'alert-type' => 'error',
            ]);
        }



     if (!$request->has('_validate')) {


        //check required_condition
        $condition = $request->required_condition == 'on' ? 1 : 0;

        $cf1_arr = NULL;
        if($request->custom_filed_one === 'on'){
            $cf1_arr['key_name'] = $request->field_key_one;
            $cf1_arr['label_name'] = $request->field_label_one;
            $cf1_arr['label_name_en'] = $request->field_label_one_en;
            $cf1_arr['opt'] = $request->required_field_one;
            $cf1_arr = json_encode($cf1_arr);
        }


        $cf2_arr = NULL;
        if($request->custom_filed_two === 'on'){
            $cf2_arr['key_name'] = $request->field_key_two;
            $cf2_arr['label_name'] = $request->field_label_two;
            $cf2_arr['label_name_en'] = $request->field_label_two_en;
            $cf2_arr['opt'] = $request->required_field_two;
            $cf2_arr = json_encode($cf2_arr);
        }
        
        $convenient_times = NULL;
        if($request->convenient_times === 'on'){
            $convenient_times = 'Y';
        }

        //insert
        $form = new Leaveform;
        $form->title                = $request->title;
        $form->description          = $request->description;
        $form->content_heading      = $request->heading;
        $form->content_sub_heading  = $request->sub_heading;
        $form->ch                   = $condition;
        $form->content_condition    = $request->condition;
        $form->content_contact      = $request->contact;
        $form->opt_salary_group_id  = $request->opt_group;
        $form->cf_1                 = $cf1_arr;
        $form->cf_2                 = $cf2_arr;
        $form->convenient_times     = $convenient_times;
        $form->create_by            = auth()->user()->id;
        $form->modified_by          = auth()->user()->id;
        $form->updated_at= now();
        $form->save();

        $this->saveEnglishFields($request, $form);


        if ($request->ajax()) {
            return response()->json(['success' => true, 'data' => $data]);
        }

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,$id)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? app($dataType->model_name)->with($relationships)->findOrFail($id)
        : DB::table($dataType->name)->where('id', $id)->first();

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        /*if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }*/

        // return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
        return view('backend.form-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // set validation
        $validator = Validator::make($request->all(),
            [
             'title' => 'required',
             'heading' => 'required',
         ]);
        //set nicename to display
        $niceNames = array(
         'title' => 'Title',
         'heading' => 'Heading',
     );

        $validator->setAttributeNames($niceNames);

        if(self::checkDupTitle($request) > 0){
         return redirect()
         ->back()
         ->with([
            'message'    => __('voyager::generic.update_failed').". This title already exists",
            'alert-type' => 'error',
        ]);
     }

     // go out if have any errors
     if ($validator->fails()) {
        //put default errors array messages to our noti function
        $messenges = $validator->errors()->all();
        $mssg_list = array_map(function ($item) {
            return ['error', $item];
        }, $messenges);
        return redirect()
        ->back()
        ->with([
            'message'    => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
            'alert-type' => 'error',
        ]);
    }

    //check required_condition
    $condition = $request->required_condition == 'on' ? 1 : 0;

    $cf1_arr = NULL;
    if($request->custom_filed_one === 'on'){
        $cf1_arr['key_name'] = $request->field_key_one;
        $cf1_arr['label_name'] = $request->field_label_one;
        $cf1_arr['label_name_en'] = $request->field_label_one_en;
        $cf1_arr['opt'] = $request->required_field_one;
        $cf1_arr = json_encode($cf1_arr);
    }

    $cf2_arr = NULL;
    if($request->custom_filed_two === 'on'){
        $cf2_arr['key_name'] = $request->field_key_two;
        $cf2_arr['label_name'] = $request->field_label_two;
        $cf2_arr['label_name_en'] = $request->field_label_two_en;
        $cf2_arr['opt'] = $request->required_field_two;
        $cf2_arr = json_encode($cf2_arr);
    }

    $convenient_times = NULL;
    if($request->convenient_times === 'on'){
        $convenient_times = 'Y';
    }



    $form = Leaveform::find($id);

    $form->title                = $request->title;
    $form->description          = $request->description;
    $form->content_heading      = $request->heading;
    $form->content_sub_heading  = $request->sub_heading;
    $form->ch                   = $condition;
    $form->content_condition    = $request->condition;
    $form->content_contact      = $request->contact;
    $form->opt_salary_group_id  = $request->opt_group;
    $form->cf_1                 = $cf1_arr;
    $form->cf_2                 = $cf2_arr;
    $form->convenient_times     = $convenient_times;
    $form->updated_at           = now();
    $form->modified_by          = auth()->user()->id;

    $form->save();

    $this->saveEnglishFields($request, $form);

    return redirect()
    ->route("voyager.{$dataType->slug}.index")
    ->with([
        'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
        'alert-type' => 'success',
    ]);

}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Leaveform::find($id);
            $form->active                = 1;
            $form->modified_by          = auth()->user()->id;

            $form->save();
        }

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_deleted')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }

    public function restore(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        //$slug = $Vg->getSlug($request);
        $slug = "form";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Leaveform::find($id);
            $form->active = 0;
            $form->modified_by = auth()->user()->id;

            $form->save();
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => 'Successfully  Restore' . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function checkDupTitle(Request $request){

        $obj = new Leaveform();

        $count = $obj::where('title', trim($request['title']))
        ->where('id', '!=' ,trim($request['form_id']))
        ->count();

        return  $count;

    }

    public function saveEnglishFields(Request $request, Leaveform $model) {
        $model_en = $model->english();
        $model_en->content_heading = trim($request->heading_en);
        $model_en->content_sub_heading = trim($request->sub_heading_en);
        $model_en->content_condition = $request->condition_en;
        $model_en->content_contact = $request->contact_en;
        $model_en->save();
    }
}