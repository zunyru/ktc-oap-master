<?php
namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\BranchCategory;
use App\ProductCategory;
use App\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Models\DataType;
use DB;

class BranchController extends Controller
{
    use BreadRelationshipParser;

    public function index(Request $request) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', $slug)->first();

        if($request->has('status') && $request->query('status') == 'trash' || $request->segment(count($request->segments()))=='trash'):
           $branches = Branch::onlyTrashed()->get();
            $browse_state="trash";
        else:
           $branches = Branch::withTrashed()->whereNull('deleted_at')->get();
           $browse_state="active";
        endif;

        $count_state=DB::select("SELECT (SELECT count(id) FROM {$dataType->name}) AS 'all', (SELECT count(id) FROM {$dataType->name} WHERE deleted_at IS NOT NULL) AS 'trashed'")[0];

        $data['branches'] = $branches;
        $data['dataType'] = $dataType;

        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;

        return view('backend.branch-browse', $data);
    }

    public function create(Request $request) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);
        $dataType = DataType::where('slug', $slug)->first();

        //check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0 ? new Branch() : null);

        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $branch_categories = BranchCategory::all();
        $product_categories = ProductCategory::all();

        return view('backend.branch-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable','branch_categories','product_categories'));
    }

    public function store(Request $request) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();

        //check permission
        $this->authorize('add', app($dataType->model_name));

        //validation
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'product_cat' => 'required',
            'branch_cat' => 'required'
        ]);

        $displayName = [
            'code' => 'Code',
            'product_cat' => 'Product Category',
            'branch_cat' => 'Branch Category'
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = Branch::where('code', trim($request->code))->count();
            // ->orWhere(function($q) use($request) {
            //       $q->where('product_category_id', trim($request->product_cat))
            //         ->Where('branch_category_id', trim($request->branch_cat));
            // })->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Item cannot be added since name and/or code is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        //save category info
        $this->saveBranch($request);

        return redirect()->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'   => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type'    => 'success'
            ]);
    }

    public function show($id) {

    }

    public function edit(Request $request, $id) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();
        $dataTypeContent = (strlen($dataType->model_name) > 0
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->find($id));

        $this->authorize('edit', app($dataType->model_name));
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $branch_categories = BranchCategory::all();
        $product_categories = ProductCategory::all();

        return view('backend.branch-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable','branch_categories','product_categories'));
    }

    public function update(Request $request, $id) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();

        //validation
        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'product_cat' => 'required',
            'branch_cat' => 'required'
        ]);

        $displayName = [
            'code' => 'Code',
            'product_cat' => 'Product Category',
            'branch_cat' => 'Branch Category'
        ];

        $validator->setAttributeNames($displayName);

        $product_cat=explode(";",$request->product_cat);
        $branch_cat=explode(";",$request->branch_cat);

        $count = Branch::where('id','!=',trim($id))
        ->where(function($q) use($request,$product_cat,$branch_cat){
            $q->where('code', trim($request->code));
            // ->orWhere(function($q) use($product_cat,$branch_cat) {
            //       $q->where('product_category_id', trim($product_cat[0]))
            //         ->Where('branch_category_id', trim($branch_cat[0]));
            // });
        })->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Category cannot be added since name and/or code is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        $branch = Branch::findOrFail($id);

        $this->saveBranch($request, $branch);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();

        $this->authorize('delete', app($dataType->model_name));

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        // Product::whereIn('product_category_id', $ids)->update(['product_category_id' => 0]);
        Branch::destroy($ids);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function restore(Request $request, $id) {
        
        $dataType = DataType::where('slug', 'branches')->first();

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $category = Branch::withTrashed()->find($id);
            $category->restore();
        }
        
        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function checkDuplicated(Request $request, $field) {
        $value = $request->value;
        $count = Branch::where($field, $value)->count();
        return $count;
    }


    protected function saveBranch(Request $request, $branch = null) {
        if(!isset($branch)) {
            $branch = new Branch();
        }

        $product_cat=explode(";",$request->product_cat);
        $branch_cat=explode(";",$request->branch_cat);
        $title=$branch_cat[1]." ".$product_cat[1];

        //create slug
        $branchSlug = str_slug($request->code, '-');

        $branch->title             = $title;
        $branch->code              = $request->code;
        $branch->slug             = $branchSlug ?: "";
        $branch->branch_category_id = $branch_cat[0] ?: "";
        $branch->product_category_id  = $product_cat[0] ?: "";
        $branch->description      = $request->description ?: "";
        $branch->create_by        = auth()->user()->id;
        $branch->modified_by      = auth()->user()->id;
        $branch->updated_at       = now();
        $branch->save();
    }
}