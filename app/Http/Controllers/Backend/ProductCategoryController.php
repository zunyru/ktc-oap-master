<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 17/11/18
 * Time: 5:47 PM
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Models\DataType;
use DB;

class ProductCategoryController extends VoyagerBaseController
{
    use BreadRelationshipParser;

    public function index(Request $request) {
        $dataType = $this->getCurrentDataType($request);
        if($request->has('status') && $request->query('status') == 'trash' || $request->segment(count($request->segments()))=='trash'):
           $categories = ProductCategory::onlyTrashed()->get();
            $browse_state="trash";
        else:
           $categories = ProductCategory::withTrashed()->whereNull('deleted_at')->get();
           $browse_state="active";
        endif;

        $count_state=DB::select("SELECT (SELECT count(id) FROM {$dataType->name}) AS 'all', (SELECT count(id) FROM {$dataType->name} WHERE deleted_at IS NOT NULL) AS 'trashed'")[0];

        $data['categories'] = $categories;
        $data['dataType'] = $dataType;
        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;

        return view('backend.product-category-browse', $data);
    }

    public function create(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        //check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0 ? new ProductCategory() : null);

        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.product-category-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        //check permission
        $this->authorize('add', app($dataType->model_name));

        //validation
        $validator = Validator::make($request->all(), [
            'name_th' => 'required',
            'name_en' => 'required',
            'category_code' => 'required'
        ]);

        $displayName = [
            'name_th' => 'Category Thai Name',
            'name_en' => 'Category English Name',
            'category_code' => 'Code'
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = ProductCategory::withTranslations(['th', 'en'])
            ->where(function($query) use ($request) {
                $query->where('name', trim($request['name_th']))
                    ->orWhere('name', trim($request['name_en']))
                    ->orWhere('category_code', trim($request['category_code']));
            })
            ->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Category cannot be added since name and/or code is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$validator->errors()->toArray()}",
                    'alert-type' => 'error'
                ]);
        }

        //save category info
        $this->saveCategory($request);

        return redirect()->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'   => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type'    => 'success'
            ]);
    }

    public function show(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        $category = app($dataType->model_name)->withTrashed()->findOrFail($id);
        $products = Product::where('product_category_id', $category->id)->get();

        $productDataType = DataType::where('slug', 'products')->first();

        return view('backend.product-category-show', compact('dataType', 'category', 'products', 'productDataType'));
    }

    public function edit(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);
        $dataTypeContent = (strlen($dataType->model_name) > 0
            ? app($dataType->model_name)->withTrashed()->findOrFail($id)
            : DB::table($dataType->name)->withTrashed()->find($id));

        $this->authorize('edit', app($dataType->model_name));
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.product-category-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function update(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        //validation
        $validator = Validator::make($request->all(), [
            'name_th' => 'required',
            'name_en' => 'required',
            'category_code' => 'required'
        ]);

        $displayName = [
            'name_th' => 'Category Thai Name',
            'name_en' => 'Category English Name',
            'category_code' => 'Code'
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = ProductCategory::withTranslations(['th', 'en'])
            ->where('id', '!=', $id)
            ->where(function($query) use ($request) {
                $query->where('name', trim($request['name_th']))
                    ->orWhere('name', trim($request['name_en']))
                    ->orWhere('category_code', trim($request['category_code']));
            })
            ->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Category cannot be added since name and/or code is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$validator->errors()->toArray()}",
                    'alert-type' => 'error'
                ]);
        }

        $category = ProductCategory::withTrashed()->findOrFail($id);

        $this->saveCategory($request, $category);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        $this->authorize('delete', app($dataType->model_name));

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        Product::whereIn('product_category_id', $ids)->update(['product_category_id' => 0]);
        ProductCategory::destroy($ids);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function restore(Request $request, $category_id) {
        $dataType = DataType::where('slug', 'product-categories')->first();

        $category = ProductCategory::withTrashed()->find($category_id);
        $category->restore();

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function checkDuplicated(Request $request, $field) {
        $value = $request->value;
        $categoryId = $request->category_id;

        $lang = ends_with($field, '_en') ? 'en' : 'th';
        $count = ProductCategory::withTranslation($lang)
            ->where($field, $value)
            ->where('id', '!=', $categoryId)
            ->count();

        return $count;
    }


    protected function saveCategory(Request $request, $category = null) {
        if(!isset($category)) {
            $category = new ProductCategory();
        }

        //create slug
        $categorySlug = str_slug($request->name, '-');

        $category->name             = $request->name_th;
        $category->category_code    = $request->category_code;
        $category->slug             = $categorySlug ?: "";
        $category->description      = $request->description_th ?: "";
        $category->updated_at       = now();
        $category->save();

        $cat_en = $category->english();
        $cat_en->name     = $request->name_en;
        $cat_en->description      = $request->description_en ?: "";
        $cat_en->updated_at       = now();
        $cat_en->save();
    }
}