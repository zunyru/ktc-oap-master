<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OptSalary;
use App\Pageleaveform;
use App\FormLayout;
use App\Product;
use App\Leaveform;
use DB;

class KTCsettingController extends Controller
{
    /**
     * Display settings and edit form already
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageleaveforms = DB::select("SELECT slug, title as title_page FROM pageleaveforms WHERE active=0 AND deleted_at is NULL AND public=0");
        $global_dest=json_decode(setting('pages.global_dest'));
        $custom_script=json_decode(setting('pages.custom_script'));
        $hide_header_on_pages=setting('pages.hide_header_on_pages');
        return view('backend.ktc-settings', compact('global_dest','custom_script','pageleaveforms','hide_header_on_pages'));
    }

    /**
     * Update setting by tabs
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $global_dest=json_encode(array(
            "CC"=>[$request->cc,$request->custom_cc],
            "PL"=>[$request->pl,$request->custom_pl],
            "FIXED"=>[$request->fixed,$request->custom_fixed],
            "PLCFC"=>[$request->plcfc,$request->custom_plcfc]
        ));

        $custom_script=json_encode(array(
            "header"=>$request->header_content,
            "footer"=>$request->footer_content
        ));

        DB::table('settings')
            ->where('key', 'pages.global_dest')
            ->update(['value' => $global_dest]);

        activity('KTC Settings')
           ->causedBy(auth()->user()->id)
           ->withProperties(['attributes'=> [ 'user' => auth()->user()->id , 'value' => $global_dest]])
           ->log('General');

        DB::table('settings')
                ->where('key', 'pages.custom_script')
                ->update(['value' => $custom_script]);

        activity('KTC Settings')
           ->causedBy(auth()->user()->id)
           ->withProperties(['attributes'=> [ 'user' => auth()->user()->id , 'value' => $global_dest]])
           ->log('Custom Script');        

        DB::table('settings')
                ->where('key', 'pages.hide_header_on_pages')
                ->update(['value' => $request->hide_header_on_pages]);


        activity('KTC Settings')
           ->causedBy(auth()->user()->id)
           ->withProperties(['attributes'=> [ 'user' => auth()->user()->id , 'value' => $global_dest]])
           ->log('Hide Header on Pages');         

        return redirect()
        ->route("admin.ktc-settings.index")
        ->with([
            'message'       => __('voyager::generic.successfully_updated'),
            'alert-type'    => 'success',
        ]);
    }
}