<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\OptSalary;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

class OptsalaryController extends VoyagerBaseController
{
    use BreadRelationshipParser;

    public function index(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        if($request->has('status') && $request->query('status') == 'trash'|| $request->segment(count($request->segments()))=='trash'):
           $options = OptSalary::onlyTrashed()->get();
            $browse_state="trash";
        else:
           $options = OptSalary::withTrashed()->whereNull('deleted_at')->get();
           $browse_state="active";
        endif;

        $count_state=DB::select("SELECT (SELECT count(id) FROM {$dataType->name}) AS 'all', (SELECT count(id) FROM {$dataType->name} WHERE deleted_at IS NOT NULL) AS 'trashed'")[0];

        $data['options'] = $options;
        $data['dataType'] = $dataType;

        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;

        return view('backend.opt-salary-browse', $data);
    }

    public function create(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        //check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0 ? new OptSalary() : null);

        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.opt-salary-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        //check permission
        $this->authorize('add', app($dataType->model_name));

        //validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        $displayName = [
            'title' => 'Option Name',
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = OptSalary::withTrashed()
            ->where('title', trim($request['title']))
            ->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Option name is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        //save record
        $this->saveRecord($request);

        return redirect()->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'   => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type'    => 'success'
            ]);
    }

    public function show(Request $request, $id) {

    }

    public function edit(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);
        $dataTypeContent = (strlen($dataType->model_name) > 0
            ? app($dataType->model_name)->withTrashed()->findOrFail($id)
            : DB::table($dataType->name)->withTrashed()->find($id));

        $this->authorize('edit', app($dataType->model_name));
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        $valueComponents = $dataTypeContent->valueComponents();

        return view('backend.opt-salary-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'valueComponents'));
    }

    public function update(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        //validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        $displayName = [
            'title' => 'Option Name',
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = OptSalary::where('title', trim($request['title']))
            ->where('id', '!=', $id)
            ->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Option is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        $option = OptSalary::withTrashed()->findOrFail($id);

        $this->saveRecord($request, $option);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        $this->authorize('delete', app($dataType->model_name));

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        $options = OptSalary::whereIn('id', $ids)->get();
        foreach ($options as $option) {
            $option->groups()->detach();
        }

        OptSalary::destroy($ids);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function restore(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        foreach ($ids as $id) {
            $option = OptSalary::withTrashed()->find($id);
            $option->restore();
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function checkDuplicated(Request $request) {
        $value = $request->value;
        $optionId = $request->option_id;
        $count = OptSalary::withTrashed()
            ->where('title', $value)
            ->where('id', '!=', $optionId)
            ->count();

        return $count;
    }

    protected function saveRecord(Request $request, $option = null) {
        if(!isset($option)) {
            $option = new OptSalary();
            $option->create_by = auth()->user()->id;
            $option->active = 0;
        }

        $option->title = $request->title;
        $option->opt_value = $this->parseOptionValue($request);
        $option->modified_by = auth()->user()->id;
        $option->updated_at= now();
        $option->save();

        $option_en = $option->english();
        $option_en->title = $request->title_en;
        $option_en->save();
    }

    protected function parseOptionValue(Request $request) {
        $group = $request->opt_group;
        $operands = [
            "min" => "<=", "between" => "-", "max" => ">="
        ];

        if($group == "between") {
            return $request->opt_between_min.$operands["between"].$request->opt_between_max;
        } else {
            return $operands[$group].$request["opt_".$group.'_'.$group];
        }
    }
}