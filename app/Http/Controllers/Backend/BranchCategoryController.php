<?php
namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Product;
use App\BranchCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use TCG\Voyager\Models\DataType;
use DB;

class BranchCategoryController extends Controller
{
    use BreadRelationshipParser;

    public function index(Request $request) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', $slug)->first();
        if($request->has('status') && $request->query('status') == 'trash' || $request->segment(count($request->segments()))=='trash'):
           $branch_categories = BranchCategory::onlyTrashed()->get();
            $browse_state="trash";
        else:
           $branch_categories = BranchCategory::withTrashed()->whereNull('deleted_at')->get();
           $browse_state="active";
        endif;

        $count_state=DB::select("SELECT (SELECT count(id) FROM {$dataType->name}) AS 'all', (SELECT count(id) FROM {$dataType->name} WHERE deleted_at IS NOT NULL) AS 'trashed'")[0];


        $data['branch_categories'] = $branch_categories;
        $data['dataType'] = $dataType;
        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;

        return view('backend.branch-category-browse', $data);
    }

    public function create(Request $request) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);
        $dataType = DataType::where('slug', $slug)->first();

        //check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0 ? new BranchCategory() : null);

        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.branch-category-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();

        //check permission
        $this->authorize('add', app($dataType->model_name));

        //validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'code' => 'required'
        ]);

        $displayName = [
            'title' => 'Category Title',
            'category_code' => 'Code'
        ];

        $validator->setAttributeNames($displayName);

        //check duplicated records
        $count = BranchCategory::where('title', trim($request['title']))
            ->orWhere('code', trim($request['code']))
            ->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Category cannot be added since name and/or code is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        //save category info
        $this->saveCategory($request);

        return redirect()->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'   => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type'    => 'success'
            ]);
    }

    public function show($id) {

    }

    public function edit(Request $request, $id) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();
        $dataTypeContent = (strlen($dataType->model_name) > 0
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->find($id));

        $this->authorize('edit', app($dataType->model_name));
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.branch-category-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function update(Request $request, $id) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();

        //validation
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'code' => 'required'
        ]);

        $displayName = [
            'title' => 'Category Title',
            'code' => 'Category Code'
        ];

        $validator->setAttributeNames($displayName);

        $count = BranchCategory::Where('id', '!=',trim($id))
            ->where(function($q) use($request) {
                  $q->where('title', trim($request['title']))
                    ->orWhere('code', trim($request['code']));
            })->count();

        if($count > 0) {
            return redirect()->back()
                ->with([
                    'message'       => 'Category cannot be added since name and/or code is already existed',
                    'alert-type'    => 'error'
                ]);
        }

        if($validator->fails()) {
            return redirect()
                ->back()
                ->with([
                    'message' => __('voyager::generic.update_failed')." {$dataType->display_name_singular}",
                    'alert-type' => 'error'
                ]);
        }

        $category = BranchCategory::findOrFail($id);

        $this->saveCategory($request, $category);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);

        $dataType = DataType::where('slug', $slug)->first();

        $this->authorize('delete', app($dataType->model_name));

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        // Product::whereIn('product_category_id', $ids)->update(['product_category_id' => 0]);
        BranchCategory::destroy($ids);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function restore(Request $request, $category_id) {
        $dataType = DataType::where('slug', 'branch-categories')->first();
        
        if (empty($category_id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $category_id;
        }

        foreach ($ids as $id) {

            $category = BranchCategory::withTrashed()->find($id);
            $category->restore();
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function checkDuplicated(Request $request, $field) {
        $value = $request->value;
        $count = BranchCategory::where($field, $value)->count();
        return $count;
    }


    protected function saveCategory(Request $request, $category = null) {
        if(!isset($category)) {
            $category = new BranchCategory();
        }

        //create slug
        $categorySlug = str_slug($request->title, '-');

        $category->title             = $request->title;
        $category->code    = $request->code;
        $category->slug             = $categorySlug ?: "";
        $category->description      = $request->description ?: "";
        $category->create_by        = auth()->user()->id;
        $category->modified_by      = auth()->user()->id;
        $category->updated_at       = now();
        $category->save();
    }
}