<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\ActivitiesLogApplyCard;
use App\ActivitiesLogAppStatus;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use DateTime;

class ActivitiesLogController extends Controller
{
    //

    public function getLog(Request $request)
    {
        $type1 = array('citizenID');
        $type2 = array('phoneNo', 'email');
        $check_filter = array('logDateBegin', 'logDateEnd', 'logTimeBegin', 'logTimeEnd', 'os', 'logTypeID');

        try {
            // check mandatory params
        if ($request->requestID == null || $request->pageNo == null || $request->itemPerPage == null) {
                return $this->responseSystemError($request->requestID);
        }

        $requestID = $request->requestID;
            if (gettype($request->itemPerPage) != "integer" || gettype($request->pageNo) != "integer") {
                return $this->responseSystemError($request->requestID);
            }
        $per_page = $request->itemPerPage;
        $request->page = $request->pageNo;
        if (!isset($request->logFilter)) {
            $app_status = ActivitiesLogAppStatus::all();
            $apply_card = ActivitiesLogAppStatus::all();

            foreach ($apply_card as $key => $value) {
                $merge_log = $app_status->push($value);
            }

            $sorted = $merge_log->sortByDesc('date_time');
            $paginator = new Paginator($sorted->values()->all(), $per_page, $request->page);

            $new_logs = array();
            foreach ($paginator->items() as $key => $value) {
                $new_logs[$key]['logID'] = $value->log_id;
                    if (mb_substr($value->log_id, 0, 1) == 'A') {
                        $type = 2;
                    } elseif (strrpos($value->log_id, 'B') != null) {
                    $type = 1;
                }
                $new_logs[$key]['logTypeID'] = $type;

                $newdate = new DateTime($value->date_time);

                    $new_logs[$key]['logDate'] = $newdate->format('YmdHms');

                $new_logs[$key]['os'] = $value->os;
                    $new_logs[$key]['browserIp'] = $value->browser;

                if ($type == 1) {
                    $new_logs[$key]['citizenID'] = $value->citizen_id;
                } else {
                    $new_logs[$key]['phoneNo'] = $value->phone_no;
                    $new_logs[$key]['email'] = $value->email;
                }

                    $value->cf1 = json_decode($value->cf1);
                    $value->cf2 = json_decode($value->cf2);
                    $value->custom_params = json_decode($value->custom_params);

                    //custom_params

                $new_logs[$key]['logDetail'] = $value;

            }
            $str = array(
                'requestID' => $request->requestID,
                'responseID' => $this->genResNo(),
                'responseCode' => '100',
                'responseMessage' => 'SUCCESS',
                'logs' => $new_logs, //$logs->items(), // get data from paginator
                'totalItem' => $paginator->count(),
            );

            return $str;

            } else {
                foreach ($request->logFilter as $key => $value) {
                    // check optional if params are in spec
                    if (!in_array($key, $check_filter) && !in_array($key, $type1) && !in_array($key, $type2)) {
                        return $this->responseSystemError($request->requestID);

        }
                }
            }

        $log_filter = $request->logFilter;

        if (isset($log_filter['logTypeID'])) {

            $type = $log_filter['logTypeID'];

            if (isset($log_filter['logDateBegin']) && isset($log_filter['logDateEnd'])) {
                $start = $log_filter['logDateBegin'];
                $end = $log_filter['logDateEnd'];
            } elseif (isset($log_filter['logDateEnd'])) {
                $end = $log_filter['logDateEnd'];
            } elseif (isset($log_filter['logDateBegin'])) {
                $start = $log_filter['logDateBegin'];
            }

                if (isset($log_filter['logTimeBegin']) && isset($log_filter['logTimeEnd'])) {
                    $time_start = $log_filter['logTimeBegin'];
                    $time_end = $log_filter['logTimeEnd'];
                } elseif (isset($log_filter['logTimeEnd'])) {
                    $time_end = $log_filter['logTimeEnd'];
                } elseif (isset($log_filter['logTimeBegin'])) {
                    $time_start = $log_filter['logTimeBegin'];
                }

            foreach ($log_filter as $key => $value) {
                if ($type == 1) {
                    if (in_array($key, $type2)) {
                            /*  $result = null;
                            break; */
                            return $this->responseNoData($request->requestID);

                    }
                } elseif ($type == 2) {
                    if (in_array($key, $type1)) {
/*                             $result = null;
break; */
                            return $this->responseNoData($request->requestID);

                    }
                }
            }
        } elseif (!isset($log_filter['logTypeID'])) {

                if (isset($log_filter['logDateBegin']) && isset($log_filter['logDateEnd'])) {
                    $start = $log_filter['logDateBegin'];
                    $end = $log_filter['logDateEnd'];
                } elseif (isset($log_filter['logDateEnd'])) {
                    $end = $log_filter['logDateEnd'];
                } elseif (isset($log_filter['logDateBegin'])) {
                    $start = $log_filter['logDateBegin'];
                }

                if (isset($log_filter['logTimeBegin']) && isset($log_filter['logTimeEnd'])) {
                    $time_start = $log_filter['logTimeBegin'];
                    $time_end = $log_filter['logTimeEnd'];
                } elseif (isset($log_filter['logTimeEnd'])) {
                    $time_end = $log_filter['logTimeEnd'];
                } elseif (isset($log_filter['logTimeBegin'])) {
                    $time_start = $log_filter['logTimeBegin'];
                }

                $filter = $this->mapFilter($log_filter);
                $a1 = 0;
                $a2 = 0;

                foreach ($log_filter as $key => $value) {
                    if (in_array($key, $type1)) {
                        $a1++;
                    } elseif (in_array($key, $type2)) {
                        $a2++;
                    }
                }

                if ($a1 > 0 && $a2 > 0) { // type error
                    $merge_log = null;
                } elseif ($a1 > 0 && $a2 == 0) { // type ActivitiesLogAppStatus
                    //$merge_log = ActivitiesLogAppStatus::where($filter)->get();
                    /* TODO */

                    /*   if (isset($start) && isset($end)) {

                    $merge_log = ActivitiesLogAppStatus::whereDate('date_time', '>=', $start)
                    ->whereDate('date_time', '<=', $end)
                    ->where($filter)
                    ->paginate($per_page);
                    } elseif (isset($start)) {
                    $merge_log = ActivitiesLogAppStatus::whereDate('date_time', '>=', $start)
                    ->where($filter)
                    ->paginate($per_page);
                    } elseif (isset($end)) {
                    $merge_log = ActivitiesLogAppStatus::whereDate('date_time', '<=', $end)
                    ->where($filter)
                    ->paginate($per_page);
                    } else {
                    $merge_log = ActivitiesLogAppStatus::where($filter)
                    ->paginate($per_page);
                    } */

                    if (isset($start) && isset($end)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $merge_log = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                    } elseif (isset($start)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $merge_log = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->paginate($per_page);

                        }

                    } elseif (isset($end)) { //done
                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $app_status_logs = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                        // dd(request()->ip());

                        //   $app_status_logs = ActivitiesLogAppStatus::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                        //   $apply_card_logs = ActivitiesLogApplyCard::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                    } else { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $merge_log = ActivitiesLogAppStatus::where($filter)
                                ->paginate($per_page);

                        }

                    }
                } elseif ($a1 == 0 && $a2 > 0) { // type ActivitiesLogApplyCard
                    // $merge_log = ActivitiesLogApplyCard::where($filter)->get();
                    /*    if (isset($start) && isset($end)) {
                    $merge_log = ActivitiesLogApplyCard::whereDate('date_time', '>=', $start)
                    ->whereDate('date_time', '<=', $end)
                    ->where($filter)
                    ->paginate($per_page);
                    } elseif (isset($start)) {
                    $merge_log = ActivitiesLogApplyCard::whereDate('date_time', '>=', $start)
                    ->where($filter)
                    ->paginate($per_page);
                    } elseif (isset($end)) {
                    $merge_log = ActivitiesLogApplyCard::whereDate('date_time', '<=', $end)
                    ->where($filter)
                    ->paginate($per_page);
                    } else {
                    $merge_log = ActivitiesLogApplyCard::where($filter)
                    ->paginate($per_page);
                    } */
                    if (isset($start) && isset($end)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $merge_log = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                    } elseif (isset($start)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $merge_log = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->paginate($per_page);

                        }

                    } elseif (isset($end)) { //done
                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $merge_log = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                        // dd(request()->ip());

                        //   $app_status_logs = ActivitiesLogAppStatus::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                        //   $apply_card_logs = ActivitiesLogApplyCard::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                    } else { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $merge_log = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $merge_log = ActivitiesLogApplyCard::where($filter)
                                ->paginate($per_page);

                        }

                    }

                } elseif ($a1 == 0 && $a2 == 0) { // both type

                    if (isset($start) && isset($end)) { //done
                        /* $app_status_logs = ActivitiesLogAppStatus::whereDate('date_time', '>=', $start)
                        ->whereDate('date_time', '<=', $end)
                        ->where($filter)
                        ->paginate($per_page);

                        $apply_card_logs = ActivitiesLogApplyCard::whereDate('date_time', '>=', $start)
                        ->whereDate('date_time', '<=', '$end')
                        ->where($filter)
                        ->paginate($per_page);

                         */
                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $app_status_logs = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                            $apply_card_logs = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                    } elseif (isset($start)) { //done

                        /*    $app_status_logs = ActivitiesLogAppStatus::whereDate('date_time', '>=', $start)
                        ->where($filter)
                        ->paginate($per_page);

                        $apply_card_logs = ActivitiesLogApplyCard::whereDate('date_time', '>=', $start)
                        ->where($filter)
                        ->paginate($per_page);
                         */
                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $app_status_logs = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->paginate($per_page);

                            $apply_card_logs = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->paginate($per_page);

                        }

                    } elseif (isset($end)) { //done
                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $app_status_logs = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                            $apply_card_logs = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                        // dd(request()->ip());

                        //   $app_status_logs = ActivitiesLogAppStatus::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                        //   $apply_card_logs = ActivitiesLogApplyCard::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                    } else { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $app_status_logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);
                            $apply_card_logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $app_status_logs = ActivitiesLogAppStatus::where($filter)
                                ->paginate($per_page);

                            $apply_card_logs = ActivitiesLogApplyCard::where($filter)
                                ->paginate($per_page);

                        }

                    }

                    if ($app_status_logs->count() == 0 && $apply_card_logs->count() == 0) {
                        $merge_log = null;
                    } elseif ($app_status_logs->count() > 0 && $apply_card_logs->count() == 0) {
                        $merge_log = $app_status_logs;
                    } elseif ($app_status_logs->count() == 0 && $apply_card_logs->count() > 0) {
                        $merge_log = $apply_card_logs;
                    } elseif ($app_status_logs->count() > 0 && $apply_card_logs->count() > 0) {
                        foreach ($apply_card_logs as $key => $value) {
                            $merge_log = $app_status_logs->push($value);
                        }
                    }

                }

                {
                    if ($merge_log == null) {
                        //  $result = null;

                        return $this->responseNoData($request->requestID);
                    } else {

                        $sorted = $merge_log->sortByDesc('date_time');
                        $paginator = new Paginator($sorted->values()->all(), $per_page, $request->page);

                        $new_logs = array();
                        foreach ($paginator->items() as $key => $value) {
                            //return $value;
                            $new_logs[$key]['logID'] = $value->log_id;
                            /*   if (strrpos($value->log_id, 'A') != null) {
                                $type = 1;
                            } elseif (strrpos($value->log_id, 'B') != null) {
                                $type = 2;
                            } */
                            if (mb_substr($value->log_id, 0, 1) == 'A') {
                                $type = 2;
                            } elseif (strrpos($value->log_id, 'B') != null) {
                                $type = 1;
                            }
                            $new_logs[$key]['logTypeID'] = $type;

                            $newdate = new DateTime($value->date_time);

                            $new_logs[$key]['logDate'] = $newdate->format('YmdHms');

                            $new_logs[$key]['os'] = $value->os;
                            $new_logs[$key]['browserIp'] = $value->browser;
                            if ($type == 1) {
                                $new_logs[$key]['citizenID'] = $value->citizen_id;
                            } else {
                                $new_logs[$key]['phoneNo'] = $value->phone_no;
                                $new_logs[$key]['email'] = $value->email;
                            }
                            $value->cf1 = json_decode($value->cf1);
                            $value->cf2 = json_decode($value->cf2);
                            $value->custom_params = json_decode($value->custom_params);

                            $new_logs[$key]['logDetail'] = $value;

                        }
                        if ($new_logs == null) {
                            return $this->responseNoData($request->requestID);
                        }

            $str = array(
                'requestID' => $request->requestID,
                'responseID' => $this->genResNo(),
                            'responseCode' => '100',
                            'responseMessage' => 'SUCCESS',
                            'logs' => $new_logs, //$logs->items(), // get data from paginator
                            'totalItem' => $paginator->count(),
            );

            return $str;

        }
                }

            }

        //set current page
        $currentPage = $request->page;
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });

            if (isset($result)) {
                $str = $this->responseNoData($request->requestID);
        } else {
                $filter = $this->mapFilter($log_filter);
                /* TODO */
                if (isset($type) && $type == 1) {
                    /*  if (isset($start) && isset($end)) {
                    $logs = ActivitiesLogAppStatus::whereDate('date_time', '>=', $start)
                        ->whereDate('date_time', '<=', $end)
                        ->where($filter)
                        ->paginate($per_page);
                } elseif (isset($start)) {
                    $logs = ActivitiesLogAppStatus::whereDate('date_time', '>=', $start)
                        ->where($filter)
                        ->paginate($per_page);
                } elseif (isset($end)) {
                    $logs = ActivitiesLogAppStatus::whereDate('date_time', '<=', $end)
                    ->where($filter)
                    ->paginate($per_page);
                    } else {
                    $logs = ActivitiesLogAppStatus::where($filter)
                    ->paginate($per_page);
                    } */

                    if (isset($start) && isset($end)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $logs = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                    } elseif (isset($start)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $logs = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->paginate($per_page);

                        }

                    } elseif (isset($end)) { //done
                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {
                            $logs = ActivitiesLogAppStatus::where($filter)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                        }

                        // dd(request()->ip());

                        //   $app_status_logs = ActivitiesLogAppStatus::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                        //   $apply_card_logs = ActivitiesLogApplyCard::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                    } else { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                        ->where($filter)
                        ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogAppStatus::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                } else {
                    $logs = ActivitiesLogAppStatus::where($filter)
                        ->paginate($per_page);
                }
            }

                } elseif (isset($type) && $type == 2) {
                    /*  if (isset($start) && isset($end)) {
                    $logs = ActivitiesLogApplyCard::whereDate('date_time', '>=', $start)
                    ->whereDate('date_time', '<=', $end)
                        ->where($filter)
                        ->paginate($per_page);
                } elseif (isset($start)) {
                    $logs = ActivitiesLogApplyCard::whereDate('date_time', '>=', $start)
                        ->where($filter)
                        ->paginate($per_page);
                } elseif (isset($end)) {
                    $logs = ActivitiesLogApplyCard::whereDate('date_time', '<=', '$end')
                        ->where($filter)
                        ->paginate($per_page);
                } else {
                    $logs = ActivitiesLogApplyCard::where($filter)
                        ->paginate($per_page);
                    } */
                    if (isset($start) && isset($end)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $logs = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

                }

                    } elseif (isset($start)) { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '>=', $start)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $logs = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '>=', $start)
                                ->paginate($per_page);

                        }

                    } elseif (isset($end)) { //done
                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->whereDate('date_time', '<=', $end)
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $logs = ActivitiesLogApplyCard::where($filter)
                                ->whereDate('date_time', '<=', $end)
                                ->paginate($per_page);

            }

                        // dd(request()->ip());

                        //   $app_status_logs = ActivitiesLogAppStatus::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                        //   $apply_card_logs = ActivitiesLogApplyCard::whereDate('date_time', '<=', $end)
                        //       ->where($filter)
                        //       ->paginate($per_page);

                    } else { //done

                        if (isset($time_start) && isset($time_end)) {
                            $time_start = new DateTime($time_start);
                            $time_end = new DateTime($time_end);

                            $new_time_start = $time_start->format('Hms');
                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_start)) {

                            $time_start = new DateTime($time_start);

                            $new_time_start = $time_start->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '>=', \Carbon\Carbon::parse($new_time_start))
                                ->where($filter)
                                ->paginate($per_page);

                        } elseif (isset($time_end)) {

                            $time_end = new DateTime($time_end);

                            $new_time_end = $time_end->format('Hms');

                            $logs = ActivitiesLogApplyCard::whereTime('date_time', '<=', \Carbon\Carbon::parse($new_time_end))
                                ->where($filter)
                                ->paginate($per_page);

                        } else {

                            $logs = ActivitiesLogApplyCard::where($filter)
                                ->paginate($per_page);

                        }

                    }

                }
            //$log_detail = array('logD')

                if ($logs->count() == 0) {

                    return $this->responseNoData($request->requestID);
                }
            if ($logs->items() != null) {
                $new_logs = array();
                foreach ($logs as $key => $value) {
                    //return $value;
                        //dd($value->log_id);
                    $new_logs[$key]['logID'] = $value->log_id;
                    $new_logs[$key]['logTypeID'] = $type;

                    $newdate = new DateTime($value->date_time);

                        $new_logs[$key]['logDate'] = $newdate->format('YmdHms');

                    $new_logs[$key]['os'] = $value->os;
                        $new_logs[$key]['browserIp'] = $value->browser;

                    if ($type == 1) {
                        $new_logs[$key]['citizenID'] = $value->citizen_id;
                    } else {
                        $new_logs[$key]['phoneNo'] = $value->phone_no;
                        $new_logs[$key]['email'] = $value->email;
                    }

                        $value->cf1 = json_decode($value->cf1);
                        $value->cf2 = json_decode($value->cf2);
                        $value->custom_params = json_decode($value->custom_params);

                    $new_logs[$key]['logDetail'] = $value;

                }
            } else {
                    $str = $this->responseNoData($request->requestID);

            }

            $str = array(
                'requestID' => $request->requestID,
                'responseID' => $this->genResNo(),
                'responseCode' => '100',
                'responseMessage' => 'SUCCESS',
                'logs' => $new_logs, //$logs->items(), // get data from paginator
                'totalItem' => $logs->total(),
            );

        }

        return $str;
        } catch (Exception $e) {
            return $this->responseSystemError($request->requestID);

        }

    }

    public function getType(Request $request)
    {

        $requestID = $request->requestID;
        $types = array();
        $types[0]['id'] = 1;
        $types[0]['name'] = 'CheckedAppStatus';
        $types[1]['id'] = 2;
        $types[1]['name'] = 'AppliedCard';

        $str = array(
            'requestID' => $requestID,
            'responseID' => $this->genResNo(),
            'responseCode' => '100',
            'responseMessage' => 'SUCCESS',
            'logTypes' => $types,
        );

        return $str;
    }

    /**
     * @return string
     */
    protected function genResNo()
    {
        $digit = 18;
        $no1 = rand(pow(10, $digit - 1), pow(10, $digit) - 1);
        $no2 = rand(pow(10, $digit - 1), pow(10, $digit) - 1);
        $res_no = $no1 . '' . $no2;

        return $res_no;

    }

    protected function mapFilter($logFilter)
    {
        $filter = array();
            foreach ($logFilter as $key => $value) {
                switch ($key) {
                    case 'citizenID':
                        $filter['citizen_id'] = $value;
                        break;
                    case 'os':
                        $filter['os'] = $value;
                        break;
                case 'browserIp':
                        $filter['browser'] = $value;
                        break;
                    case 'phoneNo':
                        $filter['phone_no'] = $value;
                        break;
                    case 'cardType':
                        $filter['card_type'] = $value;
                        break;
                    case 'email':
                        $filter['email'] = $value;
                        break;
                    case 'firstName':
                        $filter['first_name'] = $value;
                        break;
                    case 'lastName':
                        $filter['last_name'] = $value;
                        break;
                    case 'branchCode':
                        $filter['branch_code'] = $value;
                        break;
                    case 'productCode':
                        $filter['product_code'] = $value;
                        break;
                    case 'productName':
                        $filter['product_name'] = $value;
                        break;
                    case 'optionSalary':
                        $filter['opt_salary'] = $value;
                        break;
                    case 'customField1Key':
                        $filter['cf1_key'] = $value;
                        break;
                    case 'customField1Value':
                        $filter['cf1_value'] = $value;
                        break;
                    case 'customField2Key':
                        $filter['cf2_key'] = $value;
                        break;
                    case 'customField2Value':
                        $filter['cf2_value'] = $value;
                        break;
                    case 'indicator':
                        $filter['indicator'] = $value;
                        break;
                    case 'sub_indicator':
                        $filter['sub_indicator'] = $value;
                        break;
                    case 'submit_date':
                        $filter['submit_date'] = $value;
                        break;
                    case 'previous_page':
                        $filter['previous_page'] = $value;
                        break;
                    case 'callback_link':
                        $filter['callback_link'] = $value;
                        break;
                    case 'submit_form_url':
                        $filter['submit_form_url'] = $value;
                        break;
                }
            }

        return $filter;
    }

    protected function responseNoData($requestID)
    {
        $str = array(
            'requestID' => $requestID,
            'responseID' => $this->genResNo(),
            'responseCode' => '200',
            'responseMessage' => 'NO_DATA',
        );

        return $str;

        }

    protected function responseSystemError($requestID)
    {
        $str = array(
            'requestID' => $requestID,
            'responseID' => $this->genResNo(),
            'responseCode' => '300',
            'responseMessage' => 'SYSTEM_ERROR',
        );

        return $str;

    }

}