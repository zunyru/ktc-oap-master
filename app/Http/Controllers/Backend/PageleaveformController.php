<?php

namespace App\Http\Controllers\Backend;

use App\FormLayout;
use App\Http\Controllers\Backend\UploaderController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Pageleaveform;
use App\Branch;
use App\Leavefromapp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Validator;
use Voyager;

class PageleaveformController extends Controller
{
    use BreadRelationshipParser;

    private $directory = '';

    public function __construct()
    {
        $this->filesystem = config('voyager.storage.disk');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        //$slug = 'pageleaveform';
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $status = 0;
        $browse_state="active";
        if($request->has('status') && $request->query('status') == 'trash' || $request->segment(count($request->segments()))=='trash'){
            $status = 1;$browse_state="trash";
        }

        $objs_pages = DB::table('pageleaveforms')
            ->join('products', 'pageleaveforms.Product_id', '=', 'products.id')
            ->where('pageleaveforms.active', $status)
            ->get(['pageleaveforms.*', 'products.title as product_title']);

        $count_state=DB::select("SELECT (SELECT count(id) FROM pageleaveforms) AS 'all', (SELECT count(id) FROM pageleaveforms WHERE active=1) AS 'trashed'")[0];

        $data['objs_forms'] = $objs_pages;
        $data['dataType'] = $dataType;
        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;

        return view('backend.page-browse', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        //Get product
        $products = DB::table('products')->where('active', 0)->get();

        //Get form
        $leaveform = DB::table('leaveforms')->where('active', 0)->get();

        $branches = Branch::all();

        $indicators = Leavefromapp::all();

        $indicator_values = [];
        foreach($indicators as $index=> $item){
        $custom_param  = json_decode($item->custom_param);
           if(!is_null($custom_param) && !is_null($custom_param->indicator)){
                 $indicator_values[] = $custom_param->indicator;
             }
        }
        sort($indicator_values);

        $indicator_values  = array_unique($indicator_values);
        $indicator_values = collect($indicator_values);

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;

        $layouts = DB::table('formlayouts')->where('PageLeaveForm_id', $dataTypeContent->id)->first();

        foreach ($dataType->addRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.page-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'products', 'leaveform', 'layouts', 'branches' ,'indicator_values'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // set validation
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'slug' => 'required',
                'title_page' => 'required',
                'product' => 'required',
                'form' => 'required',
                'layout' => 'required',
            ]);
        //set nicename to display
        $niceNames = array(
            'title' => 'Title',
            'slug' => 'Slug',
            'title_page' => 'Title page',
            'product' => 'Product',
            'form' => 'Form',
            'layout' => 'Layout',
        );

        $validator->setAttributeNames($niceNames);

        //Check Dup Title
        if (self::checkDupTitle($request) > 0) {
            return redirect()
                ->back()
                ->with([
                    'message' => "Save Faild. This title already exists",
                    'alert-type' => 'error',
                ]);
        }

        //Ceck dup slug
        $cont_slug = self::checkDupSlug($request);
        if ($cont_slug > 0) {
            $cont_slug = $cont_slug + 1;
            $url_slug = $request->slug . "-" . $cont_slug;
        } else {
            $url_slug = $request->slug;
        }

        if (!$request->has('_validate')) {

            //insert pages
            $form = new Pageleaveform;
            $form->title = $request->title;
            $form->title_page = $request->title_page;
            $form->slug = $url_slug;
            $form->description = $request->description;
            $form->content = $request->input("content");
            $form->layout = $request->layout;
            $form->active = 0;
            $form->public = $request->public == 'on' ? 0 : 1;
            $form->LeaveForm_id = $request->form;
            $form->Product_id = $request->product;
            $form->branch_id = $request->branch_code;
            $form->custom_param = json_encode(array(
                "indicator" => $request->indicator,
                "sub_indicator" => $request->sub_indicator,
                "inbound" => $request->inbound,
                "outbound" => $request->outbound,
                "callback" => $request->callback,
            ));
            $form->create_by = auth()->user()->id;
            $form->modified_by = auth()->user()->id;
            $form->updated_at = now();
            $form->save();

            $this->saveEnglishFields($request, $form);

            //UploaderController
            $uploader = new UploaderController;

            /*if ($request->hasFile('slide_image')) {
                //MultipleImage
                $fullPath_Multi = $uploader->MultipleImage_handel($request->slide_image, $slug . "-slide");

            }*/

            $card_slides =  ["default" => [], "en" => []];
            if ($request->hasFile('slide_image')) {
                //MultipleImage
                $fullPath_Multi = $uploader->MultipleImage_handel($request->slide_image, $slug . "-slide");
                $card_slides["default"] = $fullPath_Multi;
                $uploade_more = 'add_default';
            }

            if ($request->hasFile('slide_image_en')) {
                $full_path_en = $uploader->MultipleImage_handel($request->slide_image_en, $slug."-slide-en");
                $card_slides["en"] = $full_path_en;
                $uploade_more = 'add_en';
            }

            if ($request->hasFile('bg_image')) {
                //Image Single
                $fullPath_Bg = $uploader->SingleImage_handel($request->bg_image, $slug . "-cover-card");
            }
            if ($request->hasFile('card_image')) {
                $fullPath_Card = $uploader->SingleImage_handel($request->card_image, $slug . "-card");
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            //Check Layout page
            switch ($request->layout) {

                case 'card_hero':
                    if ($request->setting === 'on') {
                        $last_id = $this->insertCardHero($request, $form, $fullPath_Bg, "");
                    } else {
                        $last_id = $this->insertCardHero($request, $form, $fullPath_Bg, $fullPath_Card);
                    }
                    break;

                case 'slide_hero':
                    $last_id = $this->insertSlideHero($request, $form, $card_slides );
                    break;

                default:

                    break;
            }

            return redirect()
                // ->route("voyager.{$dataType->slug}.index")
                ->route("voyager.{$dataType->slug}.edit",$form->id)
                ->with([
                    'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
        //endif
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? app($dataType->model_name)->with($relationships)->findOrFail($id)
        : DB::table($dataType->name)->where('id', $id)->first();

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        $dataTypeContent = Pageleaveform::findOrFail($id);

        $layouts = $dataTypeContent->formLayout;

        //Get product
        $products = DB::table('products')->where('active', 0)->get();

        //Get form
        $leaveform = DB::table('leaveforms')->where('active', 0)->get();

        $branches = Branch::all();

        $indicators = Leavefromapp::all();

        $indicator_values = [];
        foreach($indicators as $index=> $item){
        $custom_param  = json_decode($item->custom_param);
           if(!is_null($custom_param) && !is_null($custom_param->indicator)){
                 $indicator_values[] = $custom_param->indicator;
             }
        }

        $indicator_values  = array_unique($indicator_values);
        $indicator_values = collect($indicator_values);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
        return view('backend.page-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'products', 'leaveform', 'layouts', 'branches' ,'indicator_values'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // set validation
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'slug' => 'required',
                'title_page' => 'required',
                'product' => 'required',
                'form' => 'required',
                'layout' => 'required',
            ]);
        //set nicename to display
        $niceNames = array(
            'title' => 'Title',
            'slug' => 'Slug',
            'title_page' => 'Title page',
            'product' => 'Product',
            'form' => 'Form',
            'layout' => 'Layout',
        );

        $validator->setAttributeNames($niceNames);

        //Check Dup Title
        if (self::checkDupTitle($request) > 0) {
            return redirect()
                ->back()
                ->with([
                    'message' => "Save Faild. This title already exists",
                    'alert-type' => 'error',
                ]);
        }

        //Ceck dup slug
        $cont_slug = self::checkDupSlug($request);
        if ($cont_slug > 0) {
            $cont_slug = $cont_slug + 1;
            $url_slug = $request->slug . "-" . $cont_slug;
        } else {
            $url_slug = $request->slug;
        }

        if (!$request->has('_validate')) {

            //Select old layout
            $DB = new DB;
            $objs = $DB::table('pageleaveforms')
                ->where('pageleaveforms.id', $id)
                ->get();

            $page = Pageleaveform::find($id);

            $layouts = $page->formLayout;

            //UploaderController
            $uploader = new UploaderController;
            $uploade_more_default = '';
            $uploade_more_en = '';
            $card_slides = isset($layouts->card_slide) ? json_decode($layouts->card_slide, true) : ["default" => [], "en" => []];
            if ($request->hasFile('slide_image')) {
                //MultipleImage
                $fullPath_Multi = $uploader->MultipleImage_handel($request->slide_image, $slug . "-slide");
                $card_slides["default"] = $fullPath_Multi;
                $uploade_more_default = 'add_default';
            }

            if ($request->hasFile('slide_image_en')) {
                $full_path_en = $uploader->MultipleImage_handel($request->slide_image_en, $slug."-slide-en");
                $card_slides["en"] = $full_path_en;
                $uploade_more_en = 'add_en';
            }

            if ($request->hasFile('bg_image')) {
                //Image Single
                $fullPath_Bg = $uploader->SingleImage_handel($request->bg_image, $slug . "-cover-card");
            } else {
                if (!is_null($layouts)) {
                    $fullPath_Bg = $layouts->card_cover;
                }
            }
            if ($request->hasFile('card_image')) {
                $fullPath_Card = $uploader->SingleImage_handel($request->card_image, $slug . "-card");
            } else {
                if (!is_null($layouts)) {
                    $fullPath_Card = $layouts->card_hero;
                }
            }

            //Update Pages
            $form = $this->updatePage($request, $id, $url_slug);



            switch ($page->layout) {
                case 'blank_form':
                    switch ($request->layout) {
                       case 'card_hero':
                         //blank_form To card_hero
                         if ($request->setting === 'on') {
                                $last_id = $this->insertCardHero($request, $form, $fullPath_Bg, "");
                         } else {
                                $last_id = $this->insertCardHero($request, $form, $fullPath_Bg, $fullPath_Card);
                         }
                       break;
                       case 'slide_hero':
                         //blank_form To slide_hero
                         $last_id = $this->insertSlideHero($request, $form, $card_slides );
                       break;
                    }
                    break;
                case 'card_hero':
                       switch ($request->layout) {
                       case 'blank_form':
                            //card_hero TO blank_form
                            DB::table('formlayouts')->where('PageLeaveForm_id', $id)->delete();
                       break;
                       case 'card_hero':
                         //card_hero To card_hero
                         if ($request->setting === 'on') {
                                $last_id = $this->updateCardHero($request ,$form, $fullPath_Bg, "");
                         } else {
                                $last_id = $this->updateCardHero($request ,$form, $fullPath_Bg, $fullPath_Card);
                         }
                       break;
                       case 'slide_hero':
                         //card_hero To slide_hero
                         $last_id = $this->updateSlideHero($request, $form, $card_slides, $uploade_more_default,$uploade_more_en);
                       break;
                    }
                    break;
                case 'slide_hero':
                    switch ($request->layout) {
                       case 'blank_form':
                            //card_hero TO blank_form
                            DB::table('formlayouts')->where('PageLeaveForm_id', $id)->delete();
                       break;
                       case 'card_hero':
                         //card_hero To card_hero
                         if ($request->setting === 'on') {
                                $last_id = $this->updateCardHero($request , $form, $fullPath_Bg, "");
                         } else {
                                $last_id = $this->updateCardHero($request , $form, $fullPath_Bg, $fullPath_Card);
                         }
                       break;
                       case 'slide_hero':
                         //card_hero To slide_hero
                         $last_id = $this->updateSlideHero($request, $form, $card_slides, $uploade_more_default,$uploade_more_en);
                       break;
                    }
                    break;
            }

            return redirect()
                // ->route("voyager.{$dataType->slug}.index")
                ->route("voyager.{$dataType->slug}.edit",$form->id)
                ->with([
                    'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Pageleaveform::find($id);
            if($form->active === 1){
                //force Delete
                //remove image
                $layouts = DB::table('formlayouts')->where('PageLeaveForm_id', $id)->first();

                if(isset($layouts)){
                    if(!is_null($layouts->card_hero)){
                        Storage::disk('s3')->delete($layouts->card_hero);
                    }
                    if($layouts->card_cover){
                        Storage::disk('s3')->delete($layouts->card_cover);
                    }
                    if($layouts->card_slide){
                        $arr_slide = json_decode($layouts->card_slide, true);
                        foreach ($arr_slide as $index => $slide) {
                            Storage::disk('s3')->delete($slide);
                        }
                    }

                    FormLayout::destroy($layouts->id);
                }

                 $form->forceDelete();
                 //Pageleaveform::destroy($id);
            }else{
                //soft Delete
                $form->active = 1;
                $form->modified_by = auth()->user()->id;
                $form->save();
            }
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function restore(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        //$slug = $Vg->getSlug($request);
        $slug = "pageleaveform";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Pageleaveform::find($id);
            $form->active = 0;
            $form->modified_by = auth()->user()->id;

            $form->save();
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => 'Successfully  Restore' . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    /**
     * [Insert Card Hero]
     * @param  Request $request          [request]
     * @param  [type]  $model            [query]
     * @param  [type]  $fullPath_Multi   [path image slide]
     * @return [type]                    [save id]
     */
    public function insertSlideHero(Request $request, $model, $fullPath_Multi )
    {
        $path = url('oap/storage');
        //old image in db

        $arrayImg = ["default" => [], "en" => []];

        if (!isset($fullPath_Multi["default"])) {
            $slides_default = $fullPath_Multi;
        } else {
            $slides_default = $fullPath_Multi["default"];
            $slides_en = $fullPath_Multi["en"];
        }

        //move image
        $arrayImgMove = [];
        $array_img_move_en = [];

        //new image uploade
        //if ($uploade_more == 'add_default') {
            //default
            if (sizeof($slides_default) > 0) {
                foreach ($slides_default as $item) {
                    $arrayImg["default"][] = $item;
                }
            }
        //}

        //if ($uploade_more == 'add_en') {
            //en
            if (sizeof($slides_en) > 0) {
                foreach ($slides_en as $item) {
                    $arrayImg["en"][] = $item;
                }
            }

        //}

        $new_image_slide = $arrayImg;

        if (count($new_image_slide["default"]) == 0 && count($new_image_slide["en"]) == 0) {
            $dataSet = array(
                'card_hero' => null,
                'card_cover' => null,
                'heading' => $request->header_text,
                'sub_heading' => $request->sub_header_text,
                'features' => null,
                'setting' => null,
            );
        } else {

            $dataSet = array(
                'card_hero' => null,
                'card_cover' => null,
                'card_slide' => json_encode($new_image_slide),
                'heading' => $request->header_text,
                'sub_heading' => $request->sub_header_text,
                'features' => null,
                'setting' => null,
            );
        }

        /*$obj = FormLayout::where('PageLeaveForm_id', $model->id)
            ->insert($dataSet);*/

        $obj = new FormLayout;
        $obj->PageLeaveForm_id = $model->id;
         if (count($new_image_slide["default"]) == 0 && count($new_image_slide["en"]) == 0) {

         } else {
            $obj->card_slide = json_encode($new_image_slide);
        }
        $obj->heading = $request->header_text;
        $obj->sub_heading = $request->sub_header_text;

        $obj->save();

        $form_en = $model->formLayout->english();
        $form_en->heading = $request->header_text_en ?: "";
        $form_en->sub_heading = $request->sub_header_text_en ?: "";
        $form_en->save();

        return $insert_id = $obj->id;

        /*$obj = new FormLayout;
        $obj->PageLeaveForm_id = $model->id;
        $obj->card_slide = $fullPath_Multi;
        $obj->heading = $request->header_text;
        $obj->sub_heading = $request->sub_header_text;

        $obj->save();

        $obj_en = $obj->english();
        $obj_en->heading = $request->header_text_en;
        $obj_en->sub_heading = $request->sub_header_text;
        $obj_en->save();

        return $insert_id = $obj->id;*/
    }

    /**
     * [Insert Card Hero]
     * @param  Request $request       [request]
     * @param  [type]  $model         [query]
     * @param  [type]  $fullPath_bg   [path image background]
     * @param  [type]  $fullPath_Card [path image card]
     * @return [type]                 [save id]
     */
    public function insertCardHero(Request $request, $model, $fullPath_bg, $fullPath_Card)
    {
        $features = $request->feature;
        $feature_sub = $request->feature_sub;

        $features_en = $request->feature_en;
        $feature_sub_en = $request->feature_sub_en;

        $arr_features = null;
        $arr_features_en = null;

        for ($i = 0; $i < sizeof($features); $i++) {
            $arr_features['title'][$i] = $features[$i];
            $arr_features['sub'][$i] = $feature_sub[$i];

            $arr_features_en['title'][$i] = $features_en[$i];
            $arr_features_en['sub'][$i] = $feature_sub_en[$i];
        }

        //if (!is_null($arr_has_slide)) {
            //Check remove image
            if (!empty($request->slide_remove)) {
                $slide_remove = explode(",", $request->slide_remove);

                $arrayImgRemove = array();
                $arrayImgRemove_path = array();
                foreach ($slide_remove as $index => $value) {
                    $img = str_replace($path . '/', '', $value);
                    // windows use "\\""
                    $img_back = str_replace('/', '/', $img);
                    if (in_array($img, $arrayImg["default"])) {
                        Storage::disk('s3')->delete($img);
                        array_push($arrayImgRemove, $img);
                    }
                    if (in_array($img_back, $arrayImg["default"])) {
                        Storage::disk('s3')->delete($img_back);
                        array_push($arrayImgRemove, $img_back);
                    }

                }
                //var_dump($arrayImgRemove);

                $arrayImg["default"] = array_diff($arrayImg["default"], $arrayImgRemove);
            }

            if(!empty($request->slide_remove_en)) {
                $slide_items = explode(",", $request->slide_remove_en);
                $removed_items = [];
                foreach ($slide_items as $index => $item) {
                    $img = str_replace($path.'/', '', $item);
                    $img_path = str_replace('/', '\\', $img);
                    if(in_array($img, $arrayImg["en"])) {
                        Storage::disk('s3')->delete($img);
                        $removed_items[] = $img;
                    }
                    if(in_array($img_path, $arrayImg["en"])) {
                        Storage::disk('s3')->delete($img_path);
                        $removed_items[] = $img_path;
                    }

                }
                $arrayImg["en"] = array_diff($arrayImg["en"], $removed_items);
            }
        //}

        $obj = new FormLayout;
        $obj->PageLeaveForm_id = $model->id;
        $obj->card_hero = $fullPath_Card;
        $obj->card_cover = $fullPath_bg;
        $obj->heading = $request->header_text_card;
        $obj->sub_heading = $request->sub_header_text_card;
        $obj->features = $arr_features != null ? json_encode($arr_features) : null;
        $obj->setting = $request->setting;

        $obj->save();

        $obj_en = $obj->english();
        $obj_en->heading = $request->header_text_card_en;
        $obj_en->sub_heading = $request->sub_header_text_card;
        $obj_en->features = $arr_features_en != null ? json_encode($arr_features_en) : null;

        return $insert_id = $obj->id;
    }

    /**
     * [Insert Card Hero]
     * @param  Request $request       [request]
     * @param  Pageleaveform  $model         [query]
     * @param  [type]  $fullPath_bg   [path image background]
     * @param  [type]  $fullPath_Card [path image card]
     * @return [type]                 [save id]
     */
    public function updateCardHero(Request $request, $model, $fullPath_bg, $fullPath_Card)
    {
        $features = $request->feature;
        $feature_sub = $request->feature_sub;

        $features_en = $request->feature_en;
        $feature_sub_en = $request->feature_sub_en;

        //old image in db
        $layouts = DB::table('formlayouts')->where('PageLeaveForm_id', $model->id)->first();
        $has_hero = $layouts->card_hero;
        $has_cover = $layouts->card_cover;

        if (!is_null($has_hero) && $request->hasFile('card_image')) {
            Storage::disk('s3')->delete($has_hero);
        }
        if (!is_null($has_cover) && $request->hasFile('bg_image')) {
            Storage::disk('s3')->delete($has_cover);
        }

        $arr_features = null;
        $arr_features_en = null;

        for ($i = 0; $i < sizeof($features); $i++) {
            $arr_features['title'][$i] = $features[$i];
            $arr_features['sub'][$i] = $feature_sub[$i];

            $arr_features_en['title'][$i] = $features_en[$i] ?: "";
            $arr_features_en['sub'][$i] = $feature_sub_en[$i] ?: "";
        }

        if($arr_features != null){
           $arr_features =  json_encode($arr_features);
        }else{
           $arr_features = null;
        }

        if($arr_features_en != null){
           $arr_features_en =  json_encode($arr_features_en);
        }else{
           $arr_features_en = null;
        }

        $updated = array(
            'card_hero' => $fullPath_Card,
            'card_cover' => $fullPath_bg,
            'card_slide' => null,
            'heading' => $request->header_text_card,
            'sub_heading' => $request->sub_header_text_card,
            'features' => $arr_features,
            'setting' => $request->setting,
        );

        FormLayout::where('PageLeaveForm_id', $model->id)
            ->update($updated);

        $form_en = $model->formLayout->english();
        $form_en->heading = $request->header_text_card_en ?: "";
        $form_en->sub_heading = $request->sub_header_text_card_en ?: "";
        $form_en->features = $arr_features_en;
        $form_en->save();
    }

    /**
     * [Insert Card Hero]
     * @param  Request $request       [request]
     * @param  [type]  $model         [query]
     * @param  [type]  $fullPath_Multi   [json path]
     * @return [type]                 [save id]
     */
    public function updateSlideHero(Request $request, $model, $fullPath_Multi, $uploade_more_default ,$uploade_more_en)
    {
        $path = "https://s3-ap-southeast-1.amazonaws.com/ktc-oap";
        $path_folder = 'https://s3-ap-southeast-1.amazonaws.com/ktc-oap/';
        //old image in db
        $layouts = $model->formLayout;
        $has_slide = $layouts->card_slide;

        $arr_has_slide = json_decode($has_slide, true);

        if (!isset($arr_has_slide["default"])) {
            $arrayImg = ["default" => $arr_has_slide, "en" => []];
        } else {
            $arrayImg = $arr_has_slide ?: ["default" => [], "en" => []];
        }

        if (!isset($fullPath_Multi["default"])) {
            $slides_default = $fullPath_Multi;
        } else {
            $slides_default = $fullPath_Multi["default"];
            $slides_en = $fullPath_Multi["en"];
        }

        //move image
        $arrayImgMove = [];
        $array_img_move_en = [];
        if (isset($request->slide_move)) {
            $slide_move = explode(",", $request->slide_move);
            foreach ($slide_move as $index => $img_move) {
                $img_move = str_replace($path . '/', '', $img_move);
                array_push($arrayImgMove, $img_move);
            }
            $arrayImg["default"] = $arrayImgMove;
        }

        if (isset($request->slide_move_en)) {
            $slide_move_en = explode(",", $request->slide_move_en);
            foreach ($slide_move_en as $index => $item) {
                $img = str_replace($path.'/', '', $item);
                $array_img_move_en[] = $img;
            }
            $arrayImg["en"] = $array_img_move_en;
        }


        if (!is_null($arr_has_slide)) {
            //Check remove image
            if (!empty($request->slide_remove)) {
                $slide_remove = explode(",", $request->slide_remove);

                $arrayImgRemove = array();
                $arrayImgRemove_path = array();
                foreach ($slide_remove as $index => $value) {
                    $img = str_replace($path . '/', '', $value);
                    $img_back = str_replace('/', '\\', $img);
                    if (in_array($img_back, $arrayImg["default"])) {
                        Storage::disk('s3')->delete($img_back);
                        array_push($arrayImgRemove, $img_back);
                    }
                    if (in_array($img, $arrayImg["default"])) {
                        Storage::disk('s3')->delete($img);
                        array_push($arrayImgRemove, $img);
                    }

                }
                // var_dump($arrayImgRemove);
                // exit();

                $arrayImg["default"] = array_diff($arrayImg["default"], $arrayImgRemove);
            }

            if(!empty($request->slide_remove_en)) {
                $slide_items = explode(",", $request->slide_remove_en);
                $removed_items = [];
                foreach ($slide_items as $index => $item) {
                    $img = str_replace($path.'/', '', $item);
                    $img_path = str_replace('/', '\\', $img);
                    if(in_array($img_path, $arrayImg["en"])) {
                        Storage::disk('s3')->delete($img_path);
                        $removed_items[] = $img_path;
                    }
                    if(in_array($img, $arrayImg["en"])) {
                        Storage::disk('s3')->delete($img);
                        $removed_items[] = $img;
                    }

                }
                $arrayImg["en"] = array_diff($arrayImg["en"], $removed_items);
            }
        }


        //new image uploade
        if ($uploade_more_default == 'add_default') {
            //default
            if (sizeof($slides_default) > 0) {
                foreach ($slides_default as $item) {
                    $arrayImg["default"][] = $item;
                }
            }
        }

        if ($uploade_more_en == 'add_en') {
            //en
            if (sizeof($slides_en) > 0) {
                foreach ($slides_en as $item) {
                    $arrayImg["en"][] = $item;
                }
            }

        }

        $new_image_slide = $arrayImg;

        if (count($new_image_slide["default"]) == 0 && count($new_image_slide["en"]) == 0) {
            $updated = array(
                'card_hero' => null,
                'card_cover' => null,
                'heading' => $request->header_text,
                'sub_heading' => $request->sub_header_text,
                'features' => null,
                'setting' => null,
            );
        } else {
            $updated = array(
                'card_hero' => null,
                'card_cover' => null,
                'card_slide' => json_encode($new_image_slide),
                'heading' => $request->header_text,
                'sub_heading' => $request->sub_header_text,
                'features' => null,
                'setting' => null,
            );
        }

        FormLayout::where('PageLeaveForm_id', $model->id)
            ->update($updated);

        $form_en = $model->formLayout->english();
        $form_en->heading = $request->header_text_en ?: "";
        $form_en->sub_heading = $request->sub_header_text_en ?: "";
        $form_en->save();
    }

    /**
     * [Check Dup Title]
     * @param  Request $request [request]
     * @return [type]           [number]
     */
    public function checkDupTitle(Request $request)
    {

        $obj = new Pageleaveform();

        $count = $obj::where('title', trim($request['title']))
            ->where('id', '!=', trim($request['form_id']))
            ->count();

        return $count;
    }

    /**
     * [Check Dup Slug ]
     * @param  Request $request [slug]
     * @return [type]           [number]
     */
    public function checkDupSlug(Request $request)
    {

        $obj = new Pageleaveform();

        $count = $obj::where('slug', trim($request['slug']))
            ->where('id', '!=', trim($request['form_id']))
            ->count();

        return $count;
    }

    public function deleteLayout(Request $request)
    {
        $request->id;

        $arrayName = array('url' => $request->id);

        echo json_encode($arrayName);
    }

    public function updatePage(Request $request, $id, $url_slug)
    {
        try {
            //Update Pages
            $form = Pageleaveform::find($id);
            $form->title = $request->title;
            $form->title_page = $request->title_page;
            $form->slug = $url_slug;
            $form->description = $request->description;
            $form->content = $request->input("content");
            $form->layout = $request->layout;
            $form->LeaveForm_id = $request->form;
            $form->Product_id = $request->product;
            $form->branch_id = $request->branch_code;
            $form->public = $request->public == 'on' ? 0 : 1;
            $form->custom_param = json_encode(array(
                "indicator" => $request->indicator,
                "sub_indicator" => $request->sub_indicator,
                "inbound" => $request->inbound,
                "outbound" => $request->outbound,
                "callback" => $request->callback,
            ));
            $form->modified_by = auth()->user()->id;
            $form->updated_at = now();
            $form->save();

            $this->saveEnglishFields($request, $form);

        } catch (Exception $e) {
            $form = $e;
        }

        return $form;
    }

    public function saveEnglishFields(Request $request, Pageleaveform $model) {
        $model_en = $model->english();
        $model_en->title_page = $request->title_page_en ?? "";
        $model_en->description = $request->description_en ?? "";
        $model_en->content = $request->content_en ?? "";
        $model_en->save();
    }
}