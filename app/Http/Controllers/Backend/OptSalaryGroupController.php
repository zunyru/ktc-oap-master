<?php
/**
 * Created by PhpStorm.
 * User: korst
 * Date: 11/28/2018
 * Time: 3:13 AM
 */

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\OptSalaryGroup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;

class OptSalaryGroupController extends VoyagerBaseController
{
    public function index(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        if($request->has('status') && $request->query('status') == 'trash' || $request->segment(count($request->segments()))=='trash'):
            $groups = OptSalaryGroup::with('options')->onlyTrashed()->get();
            $browse_state="trash";
        else:
           $groups = OptSalaryGroup::with('options')->withTrashed()->whereNull('deleted_at')->get();
           $browse_state="all";
        endif;

        $count_state=DB::select("SELECT (SELECT count(id) FROM opt_salary_groups) AS 'all', (SELECT count(id) FROM opt_salary_groups WHERE deleted_at IS NOT NULL) AS 'trashed'")[0];

        $data['groups'] = $groups;
        $data['dataType'] = $dataType;
        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;

        return view('backend.opt-salary-group-browse', $data);
    }

    public function create(Request $request) {
        $dataType = $this->getCurrentDataType($request);

        //check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0 ? new OptSalaryGroup() : null);

        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.opt-salary-group-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request)
    {
        $dataType = $this->getCurrentDataType($request);

        $this->authorize('add', app($dataType->model_name));

        $valid = $this->validateFields($request->all());

        if($valid !== true) {
            return redirect()->back()->with([
                'message' => __('voyager::generic.update_failed')." ".$valid["message"],
                'alert-type' => 'error'
            ]);
        }

        $this->save($request);

        return redirect()->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                'alert-type' => 'success'
            ]);
    }

    public function edit(Request $request, $id)
    {
        $dataType = $this->getCurrentDataType($request);
        $dataTypeContent = OptSalaryGroup::findOrFail($id);
        $this->authorize('edit', app($dataType->model_name));
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.opt-salary-group-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function update(Request $request, $id)
    {
        $dataType = $this->getCurrentDataType($request);
        $valid = $this->validateFields($request->all(), true, $id);

        if($valid !== true) {
            return redirect()->back()->with([
                'message' => __('voyager::generic.update_failed')." ".$valid["message"],
                'alert-type' => 'error'
            ]);
        }

        $item = OptSalaryGroup::findOrFail($id);
        $this->save($request, $item);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function destroy(Request $request, $id)
    {
        $dataType = $this->getCurrentDataType($request);

        $this->authorize('delete', app($dataType->model_name));

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }

        foreach (OptSalaryGroup::whereIn('id', $ids)->get() as $group) {
            $group->options()->detach();
        }
        
        OptSalaryGroup::destroy($ids);

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function restore(Request $request, $id) {
        $dataType = $this->getCurrentDataType($request);

        if(empty($id)) {
            //bulk delete
            $ids = explode(',', $request->ids);
        } else {
            //single item
            $ids[] = $id;
        }
        
        foreach ($ids as $id) {  
            $option = OptSalaryGroup::withTrashed()->find($id);
            $option->restore();
        }

        return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);
    }

    public function checkExisted(Request $request) {
        $field = $request->field;
        $value = $request->value;
        $id = $request->data_id;
        $count = OptSalaryGroup::where($field, $value)
            ->where('id', '!=', $id)
            ->count();

        return $count;
    }

    protected function validateFields($input, $isUpdate = false, $id = 0) {
        $validator = Validator::make($input, [
            'title' => 'required',
            'options' => 'required|min:1'
        ]);

        $query = OptSalaryGroup::where("title", $input["title"]);

        if($isUpdate) {
            $query = $query->where('id', '!=', $id);
        }

        $count = $query->count();

        if($count > 0) {
            return ["valid" => false, "message" => "Group name is already existed"];
        }

        if($validator->fails()) {
            return ["valid" => false, "message" => $validator->errors()->all()];
        }

        return true;
    }

    protected function save(Request $request, $item = null) {
        $options = $request->options;
        if(!isset($item)) {
            $item = new OptSalaryGroup();
            $item->title = $request->title;
            $item->save();
        }

        $item->title = $request->title;
        $item->options()->sync($options);
        $item->save();
    }
}