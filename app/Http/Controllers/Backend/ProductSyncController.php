<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 19/11/18
 * Time: 7:22 AM
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Product;
use App\ProductCategory;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProductSyncController extends Controller
{
    protected $command;

    protected $productDataUrl = "http://172.25.107.198:7004/ktcwebsite-api/ktcGetMasterData/getCreditCardList";
    protected $loanDataUrl = "http://172.25.107.198:7004/ktcwebsite-api/ktcGetMasterData/getProductList";
    protected $workingUser;

    public function __construct(Command $command = null)
    {
        $this->command = $command;
    }

    public function sync() {
        $this->workingUser = User::where('email', 'admin@ktc.co.th')->first();

        Log::channel('appdebug')->info('--Start syncing data => ProductSyncController::sync()--');
        // sync here
        Log::channel('appdebug')->debug('load product from url: '.$this->productDataUrl);
        $data = $this->load($this->productDataUrl);
        Log::channel('appdebug')->debug('load loan data from url: '.$this->loanDataUrl);
        $data2 = $this->load($this->loanDataUrl);

        if($data['error']) {
            Log::channel('appdebug')->error('sync product failed: '.$data['data']);
            if(isset($this->command)) {
                $this->command->error('Sync product data failed with cURL error: '.$data['data']);
            }

            $error['success'] = false;
            $error['data']['products'] = ['message' => $data['data']];
        }

        if($data2['error']) {
            Log::channel('appdebug')->error('sync product failed: '.$data2['data']);
            if(isset($this->command)) {
                $this->command->error('Sync product data failed with cURL error: '.$data2['data']);
            }

            $error['success'] = false;
            $error['data']['other_categories'] = ['message' => $data2['data']];
        }

        if(isset($error)) {
            return response()->json($error);
        }

        Log::channel('appdebug')->debug('sync completed');
        $s3pathCon=\Config::get('filesystems.disks');
        $s3path=$s3pathCon['s3']['url'];
        // $this->command->comment($s3path);
        $this->storeProducts($data['data'],$s3path);
        $this->storeOtherCategory($data2['data'],$s3path);

        $combined = [
            'success' => true,
            'data' => [
                'products' => json_decode($data['data'], true),
                'other_categories' => json_decode($data2['data'], true),
            ]
        ];

        Log::channel('appdebug')->info('--Finish syncing data => ProductSyncController::sync()--');
        // save data
        return response()->json($combined);
    }

    protected function storeProducts($json_str,$s3path) {
        $data = json_decode($json_str);
        $products = $data->creditcardList;

        $count = count($products);

        $category = ProductCategory::where('category_code', 'CC')->first();

        foreach ($products as $index => $product) {
            $record = Product::where('sys_product_id', $product->card_id)->orWhere('sys_product_key_id', $product->id)->first();
            if(!isset($record)) {
                $record = new Product();
                $record->sys_product_id = $product->card_id;
                $record->sys_product_key_id = $product->id;
                $record->active = 0;
                $record->description = $product->card_name
                    .' ('.strtolower($product->card_type).' / '.strtolower($product->card_lifestyle).')';
                $record->create_by = $this->workingUser->id;
            }

            $duplicated_record_count = Product::where('title', 'like', $product->card_name.'%')->count();

            $record->title = $duplicated_record_count == 0 ? $product->card_name : $product->card_name;
            $record->sys_product_id = $product->card_id;
            $record->product_category_id = $category->id; // credit card category
            $filePath = $this->storeImageS3($record->sys_product_key_id, $product->image);
            $record->pic = str_replace($s3path,"",$filePath);

            $record->modified_by = $this->workingUser->id;
            $record->save();
            $this->command->comment('synced product [sys_product_id => '.$product->card_id.', sys_product_key_id => '.$product->id.']');
            Log::channel('appdebug')->debug('product updated '.($index + 1).' of '.$count);
        }

        Log::channel('appdebug')->debug('product sync completed');
    }

    protected function storeOtherCategory($json_str,$s3path) {
        $data = json_decode($json_str);

        $cat_proud = ProductCategory::where('category_code', 'PL')->first();
        $cat_cash = ProductCategory::where('category_code', 'FIXED')->first();

        $proudProduct = $data->proud_product;
        $cashProduct = $data->cash_product;

        $workingUser = User::where('email', 'admin@ktc.co.th')->first();

        // store/update KTC Proud product
        $proudRecord = Product::where('sys_product_id', $proudProduct->product_id)->orWhere('sys_product_key_id', $proudProduct->id)->first();
        if(!isset($proudRecord)) {
            $proudRecord = new Product();
            $proudRecord->sys_product_id = $proudProduct->product_id;
            $proudRecord->sys_product_key_id = $proudProduct->id;
            $proudRecord->description = $proudProduct->product_name;
            $proudRecord->create_by = $this->workingUser->id;
            $proudRecord->active = 0;
        }

        $proudRecord->product_category_id = $cat_proud->id;
        $proudRecord->title = $proudProduct->product_name;
        $proudRecord->sys_product_id = $proudProduct->product_id;
        $proudRecord->pic = str_replace($s3path,"",$this->storeImageS3($proudRecord->sys_product_key_id, $proudProduct->image));
        $proudRecord->modified_by = $this->workingUser->id;
        $proudRecord->save();
        $this->command->comment('synced proud [sys_product_id => '.$proudProduct->product_id.', sys_product_key_id => '.$proudProduct->id.']');
        Log::channel('appdebug')->debug('KTC Proud product updated');

        // store/update KTC Cash product
        $cashRecord = Product::where('sys_product_id', $cashProduct->product_id)->orWhere('sys_product_key_id', $cashProduct->id)->first();
        if(!isset($cashRecord)) {
            $cashRecord = new Product();
            $cashRecord->sys_product_id = $cashProduct->product_id;
            $cashRecord->sys_product_key_id = $cashProduct->id;
            $cashRecord->description = $cashProduct->product_name;
            $cashRecord->create_by = $this->workingUser->id;
            $cashRecord->active = 0;
        }

        $cashRecord->product_category_id = $cat_cash->id;
        $cashRecord->sys_product_id = $cashProduct->product_id;
        $cashRecord->title = $cashProduct->product_name;
        $cashRecord->pic = str_replace($s3path,"",$this->storeImageS3($cashRecord->sys_product_key_id, $cashProduct->image));
        $cashRecord->modified_by = $this->workingUser->id;
        $cashRecord->save();
        $this->command->comment('synced cash [sys_product_id => '.$cashProduct->product_id.', sys_product_key_id => '.$cashProduct->id.']');
        Log::channel('appdebug')->debug('KTC Cash product updated');
    }

    protected function load($url) {
        $error = false;
        $ch = curl_init();
        $timeout = 30;
        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        $data = curl_exec($ch);

        if (curl_errno($ch)) {
            $data = curl_error($ch);
        }

        curl_close($ch);

        Log::channel('appdebug')->debug('[cURL] loaded data => '.$url);
        return ['error' => $error, 'data' => $data];
    }

    protected function storeImage($product_id, $url) {
        $url=str_replace("uvln-appktcmg01:7004","172.25.107.198:7004",$url);
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        $path = 'products/remote_images';
        $file_name = $product_id.'.'.$ext;
        $localpath = storage_path('app/public/'.$path.'/'.$file_name);

        $loop_path = '';
        $path_components = explode('/', 'app/public/'.$path);
        foreach ($path_components as $component) {
            $loop_path .= $component.'/';
            if(!file_exists(storage_path($loop_path))) {
                mkdir(storage_path($loop_path));
            }
        }

        if(!file_exists($localpath)) {
            touch($localpath);
        }

        $ch = curl_init($url);
        $fp = fopen($localpath, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, -1);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        Log::channel('appdebug')->debug('[cURL] saved image => '.storage_path($path));

        return $path.'/'.$file_name;
    }

    protected function storeImageS3($product_id, $url) {
        $url = str_replace("uvln-appktcmg01:7004", "172.25.107.198:7004", $url);
        $contents = file_get_contents($url);
        $ext = pathinfo($url, PATHINFO_EXTENSION);
        // $image_contents = Storage::get($url);

        // save file to S3
        $path = 'products/remote_images/'.$product_id.'.'.$ext;
        Storage::disk('s3')->put($path,$contents);

        return Storage::disk('s3')->url($path);
    }
}