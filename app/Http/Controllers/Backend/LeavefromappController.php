<?php

namespace App\Http\Controllers\Backend;

use App\Branch;
use App\Exports\LeadsExport;
use App\Exports\LeadsWithLIDExport;
use App\Http\Controllers\Controller;
use App\Pageleaveform;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Leavefromapp;
use Maatwebsite\Excel\Excel;
use TCG\Voyager\Models\DataType;
use Voyager;
use Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Spatie\Activitylog\Contracts\Activity;

class LeavefromappController extends Controller
{
    use BreadRelationshipParser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $export = Voyager::can('export_leavefromapps');

        $query = $this->filterLeads($request);
        $objs_forms = $query->get();


        $objs_forms = $query->paginate(100);

        $page = $request->input('page') ?:1;

        $num = $query->count();

        if ($page) {
            $start_of_page = 100 * ($page) - 99;
            $total_of_page = 100 * ($page);
            if(floor($num/100) == $page-1){
             $total_of_page = $num;
            }

        } else {
            $start_of_page = 100 * ($page) / 100;
            $total_of_page = 100 * ($page);
        }

        //Get product
        $forms = Leavefromapp::all();

        $products = Product::whereIn('id', $forms->pluck('Product_id')->unique()->all())->get();
//        $products = DB::select("SELECT id,title FROM products WHERE products.id IN (SELECT Product_id FROM leavefromapps)");

        //Get branches
        $branches = Branch::select('id','code')->whereIn('code', $forms->pluck('branch_code')->unique()->all())->get();
//        $branches = DB::select("SELECT id,code FROM branches WHERE branches.code IN (SELECT branch_code FROM leavefromapps)");

        //Get pages
        $pages = Pageleaveform::select('id','title')->whereIn('id', $forms->pluck('PageLeaveForm_id')->unique()->all())->get();
//        $pages = DB::select("SELECT id,title FROM pageleaveforms WHERE id IN (SELECT PageLeaveForm_id FROM leavefromapps)");

        //Get indicators
        $indicators = $forms;
//        $indicators = DB::select("SELECT id,custom_param FROM leavefromapps");

        //Get sub indicators
        $sub_indicators = $forms;
//        $sub_indicators = DB::select("SELECT id,custom_param FROM leavefromapps");

        $sub_indicator_values = [];
        $indicator_values = [];
        foreach($sub_indicators as $index=> $item){
            $custom_param  = json_decode($item->custom_param);
            if(!is_null($custom_param) && !is_null($custom_param->sub_indicator)){
               $sub_indicator_values[] = $custom_param->sub_indicator;
           }
       }
       foreach($sub_indicators as $index=> $item){
        $custom_param  = json_decode($item->custom_param);
           if(!is_null($custom_param) && !is_null($custom_param->indicator)){
                 $indicator_values[] = $custom_param->indicator;
           }
     }

     $sub_indicator_values = array_unique($sub_indicator_values);
     $indicator_values     = array_unique($indicator_values);


     $data['objs_forms']      =  $objs_forms;
     $data['dataType']        =  $dataType;
     $data['products']        =  $products;
     $data['branches']        =  $branches;
     $data['pages']           =  $pages;
     $data['indicators']      =  $indicators;
     $data['sub_indicators']  =  $sub_indicators;
     $data['indicators_distinct']  =  $indicator_values;
     $data['sub_indicator_distinct']  =  $sub_indicator_values;
     //$data['paginator'] = $paginator;
     $data['start_of_page'] = $start_of_page;
     $data['total_of_page'] = $total_of_page;
     $data['export']              = $export;

        // Filter date options
     $filterDateSettings = DB::table('settings')
     ->where('key', 'leads.duplicate_filter_time_unit')
     ->first();
     $filterDateDetails = json_decode($filterDateSettings->details, true);
     $filterOptions = $filterDateDetails["options"];

     $data['filter_date_options'] = $filterOptions;
     $data['filter_date_number'] = setting('leads.duplicate_filter_time_number');
     $data['filter_date_unit'] = setting('leads.duplicate_filter_time_unit');
     $data['query'] = $request->getQueryString();


     return view('backend.leavefromapp-browse', $data);
 }

    /**
     * @param Request $request
     * @return Leavefromapp|\Illuminate\Database\Eloquent\Builder
     */
    protected function filterLeads(Request $request, $order_direction = 'DESC') {
       $status = 0;
       if($request->has('status') && $request->query('status') == 'trash') {
           $status = 1;
       }

       $query = Leavefromapp::with('product')->where('leavefromapps.active', $status);

       if($request->fname !== NULL && !empty($request->fname))
           $query->where('fname', 'LIKE', '%' . $request->fname . '%');

       if($request->lname !== NULL && !empty($request->lname))
           $query->where('lname', 'LIKE', '%' . $request->lname . '%');

       if($request->product !== NULL && !empty($request->product))
           $query->whereIn('Product_id' , $request->product);

       if($request->branche !== NULL && !empty($request->branche))
           $query->whereIn('branch_code', $request->branche);

       if($request->pages !== NULL && !empty($request->pages))
           $query->whereIn('PageLeaveForm_id', $request->pages);

       if($request->indicator !== NULL && !empty($request->indicator))
           $query->whereIn('custom_param->indicator', $request->indicator);

       if($request->sub_indicator !== NULL && !empty($request->sub_indicator))
           $query->whereIn('custom_param->sub_indicator', $request->sub_indicator);

       if($request->filled('phone'))
           $query->where('tel', 'LIKE', '%' . $request->phone . '%');

       if($request->filled('email'))
           $query->where('email', 'LIKE', '%' . $request->email . '%');

       if($request->daterange !== NULL && !empty($request->daterange)){

           /*27/11/2018 - 28/11/2018*/
           $dates   = explode(" - ",$request->daterange);
           $start_d = explode("/", $dates[0]);
           $end_d   = explode("/", $dates[1]);

           $dates_start = $start_d[2]."-".$start_d[1]."-".$start_d[0];
           $dates_end = $end_d[2]."-".$end_d[1]."-".$end_d[0];

           $start = Carbon::parse($dates_start)->startOfDay();
           $end = Carbon::parse($dates_end)->endOfDay();

           if($start == $end){
               $query->whereDate('leavefromapps.created_at', $start);
           }else{

               $query->whereBetween('leavefromapps.created_at', [$start, $end]);
           }
       }else{
            //Year this
             $query->whereYear('leavefromapps.created_at', date('Y'));
       }

       $query->orderByRaw('leavefromapps.created_at '.$order_direction);


            if($request->filled('row_limited')){
                $query->take($request->row_limited);
            }else{
                $query->take(100);
            }


    return $query;
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //Get code VoyagerBaseController
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $dataTypeContent = call_user_func([$model->with($relationships), 'findOrFail'], $id);
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)->where('id', $id)->first();
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check permission
        $this->authorize('read', $dataTypeContent);


        $objs_forms = DB::table('leavefromapps')->where('active' , 0)->get();

        $data['objs_forms']      =  $objs_forms;
        $data['dataType']        =  $dataType;
        $data['dataTypeContent']=  $dataTypeContent;


        return view('backend.leave-show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? app($dataType->model_name)->with($relationships)->findOrFail($id)
        : DB::table($dataType->name)->where('id', $id)->first();

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        $salaries = DB::table('opt_salaries')->where('active' , 0)->get();

        //Get product
        $products = DB::table('products')->where('active' , 0)->get();

        //Get Page submit
        $pageleaveforms = DB::table('pageleaveforms')->where('active' , 0)->get();

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        return view('backend.leavefromapp-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable','salaries','products','pageleaveforms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // set validation
        $validator = Validator::make($request->all(),
            [
             'fname' => 'required',
             'lname' => 'required',
             'phone' => 'required',
             'salary'=> 'required',
             'product' => 'required',
         ]);
        //set nicename to display
        $niceNames = array(
         'fname' => 'First name ',
         'lname' => 'Last name ',
         'phone' => 'Phone',
         'salary'=> 'Salary',
         'product' => 'Product',
         'email' => 'Email',
     );

        $validator->setAttributeNames($niceNames);

        $cf1_arr = NULL;
        if(isset($request->cf_key_1)){
            $cf1_arr['key_name'] = $request->cf_key_1;
            $cf1_arr['value'] = $request->cf_1;
            $cf1_arr = json_encode($cf1_arr);
        }
        $cf2_arr = NULL;
        if(isset($request->cf_key_2)){
            $cf2_arr['key_name'] = $request->cf_key_2;
            $cf2_arr['value'] = $request->cf_2;
            $cf2_arr = json_encode($cf2_arr);
        }

        $form = Leavefromapp::find($id);

        $form->fname                = $request->fname;
        $form->lname                = $request->lname;
        $form->tel                  = str_replace(' ', '', $request->phone);
        $form->cf_1                 = $cf1_arr;
        $form->cf_2                 = $cf2_arr;
        $form->opt_Salary_id        = $request->salary;
        $form->Product_id           = $request->product;
        $form->email                = $request->email;
        $form->custom_param = json_encode(array(
            "indicator"=>$request->indicator,
            "sub_indicator"=>$request->sub_indicator,
            "inbound"=>$request->inbound,
            "outbound"=>$request->outbound,
            "callback"=>$request->callback,
            "ref_url"=>$request->ref_url
        ));
        $form->modified_by          = auth()->user()->id;

        $form->save();

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $Vg   =  new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Leavefromapp::find($id);
            $form->active                = 1;
            $form->modified_by          = auth()->user()->id;

            $form->save();
        }

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'    => __('voyager::generic.successfully_deleted')." {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }

    public function restore(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        //$slug = $Vg->getSlug($request);
        $slug = "leads";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Leavefromapp::find($id);
            $form->active = 0;
            $form->modified_by = auth()->user()->id;

            $form->save();
        }

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message' => 'Successfully  Restore' . " {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }

    public function export(Request $request) {

        $time = Carbon::now("Asia/Bangkok");
        $file_name = "oap-leads-export-".$time->format('Y-m-d');
        $query = $this->filterLeads($request);

        $filterDate = new Carbon(setting('leads.duplicate_filter_time_number').' '.setting('leads.duplicate_filter_time_unit').' ago');

        $query->selectRaw('*,leavefromapps.created_at AS lead_created_at, (select COUNT(*) from leavefromapps L where L.created_at >= ? AND L.id < `id` AND L.email = `email` AND L.tel = `tel` ORDER BY id) AS \'duplicated\'',
            [
                $filterDate->format('Y-m-d')
            ],'leavefromapps.*','opt_salaries.opt_value','pageleaveforms.slug','products.title','products.sys_product_id');
        $query->join('opt_salaries', 'opt_salaries.id', '=', 'leavefromapps.opt_Salary_id');
        $query->join('pageleaveforms', 'pageleaveforms.id', '=', 'leavefromapps.PageLeaveForm_id');
        $query->join('products', 'products.id', '=', 'leavefromapps.Product_id');
        $query->having('duplicated', '<', 1);

       activity('Leads Export')
           ->withProperties(['attributes'=> [ 'user' => auth()->user()->id , 'time' => Carbon::now()->toDateTimeString()]])
           ->log('exported');

       return (new LeadsExport($query))->download($file_name.'.csv', Excel::CSV);
    }

    public function exportWithLID(Request $request) {
        $time = Carbon::now("Asia/Bangkok");
        $file_name = "oap-leads-telesales-export_".$time->format('Y-m-d');
        $query = $this->filterLeads($request, 'ASC');

        $filterDate = new Carbon(setting('leads.duplicate_filter_time_number').' '.setting('leads.duplicate_filter_time_unit').' ago');

        $query->selectRaw('*,leavefromapps.created_at AS lead_created_at, (select COUNT(*) from leavefromapps L where L.created_at >= ? AND L.id < `id` AND L.email = `email` AND L.tel = `tel` ORDER BY id) AS \'duplicated\'',
            [
                $filterDate->format('Y-m-d')
            ]);
        $query->having('duplicated', '<', 1);

         activity('Leads Export (Tele-sale)')
           ->causedBy(auth()->user()->id)
           ->withProperties(['attributes'=> [ 'user' => auth()->user()->id , 'time' => Carbon::now()->toDateTimeString()]])
           ->log('exported');

        return (new LeadsWithLIDExport($query))->download($file_name.'.csv', Excel::CSV);
    }

    public function changeDuplicateFilterDate(Request $request) {
        $voyagerCtrl = new VoyagerBaseController();
        $slug = $voyagerCtrl->getSlug($request);
        $dataType = DataType::where('slug', $slug)->first();

        $number = $request->number;
        $unit = $request->unit;

        DB::table('settings')->where('key', '=', 'leads.duplicate_filter_time_number')->update(['value' => "$number"]);
        activity('Lead Duplication Filter')
           ->causedBy(auth()->user()->id)
           ->withProperties(['attributes'=> [ 'key' => 'leads.duplicate_filter_time_number', 'value' => "$number" , 'unit' => "$unit"]])
           ->log('updated');


        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message'       => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
            'alert-type'    => 'success',
        ]);
    }
}