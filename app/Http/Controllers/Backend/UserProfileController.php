<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2018-12-29
 * Time: 02:45
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Voyager\VoyagerBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use TCG\Voyager\Facades\Voyager;

class UserProfileController extends VoyagerBaseController
{
    public function editProfile(Request $request) {
        $dataType = Voyager::model('DataType')->where('slug', 'users')->first();
        $user = auth()->user();

        return view('backend.profile-edit', compact('dataType', 'user'));
    }

    public function updateProfile(Request $request) {
        $user = auth()->user();

        if ($request->input('name', $user->name) != $user->name) {
            $user->name = $request->input('name');
        }

        if ($request->input('email', $user->email) != $user->email) {
            $user->email = $request->input('email');
        }

        $password = $request->input('password', '');
        if (!empty($password)) {
            $user->password = Hash::make($password);
        }

        $raw_file = $request->file('avatar');
        if (isset($raw_file)) {
            $uploader = new UploaderController();
            $file = $uploader->SingleImage_handel($raw_file, 'users');
            $user->avatar = $file;
        }

        $user->save();

        return redirect(route('voyager.profile'));
    }
}