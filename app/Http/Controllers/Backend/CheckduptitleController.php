<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Leaveform;
use App\Pageleaveform;

class CheckduptitleController extends Controller {
	public function checkDupTitle(Request $request) {

		if ($request['_db'] === 'Leaveform') {
			$obj = new Leaveform();
		} elseif ($request['_db'] === 'Pageleaveform') {
			$obj = new Pageleaveform();
		} elseif ($request['_db'] === 'Product') {
			$obj = new Pageleaveform();
		}

		$count = $obj::where('title', trim($request['title']))
			->where('id', '!=', $request['form_id'])
			->where('active', 0)
			->count();

		return $count;

	}

	public function checkDupSlug(Request $request) {

		$obj = new Pageleaveform();

		$count = $obj::where('slug', trim($request['slug']))
			->where('id', '!=', $request['form_id'])
			->where('active', 0)
			->count();

		return $count;

	}

	public function checkDupTitlePage(Request $request) {

		$obj = new Pageleaveform();

		$count = $obj::where('title_page', trim($request['title_page']))
			->where('id', '!=', $request['form_id'])
			->where('active', 0)
			->count();

		return $count;

	}
}
