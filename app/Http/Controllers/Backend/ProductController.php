<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Product;
use BreadDataAdded;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Validator;
use Voyager;

class ProductController extends Controller
{
    use BreadRelationshipParser;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $status = 0;
        $browse_state="active";
        if($request->has('status') && $request->query('status') == 'trash' || $request->segment(count($request->segments()))=='trash'){
            $status = 1;$browse_state="trash";
        }

        $objs_product = DB::table('products')->where('active', $status)->get();
        $count_state=DB::select("SELECT (SELECT count(id) FROM {$dataType->name}) AS 'all', (SELECT count(id) FROM {$dataType->name} WHERE active=1) AS 'trashed'")[0];

        $data['objs_forms'] = $objs_product;
        $data['dataType'] = $dataType;
        $data['count_state']=$count_state;
        $data['browse_state'] = $browse_state;


        return view('backend.product-browse', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? new $dataType->model_name()
        : false;

        foreach ($dataType->addRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);


        return view('backend.product-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // set validation
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'description' => 'required',
                'reference' => 'required',
                'product_category' => 'required'
            ]);
        //set nicename to display
        $niceNames = array(
            'title' => 'Title',
            'description' => 'Description',
            'reference' => 'reference',
            'product_category' => 'Product Category'
        );

        $validator->setAttributeNames($niceNames);

        //Check Dup Title
        if (self::checkDupTitle($request) > 0) {
            return redirect()
            ->back()
            ->with([
                'message' => "Save Failed. This title already exists",
                'alert-type' => 'error',
            ]);
        }

        if (!$request->has('_validate')) {

            //UploaderController
            $uploader = new UploaderController;

            $ex_d = explode("/", $request->expired_date);


            //insert pages
            $form = new Product;
            $form->title = $request->title;
            $form->description = $request->description;
            if ($request->hasFile('product_image')) {
                //Image Single
                $fullPath_Product = $uploader->SingleImage_handel($request->product_image, $slug);
                $form->pic = $fullPath_Product;
            }
            $form->sys_product_id = $request->reference;
            $form->product_category_id = $request->product_category;
            $form->active = 0;
            $form->create_by = auth()->user()->id;
            $form->modified_by = auth()->user()->id;
            $form->updated_at= now();

            //updte code Visa
            $form->offer_description     = $request->offer_description;
            $form->offer_privilege       = $request->offer_privilege;
            $form->offer_contact         = $request->offer_contact;
            $form->offer_mes_thank       = $request->offer_mes_thank;
            $form->offer_mes_duplicate   = $request->offer_mes_duplicate;
            $form->offer_mes_thank_contact   = $request->offer_mes_thank_contact;
            $form->offer_mes_dup_contact     = $request->offer_mes_dup_contact;
            $form->expired_date       = $ex_d[2]."-".$ex_d[1]."-".$ex_d[0];
            $form->redirect_url       = $request->redirect_url;

            $form->save();

            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message' => __('voyager::generic.successfully_added_new') . " {$dataType->display_name_singular}",
                'alert-type' => 'success',
            ]);

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
        ? app($dataType->model_name)->with($relationships)->findOrFail($id)
        : DB::table($dataType->name)->where('id', $id)->first();

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        return view('backend.product-edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // set validation
        $validator = Validator::make($request->all(),
            [
                'title' => 'required',
                'description' => 'required',
                'reference' => 'required',
                'product_category' => 'required',
            ]);
        //set nicename to display
        $niceNames = array(
            'title' => 'Title',
            'description' => 'Description',
            'reference' => 'reference',
            'product_category' => 'Product Category'
        );

        $validator->setAttributeNames($niceNames);

        if (self::checkDupTitle($request) > 0) {
            return redirect()
            ->back()
            ->with([
                'message' => __('voyager::generic.update_failed') . ". This title already exists",
                'alert-type' => 'error',
            ]);
        }

        // go out if have any errors
        if ($validator->fails()) {
            //put default errors array messages to our noti function
            $messenges = $validator->errors()->all();
            $mssg_list = array_map(function ($item) {
                return ['error', $item];
            }, $messenges);
            return redirect()
            ->back()
            ->with([
                'message' => __('voyager::generic.update_failed') . " {$dataType->display_name_singular}",
                'alert-type' => 'error',
            ]);
        }

        //UploaderController
        $uploader = new UploaderController;

        $ex_d = explode("/", $request->expired_date);

        $form = Product::find($id);

        $form->title = $request->title;
        $form->description = $request->description;
        if ($request->hasFile('product_image')) {

            //Remove file Storage
            $_filepath = DB::table($dataType->name)->where('id', $id)->first();
            $fileFolder = str_replace('\\', '/', $_filepath->pic);
            $config_ = \Config::get('voyager.storage');
            //Storage::delete($config_['disk'].'/'.$fileFolder);

            //Image Single
            $fullPath_Product = $uploader->SingleImage_handel($request->product_image, $slug);
            $form->pic = $fullPath_Product;
        }
        $form->sys_product_id = $request->reference;
        $form->product_category_id = $request->product_category;
        $form->modified_by = auth()->user()->id;
        $form->updated_at= now();

        //updte code Visa
        $form->offer_description     = $request->offer_description;
        $form->offer_privilege       = $request->offer_privilege;
        $form->offer_contact         = $request->offer_contact;
        $form->offer_mes_thank       = $request->offer_mes_thank;
        $form->offer_mes_duplicate   = $request->offer_mes_duplicate;
        $form->offer_mes_thank_contact   = $request->offer_mes_thank_contact;
        $form->offer_mes_dup_contact     = $request->offer_mes_dup_contact;
        $form->expired_date       = $ex_d[2]."-".$ex_d[1]."-".$ex_d[0];
        $form->redirect_url       = $request->redirect_url;

        $form->save();

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message' => __('voyager::generic.successfully_updated') . " {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        $slug = $Vg->getSlug($request);


        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Product::find($id);
            $form->active = 1;
            $form->modified_by = auth()->user()->id;

            $form->save();
        }

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message' => __('voyager::generic.successfully_deleted') . " {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }

    public function restore(Request $request, $id)
    {
        $Vg = new VoyagerBaseController;
        //$slug = $Vg->getSlug($request);
        $slug = "products";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        foreach ($ids as $id) {

            $form = Product::find($id);
            $form->active = 0;
            $form->modified_by = auth()->user()->id;

            $form->save();
        }

        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
            'message' => 'Successfully  Restore' . " {$dataType->display_name_singular}",
            'alert-type' => 'success',
        ]);
    }

    /**
     * [Check Dup Title]
     * @param  Request $request [request]
     * @return [type]           [number]
     */
    public function checkDupTitle(Request $request)
    {

        $obj = new Product();

        $count = $obj::where('title', trim($request['title']))
        ->where('id', '!=', trim($request['form_id']))
        ->count();

        return $count;
    }
}