<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnionpayReportLog extends Model
{
    protected $table = 'log_unionpay_exports';
	public $timestamps = false;
}
