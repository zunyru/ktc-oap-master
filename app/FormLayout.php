<?php

namespace App;

use App\Models\ModelTranslatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;


class FormLayout extends Model
{
    use ModelTranslatable,LogsActivity;
    protected $table = 'formlayouts';
    protected $translatable = ['card_hero', 'card_cover', 'card_slide', 'heading', 'sub_heading', 'features'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Form Layout';
    
}
