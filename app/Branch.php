<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;


class Branch extends Model
{
    use SoftDeletes,LogsActivity;
    protected $dates = ['deleted_at'];
    public $timestamps = false;

    protected static $logAttributes = ['*'];
    protected static $logName = 'Branch';
}
