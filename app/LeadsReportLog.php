<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadsReportLog extends Model
{
	protected $table = 'log_exports';
	public $timestamps = false;
}
