<?php
/**
 * Created by PhpStorm.
 * User: korst
 * Date: 11/26/2018
 * Time: 10:04 PM
 */

namespace App\Models;


use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;

trait ModelTranslatable
{
    use Translatable;

    public function isTranslatable() {
        return Voyager::translatable($this);
    }

    public function english() {
        return $this->translate('en', false);
    }

    public function lang() {
        return $this->translate(session('local'), false);
    }
}