<?php

namespace App\Events;

use App\Leavefromapp;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AppliedCard
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $leaveformapp;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Leavefromapp $leaveformapp)
    {
        //
        $this->leaveformapp = $leaveformapp;

        //dd($leaveformapp);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
