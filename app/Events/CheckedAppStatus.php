<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CheckedAppStatus
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $citizen_code, $log_type;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($citizen_code, $log_type)
    {
        //

        $new_code = str_replace(' ', '', $citizen_code);
        $this->citizen_code = $new_code;
        $this->log_type = $log_type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
