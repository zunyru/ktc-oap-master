<?php
/**
 * Created by PhpStorm.
 * User: korst
 * Date: 11/28/2018
 * Time: 3:13 AM
 */

namespace App;


use App\Models\ModelTranslatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class OptSalaryGroup extends Model
{
    use SoftDeletes, ModelTranslatable ,LogsActivity;

    protected $dates = ['deleted_at'];
    protected $translatable = ['title'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Option Salary Group';

    public function options() {
        return $this->belongsToMany('App\OptSalary', 'opt_salary_opt_salary_group', 'option_group_id', 'option_id');
    }

    public function sortedOptions() {
        $options = $this->options()->get();

        $values = $options->sortBy(function ($option) {
            if(starts_with($option->opt_value, '>=')) {
                return str_replace('>=', '9', $option['opt_value']);
            } elseif (starts_with($option->opt_value, '<=')) {
                return str_replace('<=', '0', $option['opt_value']);
            }

            return explode('-', $option['opt_value'])[0];
        });

        return $values->values()->all();
    }

    public function pageLeaveForms() {
        return $this->hasMany('App\Pageleaveform', 'opt_salary_group_id', 'id');
    }
}