<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitiesLogAppStatus extends Model
{
    //
    protected $connection = 'ktcoap_log';
    protected $table = 'app_status';
    public $timestamps = false;
}
