<?php

namespace App\Console\Commands;

use App\Http\Controllers\Backend\ProductSyncController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ProductSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync product data from remote server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::channel('appdebug')->info('Start syncing product data');
        $this->info('Synchronizing product data...');
        $ctrl = new ProductSyncController($this);
        $ctrl->sync();
        $this->info('Finished');
        Log::channel('appdebug')->info('End syncing product data');
    }
}
