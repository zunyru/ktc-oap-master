<?php

namespace App\Console\Commands;

use App\Exports\VisaExport;
use App\VisaReportLog;
use App\VisaApp;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;
use Storage;


class MakeVisaReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:visa-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate report for visa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::files('visa-exports');
        if (count($files) >= 500) {
            Storage::delete($files[0]);
        }
        $VisaApp = VisaApp::selectRaw(DB::raw('DISTINCT(product_name)'))->groupby('product_name')->get();

        foreach ($VisaApp as $key => $item) {

          $last_log_item = VisaReportLog::all()->where('product_name',$item->product_name)->last();
          $last_log_item_id = isset($last_log_item) ? $last_log_item->last_visa_apps_id : 0;

          $time = Carbon::now("Asia/Bangkok");
          $file_name = $item->product_name."-export_".$time->format('Y-m-d_H-i-s');
          $query = VisaApp::where('id', '>', $last_log_item_id)->where('product_name', $item->product_name)
          ->orderByRaw('created_at ASC');

          // update last id
          $all_records = $query->get();
          $last_record = $all_records->last();

          if(isset($last_record)) {
            $last_id = $last_record->id;
            Excel::store(new VisaExport($query), 'visa-exports/'.$file_name.'.csv');
            //Storage::disk('visa-sftp')->put($file_name.'.csv', Storage::disk('local')->get('visa-exports/'.$file_name.'.csv'));
            Storage::disk('local')->get('visa-exports/'.$file_name.'.csv');
            $log_item = new VisaReportLog();
            $log_item->last_visa_apps_id = $last_record->id;
            $log_item->last_customer_id = $last_record->customer_id;
            $log_item->file_name = $file_name.'.csv';
            $log_item->row_amount=count($all_records);
            $log_item->product_name= $item->product_name;
            $log_item->save();
            //return $last_id;
        }
    }



    return null;
  }
}