<?php

namespace App\Console\Commands;

use App\Exports\UnionpayExport;
use App\UnionpayReportLog;
use App\UnionpayApp;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;
use Storage;

class MakeUnionpayReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:unionpay-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate report for unionpay';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::files('unionpay-exports');
        if (count($files) >= 500) {
            Storage::delete($files[0]);
        }
        $last_log_item = UnionpayReportLog::all()->last();
        $last_log_item_id = isset($last_log_item) ? $last_log_item->last_unionpay_apps_id : 0;

        $time = Carbon::now("Asia/Bangkok");
        $file_name = "oap-unionpay-export_".$time->format('Y-m-d_H-i-s');
        $query = UnionpayApp::where('id', '>', $last_log_item_id)
            ->orderByRaw('created_at ASC');

        // update last id
        $all_records = $query->get();
        $last_record = $all_records->last();

        if(isset($last_record)) {
            $last_id = $last_record->id;
            Excel::store(new UnionpayExport($query), 'unionpay-exports/'.$file_name.'.csv');
            Storage::disk('unionpay-sftp')->put($file_name.'.csv', Storage::disk('local')->get('unionpay-exports/'.$file_name.'.csv'));
            $log_item = new UnionpayReportLog();
            $log_item->last_unionpay_apps_id = $last_record->id;
            $log_item->last_customer_id = $last_record->customer_id;
            $log_item->file_name = $file_name.'.csv';
            $log_item->row_amount=count($all_records);
            $log_item->save();
            return $last_id;
        }

        return null;
    }
}