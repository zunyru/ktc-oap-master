<?php

namespace App\Console\Commands;

use App\Exports\LeadsWithLIDExport;
use App\LeadsCsReportLog;
use App\Leavefromapp;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;
use Storage;
use File;

class MakeCsReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:cs-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate report for CS telesales';

    protected $key = 'leads.last_report_id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dir="cs-exports";
        $files = Storage::files($dir);
        if (count($files) >= 500) {
            //$this->comment(count($fi
            //les));
            Storage::delete($files[0]);
        }
        $last_log_item = LeadsCsReportLog::all()->last();
        $last_log_item_id = isset($last_log_item) ? $last_log_item->last_lead_id : 0;


        $time = Carbon::now("Asia/Bangkok");
        $file_name = "oap-leads-cs-telesales-export_".$time->format('Y-m-d_H-i-s');
        $query = Leavefromapp::where('leavefromapps.active', 0)
            ->where('leavefromapps.id', '>', $last_log_item_id)
            ->where('leavefromapps.active', 0)
            ->where('leavefromapps.is_first', 1)
            ->whereRaw("(`leavefromapps`.`lead_dest` = 'CS-Tele')")
            ->orderByRaw('leavefromapps.created_at ASC');
        $query->join('opt_salaries', 'opt_salaries.id', '=', 'leavefromapps.opt_Salary_id');
        $query->join('pageleaveforms', 'pageleaveforms.id', '=', 'leavefromapps.PageLeaveForm_id');
        $query->join('products', 'products.id', '=', 'leavefromapps.Product_id');
        $query->join('product_categories', 'product_categories.id', '=', 'products.product_category_id');
        $query->selectRaw('leavefromapps.*,leavefromapps.created_at AS lead_created_at,opt_salaries.opt_value,pageleaveforms.slug,products.title,products.sys_product_id,product_categories.category_code');

        // update last id
        $all_records = $query->get();
        $last_record = $all_records->last();

        if(isset($last_record)) {
            $last_id = $last_record->id;

            Excel::store(new LeadsWithLIDExport($query), "{$dir}/".$file_name.'.csv', 'local');
            Storage::disk('cs-sftp')->put($file_name.'.csv', Storage::disk('local')->get("{$dir}/".$file_name.'.csv'));
            $log_item = new LeadsCsReportLog();
            $log_item->last_lead_id = $last_record->id;
            $log_item->last_lead_unq_id = $last_record->lead_unique_id;
            $log_item->file_name = $file_name.'.csv';
            $log_item->row_amount=count($all_records);
            $log_item->save();
            // foreach ($all_records as $record) {
            //     $log_item = new LeadsCsReportLog();
            //     $log_item->lead_id = $record->id;
            //     $log_item->lead_unique_id = $record->lead_unique_id;
            //     $log_item->save();
            // }

            return $last_id;
        }

        return null;
    }
}