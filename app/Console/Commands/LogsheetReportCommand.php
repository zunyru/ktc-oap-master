<?php

namespace App\Console\Commands;

use App\Exports\LogsheetExport;
use App\LogsheetReportLog;
use App\LogSheet;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use League\Flysystem\Sftp\SftpAdapter;
use League\Flysystem\Filesystem;
use Storage;

class LogsheetReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:logsheet-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate report for log sheet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = Storage::files('log-sheets');
        if (count($files) >= 500) {
            Storage::delete($files[0]);
        }
        $last_log_item = LogsheetReportLog::all()->last();
        $last_log_item_id = isset($last_log_item) ? $last_log_item->last_logsheet_id : 0;

        $time = Carbon::now("Asia/Bangkok");
        $file_name = "log-sheets-export_".$time->format('Y-m-d_H-i-s');
        $query = LogSheet::where('id', '>', $last_log_item_id)
            ->orderByRaw('created_at ASC');

        // update last id
        $all_records = $query->get();
        $last_record = $all_records->last();

        if(isset($last_record)) {
            $last_id = $last_record->id;
            Excel::store(new LogsheetExport($query), 'logsheet-exports/'.$file_name.'.csv');
            //Storage::disk('local')->get('logsheet-exports/'.$file_name.'.csv');
            Storage::disk('visa-sftp')->put($file_name.'.csv', Storage::disk('local')->get('logsheet-exports/'.$file_name.'.csv'));
            $log_item = new LogsheetReportLog();
            $log_item->last_logsheet_id = $last_record->id;
            $log_item->file_name = $file_name.'.csv';
            $log_item->row_amount=count($all_records);
            $log_item->save();
            return $last_id;
        }

        return null;
    }
}