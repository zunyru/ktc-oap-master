<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 2019-01-10
 * Time: 02:03
 */

namespace App\Repositories;


use App\Leavefromapp;
use Carbon\Carbon;

class LeadRepo
{
    /**
     * @var Leavefromapp
     */
    private $model;

    public function __construct(Leavefromapp $model = null)
    {
        $this->model = $model ?? new Leavefromapp();
    }

    public function getNextUniqueID() {
        //get current year
        $time = Carbon::now('Asia/Bangkok');
        $year = substr(''.$time->year, 2);

        $last = Leavefromapp::whereRaw("YEAR(created_at) = {$time->year} AND lead_unique_id IS NOT NULL")
            ->orderBy('id', 'desc')
            ->first(['lead_unique_id']);
            
        if (!isset($last)) {
            return sprintf('A%s%07d', $year, 1);
        }

        $last_id = substr($last->lead_unique_id, 1);
        return 'A'.($last_id + 1);
    }
}