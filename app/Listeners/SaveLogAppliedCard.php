<?php

namespace App\Listeners;

use App\Events\AppliedCard;
use App\ActivitiesLogApplyCard;
use DateTime;
use Illuminate\Database\Eloquent\Builder;

class SaveLogAppliedCard
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct( /*Builder $query = null*/)
    {
        //
        //  $this->query = $query;
    }

    /**
     * @return Builder
     */
    /* public function query()
    {
    if(isset($this->query)) {
    return $this->query;
    }

    return Leavefromapp::with(['option','product','page']);
    }*/

    /**
     * Handle the event.
     *
     * @param  AppliedCard  $event
     * @return void
     */
    public function handle(AppliedCard $event)
    {
        //
        // dd($event->leaveformapp->Product_id);
        //$this->map($event->leaveformapp);
        $log_type = 2;
        $log_id = $this->genLogId($log_type);
        $date_time = new DateTime();
        $new_date_time = date_format($date_time, "Y-m-d H:i:s");
        // $browser = get_browser();
        $data = $this->map($event->leaveformapp);

        $log_app_card = new ActivitiesLogApplyCard;
        $log_app_card->log_id = $log_id;
        $log_app_card->date_time = $new_date_time;
        $log_app_card->os = $browser->platform;
        $log_app_card->browser = request()->ip() /* $browser->browser */;
        $log_app_card->first_name = $data['first_name'];
        $log_app_card->last_name = $data['last_name'];
        $log_app_card->phone_no = $data['phone_no'];
        $log_app_card->email = $data['email'];
        $log_app_card->card_type = $data['card_type'];
        $log_app_card->branch_code = $data['branch_code'];
        $log_app_card->product_code = $data['product_code'];
        $log_app_card->product_name = $data['product_name'];
        $log_app_card->indicator = $data['indicator'];
        $log_app_card->sub_indicator = $data['sub_indicator'];
        $log_app_card->submit_date = $data['submit_date'];
        $log_app_card->previous_page = $data['previous_page'];
        $log_app_card->callback_link = $data['callback_link'];
        $log_app_card->submit_form_url = $data['submit_form_url'];
        $log_app_card->opt_salary = $data['option'];
        $log_app_card->cf1_key = $data['cf1_key'];
        $log_app_card->cf1_value = $data['cf1_val'];
        $log_app_card->cf2_key = $data['cf2_key'];
        $log_app_card->cf2_value = $data['cf2_val'];
        $log_app_card->save();

    }

    /**
     * Generate LogID.
     * @param $citizen_code
     * @param $log_type
     * @return string
     * @throws \Exception
     */

    protected function genLogId($log_type)
    {
        $now = new DateTime();
        $count = ActivitiesLogApplyCard::whereDate('date_time', date('Y-m-d'))->count();
        if ($count == 0) {
            $no = 1;
        } else {
            $no = $count + 1;
        }

        $log_no = str_pad($no, 6, '0', STR_PAD_LEFT);
        $now_str = $now->format('YmdHis');
        if ($log_type == 1) {
            $type = 'A';
        } else {
            $type = 'B';
        }
        $log_id = $now_str . '' . $type . '' . $log_no;

        return $log_id;

    }

    protected function map($lead)
    {
        //dd($lead);

        /**
         * {"inbound": null, "callback": null, "outbound": null, "indicator": null, "sub_indicator": null}
         */
        $params = json_decode($lead->custom_param, true);

        /**
         * {"value": "222", "key_name": "bbb"}
         */
        $cf_1 = json_decode($lead->cf_1, true);
        $cf_2 = json_decode($lead->cf_2, true);

        $card_type = null;
        if (isset($lead->product->category)) {
            $code = $lead->product->category->category_code;

            if ($code == "CC") {
                $card_type = "CREDIT";
            } elseif ($code == "PL") {
                $card_type = "PROUD";
            } elseif ($code == "FIXED") {
                $card_type = "CASH";
            }
        }

        // dd($card_type);

        if (!isset($card_type)) {
            return [];
        }

        /**
         * ID
        FIRST_NAME
        LAST_NAME
        TELEPHONE
        EMAIL
        INCOME
        CARD_TYPE
        PRODUCT_NAME
        PRODUCT_CODE
        INDICATOR
        SUB_INDICATOR
        BRANCH_CODE
        SUBMIT_DATE
        PREVIOUS_PAGE
        CALLBACK_LINK
        SUBMIT_FORM_URL
        "CF1_KEY",
        "CF1_VAL",
        "CF2_KEY",
        "CF2_VAL",
         */

        $str = [
            "lead_unique_id" => $lead->lead_unique_id ?? '',
            "first_name" => $lead->fname,
            "last_name" => $lead->lname,
            "phone_no" => $lead->tel,
            "email" => $lead->email,
            "option" => $lead->option ? $lead->option->opt_value : null,
            "card_type" => $card_type,
            "product_name" => $lead->product->title,
            "product_code" => $lead->product->sys_product_id,
            "indicator" => $params["indicator"] ?: null,
            "sub_indicator" => $params["sub_indicator"] ?: null,
            "branch_code" => $lead->branch_code,
            "submit_date" => $lead->created_at->format('Y-m-d H:i:s'),
            "previous_page" => $params["inbound"] ?: null,
            "callback_link" => $params["callback"] ?: null,
            "submit_form_url" => $lead->page ? route('page-index', $lead->page->slug) : "404",
            "cf1_key" => $cf_1["key_name"] ?? null,
            "cf1_val" => $cf_1["value"] ?? null,
            "cf2_key" => $cf_2["key_name"] ?? null,
            "cf2_val" => $cf_2["value"] ?? null,
        ];

        return $str;
    }

}
