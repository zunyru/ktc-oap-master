<?php

namespace App\Listeners;

use App\Events\CheckedAppStatus;
use App\LogActivity;
use App\ActivitiesLogAppStatus;
use DateTime;
use Illuminate\Support\Facades\Log;

class SaveLogCheckedAppStatus
{

    // protected $log_activity;
    // /**
    //  * Create the event listener.
    //  *
    //  * @return void
    //  */
    // public function __construct(LogActivity $log_activity)
    // {
    //     //

    //     $this->log_activity = $log_activity;
    // }

    /**
     * Handle the event.
     *
     * @param  CheckedAppStatus  $event
     * @return void
     */
    public function handle(CheckedAppStatus $event)
    {
        $log_id = $this->genLogId($event->citizen_code, $event->log_type);
        $date_time = new DateTime();
        $new_date_time = date_format($date_time, "Y-m-d H:i:s");
        // $browser = get_browser();

        $log_app_status = new ActivitiesLogAppStatus;
        $log_app_status->log_id = $log_id;
        $log_app_status->citizen_id = $event->citizen_code;
        $log_app_status->date_time = $new_date_time;
        // $log_app_status->os = $browser->platform;
        $log_app_status->browser = request()->ip() /* $browser->browser */;
        $log_app_status->save();
    }

    /**
     * Generate LogID.
     * @param $citizen_code
     * @param $log_type
     * @return string
     * @throws \Exception
     */

    protected function genLogId($citizen_code, $log_type)
    {
        $now = new DateTime();
        $count = ActivitiesLogAppStatus::whereDate('date_time', date('Y-m-d'))->count();
        if ($count == 0) {
            $no = 1;
        } else {
            $no = $count + 1;
        }

        $log_no = str_pad($no, 6, '0', STR_PAD_LEFT);
        $now_str = $now->format('YmdHis');
        if ($log_type == 1) {
            $type = 'A';
        } else {
            $type = 'B';
        }
        $log_id = $now_str . '' . $type . '' . $log_no;

        return $log_id;

    }

}
