<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivitiesLogApplyCard extends Model
{
    //
    protected $connection = 'mysql';
    protected $table = 'view_leave_form';
    public $timestamps = false;
}
