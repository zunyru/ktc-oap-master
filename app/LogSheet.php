<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSheet extends Model
{
    protected $table = 'log_sheets';
}