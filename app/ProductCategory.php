<?php
/**
 * Created by PhpStorm.
 * User: korstudio
 * Date: 17/11/18
 * Time: 5:54 PM
 */

namespace App;


use App\Models\ModelTranslatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class ProductCategory extends Model
{
    use SoftDeletes, ModelTranslatable ,LogsActivity;
    public $timestamps = false;

    protected $dates = ['deleted_at'];
    protected $translatable = ['name', 'description'];

    protected static $logAttributes = ['*'];
    protected static $logName = 'Product Category';
}