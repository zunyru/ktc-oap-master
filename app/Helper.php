<?php
function formatDateThat($strDate)
{
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear ";
}

function formatDateEng($strDate)
{
    $strYear = date("Y",strtotime($strDate));
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","Jan.","Feb.","Mar.","Apr.","May.","Jun.","Jul.","Aug.","Sep.","Oct.","Nov.","Dec.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear ";
}

function formatDateTimeThat($strDate)
{
    $strYear = date("Y",strtotime($strDate))+543;
    $strMonth= date("n",strtotime($strDate));
    $strDay= date("j",strtotime($strDate));
    $strHour= date("H",strtotime($strDate));
    $strMinute= date("i",strtotime($strDate));
    $strSeconds= date("s",strtotime($strDate));
    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
    $strMonthThai=$strMonthCut[$strMonth];
    return "$strDay $strMonthThai $strYear $strHour:$strMinute น.";
}

function formatPhone($tell){
    $txt1 = substr($tell, 0,2);
    $txt2 = substr($tell, 3,5);
    $txt3 = substr($tell, 6,9);

    // return "$txt1 $txt2 $txt3";
    return $tell;
}
function customAsset($path){
   return config('app.url').'/oap/'.$path;
}
function parentURLGenerate(){
    if(session()->has('sso_token')){
        $new_url = config('app.parent_url').'?token='.session()->get('sso_token');
        if(session()->has('local')){
            $new_url = $new_url.''.'&lang='.session()->get('local');
        }
        return $new_url;
    }else{
        $url = config('app.parent_url');
        $pos =  strpos($url,"/login");
        $new_url = substr($url,0,$pos);
        if (session()->get('local')=="en") {
            $new_url="https://{$_SERVER['SERVER_NAME']}/en/home";
        }else{
            $new_url="https://{$_SERVER['SERVER_NAME']}/home";
        }

        return $new_url;
    }
}

function hideHeader(){
    $url = strtok($_SERVER["REQUEST_URI"],'?');
    $page_path = preg_replace( '%^(.+)/%', '', $url );
    if(config('app.hide_header_on_pages')==null) {
        return false;
    }else{
        //dd(config('app.hide_header_on_pages'));
        $array_hide_url = explode(',', setting('pages.hide_header_on_pages'));
        if (count($array_hide_url)<1) {
             $array_hide_url=[setting('pages.hide_header_on_pages')];
        }
        if (in_array($page_path,$array_hide_url)) {
            return false;
        }else{
            return true;
        }
    }
}
