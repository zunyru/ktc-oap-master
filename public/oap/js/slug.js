//slug
var slug = function(str) {
	var $slug = '';
	var trimmed = $.trim(str);
	$slug = trimmed.replace(/[^a-z0-9ก-๙-]/gi, '-').
	replace(/-+/g, '-').
	replace(/^-|-$/g, '');
	return $slug.toLowerCase();
}

//custom_field
var custom_field = function(str) {
	var $slug = '';
	var trimmed = $.trim(str);
	$slug = trimmed.replace(/[^a-z0-9]/gi, '-').
	replace(/-+/g, '_').
	replace(/^-|-$/g, '');
	return $slug.toLowerCase();
}