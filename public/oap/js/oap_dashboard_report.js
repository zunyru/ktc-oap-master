var DashboardReport = function (options) {
    var isInitForm = false;
    var $_form = null;
    this.options = options || {};
    this.debug = options.debug || true;

    this.filters = {
        branches: [],
        categories: [],
        dates: ''
    };

    // constructor body
    /**
     *
     * @returns jQuery
     */
    this.form = function () {
        if(this.options.form == null) {
            return;
        }

        if(!isInitForm) {
            var _this = this;
            var filterButton = $(this.options.form + ' .btn-filter');
            filterButton.on('click', function () {
                _this.applyFilters();
            });

            $_form = $(this.options.form);
            isInitForm = true;
        }
    };

    this.form();
};

DashboardReport.prototype.onsuccess = function(data) {
    if(this.debug) {
        console.log('[DashboardReport] success:', data);
    }

    if(this.options == null || this.options.success == null) {
        return;
    }

    this.options.success(data);
};

DashboardReport.prototype.applyFilters = function() {
    var timestamp = (new Date()).getTime();
    var filters = this.filters;
    var token = $('meta[name="csrf-token"]').attr('content');
    var _this = this;

    this.form();

    $.ajax(this.options.url + '?_=' + timestamp, {
        dataType: 'json',
        method: 'post',
        jsonp: false,
        data: {
            '_token': token,
            '_': timestamp,
            'branches': filters.branches,
            'categories': filters.categories,
            'date_range': filters.dates
        },
        headers: {
            'X-CSRF-TOKEN': token
        },
        error: function (jqxhr, textStatus, error) {
            console.log('[DashboardReport] error:', error);
        }
    })
        .done(function (data) {
            _this.onsuccess(data);
        });

};