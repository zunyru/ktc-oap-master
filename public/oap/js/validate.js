//validate Email
function validateEmail(sEmail){
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}
$(document).ready(function(){

 $('.email-error').hide()
 $('.fullname-error').hide()
 $('.tel-error').hide()

 $('#long-button').click(function() {


    var sFullname = $('input[name^=full_name]').val()
    var sTel = $('input[name^=tel]').val()
    var sEmail = $('#txtEmail').val()
    var tSucess = true


    if (typeof sFullname !== "undefined") {
        if ($.trim(sFullname).length == 0){

            $('.fullname-error').show()
            $('.fullname-error').text('กรูณาระบชื่อ นามสกุล')
            tSucess = false

        }else{
            $('.fullname-error').hide()
        }
    }

    if ($.trim(sEmail).length == 0) {

        $('.email-error').show()
        $('.email-error').text('กรุณาระบุ Email ')
        tSucess = false
    }
    if($.trim(sTel).length == 0) {

        $('.tel-error').show()
        $('.tel-error').text('กรุณาระบุ หมายเลขโทรศัพท์ ')
        tSucess = false
    }

    if($.trim(sTel).length < 10) {

        $('.tel-error').show()
        $('.tel-error').text('กรุณาระบุ หมายเลขโทรศัพท์ ให้ถูกต้อง ')
        tSucess = false
    }
    if (!validateEmail(sEmail)) {
       $('.email-error').show()
       $('.email-error').text('กรุณาระบุ Email ให้ถูกต้อง ')
       tSucess = false
   }


   return tSucess;


});
});
