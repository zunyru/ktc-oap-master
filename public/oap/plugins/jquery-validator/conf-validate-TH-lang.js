jQuery(document).ready(function($) {
    var validation_lang = $("html").attr("lang");

    if ($("input[name$='term_acceptance']").is(":checked")) {
        $("input[name$='term_acceptance']").prop("checked", false);
        //console.log($("input[name$='term_acceptance']").val())
    }
    $.formUtils.addValidator({
        name: "custom_character",
        validatorFunction: function(value, $el, config, language, $form) {
            var regex = new RegExp(
                /^[A-Za-zก-ฮ็-์ .ะัาำิีึืุูเแโใไ]*((-|\s)*[A-Za-zก-ฮ็-์ .ะัาำิีึืุูเแโใไ])*$/
            );
            if (regex.test(value)) {
                return true;
            } else {
                return false;
            }
        },
        errorMessage: function() {
            if (validation_lang == "en") {
                return "Please correct your information";
            } else {
                return "กรุณากรอกข้อมูลให้ถูกต้อง";
            }
        },
        errorMessageKey: "badCustomCharacter"
    });

    $.formUtils.addValidator({
        name: "custom_phonenumber",
        validatorFunction: function(value, $el, config, language, $form) {
            var regex = new RegExp(/^[0-9 ]*((-|\s)*[0-9 ])*$/);
            var regex_phone = new RegExp(/((0)[0-9])\w+/);
            if (regex.test(value) && regex_phone.test(value)) {
                return true;
            } else {
                return false;
            }
        },
        errorMessage: function() {
            if (validation_lang == "en") {
                return "Please correct your phone number (numeric characters only)";
            } else {
                return "กรุณาระบุหมายเลขโทรศัพท์ให้ถูกต้อง และเป็นตัวเลขเท่านั้น";
            }
        },
        errorMessageKey: "badCustomCharacter"
    });
    var config = {
        lang: validation_lang,
        borderColorOnError: "#f44336",
        // language: validateTH_Lang,
        validateOnBlur: true,
        validateHiddenInputs: true,
        // ordinary validation config
        form: "form",
        // reference to elements and the validation rules that should be applied
        validate: {
            fname: {
                validation: "required, custom_character",
                "_data-sanitize": "trim"
            },
            lname: {
                validation: "required, custom_character",
                "_data-sanitize": "trim"
            },
            email: {
                validation: "required, email, length",
                "_data-sanitize": "trim",
                length: "max50",
                "error-msg-required": function() {
                    if (validation_lang == "en") {
                        return "Email format is incorrect";
                    } else {
                        return "กรุณาระบุอีเมลให้ถูกต้อง";
                    }
                }
            },
            tel: {
                validation: "required, length ,custom_phonenumber",
                "_data-sanitize": "trim",
                length: "11-12",
                "error-msg-length": function() {
                    if (validation_lang == "en") {
                        return "Please correct your phone number (numeric characters only)";
                    } else {
                        return "กรุณาระบุหมายเลขโทรศัพท์ให้ถูกต้อง และเป็นตัวเลขเท่านั้น";
                    }
                },
                "error-msg-required": function() {
                    if (validation_lang == "en") {
                        return "Please correct your phone number (numeric characters only)";
                    } else {
                        return "กรุณาระบุหมายเลขโทรศัพท์ให้ถูกต้อง และเป็นตัวเลขเท่านั้น";
                    }
                }
            },
            salary: {
                validation: "required",
                "_data-sanitize": "trim"
            },
            convenient_times: {
                validation: "required",
                "_data-sanitize": "trim"
            },
            "#cf1": {
                validation: function() {
                    if ($(this).prop("required")) {
                        return "required";
                    }
                },
                "_data-sanitize": "trim"
            },
            "#cf2": {
                validation: function() {
                    if ($(this).prop("required")) {
                        return "required";
                    }
                },
                "_data-sanitize": "trim"
            },
            term_acceptance: {
                validation: "required",
                "_data-sanitize": "trim",
                "error-msg-container": ".box-other-form.checkbox-form",
                "error-msg-required": function() {
                    if (validation_lang == "en") {
                        return "please accept terms and conditions before proceeding";
                    } else {
                        return "กรุณายอมรับเงื่อนไขก่อนการส่งข้อมูล";
                    }
                }
            }
        }
    };

    $.validate({
        modules: "jsconf, toggleDisabled",
        onModulesLoaded: function() {
            $.setupValidation(config);
        }
    });

    //preventing form submit duplication
    $("form").submit(function() {
        $this = $(this);

        /** prevent double posting */
        if ($this.data().isSubmitted) {
            return false;
        }

        /** do some processing */

        /** mark the form as processed, so we will not process it again */
        $this.data().isSubmitted = true;

        return true;
    });

    setTimeout(function() {
        $(".phone_with_ddd").mask("Z00 000 0000", {
            translation: {
                Z: { pattern: /[0]/ }
            }
        });
        $(".text_only").mask(
            "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
            {
                translation: {
                    //A: {pattern: /[A-Za-zก-๙ .]/},
                    A: {
                        pattern: /^[A-Za-zก-ฮ็-์ .ะัาำิีึืุูเแโใไ]*((-|\s)*[A-Za-zก-ฮ็-์ .ะัาำิีึืุูเแโใไ])*$/
                    }
                    /*S: {pattern: /[A-Za-z]/},
      Y: {pattern: /[0-9]/}*/
                }
            }
        );
    }, 1000);
});