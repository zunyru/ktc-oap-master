<?php
### Backend
use App\Http\Controllers\Backend\BranchCategoryController;
use App\Http\Controllers\Backend\BranchController;
use App\Http\Controllers\Backend\KTCsettingController;
### Frontend
use App\Http\Controllers\Frontend\CardpageController;
use App\Http\Controllers\Frontend\Unionpay\UnionpayController;
use App\Http\Controllers\Frontend\Visa\VisaController;

use App\Http\Controllers\Frontend\AppStatusController;
use App\Http\Controllers\Frontend\CreditcardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'oap/admin','middleware' => 'admin.user'], function () {

	Route::post('/check/check-dup-title/', ['uses' =>'Backend\\CheckduptitleController@checkDupTitle'])->name('check.check-dup-title');

	Route::post('/check/check-dup-slug/', ['uses' =>'Backend\\CheckduptitleController@checkDupSlug'])->name('check.check-dup-slug');

	Route::post('/check/check-dup-titlepage/', ['uses' =>'Backend\\CheckduptitleController@checkDupTitlePage'])->name('check.check-dup-titlepage');

	Route::any('/file-delete', ['uses' =>'Backend\\PageleaveformController@deleteLayout'])->name('file-delete');

	Route::any('/file-muti-delete', ['uses' =>'Backend\\PageleaveformController@deleteLayoutMulti'])->name('file-muti-delete');

	Route::post('/check/duplication/product-category/{field}', ['as' => 'check.duplicated.product-category', 'uses' => 'Backend\\ProductCategoryController@checkDuplicated']);
	Route::any('/product-categories/{category_id}/restore', ['as' => 'restore.product-category', 'uses' => 'Backend\\ProductCategoryController@restore']);

   ### RESTORE
	Route::put('leads/restore/{id}',['uses' =>'Backend\\LeavefromappController@restore'])->name('lead-restore');

	### FOR Branch Categories
	Route::get('/branch-categories/{category_id}/restore', [BranchCategoryController::class, 'restore'])->name('restore.branch-category');


	### FOR Leads
	Route::post('/leads/edit/time-filter', ['as' => 'edit.leads.time-filter', 'uses' => 'Backend\\LeavefromappController@changeDuplicateFilterDate']);
	Route::any('/leads/export', ['as' => 'export.leads', 'uses' => 'Backend\LeavefromappController@export']);
	Route::any('/leads/exportLID', ['as' => 'export.leads-lid', 'uses' => 'Backend\LeavefromappController@exportWithLID']);
	Route::get('/leads/next-id', function () {
	    return response(["id" => (new \App\Repositories\LeadRepo())->getNextUniqueID()]);
    });

	### FOR Product Sync

	Route::get('/dev/product/sync', ['as' => 'product.sync', 'uses' => 'Backend\\ProductSyncController@sync']);
	Route::get('/dev/date/{num}/{unit}', function($num, $unit){
	    $dt = new \Carbon\Carbon($num.' '.$unit.' ago');
	    return $dt->startOfDay();
    });

	### OPTION SALARY
    Route::post('check/duplication/opt-salaries', ['as' => 'check.opt-salaries.existed', 'uses' => 'Backend\\OptsalaryController@checkDuplicated']);

	### OPTION Convenien Time
    Route::post('check/duplication/opt-convenien-time', ['as' => 'check.opt-convenien-time.existed', 'uses' => 'Backend\\OptconvenienttimeController@checkDuplicated']);

	Route::get('opt-convenient-times/order', 'Backend\OptconvenienttimeController@order')->name('opt-convenient-times-order');
	Route::post('opt-convenient-times/order-list', 'Backend\OptconvenienttimeController@orderlist')->name('opt-convenient-times-order-list');

	### FOR All restore routes
	try {
    foreach (\TCG\Voyager\Models\DataType::all() as $dataType) {
		    $breadController = $dataType->controller
		    					? $dataType->controller
                                : 'Voyager\VoyagerBaseController';
        Route::any($dataType->slug.'/{id}/restore', $breadController.'@restore')->name('record.'.$dataType->slug.'.restore');
        Route::get($dataType->slug.'/trash', $breadController.'@index')->name('voyager.'.$dataType->slug.'.trash');
        Route::post($dataType->slug.'/check_existed', $breadController.'@checkExisted')->name('record.'.$dataType->slug.'.check-existed');
    }
	} catch (\InvalidArgumentException $e) {
        throw new \InvalidArgumentException("Custom routes hasn't been configured because: ".$e->getMessage(), 1);
    } catch (\Exception $e) {
        // do nothing, might just be because table not yet migrated.
    }

	### For KTC Settings
	Route::group(['as' => 'admin.'], function () {
		Route::resource('ktc-settings', 'Backend\KTCsettingController')->only(['index', 'update']);
		Route::group(['as' => 'profile.', 'prefix' => 'profile'], function() {
		    Route::get('edit', 'Backend\UserProfileController@editProfile')->name('edit');
		    Route::put('edit', 'Backend\UserProfileController@updateProfile')->name('update');
        });
	});

	Route::get('/report/view', 'Backend\ReportController@view');
	Route::get('/report/export', 'Backend\ReportController@export')->name('export.report');

    Route::any('/api/report', ['uses' => 'Backend\\ReportController@getDashboardReport'])->name('api.oap.dashboard_report');
});

//Route::post('/file-delete', ['uses' =>'Backend\\UploaderController@myupload']);


//Frontend Route
Route::get('/', function () {
	if (config('app.debug') == false) {
		return false;
	}else{
		return view('welcome');
	}
});

// Route::get('/oap/phpinfo', function () {
// 	echo "2";
// 	return utf8_encode(phpinfo());
// });

// //mockup for api data
// Route::group(['as' => 'mockup.','prefix' => 'mockup'], function () {
//     include_once(__DIR__.'/mockup.php');
// });

Route::resource('oap/lead-submit', 'Frontend\\CreditcardController')->except([
	'index', 'create', 'show', 'edit' , 'update', 'destroy'
]);

//unionpay
Route::any('apply/ktc-unionpay/{namepage}/thankyou', [UnionpayController::class, 'thankyou'])->name('unionpay-thankyou');
Route::get('apply/ktc-unionpay/{namepage}', [UnionpayController::class, 'index'])->where('namepage', '^((?!\w*thankyou\w*).)*$')->name('unionpay');
//Route::get('apply/ktc-unionpay/{namepage}', [UnionpayController::class, 'index'])->where('namepage', '\b(\w*thankyou\w*)\b')->name('unionpay2');
//visa
Route::any('offer/{product}/{namepage}/thankyou',[VisaController::class, 'thankyou'])->name('visa-thankyou');

Route::get('offer/{product}/{namepage}/',[VisaController::class, 'index'])->where('namepage', '^((?!\w*thankyou\w*).)*$')->name('visa');



Route::get('apply/global-menu', [CardpageController::class, 'pageGlobal'])->name('page-global');
Route::get('apply/{namepage}', ['uses' =>'Frontend\\CardpageController@page'])->name('page-index');


Route::get('apply/{namepage}/thankyou', [CreditcardController::class, 'leadSubmitResult'])->name('thankyou');
Route::get('apply/{namepage}/repetitive', [CreditcardController::class, 'leadSubmitResult'])->name('repetitive');



//app-check
Route::get('application-status', [AppStatusController::class, 'index'])->name('app-status.index');
Route::post('application-status', [AppStatusController::class, 'otpReq'])->name('app-status.otp-req');
Route::get('application-status/result', [AppStatusController::class, 'result'])->name('app-status.result');

//ALL Voyager route can  be overridden by Custom
Route::group(['prefix' => 'oap/admin'], function () {
	Voyager::routes();
});