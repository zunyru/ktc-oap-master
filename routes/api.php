<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {

    return $request->user();
});

Route::post('/oap/GetLogTypes', 'Backend\ActivitiesLogController@getType')->name('api1');
Route::post('/oap/GetActivityLogs', 'Backend\ActivitiesLogController@getLog')->name('api2');

Route::group([ 'prefix' => 'auth'], function (){

        Route::get('login', 'API\AuthController@login')->name('login');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::post('getuser', 'API\AuthController@getUser');
        Route::post('getpage', 'API\AuthController@getPage');
        Route::post('save', 'API\AuthController@getSave');
    });
});
