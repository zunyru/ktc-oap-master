@extends('frontend.app-status.master')
@section('title', "KTC Application Status")
@push('page-styles')
  <style type="text/css">
    @if(count($appStatusList_fronts)==1)
      .choose-card,.select-arrow,.choice-mask,.choice-wrap{
        display: none !important;
      }
      #pg-regis.box-login-con input{
        cursor: context-menu !important;
        margin-top:40px;
      }
    @endif
    #choice-mask {
        z-index: 2;
    }
    #card-check{
      -webkit-box-shadow:none !important;
      background-color: transparent;
    }
    #choice-mask p{
      font-size:15px;
    }
    .show-status .left-thin{
      margin-top:10px;
    }
    .app-check button.btn-next-step {
        margin-bottom: 50px;
    }
  </style>
@endpush
@section('page-content')

  <style type="text/css">
    .bg-box-status{
      background-color: #f6f7f8;
      border-radius: 6px;
      padding: 14px;
      margin-top: 60px;
    }
    .bg-box-status .box-input-form .status-sub-title{
      display: block;
      position: absolute;
      left: 0;
      top: -96px;
      color: rgba(91,102,112,.7);
      font-size: 16px;
      font-family: 'Conv_SukhumvitTadmai_Tex';
    }
    #pg-regis.box-login-con .bg-box-status input{
      margin-top: 0px;
    }
    .bg-box-status .result-items .icon-status img{
      width: 48px;
      margin-bottom: 15px;
    }
    .bg-box-status .pass-state{
      margin-bottom: 28px;
    }
    .bg-box-status .show-status{
      display: none;
    }
    .bg-box-status .show-status.active{
      display: block;
    }
    #choice-mask p {
      display: -webkit-inline-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    .choice-img-wrap{
      display: none !important;
    }
    #choice-wrap+#choice-mask .choice-list{
      min-height: 30px;
      vertical-align: middle !important;
      display: block;
      line-height: 60px;
    }
    .card-title {
      margin-bottom: 0px !important;
      text-transform: uppercase;
    }
    .choice-list{
      line-height: 30px;
    }
    @media (max-width: 767px){
      .bg-box-status .box-input-form .status-sub-title{
        top: -65px;
      }
      .bg-box-status{
        margin-top: 48px;
      }
    }
  </style>

  <form action="#" id="pg-regis" class="row box-login-con form-wrap">
    <!--กลับหน้าindex web หลัก-->
    <div class="index-head">
      <h3>{{ __('สถานะการสมัครบัตร') }}</h3>
    </div>
    <div class="bg-box-status">
      <div class="box-input-form">
        <div class="form-group box-input-form txt-desc-input">
          <label class="status-sub-title choose-card">{{ __('เลือกบัตร') }}</label>
          <input id="card-check" type="text"  readonly>
          <div tabindex="0" id="choice-wrap" class="choice-wrap">
              @foreach($appStatusList_fronts as $key => $appStatusList_front)
              <div class="choice-list" data-p_code="{{$appStatusList_front['app_code'].$appStatusList_front['p_code'].$key}}">
                {{--
                <div class="choice-img-wrap">
                  @if(isset($appStatusList_front['p_pic']->pic))
                    <img src="{{customAsset('storage')}}/{{$appStatusList_front['p_pic']->pic}}">
                  @else
                    <img src="{{customAsset('img/')}}{{'creditcard-placeholder.png'}}">
                  @endif
                </div>
                --}}
                <p class="card-title">{!! preg_replace("/\s+/u"," ",$appStatusList_front['p_title']) !!}</p>
              </div>
              @endforeach
          </div>
          <div id="choice-mask"></div>
          <span class="select-arrow"></span>
        </div>
      </div>
      <div class="result-items">
        @foreach($appStatusList_fronts as $key => $appStatusList_front)
        <div class="result-item text-center {{$appStatusList_front['app_code'].$appStatusList_front['p_code'].$key}}">
          <div class="box-input-form txt-err-padd txt-desc-input only-na">
            <div class="text-center show-status">
              <div class="icon-status">

                <img src="{{ customAsset('/img/ico-status-process.svg') }}">
              </div>
              <div class="pass-state">
                <p class="left-bold">{{ __('KTC ได้รับเอกสารการสมัครแล้ว') }}</p>
                <p class="left-thin">วันที่ {!! $appStatusList_front["date"] !!}</p>
              </div>
            </div>
            <div class="text-center show-status {{ ($appStatusList_front["state"]==2?"active":"") }}">
              <div class="icon-status">
                <img src="{{ customAsset('/img/ico-status-process.svg') }}">
              </div>
              <div class="pass-state">
                @if($appStatusList_front["result"]!=="N/A")
                  <p class="left-bold">{!! $appStatusList_front["result"] !!}</p>
                @endif
                <!-- <p class="left-thin">วันที่ 14 ก.ย. 2561 เวลา 13.00 น.<br> -->
                @if($appStatusList_front["result"]!=="-" && $appStatusList_front["state"]==2)
                <p class="left-thin">{!! preg_replace("/\s+/u"," ",$appStatusList_front["reason"]) !!}</p>
                @endif
              </div>
            </div>
            <div class="text-center show-status {{ ($appStatusList_front["state"]==3?"active":"") }}">
              <div class="icon-status">
                @if($appStatusList_front["result"]=="Declined" || $appStatusList_front["result"]=="Cancelled" || $appStatusList_front["result"]=="ไม่ผ่านการพิจารณาอนุมัติ" || $appStatusList_front["result"]=="ยกเลิกการสมัคร")
                <img src="{{ customAsset('/img/ico-status-not-success.svg') }}">
                @else
                 <img src="{{ customAsset('/img/ico-status-success.svg') }}">
                @endif
              </div>
              <div class="pass-state">
                <p class="left-bold">{!! $appStatusList_front["result"] !!}</p>
                <!-- <p class="left-thin disable">วันที่ 31 ต.ค. 2561 เวลา 20.00 น.</p> -->
                @if($appStatusList_front["result"]!=="-" && $appStatusList_front["state"]==3)
                <p class="left-thin">{!! preg_replace("/\s+/u"," ",$appStatusList_front["reason"]) !!}</p>
                @endif
              </div>
            </div>
          </div>
        </div>
          @endforeach
      </div>
    </div>
    <div class="result-items">
        <button class="btn-next-step only-oap btn-w-link" data-target="{{parentURLGenerate()}}">{{ __('กลับสู่หน้าหลัก') }}</button>
    </div>
  </form>
@endsection