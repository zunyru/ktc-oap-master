@extends('frontend.app-status.master')
@section('title', "KTC Application Status")
@section('page-content')
  <form method="POST" id="pg-regis" class="row box-login-con form-wrap" autocomplete="false">
      <div class="index-head">
          <h3>{{ __('ตรวจสอบสถานะการสมัครบัตร') }}</h3>
      </div>
      <div class="box-input-form txt-err-padd txt-desc-input">
          <input class="required" type="text" placeholder="{{ __('รหัส OTP ที่ได้รับทางโทรศัพท์มือถือ') }}"  maxlength="6" name="otp_code">
          <p>{{ __('รหัสอ้างอิง') }}: {{$otp_ref}}</p>
          <p>{{ __('OTP สำหรับยืนยันหมายเลขโทรศัพท์จะถูกส่งไปที่') }} {{$phone_no}}</p>
          <p>{{ __('โดยรหัสผ่านมีอายุ 3 นาที') }}</p>
      </div>
      <div class="box-other-form txt-desc-input">
          <p id='inline-ei-ei'>{{ __('หากคุณยังไม่ได้รับรหัส OTP กรุณากด') }}</p>
            <a href="javascript:void(0);" onclick="$('#otp-resend').submit();"><img src="{{ customAsset('/img/ico-refresh.png') }}">{{ __('ขอรหัส OTP อีกครั้ง') }}</a>

      </div>
      <div>
          <button type="submit" class="btn-next-step" value="">{{ __('ถัดไป') }}</button>
      </div>
      <input type="hidden" name="_token" value='{{ csrf_token() }}'>
  </form>
  <form action="{{ route('app-status.otp-resend') }}" id="otp-resend" method="POST">
    <input type="hidden" name="_token" value='{{ csrf_token() }}'>
  </form>
@endsection
@if($verify_response=="otp_invalid")
  @push('page-styles')
    <link rel="stylesheet" href="customAsset('/plugins/snackbar/snackbar.min.css')">
    <style type="text/css">
      .snackbar-pos.bottom-center{
        margin:0px;
        width:100vw !important;
        max-width:100vw !important;
        padding: 2vh 5vw;
        background: rgb(191, 34, 60) !important;
        font-family: 'Conv_SukhumvitTadmai_Tex';
      }
      .snackbar-pos.bottom-center .action{
        color: #bbbbbb !important;
      }
    </style>
  @endpush
  @push('page-scripts')
        <script src="{{ customAsset('/plugins/snackbar/snackbar.min.js') }}"></script>
        <script type="text/javascript">
          jQuery( document ).ready(function($) {
            Snackbar.show({ actionTextColor: '#ff0000',duration:0,pos:'bottom-center',text:"{{ __('OTP ไม่ถูกต้อง') }}",actionText:"{{ __('ปิด') }}" });
          });
        </script>
   @endpush
@endif