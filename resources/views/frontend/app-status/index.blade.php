@extends('frontend.app-status.master')
@section('title', "KTC ".__('ตรวจสอบสถานะการสมัครบัตร'))
@push('page-scripts')
  <script type="text/javascript">
    jQuery( document ).ready(function($) {
      $('#idcard input').mask('0 0000 00000 00 0');
      // $('#passport input').mask('ABC CCC0B', {
      //   'translation': {
      //     A: {pattern: /[A-QP-WY]/},
      //     B: {pattern: /[1-9]/},
      //     C: {pattern: /[0-9]/}
      //   },
      //   placeholder: "___.___.___.___"
      // });
    });
  </script>
  <script src="{{ customAsset('/plugins/jquery-validator/jquery.form-validator.min.js') }}"></script>
  <script type="text/javascript">
  jQuery( document ).ready(function($) {
      var validation_lang="{{session('local')}}";
    var config = {
        lang:validation_lang,
        borderColorOnError: '#f44336',
        // language: validateTH_Lang,
        validateOnBlur: true,
        validateHiddenInputs: false,
        // ordinary validation config
        form: 'form',
        // reference to elements and the validation rules that should be applied
        validate: {
            ".citizen_code-validation": {
                validation: 'required, length',
                length : '17-17',
                '_data-sanitize': 'trim',
                'error-msg-required': function(){
                    if (validation_lang=="en") {
                        return "Please specify Citizen ID";
                    }else{
                        return "กรุณาระบุหมายเลขบัตรประชาชน 13 หลัก";
                    }
                },
                'error-msg-length': function(){
                    if (validation_lang=="en") {
                        return "Citizen ID is incorrect";
                    }else{
                        return "หมายเลขบัตรประชาชนไม่ถูกต้อง";
                    }
                }
            },
            ".passport-validation": {
                validation: 'required',
                '_data-sanitize': 'trim',
                'error-msg-required': function(){
                    if (validation_lang=="en") {
                        return "Please specify Passport No.";
                    }else{
                        return "กรุณาระบุหมายเลขหนังสือเดินทาง";
                    }
                }
            },
        }
    };

    $.validate({
        modules: 'jsconf, toggleDisabled',
        onModulesLoaded: function() {
            $.setupValidation(config);
        }
    });
    setTimeout(function(){
        $("#passport>input").removeAttr( "data-validation");
    }, 1000);
  });
  </script>
@endpush
@if(session()->get('noti',NULL))
    @push('page-scripts')
      <script src="{{ customAsset('/plugins/snackbar/snackbar.min.js') }}"></script>
      <script type="text/javascript">
        jQuery( document ).ready(function($) {
          Snackbar.show({ actionTextColor: '#ff0000',duration:0,pos:'bottom-center',text:"{{ __('ไม่พบข้อมูลการสมัครของท่าน กรุณาตรวจสอบใหม่อีกครั้ง') }}",actionText:"{{ __('ปิด') }}" });
        });
      </script>
    @endpush
    @push('page-styles')
      <link rel="stylesheet" href="{{ customAsset('/plugins/snackbar/snackbar.min.css')}}">
      <style type="text/css">
        .snackbar-pos.bottom-center{
          margin:0px;
          width:100vw !important;
          max-width:100vw !important;
          padding: 2vh 5vw;
          background: rgb(191, 34, 60) !important;
          font-family: 'Conv_SukhumvitTadmai_Tex';
        }
        .snackbar-pos.bottom-center .action{
          color: #bbbbbb !important;
        }
      </style>
    @endpush
@endif
@section('page-content')
  <form action="{{ route('app-status.otp-req') }}" method="POST" id="pg-regis" class="row box-login-con form-wrap app-check-form" autocomplete="false">
    <div class="index-head">
      <h3>{{ __('ตรวจสอบสถานะการสมัครบัตร') }}</h3>
    </div>
    <div id="type1" class="box-input-form">
      <div class="form-group">
        <label for="citizen-code-type">{{ __('เลือกประเภท') }}</label>
        <select class="form-control" id="citizen-code-type">
          <option class="select-item" id="idcardSelected" value="idcard">{{ __('หมายเลขบัตรประชาชน') }}</option>
          <option class="select-item">{{ __('หมายเลขหนังสือเดินทาง_label') }}</option>
        </select>
      </div>
      <div class="box-input-form" id="idcard">
        <input name="citizen_code" class="citizen_code-validation" type="tel" placeholder="{{ __('หมายเลข 13 หลัก') }}">
      </div>
      <div class="box-input-form" id="passport">
        <input name="" class=" passport-validation" type="text" placeholder="{{ __('หมายเลขหนังสือเดินทาง') }}"  onkeyup="this.value = this.value.toUpperCase();">
      </div>
      <!-- ขึ้นยอมรับ -->

      <div class="box-other-form">
        <button class="btn-next-step" id="" type="submit">{{ __('ถัดไป') }}</button>
      </div>
    </div>
    <input type="hidden" name="_token" value='{{ csrf_token() }}'>
  </form>
@endsection