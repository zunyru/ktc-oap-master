@include('frontend.master.header')
@include('frontend.master.menu')
	<!-- start page -->
	<div class="bg-login app-check-page">
	  <div class="content-wrap oap app-check">
	  	@yield('page-content')
	  </div>
	</div>
@include('frontend.master.footer')
@include('frontend.master.footer-scripts')