@push('page-styles-footer')
<link href="{{customAsset('/plugins/perfect-scrollbar/perfect-scrollbar.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('page-scripts')
<script src="{{ customAsset('/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}" type="text/javascript"></script>
@endpush
@if($page_model->layout === 'card_hero')
<div class="oap-left bg-grey toggle-height-screen card-hero">
	<header id="header-card-detail" class="page-merchant-inner-header">
		<div class="proud-head" style="background-image: url('{{Voyager::image($page_model->formLayout->card_cover)}}');">
			@if($page_model->formLayout->setting !== 'on')
			<img src="{{Voyager::image($page_model->formLayout->card_hero)}}">
			@else
			<img src="{{Voyager::image($page_model->product->pic)}}">
			@endif
		</div>
		<div class="content-wrap center-responsive">
			<h1 class="header-title">{{ $page_model->lang()->title_page }}</h1>
			<h2 class="header-desc only-desktop">{{ $page_model->lang()->description }}</h2>
			<div class="content-desc only-desktop">
				<div class="row">

					@php
                      $data = $page_model->formLayout->lang()->features;
                      $features = json_decode($data);
                      $cal = 12/sizeof($features->title);
					@endphp

					@if(sizeof($features->title) !== 0)
					   @for($i=0; $i < sizeof($features->title); $i++)

					   <div class="col-lg-{{ $cal }} text-center">
						<h2>{{ $features->title[$i] }}</h2>
						<!-- <h2>x<span>2</span></h2> -->
						<h3>{{ $features->sub[$i] }}</h3>
					   </div>

					   @endfor
					@endif
				</div>
			</div>
			<div class="content-desc only-mobile">
				<div class="row">

					@if(sizeof($features->title) !== 0)
					   @for($i=0; $i < sizeof($features->title); $i++)

                       @php
                           $cal_mb = 6;
	                       if(sizeof($features->title) === 1){
	                       	 $cal_mb = 12;
	                       }elseif(sizeof($features->title) === 3){
	                       	 $cal_mb = 6;
	                       	 if($i === 2){
	                       	 	 $cal_mb = 12;
	                       	 }
	                       }

					   @endphp

					   <div class="col-{{ $cal_mb }} col-sm-{{ $cal }} text-center">
						<h2>{{ $features->title[$i] }}</h2>
						<h3>{{ $features->sub[$i] }}</h3>
					   </div>

					   @endfor
					@endif

				</div>
			</div>
			<div id="sticky-trigger"></div>
		</div>
	</header>
	{{-- bodt content --}}
	<div class="body_content">

		{!! $page_model->lang()->content !!}

    </div>
    {{-- end body content --}}
	<div class="toggle-height-button less">
		<p>
			<span class="text-less">{{ __('อ่านรายละเอียดเพิ่มเติม') }}</span>
			<span class="text-more">{{ __('ซ่อนรายละเอียดเพิ่มเติม') }}</span>
			<i></i>
		</p>
	</div>
</div>
@elseif($page_model->layout === 'slide_hero')
<div class="oap-left bg-grey toggle-height-screen slide-hero">
	<div class="slide-wrap promo-wrap no-preview">
		<div class="index-slide1">
			@php
				$data = $page_model->formLayout->card_slide;
                $slide = json_decode($data, true);
                if (!isset($slide["default"])) {
                    $slide_arr = $slide;
                } else {
                	$slide_arr = (session('local') == 'en' && isset($slide["en"]) && count($slide["en"]) > 0) ? $slide["en"] : $slide["default"];
                }
			@endphp

            @foreach($slide_arr as $key => $item)
			<div class="item">
				<img src="{{Voyager::image($item)}}">
			</div>
		    @endforeach
		</div>
	</div>
	<div class="content-wrap oap-content">
		<!-- <h2 class="left-title"> -->
		<h1 class="left-title">
			{{ $page_model->formLayout->lang()->heading }}
		</h1>
		<p class="left-desc">
			{!! $page_model->formLayout->lang()->sub_heading !!}
		</p>
	</div>
	<div class="toggle-height-button less">
		<p>
			<span class="text-less">{{ __('อ่านรายละเอียดเพิ่มเติม') }}</span>
			<span class="text-more">{{ __('ซ่อนรายละเอียดเพิ่มเติม') }}</span>
			<i></i>
		</p>
	</div>
	<div class="toggle-height-mobile less">
		<p>
			<span class="text-less">{{ __('อ่านรายละเอียดเพิ่มเติม') }}</span>
			<span class="text-more">{{ __('ซ่อนรายละเอียดเพิ่มเติม') }}</span>
			<i></i>
		</p>
	</div>
</div>
@push('page-scripts')
<script type="text/javascript" src="{{customAsset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.index-slide1').owlCarousel({
			loop:true,
			items:1,
			nav:true,
			margin:0,
			autoplay:true,
			autoplayTimeout:7000,
			autoplayHoverPause:true
		});
	});
</script>
@endpush
@endif