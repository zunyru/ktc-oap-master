  <!-- แบบฟอร์มสมัคร -->
  <div  class="content-wrap blank-form">
    <h2 class="content-title">{{ $page_model->leaveForm->lang()->content_heading }}</h2>
    <p class="sub-title">{{ $page_model->leaveForm->lang()->content_sub_heading }}</p>

    <form action="{{ URL('oap/lead-submit') }}" method="{{ $method }}" id="" class="row  form-wrap form-wrap-regis" autocomplete="off">
      @csrf
      <input type="hidden" name="page" value="{{ $page_model->id }}">
      <input type="hidden" name="product" value="{{ $page_model->Product_id }}">
      <input type="hidden" name="segment" value="{{ $page_model->slug }}">
      <div class="col-sm-6 col-12 box-input-form">
        <input class="required text_only" type="text" placeholder="{{ __('ชื่อ') }}" name="fname">
      </div>
      <div class="col-sm-6 col-12 box-input-form">
        <input class="required text_only" type="text" placeholder="{{ __('นามสกุล') }}" name="lname">
      </div>
      <div class="col-sm-6 col-12 box-input-form">
            <input class="required phone_with_ddd" type="tel" name="tel" inputmode="numeric" placeholder="{{ __('หมายเลขโทรศัพท์ (10 หลัก)') }}">
      </div>
      <div class="col-sm-6 col-12 box-input-form">
        <input class="required" type="text" id="txtEmail" name="email" maxlength="50" placeholder="{{ __('อีเมล') }}">
      </div>
      <div class="col-sm-6 col-12 box-input-form">
        <label class="before-dropdown">{{ __('รายได้ต่อเดือน') }}</label>
        <select class="form-control" name="salary" id="salary">
          <option  selected value="">- {{ __('กรุณาเลือก') }} -</option>
          @foreach($salaries as $index => $salary)
          <option value="{{ $salary->id }}">{{ $salary->lang()->title }}</option>
          @endforeach
        </select>
      </div>
      @if($convenient_times && 1==2)
      <div class="col-sm-6 col-12 box-input-form">
        <label class="before-dropdown">{{ __('เวลาที่สดวกติดต่อกลับ') }}</label>
         <select class="form-control" name="convenient_times" id="convenient_times">
            <option value="">- {{ __('กรุณาเลือก') }} -</option>
            @foreach($optionsTime as $index=> $times)
             @php
             if($times['time_type'] == 1):
                $mesege_lang  = __('ติดต่อกลับภายใน',['minite' => $times['time_text']] );
             elseif($times['time_type'] == 2):
                $mesege_lang  = __('ช่วงเวลา',['minite' => $times['time_text']]);
             else:
                 $mesege_lang  = __('หลังเวลา',['minite' => $times['time_text']]);
             endif;

            @endphp
            <option value="{{ $times['operand_id'] }}">{{ $mesege_lang }} </option>
            @endforeach
          </select>
      </div>
      @endif
      @php
        $cf_1 = $page_model->leaveForm->customField(1);
        $cf_2 = $page_model->leaveForm->customField(2);
      @endphp
      @if(!is_null($cf_1) && $cf_1['label_name'] !=NULL)
        <div class="col-sm-6 col-12 box-input-form">
          <input type="text" id="cf1" name="{{ "kcf1_".$cf_1['key_name'] }}" placeholder="{{ $cf_1['label_name']}}" {{($cf_1['opt']=='required'?"required":NULL)}}>
          <input type="hidden" name="cf1" value="{{ $cf_1['key_name'] }}">
        </div>
      @endif
      @if(!is_null($cf_2) && $cf_2['label_name'] !=NULL)
        <div class="col-sm-6 col-12 box-input-form">
          <input type="text" id="cf2" name="{{ "kcf2_".$cf_2['key_name'] }}" placeholder="{{ $cf_2['label_name'] }}" {{($cf_2['opt']=='required'?"required":NULL)}}>
          <input type="hidden" name="cf2" value="{{ $cf_2['key_name'] }}">
        </div>
      @endif
      <div class="input-notice col-12">@yield('content-condition')</div>
      <div class="btn-wrap">
        <button type="submit" class="card-register-center">{{ __('ส่งข้อมูล') }}</button>
      </div>
      <input type="hidden" name="custom_param" value='{{$page_model->custom_param}}'>
      <input type="hidden" name="iframe" value="{{$iframe}}">
      <input type="hidden" name="pcode" value="{{$pcode}}">
      <input type="hidden" name="callback" value="{{$callback ?? NULL}}">
        <input type="hidden" name="branch_code" value='{{$page_model->branch->code ?? NULL}}'>
        {!! Honeypot::generate('spam_lead', 'spam_time_lead') !!}
    </form>

@push('page-scripts')
<script src="{{ customAsset('/plugins/jquery-validator/jquery.form-validator.min.js') }}"></script>
<script src="{{ customAsset('/plugins/jquery-validator/conf-validate-TH-lang.js') }}"></script>
@endpush