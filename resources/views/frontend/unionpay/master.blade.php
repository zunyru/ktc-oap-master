@include('frontend.master.header')
@if(isset($Product_ID))
  @include('frontend.unionpay.menu')
@else
  @include('frontend.master.menu')
@endif
	<!-- start page -->
	<div class="bg-unionpay oap-wrap">
	  <div class="content-wrap oap app-check">
	  	@yield('page-content')
	  </div>
	</div>

@include('frontend.master.footer-scripts')