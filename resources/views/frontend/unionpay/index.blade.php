@extends('frontend.unionpay.master')
@section('title', "KTC ".__('UNIONPAY Credit Card'))

@section('page-content')
    <div class="content-unionpay">
    <h3 class="mt-3 heder-txt mb-1">สวัสดี คุณ{{$Cust_Name}}</h3>
        <p class="lead-p">ขอบคุณที่สนใจสมัครบัตรเครดิต {{ ($Product_ID === 'UPPP01' ? 'KTC UNIONPAY PLATINUM' : 'KTC UNIONPAY DIAMOND')  }}  โดยการสมัครนี้คุณจะได้รับวงเงินภายใต้วงเงินเดียวกันกับวงเงินปัจจุบันของคุณ และบัตรฯ จะถูกส่งไปยังที่อยู่ที่ระบุในใบแจ้งยอดค่าใช้จ่ายที่ได้แจ้งไว้กับ KTC ทั้งนี้การสมัครบัตรฯ ดังกล่าวถือว่าคุณได้ยอมรับข้อกําหนดและเงื่อนไขการใช้บริการที่เกี่ยวกับบัตรเครดิต KTC แล้ว </p>

        <div class="text-center my-3">
             <img class="img-fluid img-small" src="{{Voyager::image($image->pic)}}" alt="" title="">
        </div>

        <h3 class="mt-3 heder-txt mb-1">สิทธิประโยชน์ของบัตรฯ</h3>
        @if($Product_ID === 'UPPP01')
        <div class="content">
            <p class="mt-2">รับคะแนน KTC FOREVER x2 เมื่อใช้จ่ายเป็นสกุลเงินท้องถิ่นที่จีน ฮ่องกง มาเก๊า และไต้หวัน (CNY/HKD/MOP/TWD)</p>
            <p class="mt-2">
                พร้อมรับส่วนลดและสิทธิประโยชน์มากมายในต่างประเทศ อาทิ ร้านอาหาร Jade Garden ที่จีน, Shanghai Disneyland ที่จีน, Sasa ที่ฮ่องกง, Outlets ชั้นนําในยุโรป อาทิ Bicester Village ที่อังกฤษ, The Mall Luxury ที่อิตาลี และห้างปลอดภาษี Benlux Tax Free Shop ที่ฝรั่งเศส
            </p>

            <p class="mt-2">
               และส่วนลดและสิทธิประโยชน์ในประเทศไทย อาทิ BTS, Grab, Major Cineplex และซูเปอร์มาร์เก็ตชั้นนํา อาทิ Gourmet Market, Home Fresh Mart, MaxValu และ Villa Market (ศึกษาข้อกําหนด และเงื่อนไขของคะแนน KTC FOREVER ได้ที่ www.ktc.co.th/rewards)
            </p>

            <p class="mt-2">สอบถามข้อมูลผลิตภัณฑ์และสิทธิประโยชน์เพิ่มเติมได้ที่ KTC PHONE <a class="pain-link" href="tel:021235000"> 02 123 5000 </a></p>
        </div>
        @else
        <div class="content">
            <ul>
                <li>รับคะแนน KTC FOREVER x2 เมื่อใช้จ่ายเป็นสกุลเงินต่างประเทศ </li>
                <li>ฟรีบริการห้องรับรอง Royal Silk Lounge 2 ครั้งต่อปี </li>

            </ul>

            <p class="mt-2">
                พร้อมรับส่วนลดและสิทธิประโยชน์มากมายในต่างประเทศ อาทิ ร้านอาหาร Jade Garden ที่จีน, Shanghai Disneyland ที่จีน, Sasa ที่ฮ่องกง, Outlets ชั้นนําในยุโรป อาทิ Bicester Village ที่อังกฤษ, The Mall Luxury ที่อิตาลี และห้างปลอดภาษี Benlux Tax Free Shop ที่ฝรั่งเศส 
            </p>

            <p class="mt-2">
                และส่วนลดและสิทธิประโยชน์ในประเทศไทย อาทิ BTS, Grab, Major Cineplex และซูเปอร์มาร์เก็ตชั้นนํา อาทิ Gourmet Market, Home Fresh Mart, MaxValu และ Villa Market (ศึกษาข้อกําหนด และเงื่อนไขของคะแนน KTC FOREVER ได้ที่ www.ktc.co.th/rewards)
            </p>

            <p class="mt-2">สอบถามข้อมูลผลิตภัณฑ์และสิทธิประโยชน์เพิ่มเติมได้ที่ KTC EXCLUSIVE SERVICE  <a class="pain-link" href="tel:021235444"> 02 123 5444 </a></p>
        </div>

        @endif


    </div>
  <div class="footer">
      <div class="form-notice line">
        <form action="{{ route('unionpay-thankyou',['namepage' => $slug]) }}" method="post"  class="" autocomplete="off">
            @csrf
            <button id="long-button" class="btn-unionpay" type="submit" >{{ __('ยืนยันการสมัคร') }}</button>
        </form>
      </div>
   </div>
@endsection
