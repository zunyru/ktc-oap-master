@extends('frontend.unionpay.master')
@section('title', "KTC ".__('UNIONPAY Credit Card - Thank you'))

@section('page-content')
    <div class="content-unionpay text-center">
        @if(isset($dup))
            <h3 class="mt-5 heder-txt ">ระบบได้รับข้อมูลของคุณเรียบร้อยแล้ว </h3>
            <h4 class="mt-2 heder-txt-date">เมื่อวันที่ {{formatDateThat($dup->created_at)}}</h4>
        @else
          <h3 class="mt-5 heder-txt ">ขอบคุณที่ไว้วางใจเลือกใช้ผลิตภัณฑ์และบริการจาก KTC</h3>
        @endif

        <div class="my-3">
            <img class="img-fluid img-small" src="{{Voyager::image($image->pic)}}" alt="" title="">
        </div>


        @if($Product_ID === 'UPPP01')
            <p class="mb-3 mt-3">
            หากต้องการติดต่อเจ้าหน้าที่ <br>เพื่อสอบถามสถานะการสมัคร<br>KTC PHONE <a class="pain-link" href="tel:021235000"> 02 123 5000 </a>
            </p>
        @else
            <p class="mb-3 mt-3">
            หากต้องการติดต่อเจ้าหน้าที่ <br> เพื่อสอบถามสถานะการสมัคร <br> KTC EXCLUSIVE SERVICE <br><a class="pain-link" href="tel:021235444"> 02 123 5444 </a>
            </p>
        @endif
    </div>
  {{-- <div class="footer">
      <div class="form-notice">
       <div class="box-other-form checkbox-form">
            <div class="btn-wrap">
             <button id="btn-thankyou" class="btn-unionpay" >{{ __('เข้าสู่เว็บไซต์ KTC') }}</button>
            </div>
        </div>
      </div>
   </div> --}}
@endsection
@push('page-scripts')
  <script type="text/javascript">
    jQuery( document ).ready(function($) {
        $('#btn-thankyou').click(function() {
           window.location = 'https://www.ktc.co.th';
        });
    });
  </script>
@endpush