@extends('frontend.master.templates')

@section('title', $page_model->lang()->title_page." | KTC")

@section('title-head', $page_model->leaveForm->lang()->content_heading)

@section('sub-head', $page_model->leaveForm->lang()->content_sub_heading)

@section('content-condition')
	@if($page_model->leaveForm->ch === 0)
	{!! $page_model->leaveForm->lang()->content_condition !!}
	@else
		<div class="box-other-form checkbox-form">
			<label>
				<input type="checkbox" name="term_acceptance" autocomplete="false">
				<span class="checkmark"></span>
			</label>
			{!! $page_model->leaveForm->lang()->content_condition !!}
		</div>
	@endif
@stop
@section('content-contact')
{!!$page_model->leaveForm->lang()->content_contact !!}
<div class="mg-bottom"></div>
@stop
{{-- Request::segment(1) --}}
{{-- URL::current() --}}