@php
  $custom_script=json_decode(setting('pages.custom_script'));
@endphp
@php
  $data['thankyou_title']=true;
@endphp
@include('frontend.master.header',$data)
@include('frontend.master.menu')
@if(session()->get('iframe',false))
  <script type="text/javascript">
    if (self != top) {
      parent.location.href="{{session('thankyou_callback','')}}";
    }
  </script>
  <style type="text/css">
    #menu-global,.oap-footer{
      display: none;
    }
  </style>
@else
{{-- start page --}}
<div id="oap-loan-success" class="bg-login oap-success-wrap">
  <div id="proud-regis-complete" class="content-wrap proud-regis-complete">
    <p class="desc-title oap-top-padd">{{__("ระบบได้รับข้อมูลของคุณเรียบร้อยแล้ว")}}
     @if (session('dates'))
     <span>
      {{-- เมื่อวันที่ 24 ก.ย. 61 --}}
      {{ __("เมื่อวันที่")}} {{ session('local')=="th" ? formatDateThat(session('dates')) : formatDateEng(session('dates')) }}
    </span>
    @endif
  </p>
  @if(session('product_type')!=="CS-Tele")
  <p class="desc-result txt-on-desktop">{{__("หากต้องการติดต่อเจ้าหน้าที่เพื่อสอบถามสถานะการสมัคร")}} {{__("โทร.")}} 02 123 6000<span class="text-en-el">.</span><br>{{__("ขอบคุณที่ไว้วางใจเลือกใช้ผลิตภัณฑ์และบริการจาก KTC")}}<span class="text-en-el">.</span></p>
  <p class="desc-result txt-on-mobile">{{__("หากต้องการติดต่อเจ้าหน้าที่เพื่อสอบถามสถานะการสมัคร")}} {{__("โทร.")}} 02 123 6000<span class="text-en-el">.</span> {{__("ขอบคุณที่ไว้วางใจเลือกใช้ผลิตภัณฑ์และบริการจาก KTC")}}<span class="text-en-el">.</span></p>
  @else
    <p class="desc-result txt-on-desktop">{{__("หากต้องการติดต่อเจ้าหน้าที่")}} {{__("โทร.")}} 02 123 5300<span class="text-en-el">.</span><br>{{__("ขอบคุณที่ไว้วางใจเลือกใช้ผลิตภัณฑ์และบริการจาก KTC")}}<span class="text-en-el">.</span></p>
    <p class="desc-result txt-on-mobile">{{__("หากต้องการติดต่อเจ้าหน้าที่")}} {{__("โทร.")}} 02 123 5300<span class="text-en-el">.</span> {{__("ขอบคุณที่ไว้วางใจเลือกใช้ผลิตภัณฑ์และบริการจาก KTC")}}<span class="text-en-el">.</span></p>
  @endif
  <div class="btn-wrap">
    <a href="{{parentURLGenerate()}}"><button class="card-register-center">{{__("กลับสู่หน้าหลัก")}}</button></a>
  </div>
  @if(session('product_type')!=="CS-Tele")
  <div class="col-12 col-sm-8 col-lg-6 app-download">
    <div class="app-download-box">
      <div class="media">
        <img class="align-self-start" src="{{customAsset('img/KTC-promo-2019.png')}}">
        <div class="media-body">
          <h5 class="mt-0">{{__("ติดตามสถานะการสมัครผ่าน")}}</h5>
          <p>KTC Promo</p>
        </div>
      </div>
    </div>
    <div class="btn-app-download">
      <p>{{__("สามารถดาวน์โหลดได้ที่")}}</p>
      <a href="https://itunes.apple.com/th/app/ktc-mobile-tapktc/id723657868?mt=8"><img class="align-self-start" src="{{customAsset('img/ico-app-store.png')}}"></a>
    <a href="https://play.google.com/store/apps/details?id=com.tapktc.app"><img class="align-self-start" src="{{customAsset('img/ico-google-play.png')}}"></a>
    </div>
  </div>
  @endif
</div>
</div>
<img style="display:none;" src="{{session('callback_3rd','')}}">
@endif
{{--end page--}}

{{--start Footer--}}

@include('frontend.master.footer')
@include('frontend.master.footer-scripts')
{{--end Footer--}}