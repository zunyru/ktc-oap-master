@extends('frontend.visa.master')
@section('title',  $products->title .__(' - Thank you'))

@section('page-content')
    <div class="content-unionpay text-center">
        @if(isset($dup))
            <h3 class="mt-5 heder-txt ">{!! $product->offer_mes_duplicate !!} </h3>
            <h4 class="mt-2 heder-txt-date">เมื่อวันที่ {{formatDateThat($dup->created_at)}}</h4>
        @else
          <h3 class="mt-5 heder-txt ">{!! $product->offer_mes_thank !!}</h3>
        @endif

        <div class="my-3">
            <img class="img-fluid img-small" src="{{Voyager::image($image->pic)}}" alt="" title="">
        </div>
       
       @if(isset($dup))
          <p class="mb-3 mt-3">
               {!! $product->offer_mes_dup_contact !!}
          </p>
       @else
           <p class="mb-3 mt-3">
               {!! $product->offer_mes_thank_contact !!}
          </p>  
       
       @endif
          
    </div>
  {{-- <div class="footer">
      <div class="form-notice">
       <div class="box-other-form checkbox-form">
            <div class="btn-wrap">
             <button id="btn-thankyou" class="btn-unionpay" >{{ __('เข้าสู่เว็บไซต์ KTC') }}</button>
            </div>
        </div>
      </div>
   </div> --}}
@endsection
@push('page-scripts')
  <script type="text/javascript">
    jQuery( document ).ready(function($) {
        $('#btn-thankyou').click(function() {
           window.location = 'https://www.ktc.co.th';
        });
    });
  </script>
@endpush