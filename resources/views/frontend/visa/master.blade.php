@include('frontend.master.header')
<style type="text/css">
	.visa strong{
		font-weight: bold !important;
	}
	.visa em{
		font-style: italic !important;
	}
</style>
@if(isset($Product_ID))
  @include('frontend.visa.menu')
@else
  @include('frontend.master.menu')
@endif
	<!-- start page -->
	<div class="bg-unionpay oap-wrap">
	  <div class="content-wrap oap app-check">
	  	@yield('page-content')
	  </div>
	</div>

@include('frontend.master.footer-scripts')