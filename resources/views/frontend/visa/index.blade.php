@extends('frontend.visa.master')
@section('title',  $products->title . __('VISA Credit Card'))

@section('page-content')
    <div class="content-unionpay">
    <h3 class="mt-3 heder-txt mb-1">สวัสดี คุณ{{$Cust_Name}}</h3>
        <p class="lead-p">{!! $products->offer_description !!}</p>

        <div class="text-center my-3">
             <img class="img-fluid img-small" src="{{Voyager::image($products->pic)}}" alt="" title="">
        </div>

        {{-- <h3 class="mt-3 heder-txt mb-1">สิทธิประโยชน์ของบัตรฯ</h3> --}}
    
        <div class="content">
            <div>
                {!! $products->offer_privilege !!}
            </div>

            <p class="mt-2">
                {!! $products->offer_contact !!}
            </p>
        </div>


    </div>
  <div class="footer">
      <div class="form-notice line">
        <form action="{{ route('visa-thankyou',['product' => $offer,'namepage' => $slug]) }}" method="post"  class="" autocomplete="off">
            @csrf
            <button id="long-button" class="btn-unionpay btn-visa" type="submit" >{{ __('ยืนยันการสมัคร') }}</button>
        </form>
      </div>
   </div>
@endsection
