 <div class="content-wrap last-piece for-login-pg oap-footer">
  <div class="row">
        <div class="col-10 col-lg-6 license">{{__('สงวนลิขสิทธิ์')}} ©
            {{(session('local')==="en"?date("Y"):date("Y")+543)}} {{__('บริษัท บัตรกรุงไทย จำกัด (มหาชน)')}}</div>
    @if(hideHeader())
    <div class="col-2 col-lg-6 text-right">
      <a class="lang-footer">
        <div class="lang-wrap {{(session('local')=="th"?"active":NULL)}}">
                   {{--   <label for="lang-change">Language</label>  --}}
          <p id="lang-change" data-local="en_US">EN</p>
        </div>
        <div class="lang-wrap {{(session('local')=="en"?"active":NULL)}}">
                  {{--    <label for="lang2-change">Language</label>  --}}
          <p id="lang2-change" data-local="th_TH">TH</p>
        </div>
      </a>
    </div>
    @endif
  </div>
</div>
<div id="ieDialog" class="modal fade full-modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
          <h5>www.ktc.co.th ไม่รองรับเบราว์เซอร์ Internet Explorer หากดำเนินการต่อ การใช้งานในบางเมนู/รายการอาจไม่สมบูรณ์</h5>
        </div>
        <div class="modal-footer">
            <a href="#" onclick="window.close();return false;">
               <button type="button" class="btn btn-danger">ปิด</button>
            </a>
            <button type="button" class="btn btn-light" data-dismiss="modal" id="modal-btn-si">เข้าสู่เว็บไซต์</button>
        </div>
      </div>
    </div>
</div>
<div class="to-top ghost-footer">↑</div>
</div>
{{--block KTC--}}