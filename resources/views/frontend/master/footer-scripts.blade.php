<!-- <script type="text/javascript" src="{{customAsset('js/all.min.js')}}"></script> -->
<script type="text/javascript" src="{{customAsset('js/jquery-1.12.4.js')}}"></script>
<script type="text/javascript" src="{{customAsset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{customAsset('js/win-ui.js')}}"></script>
<script type="text/javascript" src="{{customAsset('js/jquery.mask.min.js')}}"></script>
<script type="text/javascript" src="{{customAsset('plugins/browser-detect/browser-detect.umd.js')}}"></script>
<script type="text/javascript" src="{{customAsset('plugins/js-cookie/js.cookie.min.js')}}"></script>
	@stack('page-styles-footer')
	@stack('page-scripts')
    {!!$custom_script->footer ?? NULL!!}

@include('backend.javascript.handling-expired-token');
</body>
</html>