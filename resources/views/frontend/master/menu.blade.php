<div id="menu-global" class="menu-global full-width-menu alway-mini mini-header ">
    <div class="content-wrap">
        <div id="ktc-logo">
           <a href="{{parentURLGenerate()}}">
              <img style="max-width: 61px;" src="{{customAsset('img/logo-ktc.svg')}}">
            </a>
        </div>
        @php /*
        <div class="on-right">
            @if(Session::has('sso_verify_status') and Session::get('sso_verify_status')==100)
              <div class="login-btn logged-in">
                  <div class="profile-image">
                    <img src="{{customAsset('img/img-profile.png')}}">
                  </div>
                    <p class="profile-name">{{Session::get('sso_first_name')}}</p>
                    <i class="fa fa-angle-down"></i>
                    <ul>
                        <li><a href="#"><div class="subimg-wrap"><img src="{{customAsset('img/ico-subprofile-1.png')}}"></div><p>บัตร KTC ของฉัน</p></a></li>
                        <li><a href="#"><div class="subimg-wrap"><img src="{{customAsset('img/ico-subprofile-2.png')}}"></div><p>โปรโมชั่นของฉัน</p></a></li>
                        <li><a href="#"><div class="subimg-wrap"><img src="{{customAsset('img/ico-subprofile-3.png')}}"></div><p>ออกจากระบบ</p></a></li>
                    </ul>
                    <div class="for-sub-profile-mobile">
                        <div class="profile-image">
                          <img src="{{customAsset('img/img-profile.png')}}">
                        </div>
                        <button type="button" class="close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
              </div>
            @else
                {{-- ก่อนล็อกอิน --}}
                <a class="login-btn" href="{{ env('LOGIN_PAGE') }}">
                  <i class="fa fa-lock"></i>
                  {{__('เข้าสู่ระบบ')}}
                </a>
                          <a class="login-mobile only-mobile" href="{{ env('LOGIN_PAGE') }}">
                  <img src="{{customAsset('img/ico-login.png')}}">
                </a>
           @endif
        </div>
        */
        @endphp
    </div>
</div>