<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="shortcut icon" href="{{ Voyager::image( Voyager::setting("site.logo")) }}" type="image/x-icon">
    <link rel="stylesheet" href="{{customAsset('css/bootstrap.min.css')}}">
    {{-- <link rel="stylesheet" href="{{customAsset('css/merchant.css')}}"> --}}
    <link rel="stylesheet" href="{{customAsset('css/style.css')}}">
    <link rel="stylesheet" href="{{customAsset('css/tablet-style.css')}}">
    <link rel="stylesheet" href="{{customAsset('css/mobile-style.css')}}">
   <!--  <link rel="stylesheet" href="{{URL::asset('css/all.min.css')}}"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @stack('page-styles')
    @if(isset($data['thankyou_title']))
        <title>{{__("ขอบคุณที่ไว้วางใจเลือกใช้ผลิตภัณฑ์และบริการจาก KTC")}}</title>
    @else
        <title>@yield('title',"KTC")</title>
    @endif
    @php
        $custom_script=json_decode(setting('pages.custom_script'));
    @endphp
    {!! $custom_script->header ?? NULL!!}
</head>
<body class="{{str_replace(".","_",Route::currentRouteName())}}">