{{-- Custom script initial  --}}
@include('frontend.master.header')
@if($page_model->layout !== 'blank_form')

    @if(hideHeader())
        @include('frontend.master.menu')
    @endif

	{{-- start page --}}
	@if($page_model->layout !== 'card_hero')
		<div id="oap-loan">
	@endif
		<div class="bg-login oap-wrap">
			 @include('frontend.layouts.contents')
			 @include('frontend.layouts.forms')
		</div>
	@if($page_model->layout !== 'card_hero')
		</div>
	@endif
		{{--end page--}}
		@include('frontend.master.footer')
		@include('frontend.master.footer-scripts')
	@else
	{{-- loade page blank_form --}}
	@include('frontend.layouts.blank')
	@include('frontend.master.footer-scripts')

@endif