<?php
function calPercentIncrease($from, $to) {
    if ($from == 0 || $to == 0) {
        return "0.00";
    }

    $diff = $to - $from;
    $percent = $to * 100 / $from;
    if ($diff < 0) {
        $percent = $percent - 100;
    }
    return formatNumber($percent, 2);
}

function formatNumber($num, $positions = 4) {
    return number_format(round($num, $positions), $positions);
}

function getBranchCategoryTitle ($id, $haystack) {
    return $haystack->find($id)->title ?? "(unknown)";
}

function avg($subject, $divider) {
    if ($divider == 0) {
        return formatNumber(0);
    }

    return formatNumber($subject / $divider);
}

$short_months = [];

?>
<table>
    <thead>
    <tr>
        <th>Y{{ $year }}</th>
        <th>Branch Code</th>
        <?php
        $current_month = \Carbon\Carbon::now()->startOfYear();
        for($m = $current_month->month; $m <= 12; $m++) {
            $loop_month = $current_month->month($m)->shortEnglishMonth;
            $short_months[] = $loop_month;
            echo "<th>{$loop_month}</th>";
        }
        ?>
        <th>Total</th>
        <th>Leads / Month</th>
        <th>Leads / Available Month</th>
        <th>Leads / Day</th>
        <th>Leads / Available Day</th>
        <th>%Increases from previous month</th>
        <th>Yearly Forecast</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $current_branch_cat_id = $leads->first()->branch_category_id;
    $current_branch_title = $leads->first()->title;
    $branch_title_repeat_count = -1;
    $sum_col = [];
    $branch_sum_col = [];
    $grand_totals = [];
    $current_month = $start_date->copy()->startOfMonth();
    $sum_available_days = 0;
    $grand_total_days = 0;
    $sum_available_months = 0;
    $last_row_type = 'branch_product';
    ?>
    @foreach($leads as $row)
        @if($row->title != $current_branch_title)
            @if($branch_title_repeat_count > 0)
                @php($branch_product_item = $leads_total_year_branch_product->where('title', $current_branch_title)->first())
                <tr>
                    <td colspan="2">{{ strtoupper("Total {$current_branch_title}") }}</td>
                    @for($m = 0; $m < 12; $m++)
                        <td>{{ $branch_product_item->{$short_months[$m]} ?? 0 }}</td>
                    @endfor
                    <td>{{ $branch_product_item->total }}</td>
                    <td>{{ $branch_product_item->avg_per_month }}</td>
                    <td>{{ $branch_product_item->avg_per_available_month }}</td>
                    <td>{{ $branch_product_item->avg_per_day }}</td>
                    <td>{{ $branch_product_item->avg_per_available_day }}</td>
                    <td>{{ calPercentIncrease($branch_product_item->{$start_date->shortEnglishMonth}, $branch_product_item->{$end_date->shortEnglishMonth}) }}%</td>
                </tr>
            @endif
            @php($last_row_type = 'branch_product')
            @php($current_branch_title = $row->title)
            @php($branch_title_repeat_count = 0)
        @else
            @php($branch_title_repeat_count += 1)
        @endif
        @if($row->branch_category_id != $current_branch_cat_id)
            @php($branch_cat_item = $leads_total_year_branch_category->where('title', getBranchCategoryTitle($current_branch_cat_id, $branch_categories))->first())
            <tr>
                <td colspan="2">{{ strtoupper("Total ".getBranchCategoryTitle($current_branch_cat_id, $branch_categories)) }}</td>
                @for($m = 0; $m < 12; $m++)
                    <td>{{ $branch_cat_item->{$short_months[$m]} ?? 0 }}</td>
                @endfor
                <td>{{ $branch_cat_item->total }}</td>
                <td>{{ $branch_cat_item->avg_per_month }}</td>
                <td>{{ $branch_cat_item->avg_per_available_month }}</td>
                <td>{{ $branch_cat_item->avg_per_day }}</td>
                <td>{{ $branch_cat_item->avg_per_available_day }}</td>
                <td>{{ calPercentIncrease($branch_cat_item->{$start_date->shortEnglishMonth}, $branch_cat_item->{$end_date->shortEnglishMonth}) }}%</td>
            </tr>
            @php($last_row_type = 'branch_category')
            @php($current_branch_cat_id = $row->branch_category_id)
        @endif
        <tr>
            <td>{{ $row->title }}</td>
            <td>{{ $row->branch_code }}</td>
            @for($m = 0; $m < 12; $m++)
                <?php
                $num = $row->{$current_month->shortEnglishMonth};
                ?>
                <td>{{ $num }}</td>
                @php($current_month->month = $m + 2)
            @endfor
            <td>{{ $row->TOTAL }}</td>
            <td>{{ $row->{'AVG PER MONTH'} }}</td>
            <td>{{ $row->{'AVG PER AVAILABLE MONTH'} }}</td>
            <td>{{ $row->{'AVG PER DAY'} }}</td>
            <td>{{ $row->{'AVG PER AVAILABLE DAY'} }}</td>
            <td>{{ calPercentIncrease($row->{$start_date->shortEnglishMonth}, $row->{$end_date->shortEnglishMonth}) }}%</td>
        </tr>
        @php($last_row_type = 'branch')
    @endforeach
    @if($branch_title_repeat_count > 0)
        @php($branch_product_item = $leads_total_year_branch_product->where('title', $current_branch_title)->first())
        <tr>
            <td colspan="2">{{ strtoupper("Total {$current_branch_title}") }}</td>
            @for($m = 0; $m < 12; $m++)
                <td>{{ $branch_product_item->{$short_months[$m]} ?? 0 }}</td>
            @endfor
            <td>{{ $branch_product_item->total }}</td>
            <td>{{ $branch_product_item->avg_per_month }}</td>
            <td>{{ $branch_product_item->avg_per_available_month }}</td>
            <td>{{ $branch_product_item->avg_per_day }}</td>
            <td>{{ $branch_product_item->avg_per_available_day }}</td>
            <td>{{ calPercentIncrease($branch_product_item->{$start_date->shortEnglishMonth}, $branch_product_item->{$end_date->shortEnglishMonth}) }}%</td>
        </tr>
        @php($last_row_type = 'branch_product')
    @endif
    @if($last_row_type != 'branch_category')
        @php($branch_cat_item = $leads_total_year_branch_category->where('title', getBranchCategoryTitle($current_branch_cat_id, $branch_categories))->first())
        <tr>
            <td colspan="2">{{ strtoupper("Total ".getBranchCategoryTitle($current_branch_cat_id, $branch_categories)) }}</td>
            @for($m = 0; $m < 12; $m++)
                <td>{{ $branch_cat_item->{$short_months[$m]} ?? 0 }}</td>
            @endfor
            <td>{{ $branch_cat_item->total }}</td>
            <td>{{ $branch_cat_item->avg_per_month }}</td>
            <td>{{ $branch_cat_item->avg_per_available_month }}</td>
            <td>{{ $branch_cat_item->avg_per_day }}</td>
            <td>{{ $branch_cat_item->avg_per_available_day }}</td>
            <td>{{ calPercentIncrease($branch_cat_item->{$start_date->shortEnglishMonth}, $branch_cat_item->{$end_date->shortEnglishMonth}) }}%</td>
        </tr>
    @endif
    <tr>
        @php($grand_total = $leads_grand_total->first())
        <td colspan="2"><strong>GRAND TOTAL</strong></td>
        @for($m = 0; $m < 12; $m++)
            <td>{{ $grand_total->{$short_months[$m]} ?? 0 }}</td>
        @endfor
        <td>{{ $grand_total->total }}</td>
        <td>{{ $grand_total->avg_per_month }}</td>
        <td>{{ $grand_total->avg_per_available_month }}</td>
        <td>{{ $grand_total->avg_per_day }}</td>
        <td>{{ $grand_total->avg_per_available_day }}</td>
        <td>{{ calPercentIncrease($grand_total->{$start_date->shortEnglishMonth}, $grand_total->{$end_date->shortEnglishMonth}) }}%</td>
    </tr>
    </tbody>
</table>

{{-- MONTHLY TABLE --}}
<?php
$printTable = function ($data, $month) use ($__env, $leads, $start_date, $end_date, $year, $branch_categories) {
    $current_branch_cat_id = $leads->first()->branch_category_id;
    $current_branch_title = $leads->first()->title;
    $branch_title_repeat_count = -1;
    $sum_col = [];
    $branch_sum_col = [];
    $grand_totals = [];
    $sum_available_days = 0;
    $grand_total_days = 0;
    $current_month = $start_date->copy()->month($month)->startOfMonth();
    $last_branch_day_value = 0;
    $last_sum_day_value = 0;
    $last_row_type = 'branch';
?>
<table>
    <thead>
    <tr>
        <th rowspan="2">{{ strtoupper($current_month->shortEnglishMonth) }}</th>
        <th rowspan="2">Branch Code</th>
        @php($the_day = $current_month->copy())
        @for($day = 1; $day <= $current_month->daysInMonth; $day++)
            @php($the_day->day($day))
            <th>{{ $the_day->shortEnglishDayOfWeek }}</th>
        @endfor
        <th rowspan="2">Leads / Day</th>
        <th rowspan="2">Leads / Available Day</th>
        <th rowspan="2">%Increases from previous day</th>
        <th rowspan="2">Monthly Forecast</th>
    </tr>
    <tr>
        @for($day = 1; $day <= $current_month->daysInMonth; $day++)
            <th>{{ $day }}</th>
        @endfor

    </tr>
    </thead>
    <tbody>
    @foreach($leads as $row)
        @if($row->title != $current_branch_title)
            @if($branch_title_repeat_count > 0)
                <tr>
                    <td colspan="2">{{ strtoupper("Total {$current_branch_title}") }}</td>
                    @php($current_month_data = $data[$current_branch_title][$current_month->month] ?? [])
                    @for($day = 1; $day <= $current_month->daysInMonth; $day++)
                        <?php
                        $num = $current_month_data[$day] ?? 0;
                        if ($num > 0) {
                            $row_percent = calPercentIncrease($last_branch_day_value, $num);
                            $last_branch_day_value = $num;
                        }
                        ?>
                        <td>{{ $num }}</td>
                    @endfor
                    @php($sum = array_sum($current_month_data))
                    <td>{{ avg($sum, $current_month->daysInMonth) }}</td>
                    <td>{{ avg($sum, count($current_month_data)) }}</td>
                    <td>{{ $row_percent }}%</td>
                    <td></td>
                </tr>
            @endif
            @php($last_row_type = 'branch_product')
            @php($current_branch_title = $row->title)
            @php($branch_title_repeat_count = 0)
        @else
            @php($branch_title_repeat_count += 1)
        @endif
        @if($row->branch_category_id != $current_branch_cat_id)
            <tr>
                <td colspan="2">{{ strtoupper("Total ".getBranchCategoryTitle($current_branch_cat_id, $branch_categories)) }}</td>
                @php($current_month_data = $data['branch_cat_'.$current_branch_cat_id][$current_month->month] ?? [])
                @for($day = 1; $day <= $current_month->daysInMonth; $day++)
                    <?php
                    $num = $current_month_data[$day] ?? 0;
                    if ($num > 0) {
                        $row_percent = calPercentIncrease($last_sum_day_value, $num);
                        $last_sum_day_value = $num;
                    }
                    ?>
                    <td>{{ $num }}</td>
                @endfor
                @php($sum = array_sum($current_month_data))
                <td>{{ avg($sum, $current_month->daysInMonth) }}</td>
                <td>{{ avg($sum, count($current_month_data)) }}</td>
                <td>{{ $row_percent }}%</td>
                <td></td>
            </tr>
            @php($current_branch_cat_id = $row->branch_category_id)
            @php($last_row_type = 'branch_category')
        @endif
        <tr>
            <td>{{ $row->title }}</td>
            <td>{{ $row->branch_code }}</td>
            @php($row_sum = array_sum($data[$row->branch_code][$month] ?? []))
            @php($row_percent = 0)
            @php($last_day_value = 0)
            @for($day = 1; $day <= $current_month->daysInMonth; $day++)
                <?php
                $num = $data[$row->branch_code][$month][$day] ?? 0;
                if ($num > 0) {
                    $row_percent = calPercentIncrease($last_day_value, $num);
                    $last_day_value = $num;
                }
                ?>
                <td>{{ $num }}</td>
            @endfor
            <td>{{ avg($row_sum, $current_month->daysInMonth) }}</td>
            <td>{{ avg($row_sum, count($data[$row->branch_code][$month] ?? [])) }}</td>
            <td>{{ $row_percent }}%</td>
        </tr>
        @php($last_row_type = 'branch')
    @endforeach
    @if($branch_title_repeat_count > 0)
        <tr>
            <td colspan="2">{{ strtoupper("Total {$current_branch_title}") }}</td>
            @php($current_month_data = $data[$current_branch_title][$current_month->month] ?? [])
            @for($day = 1; $day <= $current_month->daysInMonth; $day++)
                <?php
                $num = $current_month_data[$day] ?? 0;
                if ($num > 0) {
                    $row_percent = calPercentIncrease($last_branch_day_value, $num);
                    $last_branch_day_value = $num;
                }
                ?>
                <td>{{ $num }}</td>
            @endfor
            @php($sum = array_sum($current_month_data))
            <td>{{ avg($sum, $current_month->daysInMonth) }}</td>
            <td>{{ avg($sum, count($current_month_data)) }}</td>
            <td>{{ $row_percent }}%</td>
            <td></td>
        </tr>
        @php($last_row_type = 'branch_product')
    @endif
    @if($last_row_type != 'branch_category')
        <tr>
            <td colspan="2">{{ strtoupper("Total ".getBranchCategoryTitle($current_branch_cat_id, $branch_categories)) }}</td>
            @php($current_month_data = $data['branch_cat_'.$current_branch_cat_id][$current_month->month] ?? [])
            @for($day = 1; $day <= $current_month->daysInMonth; $day++)
                <?php
                $num = $current_month_data[$day] ?? 0;
                if ($num > 0) {
                    $row_percent = calPercentIncrease($last_sum_day_value, $num);
                    $last_sum_day_value = $num;
                }
                ?>
                <td>{{ $num }}</td>
            @endfor
            @php($sum = array_sum($current_month_data))
            <td>{{ avg($sum, $current_month->daysInMonth) }}</td>
            <td>{{ avg($sum, count($current_month_data)) }}</td>
            <td>{{ $row_percent }}%</td>
            <td></td>
        </tr>
        @php($last_row_type = 'branch_category')
    @endif
    <tr>
        <td colspan="2"><strong>GRAND TOTAL</strong></td>
        @php($last_grand_day_value = 0)
        @php($current_grand_total = $data['grand_total'][$current_month->month] ?? [])
        @for($day = 1; $day <= $current_month->daysInMonth; $day++)
            <?php
            $num = $current_grand_total[$day] ?? 0;
            if ($num > 0) {
                $row_percent = calPercentIncrease($last_grand_day_value, $num);
                $last_grand_day_value = $num;
            }
            ?>
            <td>{{ $num }}</td>
        @endfor
        @php($sum = array_sum($current_grand_total))
        <td>{{ avg($sum, $current_month->daysInMonth) }}</td>
        <td>{{ avg($sum, count($current_grand_total)) }}</td>
        <td>{{ $row_percent }}%</td>
        <td></td>
    </tr>
    </tbody>
</table>

<?php
}

?>

<?php
for ($i = 1; $i <= 12; $i++) {
    $printTable($leads_monthly, $i);
}
