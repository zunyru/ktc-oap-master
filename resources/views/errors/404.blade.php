@extends('errors.master')
@section('title', "บริษัท บัตรกรุงไทย จำกัด (มหาชน) - Page not found")
@push('page-styles')
  <link rel="stylesheet" href="{{ customAsset('/css/error404.css')}}">
  <style>
       button.btn,
       a.btn,
       .btn {
           min-width: 214px;
           display: inline-block;
           height: 45px;
           line-height: 45px;
           padding: 0 40px;
           margin-bottom: 0;
           font-family: 'gothamrounded-medium', 'Conv_sukhumvittadmai_bol';
           font-size: 14px;
           color: #FFF;
           cursor: pointer;
           text-align: center;
           white-space: nowrap;
           vertical-align: middle;
           text-transform: uppercase;
           -ms-touch-action: manipulation;
           touch-action: manipulation;
           -webkit-user-select: none;
           -moz-user-select: none;
           -ms-user-select: none;
           user-select: none;
           background-color: #D71F3E;
           text-decoration: none;
           background-image: none;
           border: 1px solid transparent;
           -webkit-border-radius: 24px;
           -moz-border-radius: 24px;
           border-radius: 24px;
           text-decoration: none;
           -webkit-transition: all 0.3s;
           -moz-transition: all 0.3s;
           transition: all 0.3s;
       }

       h4 {
           font-family: 'Conv_sukhumvittadmai_Tex' !important;
       }
       body * {
           letter-spacing: 0.5px !important;
       }
   </style>
@endpush
@section('page-content')
  <div class="errorpage404">
      <div class="tb">
          <div class="tbcell v-middle">
              <div class="area-content">
                  <div class="area-content-img col-md-3">
                      <img style="display: none;" src="https://pvb4.com/sites/cs/assets/ktc_css/PageNotFound/img/icon-404error.png" alt="">
                  </div>
                  <div class="area-content-txt col-md-9">
                      <!--h1>404</h1-->
                      <h2>ขออภัย ไม่พบหน้าเว็บไซต์ที่ต้องการ</h2>
                      <h4 >หน้าเว็บไซต์ดังกล่าวอาจมีการเปลี่ยนที่ เปลี่ยนชื่อ หรือพิมพ์ URL ไม่ถูกต้อง <span class="br">กรุณาตรวจสอบอีกครั้ง</span></h4>
                      <a href="/" class="btn">กลับสู่หน้าหลัก</a>
                  </div>
              </div>
          </div>
      </div>
  </div>
@endsection