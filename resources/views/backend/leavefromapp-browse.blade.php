@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        @if(Request::has('status')) {{ 'Restore ' }} @endif{{ $dataType->display_name_plural }}
    </h1>
    @can('add', app($dataType->model_name))
    <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
        <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
    </a>
    @endcan
    @can('delete', app($dataType->model_name))
    @if(!Request::has('status'))
      @include('voyager::partials.bulk-delete')
    @else
      @include('backend.partials.bulk-restore')
    @endif
    @endcan
    {{-- Restore Data --}}
    @if(!Request::has('status'))
    <a href="{{ route('voyager.'.$dataType->slug.'.index').'?status=trash' }}" class="btn btn-restore"><i class="voyager-refresh"></i> <span>Restore</span></a>
    @endif
    @can('edit', app($dataType->model_name))
    @if(isset($dataType->order_column) && isset($dataType->order_display_column))
    <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
        <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
    </a>
    @endif
    @endcan
    @include('voyager::multilingual.language-selector')
</div>
@stop


@section('content')


<div class="page-content">
    @include('voyager::alerts')
    {{-- @include('voyager::dimmers') --}}

    <div class="page-content browse container-fluid">
        <div class="alerts">
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <form method="get" action="{{ route('export.report') }}">
                        <div class="panel-body">
                            <h3 class="form-sec-title"> Report </h3>
                            <span class="form-sec-divider"></span>
                            {{--<div class="form-group col-md-6">--}}
                                {{--<label for="daterange">Range</label>--}}
                                {{--<input readonly="" id="daterange-report" type="text" name="report_range" class="form-control" value="" />--}}
                            {{--</div>--}}
                            <button type="submit" class="btn btn-primary">
                                <i class="voyager-download"></i> <span>Download Report</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <form method="post" action="{{ route('edit.leads.time-filter') }}">
                        <div class="panel-body">
                            <h3 class="form-sec-title"> Leads Duplication </h3>
                            <span class="form-sec-divider"></span>
                            <div class="form-group col-md-7">
                                <label for="date-unit">Show duplicated records within:</label>
                                    {{ csrf_field() }}
                                    <div style="display: inline-block; width: 25%">
                                        <input type="number" name="number" class="form-control" placeholder="ex: 7" value="{{ $filter_date_number }}" min="1" required>
                                    </div>
                                    <div style="display: inline-block; width: 35%">
                                        <select id="date-unit" name="unit" class="form-control">
                                            @foreach($filter_date_options as $value => $text)
                                            <option value="{{ $value }}"{{ $filter_date_unit == $value ? "selected" : "" }}>{{ $text }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                            </div>
                        </div>
                        <div class="panel-footer">
                            <input type="submit" name="submit" class="btn btn-primary" value="Change">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <form  method="get" id="filter-leads">
                    @if(Request::has('page'))
                <input type="hidden" name="page" value="{{Request::input('page')}}">
                    @endif
                    @if(Request::has('status'))
                      <input type="hidden" name="status" value="trash">
                    @endif
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <h3 class="form-sec-title"> Filter </h3>
                            <span class="form-sec-divider"></span>
                            <div class="form-group col-md-6">
                                <label for="fname">First name</label>
                                <input  type="text" class="form-control" id="fname" name="fname" placeholder="Name"
                                value="@if(Request::input('fname') != '') {{ Request::input('fname') }} @endif">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="lname">Last name</label>
                                <input  type="text" class="form-control" id="lname" name="lname" placeholder="Last name" value="@if(Request::input('lname') != '') {{ Request::input('lname') }} @endif">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input  type="text" class="form-control" id="email" name="email" placeholder="Email" value="@if(Request::input('email') != '') {{ Request::input('email') }} @endif">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="phone">Phone</label>
                                <input  type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="{{Request::input('phone')??NULL}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="filter">Page<span class="red"></span></label>
                                <select name="pages[]" class="form-control filter" multiple="multiple">

                                    @foreach($pages as $index=> $item)
                                    @if(!Request::input('pages') || sizeof(Request::input('pages')) == 0)
                                    <option  @if(Request::input('pages') == $item->id) {{ 'selected' }} @endif value="{{ $item->id }}">{{ $item->title }}</option>
                                    @else
                                    <option  @if(in_array( $item->id, Request::input('pages'))) {{ 'selected' }} @endif value="{{ $item->id }}">{{ $item->title }}</option>
                                    @endif
                                    @endforeach


                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="product">Product<span class="red"></span></label>
                                <select name="product[]" class="form-control filter" multiple="multiple">

                                    @foreach($products as $index=> $item)
                                    @if(!Request::input('product') || sizeof(Request::input('product')) == 0)
                                    <option  @if(Request::input('product') == $item->id) {{ 'selected' }} @endif value="{{ $item->id }}">{{ $item->title }}</option>
                                    @else
                                    <option  @if(in_array( $item->id, Request::input('product'))) {{ 'selected' }} @endif value="{{$item->id}}">{{ $item->title }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="branche">Branch<span class="red"></span></label>
                                <select name="branche[]" class="form-control filter" multiple="multiple">

                                    @foreach($branches as $index=> $item)
                                    @if(!Request::input('branche') || sizeof(Request::input('branche')) == '')
                                    <option  @if(Request::input('branche') == $item->code) {{ 'selected' }} @endif value="{{ $item->code }}">{{ $item->code }}</option>
                                    @else
                                    <option  @if(in_array( $item->code, Request::input('branche'))) {{ 'selected' }} @endif value="{{$item->code}}">{{ $item->code }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="indicator">Indicator<span class="red"></span></label>
                                <select name="indicator[]" class="form-control filter" multiple="multiple">

                                    @foreach($indicators_distinct as $index => $indicators)
                                     @if(!Request::input('indicator') || sizeof(Request::input('indicator')) == '')
                                    <option @if(Request::input('indicator') == $indicators) {{ 'selected' }} @endif  value="{{ $indicators }}">{{ $indicators }}</option>
                                    @else
                                    <option  @if(in_array( $indicators, Request::input('indicator'))) {{ 'selected' }} @endif value="{{ $indicators }}">{{ $indicators }}</option>
                                    @endif

                                    @endforeach
                                </select>
                            </div>
                          <div class="form-group col-md-6">
                            <label for="sub_indicator">Sub-indicator<span class="red"></span></label>
                            <select name="sub_indicator[]" class="form-control filter" multiple="multiple">

                                @foreach($sub_indicator_distinct as $index=> $sub_indicator)
                                 @if(!Request::input('sub_indicator') || sizeof(Request::input('sub_indicator')) == '')
                                <option @if(Request::input('sub_indicator') == $sub_indicator) {{ 'selected' }} @endif value="{{ $sub_indicator }}">{{ $sub_indicator }}</option>
                                @else
                                    <option  @if(in_array( $sub_indicator, Request::input('sub_indicator'))) {{ 'selected' }} @endif value="{{ $sub_indicator }}">{{ $sub_indicator }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="daterange">Submitted Date</label>
                            <input readonly="" id="daterange" type="text" name="daterange" class="form-control" value="{{Request::input('daterange')}}" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="row_limited">Row Limitation</label>
                            <input pattern="[1-9]|(-1)" min="-1" id="row_limited" type="number" name="row_limited" placeholder="fill -1 for all results" class="form-control" value="{{Request::input('row_limited')??100}}" />
                        </div>
                    </div>

                    <div class="panel-footer">
                        <input type="submit" name="submit" class="btn btn-primary" value="Filter">
                        <a href="{{ route('voyager.leads.index') }}" class="btn btn-warning" >Clear</a>
                             @if($export)
                            <div class="pull-right">
                                {{-- export --}}
                                <a href="{{ route('export.leads') }}?{{ $query }}" class="btn btn-success">
                                    <i class="voyager-download"></i> <span>Export</span>
                                </a>
                                {{-- export --}}
                                <a href="{{ route('export.leads-lid') }}?{{ $query }}" class="btn btn-success">
                                    <i class="voyager-download"></i> <span>Telesales Export</span>
                                </a>
                            </div>
                            @endif
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    @can('delete',app($dataType->model_name))
                                    <th width="3%">
                                        <input type="checkbox" class="select_all">
                                    </th>
                                    @endcan
                                    <th>ID</th>
                                    <th width="20%">
                                        Name
                                    </th>
                                    <th width="20%">
                                        Last name
                                    </th>
                                    <th width="15%">
                                        Phone
                                    </th>
                                    <th width="20%">
                                        Product
                                    </th>
                                    <th width="13%">
                                        Created date
                                    </th>
                                    <th width="2%">Duplicated</th>
                                    <th width="5%" class="actions text-right">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($objs_forms as $item => $form)
                               <tr>
                                 @can('delete',app($dataType->model_name))
                                 <td>
                                    <input type="checkbox" name="row_id" id="checkbox_{{ $form->id }} ?>" value="{{ $form->id }}">
                                </td>
                                @endcan
                                <td>{{ $form->lead_unique_id ?? '' }}</td>
                                <td class="no-sort no-click bread-actions">
                                    {{ $form->fname }}
                                </td>
                                <td>
                                    {{ $form->lname }}
                                </td>
                                <td>
                                    {{ formatPhone($form->tel) }}
                                </td>
                                <td>
                                    {{ $form->product->title ?? "(Unknown Item)" }}
                                </td>
                                <td>
                                    <input type="hidden" value="{{ $form->created_at->timestamp }}">
                                    {{ formatDateTimeThat($form->created_at) }}
                                </td>
                                <td>
                                 @if($form->isDuplicated())
                                 <span class="badge badge-pill badge-danger">ข้อมูลซ้ำ</span>
                                 @endif
                             </td>
                             <td class="no-sort no-click bread-actions">


                                @if(!Request::has('status'))
                                @can('edit',app($dataType->model_name))
                                <a href="{{ URL('oap/admin/'.$dataType->slug.'/'.$form->id.'/edit ') }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                </a>
                                @endcan
                                @can('delete',app($dataType->model_name))
                                <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $form->id }}">
                                    <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                </div>
                                @endcan
                                @else

                                <div class="btn btn-sm btn-restore pull-right restore" data-id="{{ $form->id }}">
                                    <i class="voyager-refresh"></i> {{ __('Restore') }}
                                </div>

                                @endif

                                @can('read',app($dataType->model_name))
                                <a href="{{ URL('oap/admin/'.$dataType->slug.'/'.$form->id) }}" title="View" class="btn btn-sm btn-warning pull-right view">
                                    <i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>
                                </a>
                                @endcan


                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                            Showing {{ $start_of_page}} to  {{$total_of_page}} of {{$objs_forms->total()}} entries
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                            {{ $objs_forms->appends(Request::input())->onEachSide(1)->links() }}
                            {{-- {{ $paginator->links()}} --}}
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
</div>

<div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ $dataType->display_name_singular }}?
                </h4>
            </div>
            <div class="modal-footer">
                <form action="#" id="delete_form" method="POST">
                    {{ method_field("DELETE") }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_this_confirm') }} {{ $dataType->display_name_singular }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
{{-- Restore modal--}}
<div class="modal modal-info fade" tabindex="-1" id="restore_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">
                    <i class="voyager-refresh"></i> {{ 'Are you sure you want to restore this' }} {{ $dataType->display_name_singular }}?
                </h4>
            </div>
            <div class="modal-footer">
                <form action="#" id="restore_form" method="POST">
                    {{ method_field("PUT") }}
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-success pull-right restore-confirm" value="{{ __('voyager::generic.delete_this_confirm') }} {{ 'Restore' }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
            </div>
        </div>
    </div>
</div>
@stop

<?php $__env->startSection('javascript'); ?>
<!-- DataTables -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
    $(document).ready(function () {
        $(".filter").select2({
            placeholder: "-- Select --",
            allowClear: true,
            tokenSeparators: [',']
        });

        // $('#dataTable').DataTable({
        //     @can('delete',app($dataType->model_name))
        //     "order": [[1, "desc"], [6,"desc"]],
        //     @else
        //     "order": [[0, "desc"], [5,"desc"]],
        //     @endcan
        //     "columnDefs": [
        //     {"targets": 0, "searchable":  false, "orderable": false},
        //     {"targets": 7, "searchable":  false, "orderable": false},
        //     {"targets": 5, "type": "numeric-comma" }
        //     ],
        //     "scrollX": true,
        //     responsive: true
        // });

        $('.select_all').on('click', function(e) {
            $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
        });

        $('input[name="report_range"]').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'MM/YYYY'
            }
        });

        $('input[name="report_range"]').on('apply.daterangepicker', function(e, picker) {
            $(this).val(picker.startDate.format('MM/YYYY') + ' - ' + picker.endDate.format('MM/YYYY'));
        }).on('cancel.daterangepicker', function() {
            $(this).val('');
        });
    });


    $(function() {

      $('input[name="daterange"]').daterangepicker({
          autoUpdateInput: false,
          locale: {
              cancelLabel: 'Clear',
              format: 'DD/MM/YYYY'
          }

      });

      $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

      $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
      });

  });

    $('td').on('click', '.delete', function (e) {
     $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['menu' => '__menu']) }}'.replace('__menu', $(this).data('id'));

     $('#delete_modal').modal('show');

 });

    $('td').on('click', '.restore', function (e) {
      $('#restore_form')[0].action = '{{ route('lead-restore', ['menu' => '__menu']) }}'.replace('__menu', $(this).data('id'));

      $('#restore_modal').modal('show');

  });
</script>
<?php $__env->stopSection(); ?>