<html lang="th" >
<head>
	<title>Viewing Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="MgnXcBYwGq9XaT31dqXgLiq13sgqq1ZvLIzC07VF"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

	<!-- Favicon -->

	<link rel="shortcut icon" href="http://ktcoap.test/storage/settings/October2018/cwmAFmMdtly1Qf5shShl.png" type="image/x-icon">




	<!-- App CSS -->
	<link rel="stylesheet" href="http://ktcoap.test/vendor/tcg/voyager/assets/css/app.css">


	<link rel="stylesheet" href="http://ktcoap.test/css/custom_backend.css">

	<!-- In the header -->
	<link href="https://transloadit.edgly.net/releases/uppy/v0.27.5/dist/uppy.min.css" rel="stylesheet">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />


	<!-- Few Dynamic Styles -->
	<style type="text/css">
	.voyager .side-menu .navbar-header {
		background:#22A7F0;
		border-color:#22A7F0;
	}
	.widget .btn-primary{
		border-color:#22A7F0;
	}
	.widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
		background:#22A7F0;
	}
	.voyager .breadcrumb a{
		color:#22A7F0;
	}
</style>




</head>

<body class="voyager pageleaveform">

	<div class="app-container">

		<div class="page-content edit-add container-fluid">
			<div class="row">
				<div class="col-md-12">


					<form action="{{ url('/admin/upload') }}" enctype="multipart/form-data" class="" id="my-dropzone" method="post">
						{{ csrf_field() }}

						<input id="input-id" type="file" name="file[]" class="file" data-preview-file-type="text" multiple>

						<button type="submit">Save</button>
					</form>

					<!-- Where the drag and drop will be -->
					

					<!-- form end -->
				</div>
			</div>
		</div>
	</div>

	<!-- End Delete File Modal -->
	<footer class="app-footer">
		<div class="site-footer-right">
			Made with <i class="voyager-heart"></i> by <a href="http://thecontrolgroup.com" target="_blank">The Control Group</a>
			- v1.1.7
		</div>
	</footer>

	<script type="text/javascript" src="http://ktcoap.test/vendor/tcg/voyager/assets/js/app.js"></script>
	<!-- Javascript Libs -->


	<script type="text/javascript" src="http://ktcoap.test/js/slug.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/plugins/sortable.min.js" type="text/javascript"></script>

	<!-- Bottom of the page -->
	{{-- <script src="https://transloadit.edgly.net/releases/uppy/v0.27.5/dist/uppy.min.js"></script> --}}
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" type="text/javascript"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
	
	<script>
		jQuery(document).ready(function($) {
			$("#input-id").fileinput({
				browseClass: "btn btn-primary btn-block",
				showCaption: false,
				showRemove: false,
				showUpload: false,
				overwriteInitial: true,
			});
		});
		
	</script>
	
</body> 

