@extends('voyager::master')

@section('page_title', $dataType->display_name_singular.' - '.$category->name)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_singular }}: {{ $category->name }}
        </h1>
        @include('voyager::multilingual.language-selector')
        @if($category->trashed())
            <div class="box-deleted">
                <span class="badge badge-pill badge-danger">DELETED</span>
                <a href="{{ route('restore.product-category', $category->id) }}" type="button" class="btn btn-link">Restore</a>
            </div>
        @endif
    </div>
@stop

@section('content')
    <div class="page-content">
        @include('voyager::alerts')

        <div class="page-content browse container-fluid">
            <div class="alerts">
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                            <p>List of all products in {{ $category->name }}</p>
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    <th width="80%">Product Name</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{ $product->title }}</td>
                                        <td>
                                            @can('delete',app($productDataType->model_name))
                                                <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $product->id }}">
                                                    <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                                </div>
                                            @endcan
                                            @can('edit',app($productDataType->model_name))
                                                <a href="{{ URL('oap/admin/products/'.$product->id.'/edit ') }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                                    <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                                </a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{--<div class="panel-body">--}}
                            {{--<table id="dataTable" class="table table-hover">--}}
                                {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--@can('delete',app($dataType->model_name))--}}
                                        {{--<th width="5%">--}}
                                            {{--<input type="checkbox" class="select_all">--}}
                                        {{--</th>--}}
                                    {{--@endcan--}}
                                    {{--<th width="5%">--}}
                                        {{--#--}}
                                    {{--</th>--}}
                                    {{--<th width="25%">--}}
                                        {{--Category Name--}}
                                    {{--</th>--}}
                                    {{--<th width="10%">--}}
                                        {{--Code--}}
                                    {{--</th>--}}
                                    {{--<th>--}}
                                        {{--Description--}}
                                    {{--</th>--}}
                                    {{--<th width="13%">--}}
                                        {{--Update date--}}
                                    {{--</th>--}}
                                    {{--<th width="10%">--}}
                                        {{--Deleted--}}
                                    {{--</th>--}}
                                    {{--<th class="actions text-right" width="20%">--}}
                                        {{--Action--}}
                                    {{--</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}
                                {{--@foreach($categories as $item => $form)--}}
                                    {{--<tr>--}}
                                        {{--@can('delete',app($dataType->model_name))--}}
                                            {{--<td>--}}
                                                {{--<input type="checkbox" name="row_id" id="checkbox_{{ $form->id }} ?>" value="{{ $form->id }}">--}}
                                            {{--</td>--}}
                                        {{--@endcan--}}
                                        {{--<td>{{ $form->id }}</td>--}}
                                        {{--<td>--}}
                                            {{--{{ $form->name }}--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--{{ $form->category_code }}--}}
                                        {{--</td>--}}
                                        {{--<td class="no-sort no-click">--}}
                                            {{--{{ $form->description }}--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--<input type="hidden" value="{{ $form->updated_at }}">--}}
                                            {{--{{ formatDateTimeThat($form->updated_at) }}--}}
                                        {{--</td>--}}
                                        {{--<td>--}}
                                            {{--@if($form->trashed())--}}
                                                {{--<span class="badge badge-pill badge-danger">DELETED</span>--}}
                                                {{--<a href="{{ route('restore.product-category', $form->id) }}" type="button" class="btn btn-link">Restore</a>--}}
                                            {{--@endif--}}
                                        {{--</td>--}}
                                        {{--<td class="no-sort no-click bread-actions">--}}



                                            {{--@can('delete',app($dataType->model_name))--}}
                                                {{--@if(!$form->trashed())--}}
                                                    {{--<div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $form->id }}" data-action="{{ route('voyager.'.$dataType->slug.'.destroy', $form->id) }}">--}}
                                                        {{--<i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}--}}
                                                    {{--</div>--}}
                                                {{--@endif--}}
                                            {{--@endcan--}}

                                            {{--@can('edit',app($dataType->model_name))--}}
                                                {{--<a href="{{ URL('oap/admin/'.$dataType->slug.'/'.$form->id.'/edit ') }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">--}}
                                                    {{--<i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>--}}
                                                {{--</a>--}}
                                            {{--@endcan--}}
                                            {{--@can('read',app($dataType->model_name))--}}
                                                {{--<a href="{{ URL('oap/admin/'.$dataType->slug.'/'.$form->id) }}" title="View" class="btn btn-sm btn-warning pull-right view">--}}
                                                    {{--<i class="voyager-eye"></i> <span class="hidden-xs hidden-sm">View</span>--}}
                                                {{--</a>--}}
                                            {{--@endcan--}}


                                        {{--</td>--}}
                                    {{--</tr>--}}
                                {{--@endforeach--}}
                                {{--</tbody>--}}
                            {{--</table>--}}

                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
@stop