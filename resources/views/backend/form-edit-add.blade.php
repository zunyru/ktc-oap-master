@extends('voyager::master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
<h1 class="page-title">
	<i class="{{ $dataType->icon }}"></i>
	{{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
</h1>
@stop

@section('content')
{{-- CSS --}}
<style type="text/css">
.row>[class*=col-]{
	margin-bottom: 0;
}
</style>
{{-- End CSS --}}
<div class="page-content edit-add container-fluid">
	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-bordered">
				<!-- form start -->

				<form
				id="ktc_form"
				role="form"
				{{-- class="form-edit-add" --}}
				action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
				method="POST" enctype="multipart/form-data">
				<!-- PUT Method if we are editing -->
				@if(!is_null($dataTypeContent->getKey()))
				{{ method_field("PUT") }}
				@endif

				<!-- CSRF TOKEN -->
				{{ csrf_field() }}

				<div class="panel-body">
					@if (count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif

					<!-- Adding / Editing -->
					@php
					$dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
					@endphp

					<input type="hidden" name="form_id" id="form_id" value="{{ $dataTypeContent->id }}">
					<div class="form-group col">
						<label for="title">Title <span class="red">*</span></label>
						<input  type="text" class="form-control" id="title" name="title" placeholder="Title (TH)" value="@if(isset($dataTypeContent->title)){{ trim($dataTypeContent->title) }}@endif">
						<label  id="title-error-dup" class="dup-error" for="title" style="display: none;">This title already exists</label>
					</div>
					<div class="form-group col">
						<label for="description">Description </label>
						<textarea class="form-control" id="description" name="description" placeholder="Description (TH)" rows="5">@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
					</div>
					<hr>

					<div class="form-group col">
						<label for="heading">Heading (TH) <span class="red">*</span></label>
						<input  type="text" class="form-control" id="heading" name="heading" placeholder="Heading (TH)" value="@if(isset($dataTypeContent->content_heading)){{ $dataTypeContent->content_heading }}@endif">
					</div>
					<div class="form-group col">
						<label for="heading-en">Heading (EN) <span class="red">*</span></label>
						<input  type="text" class="form-control" id="heading-en" name="heading_en" placeholder="Heading (EN)" value="{{ $dataTypeContent->english()->content_heading ?: "" }}">
					</div>

					<div class="form-group col">
						<label for="sub_heading">Sub-heading (TH)</label>
						<input type="text" class="form-control" id="sub_heading" name="sub_heading" placeholder="Sub-heading (TH)" value="@if(isset($dataTypeContent->content_sub_heading)){{ $dataTypeContent->content_sub_heading }}@endif">
					</div>
					<div class="form-group col">
						<label for="sub_heading-en">Sub-heading (EN)</label>
						<input type="text" class="form-control" id="sub_heading-en" name="sub_heading_en" placeholder="Sub-heading (EN)" value="{{ $dataTypeContent->english()->content_sub_heading ?: "" }}">
					</div>
					<hr>

					<div class="form-group col">
						<label for="opt-group">Salary Group <span class="red">*</span></label>
						<select class="form-control select2" name="opt_group" id="opt-group">
							<option value="">-- select --</option>
							@foreach(\App\OptSalaryGroup::all() as $item)
								<option value="{{ $item->id }}" @if($dataTypeContent->opt_salary_group_id === $item->id) {{ 'selected' }} @endif>{{ $item->title }}</option>
							@endforeach
						</select>
					</div>
					<hr>
					<div class="form-group col">
						<label for="required_condition">Condition need acceptable check-box </label>
						<input type="checkbox" name="required_condition" id="required_condition" class="toggleswitch"  data-on="yes" data-off="no"  @if($dataTypeContent->ch == 1 ){{ 'checked' }}@endif>
					</div>

					<div class="form-group col">
						<label for="condition">Condition (TH)</label>
						<textarea class="form-control richTextBox_ktc" id="condition" name="condition" placeholder="Condition (TH)" rows="5">@if(isset($dataTypeContent->content_condition)){{ $dataTypeContent->content_condition }}@endif</textarea>
					</div>
					<div class="form-group col">
						<label for="condition-en">Condition (EN)</label>
						<textarea class="form-control richTextBox_ktc" id="condition-en" name="condition_en" placeholder="Condition (EN)" rows="5">{{ $dataTypeContent->english()->content_condition ?: "" }}</textarea>
					</div>
					<hr>

					<div class="form-group col">
						<label for="richtextContact">Contact (TH)</label>
						<textarea   class="form-control richTextBox_ktc" name="contact" id="richtextContact">@if(isset($dataTypeContent->content_contact)){{ $dataTypeContent->content_contact }}@endif</textarea>
					</div>
					<div class="form-group col">
						<label for="richtextContact-en">Contact (EN)</label>
						<textarea   class="form-control richTextBox_ktc" name="contact_en" id="richtextContact-en">{{ $dataTypeContent->english()->content_contact ?: "" }}</textarea>
					</div>
					<hr>

                    @php

                    $cf_1 = json_decode($dataTypeContent->cf_1);
                    $cf_2 = json_decode($dataTypeContent->cf_2);

                    @endphp

					<div class="form-group col">
						<label for="custom_filed_one">Custom Field 1</label>
						<input type="checkbox" name="custom_filed_one" id="custom_filed_one" class="toggleswitch"  data-on="yes" data-off="no" @if(!is_null($cf_1)){{ 'checked' }}@endif>
					</div>

					<div class="row costom_box_one" style="display: none;">
						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="required_field_one">Required Field</label>
								<input type="checkbox" name="required_field_one" id="required_field_one"  value="required"
								@if(!is_null($cf_1) && $cf_1->opt == 'required'){{ 'checked' }}@endif>
							</div>
						</div>

						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="field_key_one">Field Key 1 <span class="red">*</span></label>
								<input  type="text" class="form-control" id="field_key_one" name="field_key_one" placeholder="Key Field" value="@if(!is_null($cf_1) && isset($cf_1->key_name)){{ $cf_1->key_name }}@endif">
							</div>
						</div>
						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="field_label_one">Field Label 1 (TH) <span class="red">*</span></label>
								<input  type="text" class="form-control" id="field_label_one" name="field_label_one" placeholder="Label 1 (TH)" value="@if(!is_null($cf_1) && isset($cf_1->label_name)){{ $cf_1->label_name }}@endif">
							</div>
						</div>
						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="field_label_one-en">Field Label 1 (EN) <span class="red">*</span></label>
								<input  type="text" class="form-control" id="field_label_one-en" name="field_label_one_en" placeholder="Label 1 (EN)" value="@if(!is_null($cf_1) && isset($cf_1->label_name_en)){{ $cf_1->label_name_en }}@endif">
							</div>
						</div>
					</div>
					<hr>
					<div class="form-group col">
						<label for="custom_filed_two">Custom Field 2</label>
						<input type="checkbox" name="custom_filed_two" id="custom_filed_two" class="toggleswitch"  data-on="yes" data-off="no" @if(!is_null($cf_2)){{ 'checked' }}@endif>
					</div>
					<div class="row costom_box_two" style="display: none;">
						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="required_field_one">Required Field </label>
								<input type="checkbox" name="required_field_two" id="required_field_two" value="required"
								 @if(!is_null($cf_2) && $cf_2->opt == 'required'){{ 'checked' }}@endif>
							</div>
						</div>


						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="field_key_two">Field Key 2 <span class="red">*</span></label>
								<input  type="text" class="form-control" id="field_key_two" name="field_key_two" placeholder="Key Field" value="@if(!is_null($cf_2) && isset($cf_2->key_name)){{ $cf_2->key_name }}@endif">
							</div>
						</div>
						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="field_label_two">Field Label 2 (TH) <span class="red">*</span></label>
								<input  type="text" class="form-control" id="field_label_two" name="field_label_two" placeholder="Label 2 (TH)" value="@if(!is_null($cf_2) && isset($cf_2->label_name)){{ $cf_2->label_name }}@endif">
							</div>
						</div>
						<div class="col-md-11 col-md-offset-1">
							<div class="form-group col">
								<label for="field_label_two-en">Field Label 2 (EN) <span class="red">*</span></label>
								<input  type="text" class="form-control" id="field_label_two-en" name="field_label_two_en" placeholder="Label 2 (EN)" value="@if(!is_null($cf_2) && isset($cf_2->label_name_en)){{ $cf_2->label_name_en }}@endif">
							</div>
						</div>
					</div>
					<hr>

					{{-- convenient_times --}}
					@if(1==2)
					<div class="form-group col">
						<label for="custom_filed_two">Option Convenient Times</label>
						<input type="checkbox" name="convenient_times" id="convenient_times" class="toggleswitch"  data-on="yes" data-off="no" @if($dataTypeContent->convenient_times == 'Y'){{ 'checked' }}@endif>
					</div>
					<hr>
					@endif

				</div>
				<!-- panel-body -->

				<div class="panel-footer">
					<button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
				</div>

			</form>

			<iframe id="form_target" name="form_target" style="display:none"></iframe>
			<form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
			enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
			<input name="image" id="upload_file" type="file"
			onchange="$('#my_form').submit();this.value='';">
			<input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
			{{ csrf_field() }}
		</form>


		<!-- form end -->
	</div>
</div>
</div>
</div>

<div class="modal fade modal-danger" id="confirm_delete_modal">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
			</div>

			<div class="modal-body">
				<h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
				<button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
			</div>
		</div>
	</div>
</div>
<!-- End Delete File Modal -->
@stop

@section('javascript')

<script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
<script src="{{customAsset('js/additional-methods.min.js')}}"></script>

{{-- javascript froms --}}
@include('backend.javascript.jquery-froms');

@stop