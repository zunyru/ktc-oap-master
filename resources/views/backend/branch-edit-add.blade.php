@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop
@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)
@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop
@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form
                            id="ktc_form"
                            role="form"
                            {{-- class="form-edit-add" --}}
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if(!is_null($dataTypeContent->getKey()))
                        {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp
                            <input type="hidden" name="form_id" id="form_id" value="{{ $dataTypeContent->id }}">
                            <div class="form-group col">
                                <label for="category-code"> Code <span class="red">*</span></label>
                                <input type="text" id="category-code" class="form-control" placeholder="Code"
                                       name="code" value="{{ isset($dataTypeContent->code) ? trim($dataTypeContent->code) : "" }}">
                                <label for="category-code" id="category-code-error-dup" class="dup-error"
                                       style="display:none;">Category code is already existed</label>
                            </div>
                            <div class="form-group col">
                                <label for="branch-cat">Branch Category <span class="red">*</span></label>
                                <select class="form-control select2" name="branch_cat" id="branch-cat">
                                    <option value="">--เลือก--</option>
                                    @foreach($branch_categories as $key=> $branch_category)
                                    <option value="{{ $branch_category->id }};{{ $branch_category->title }}" @if($dataTypeContent->branch_category_id === $branch_category->id) {{ 'selected' }} @endif>{{ $branch_category->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="product-cat">Product Category <span class="red">*</span></label>
                                <select class="form-control select2" name="product_cat" id="product-cat">
                                    <option value="">--เลือก--</option>
                                    @foreach($product_categories as $key=> $product_category)
                                    <option value="{{ $product_category->id }};{{ $product_category->category_code }}" @if($dataTypeContent->product_category_id === $product_category->id) {{ 'selected' }} @endif>{{ $product_category->name }}, {{ $product_category->category_code }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="name"> Description </label>
                                <textarea class="form-control" id="description" name="description" placeholder="Description" rows="5">@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
                            </div>
                        </div>
                        <!-- panel-body -->
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>
                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                               onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>
                    <!-- form end -->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>
                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
<script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
<script src="{{customAsset('js/additional-methods.min.js')}}"></script>
<script>
    $(function () {
        var typingTimerID;
        var flags = {
            name: {
                duplicated: false
            },
            category_code: {
                duplicated: false
            }
        };

        var setupInputField = function(selector, field) {
            var interval = 300;
            var $input = $(selector);

            $input.on('keyup', function() {
                clearTimeout(typingTimerID);
                typingTimerID = setTimeout(function() {
                    checkDuplicate(selector, field);
                }, interval);
            });
        };

        var checkDuplicate = function(selector, field) {
            var route = "";

            if(field === 'name') {
                route = "{{ route('check.duplicated.product-category', 'name') }}";
            }else if(field === 'category_code') {
                route = "{{ route('check.duplicated.product-category', 'category_code') }}";
            }

            $.post(route, {
                field: field,
                value: $(selector).val(),
                _token: '{{ csrf_token() }}'
            })
                .done(function(data) {
                    if(data === '0') {
                        $(selector).removeClass('error');
                        $(selector + "-error-dup").hide();
                        flags[field].duplicated = false;
                    } else {
                        $(selector).addClass('error');
                        $(selector + "-error-dup").show();
                        flags[field].duplicated = true;
                    }
                });
        };

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        setupInputField("#name", "name");
        setupInputField("#category-code", "category_code");

        tinymce.init({
            menubar: false,
            selector:'textarea.richTextBox_ktc',
            skin: 'voyager',
            min_height: 200,
            resize: 'vertical',
            plugins: 'link, code, textcolor, lists',
            extended_valid_elements : 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
            toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | code',
            convert_urls: false,
            image_caption: true,
            image_title: true,
            init_instance_callback: function (editor) {
                if (typeof tinymce_init_callback !== "undefined") {
                    tinymce_init_callback(editor);
                }
            }
        });

        var form = $('#ktc_form');
        form.validate({
            rules: {
                'code': { required: true },
                'product_cat': { required: true },
                'branch_cat': { required: true },
            },
            messages: {
                'code': "Please specify code",
                'product_cat': "Please specify product category",
                'branch_cat': "Please specify branch category",
            }
        });

        form.on('submit', function(e) {
            e.preventDefault();
            var duplicated = flags.name.duplicated || flags.category_code.duplicated;
            if(!duplicated && form.valid()) {
                form[0].submit();
            } else {
                if(flags.name.duplicated) {
                    $("#name").addClass("error");
                }

                if(flags.category_code.duplicated) {
                    $("#category-code").addClass("error");
                }

                $(window).animate({ scrollTop: 0 });
            }
        });
    });
</script>
@stop

