@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_plural)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_plural }}
    </h1>
    @if($dataTypeContent->trashed())
        <span class="badge badge-pill badge-danger">DELETED</span>
        <a href="{{ route('record.'.$dataType->slug.'.restore', $dataTypeContent->id) }}" type="button" class="btn btn-link">Restore</a>
    @endif
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->

                    <form
                            id="ktc_form"
                            role="form"
                            {{-- class="form-edit-add" --}}
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if(!is_null($dataTypeContent->getKey()))
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            <input type="hidden" name="form_id" id="form_id" value="{{ $dataTypeContent->id }}">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <label for="title">Title <span class="red">*</span></label>
                                    <input  type="text" class="form-control" id="title" name="title" placeholder="Convenien time Option Title" value="{{ isset($dataTypeContent->title) ? trim($dataTypeContent->title) : "" }}">
                                    <label  id="title-error-dup" class="dup-error" for="title" style="display: none;">This convenien time option is already existed</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-xs-12" data-opt-group="min">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <label><input type="radio" id="opt-group-min" aria-label="Option Value" name="opt_group" value="min" {{ isset($dataTypeContent) ? ($dataTypeContent->opt_type == 1 ? "checked" : "") : "" }}></label>
                                        </span>
                                        <input type="text" class="form-control time-minute" placeholder="ติดต่อกลับภายใน mm" name="opt_min_min" value="{{ isset($valueComponents) ? ($valueComponents["operand"] != "" ? $valueComponents["operand"] : "") : "" }}">
                                        <span class="input-group-addon">
                                            <label> นาที</label>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-12" data-opt-group="between">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Option Value" name="opt_group" value="between" {{ isset($dataTypeContent) ? ($dataTypeContent->opt_type == 2 ? "checked" : "") : "" }}>
                                        </div>
                                        <input type="text" class="form-control time" placeholder="จะติดต่อกลับ ภายในตามช่วงเวลา hh:mm" name="opt_between_min" value="{{ isset($valueComponents) ? ($valueComponents["operand"] == "-" ? $valueComponents["min_value"] : "") : "" }}">
                                        <span class="input-group-addon">ถึง</span>
                                        <input type="text" class="form-control time" placeholder="ช่วงเวลา hh:mm" name="opt_between_max" value="{{ isset($valueComponents) ? ($valueComponents["operand"] == "-" ? $valueComponents["max_value"] : "") : "" }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-xs-12" data-opt-group="max">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <label><input type="radio" aria-label="Option Value" name="opt_group" value="max" {{ isset($valueComponents) ? ($valueComponents["operand"] == "After" ? "checked" : "") : "" }}> หลัง</label>
                                        </div>
                                        <input type="text" class="form-control time" name="opt_max_max" value="{{ isset($valueComponents) ? ($valueComponents["operand"] == "After" ? $valueComponents["max_value"] : "") : "" }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>

                    </form>
                    <!-- form end -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
    <script src="{{customAsset('js/additional-methods.min.js')}}"></script>
    <script src="{{customAsset('js/jquery.mask.min.js')}}"></script>
    <script>
        $(function () {

            $('.time-minute').mask('00');
            $('.time').mask('00:00');

            var typingTimerID;
            var flags = {
                title: {
                    duplicated: false
                }
            };
            var optGroup = ['min', 'between', 'max'];

            var setupInputField = function(selector, field) {
                var interval = 300;
                var $input = $(selector);

                $input.on('keyup', function() {
                    clearTimeout(typingTimerID);
                    typingTimerID = setTimeout(function() {
                        checkDuplicate(selector, field);
                    }, interval);
                });
            };

            var checkDuplicate = function(selector, field) {
                var route = "{{ route('check.opt-convenien-time.existed') }}";

                $.post(route, {
                    value: $(selector).val(),
                    option_id: "{{ $dataTypeContent->id }}",
                    _token: '{{ csrf_token() }}'
                })
                    .done(function(data) {
                        if(data === '0') {
                            $(selector).removeClass('error');
                            $(selector + "-error-dup").hide();
                            flags[field].duplicated = false;
                        } else {
                            $(selector).addClass('error');
                            $(selector + "-error-dup").show();
                            flags[field].duplicated = true;
                        }
                    });
            };

            var validateNumberInput = function() {
                var errors = [];
                $('p.opt-error').remove();

                $('.form-group[data-opt-group="'+$('input[type=radio][name="opt_group"]:checked').val()+'"] input[type=text]').each(function(i, el) {
                    var value = $(el).val();
                    if(value.length === 0 || Number(value) <= 0) {
                        errors.push(el);
                        $(el).after('<p class="opt-error">Invalid option value</p>');
                    }
                });

                return errors.length === 0;
            };

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            setupInputField("#title", "title");

            var form = $('#ktc_form');
            form.validate({
                rules: {
                    'title': { required: true },
                    'title_en': { required: true },
                    // 'opt_min_min': { digits: false },
                    // 'opt_between_min': { digits: false },
                    // 'opt_between_max': { digits: false },
                    // 'opt_max_max': { digits: false }
                },
                messages: {
                    'title': "Please enter convenien time option title",
                    'title_en': "Please enter convenien time option title in English",
                    // 'opt_min_min': "Convenien time option required only numbers (0-9)",
                    // 'opt_between_min': "Convenien time option required only numbers (0-9)",
                    // 'opt_between_max': "Convenien time option required only numbers (0-9)",
                    // 'opt_max_max': "Convenien time option required only numbers (0-9)"
                }
            });

            form.on('submit', function(e) {
                e.preventDefault();
                var duplicated = flags.title.duplicated;
                if(!duplicated && form.valid() && validateNumberInput()) {
                    form[0].submit();
                } else {
                    if(flags.title.duplicated) {
                        $("#title").addClass("error");
                    }
                    $(window).animate({ scrollTop: 0 });
                }
            });

            $('input[type=radio][name="opt_group"]').on('change', function(e) {
                $('p.opt-error').remove();
                for(var i in optGroup) {
                    var group = optGroup[i];
                    var selector = '.form-group[data-opt-group="'+group+'"] input[type=text]';
                    if(group !== $(this).val()) {
                        $(selector).attr('disabled', 'disabled');
                    } else {
                        $(selector).removeAttr('disabled');
                    }
                }
            });

            for(var i in optGroup) {
                var selector = '.form-group[data-opt-group="'+optGroup[i]+'"] input[type=text]';
                $(selector).attr('disabled', 'disabled');
            }

            $('.form-group[data-opt-group="'+$('input[type=radio][name="opt_group"]:checked').val()+'"] input[type=text]').removeAttr('disabled');
        });
    </script>
@stop

