@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        @if($browse_state=="trash")
        <i class="voyager-trash"></i>
        {{ 'Trashed ' }}
        @else
        <i class="{{ $dataType->icon }}"></i>
        @endif{{ $dataType->display_name_plural }}
    </h1>
    @if($browse_state!=="trash")
    @can('add', app($dataType->model_name))
    <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
        <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
    </a>
    @endcan
    @endif
    @can('delete', app($dataType->model_name))
    @if($browse_state!=="trash")
      @include('voyager::partials.bulk-delete')
    @else
      @include('backend.partials.bulk-restore')
    @endif
    @endcan
    @can('edit', app($dataType->model_name))
    @if(isset($dataType->order_column) && isset($dataType->order_display_column))
    <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
        <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
    </a>
    @endif
    @endcan
    @include('voyager::multilingual.language-selector')
</div>
@stop

@section('content')
<div class="page-content">
    @include('voyager::alerts')
    {{-- @include('voyager::dimmers') --}}

    <div class="page-content browse container-fluid">
        <div class="alerts">
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="brows-state">
                    <span class="badge badge-info">{{ $count_state->all }}</span> items, <a href="{{ route('voyager.'.$dataType->slug.'.index') }}">active ({{ $count_state->all-$count_state->trashed }})</a> | <a href="{{ route('voyager.'.$dataType->slug.'.trash') }}">trash ({{ $count_state->trashed }})</a>
                </div>
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    @can('delete',app($dataType->model_name))
                                    <th width="5%">
                                        <input type="checkbox" class="select_all">
                                    </th>
                                    @endcan
                                    <th width="5%">
                                        #
                                    </th>
                                    <th width="25%">
                                        Group Title
                                    </th>
                                    <th>Options</th>
                                    <th class="actions text-right" width="20%">
                                        Actions
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($groups as $item)
                                <tr>
                                    @can('delete',app($dataType->model_name))
                                    <td>
                                        <input type="checkbox" name="row_id" id="checkbox_{{ $item->id }} ?>" value="{{ $item->id }}">
                                    </td>
                                    @endcan
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        {{ $item->title }}
                                    </td>
                                    <td>
                                        @php
                                        $option_titles = [];
                                        foreach ($item->options as $option) {
                                            $option_titles[] = $option->title;
                                        }
                                        @endphp
                                        {{ join(" | ", $option_titles) }}
                                        @php($option_titles = [])
                                    </td>
                                    <td class="no-sort no-click bread-actions">
                                        @if($browse_state!=="trash")
                                        @can('delete',app($dataType->model_name))
                                        @if($browse_state!=="trash")
                                        <div class="btn btn-sm btn-danger pull-right delete" data-id="{{ $item->id }}" data-action="{{ route('voyager.'.$dataType->slug.'.destroy', $item->id) }}">
                                            <i class="voyager-trash"></i> {{ __('voyager::generic.delete') }}
                                        </div>
                                        @endif
                                        @endcan
                                        @can('edit',app($dataType->model_name))
                                        <a href="{{ URL('oap/admin/'.$dataType->slug.'/'.$item->id.'/edit ') }}" title="Edit" class="btn btn-sm btn-primary pull-right edit">
                                            <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Edit</span>
                                        </a>
                                        @endcan
                                        @else
                                        <div class="btn btn-sm btn-restore pull-right restore" data-id="{{ $item->id }}">
                                            <i class="voyager-refresh"></i> {{ __('Restore') }}
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ $dataType->display_name_singular }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_this_confirm') }} {{ $dataType->display_name_singular }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Restore modal--}}
    <div class="modal modal-info fade" tabindex="-1" id="restore_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">
                        <i class="voyager-refresh"></i> {{ 'Are you sure you want to restore this' }} {{ $dataType->display_name_singular }}?
                    </h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="restore_form" method="POST">
                        {{ method_field("PUT") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-success pull-right restore-confirm" value="Yes, restore this item">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
<script>
    $(document).ready(function () {
            $('#dataTable').DataTable({
                "order": [[ 1, "asc" ]],
                "columnDefs": [
                {"targets": 0, "searchable":  false, "orderable": false},
                {"targets": 4, "searchable":  false, "orderable": false},
                ],
                responsive: true
            });
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
    });
    $('.btn.delete').on('click', function (e) {
        $('#delete_form').attr('action', $(this).data('action'));
        $('#delete_modal').modal('show');
    });
    $('#delete_modal').on('hide.bs.modal', function (e) {
        $('#delete_form').attr('action', '#');
    });
    $('td').on('click', '.restore', function (e) {
      $('#restore_form')[0].action = '{{ route("record.opt-salary-groups.restore", ['menu' => '__menu']) }}'.replace('__menu', $(this).data('id'));

      $('#restore_modal').modal('show');

    });
</script>
@stop