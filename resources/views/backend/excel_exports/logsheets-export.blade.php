<table>
    <thead>
    <tr>
        <th>DATE</th>
        <th>CUSTUMER_ID </th>
        <th>NAME</th>
        <th>SURNAME</th>
        <th>PRODUCT_CODE</th>
        <th>ACTION</th>
        <th>URL SUBMIT</th>
    </tr>
    </thead>
    <tbody>
        @foreach($log_sheets as $log_sheet)
        <tr>
            <td>{{$log_sheet->created_at ?? ''}}</td>
            <td>{{$log_sheet->cust_no }}</td>
            <td>{{$log_sheet->cust_name}}</td>
            <td>{{$log_sheet->cust_lname}}</td>
            <td>{{$log_sheet->product_id}}</td>
            <td>{{$log_sheet->status ?? NULL}}</td>
            <td>{{$log_sheet->url ?? NULL}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
