<table>
    <thead>
    <tr>
        <th>ID</th>
        <th>FIRST_NAME</th>
        <th>LAST_NAME</th>
        <th>TELEPHONE</th>
        <th>EMAIL</th>
        <th>INCOME</th>
        <th>CARD_TYPE</th>
        <th>PRODUCT_NAME</th>
        <th>PRODUCT_CODE</th>
        <th>INDICATOR</th>
        <th>SUB_INDICATOR</th>
        <th>BRANCH_CODE</th>
        <th>SUBMIT_DATE</th>
        <th>PREVIOUS_PAGE</th>
        <th>CALLBACK_LINK</th>
        <th>SUBMIT_FORM_URL</th>
        <th>LEAD_DEST</th>
        <th>CF1_KEY</th>
        <th>CF1_VAL</th>
        <th>CF2_KEY</th>
        <th>CF2_VAL</th>
        <!-- <th>CONVENIENT_TIME</th>
        <th>TRANSFER_TO</th>
        <th>DUPLICATED</th> -->
    </tr>
    </thead>
    <tbody>
        @foreach($leads as $lead)
        @php
            /**
                 * {"inbound": null, "callback": null, "outbound": null, "indicator": null, "sub_indicator": null}
                 */
                $params = json_decode($lead->custom_param, true);

                /**
                 * {"value": "222", "key_name": "bbb"}
                 */
                $cf_1 = json_decode($lead->cf_1, true);
                $cf_2 = json_decode($lead->cf_2, true);

                $card_type = null;
                if(isset($lead->category_code)) {
                    $code = $lead->category_code;
                    if($code == "CC") {
                        $card_type = "CREDIT";
                    } elseif($code == "PL") {
                        $card_type = "PROUD";
                    } elseif($code == "FIXED") {
                        $card_type = "CASH";
                    } elseif($code == "LCASH"){
                        $card_type = "LOAN";
                    } elseif($code == "LCAR"){
                        $card_type = "LOAN";
                    } else{
                        $card_type = "";
                    }
                }


        @endphp
        <tr>
            <td>{{$lead->lead_unique_id ?? ''}}</td>
            <td>{{$lead->fname}}</td>
            <td>{{$lead->lname}}</td>
            <td>{{$lead->tel}}</td>
            <td>{{$lead->email}}</td>
            <td>{{$lead->opt_value ?? NULL}}</td>
            <td>{{$card_type}}</td>
            <td>{{$lead->title}}</td>
            <td>{{$lead->sys_product_id}}</td>
            <td>{{$params["indicator"] ?: NULL}}</td>
            <td>{{$params["sub_indicator"] ?: NULL}}</td>
            <td>{{$lead->branch_code}}</td>
            <td>{{$lead->lead_created_at}}</td>
            <td>{{$params["inbound"] ?: NULL}}</td>
            <td>{{$params["callback"] ?: NULL}}</td>
            <td>{{$lead->slug ? route('page-index', $lead->slug) : "404"}}</td>
            <td>{{$lead->lead_dest ?? NULL}}</td>
            <td>{{$cf_1["key_name"] ?? NULL}}</td>
            <td>{{$cf_1["value"] ?? NULL}}</td>
            <td>{{$cf_2["key_name"] ?? NULL}}</td>
            <td>{{$cf_2["value"] ?? NULL}}</td>
           <!--  <td>{{$lead->convenient_time ?? NULL}}</td>
            <td>{{$lead->lead_dest ?? NULL}}</td>
            <td>{{($lead->is_first == 1) ? 'N' : 'Y'}}</td> -->
        </tr>
        @endforeach
    </tbody>
</table>