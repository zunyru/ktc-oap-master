<table>
    <thead>
    <tr>
        <th>DATE</th>
        <th>CUSTUMER_ID </th>
        <th>NAME</th>
        <th>SURNAME</th>
        <th>CARD</th>
        <th>IS_DUPPLICATED</th>
    </tr>
    </thead>
    <tbody>
        @foreach($visas as $visa)
        <tr>
            <td>{{$visa->created_at ?? ''}}</td>
            <td>{{$visa->customer_id }}</td>
            <td>{{$visa->fname}}</td>
            <td>{{$visa->lname}}</td>
            <td>{{$visa->product_id}}</td>
            <td>{{$visa->is_duplicated ?? NULL}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
