<table>
    <thead>
    <tr>
        <th>DATE</th>
        <th>CUSTUMER_ID </th>
        <th>NAME</th>
        <th>SURNAME</th>
        <th>CARD</th>
        <th>IS_DUPPLICATED</th>
    </tr>
    </thead>
    <tbody>
        @foreach($unionpays as $unionpay)
        <tr>
            <td>{{$unionpay->created_at ?? ''}}</td>
            <td>{{$unionpay->customer_id }}</td>
            <td>{{$unionpay->fname}}</td>
            <td>{{$unionpay->lname}}</td>
            <td>{{$unionpay->product_id}}</td>
            <td>{{$unionpay->is_duplicated ?? NULL}}</td>
        </tr>
        @endforeach
    </tbody>
</table>
