@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @if($dataTypeContent->trashed())
        <span class="badge badge-pill badge-danger">DELETED</span>
        <a href="{{ route('record.'.$dataType->slug.'.restore', $dataTypeContent->id) }}" type="button" class="btn btn-link">Restore</a>
    @endif
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->

                    <form
                            id="ktc_form"
                            role="form"
                            {{-- class="form-edit-add" --}}
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                    @if(!is_null($dataTypeContent->getKey()))
                        {{ method_field("PUT") }}
                    @endif

                    <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                        <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            <input type="hidden" name="form_id" id="form_id" value="{{ $dataTypeContent->id }}">
                            <div class="row">
                                <div class="form-group col-xs-12">
                                    <label for="title">Title <span class="red">*</span></label>
                                    <input  type="text" class="form-control" id="title" name="title" placeholder="Group Title" value="{{ isset($dataTypeContent->title) ? trim($dataTypeContent->title) : "" }}">
                                    <label  id="title-error-dup" class="dup-error" for="title" style="display: none;">This group title is already existed</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group box-options">
                                    @foreach(\App\OptSalary::with('groups')->get() as $option)
                                        <div class="col-md-12">
                                            @if(!is_null($dataTypeContent->getKey()))
                                                <input type="checkbox"
                                                       id="cb-{{ $option->id }}"
                                                       name="options[]"
                                                       class="option-checkbox"
                                                       value="{{ $option->id }}"
                                                        {{ $option->groups->contains('id', $dataTypeContent->id) ? "checked" : "" }}>
                                            @else
                                                <input type="checkbox" id="cb-{{ $option->id }}" name="options[]" class="option-checkbox" value="{{ $option->id }}">
                                            @endif
                                                <label for="cb-{{ $option->id }}">{{ $option->title }}</label>
                                        </div>
                                    @endforeach
                                    <div id="options-error-placeholder" class="col-xs-12"></div>
                                </div>
                            </div>
                        </div>
                        <!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>

                    </form>
                    <!-- form end -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
    <script src="{{customAsset('js/additional-methods.min.js')}}"></script>
    <script>
        $(function () {
            var typingTimerID;
            var flags = {
                title: {
                    duplicated: false
                }
            };

            var setupInputField = function(selector, field) {
                var interval = 300;
                var $input = $(selector);

                $input.on('keyup', function() {
                    clearTimeout(typingTimerID);
                    typingTimerID = setTimeout(function() {
                        checkDuplicate(selector, field);
                    }, interval);
                });
            };

            var checkDuplicate = function(selector, field) {
                var route = "{{ route('record.'.$dataType->slug.'.check-existed') }}";

                $.post(route, {
                    field: field,
                    value: $(selector).val(),
                    data_id: "{{ $dataTypeContent->id }}",
                    _token: '{{ csrf_token() }}'
                })
                    .done(function(data) {
                        if(data === '0') {
                            $(selector).removeClass('error');
                            $(selector + "-error-dup").hide();
                            flags[field].duplicated = false;
                        } else {
                            $(selector).addClass('error');
                            $(selector + "-error-dup").show();
                            flags[field].duplicated = true;
                        }
                    });
            };

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            setupInputField("#title", "title");

            var form = $('#ktc_form');
            form.validate({
                rules: {
                    'title': { required: true },
                    'options[]': {
                        require_from_group: [1, ".option-checkbox"]
                    }
                },
                groups: {
                    'option-checkbox': "options[]"
                },
                messages: {
                    'title': "Please enter salary option title",
                    'options[]': "Please select at least one option"
                },
                errorPlacement: function(error, element) {
                    if(element.attr('name') === "title") {
                        error.insertAfter(element);
                    } else {
                        $('#options-error-placeholder').append(error);
                    }
                }
            });

            form.on('submit', function(e) {
                e.preventDefault();
                var duplicated = flags.title.duplicated;
                if(!duplicated && form.valid()) {
                    form[0].submit();
                } else {
                    if(flags.title.duplicated) {
                        $("#title").addClass("error");
                    }
                    $(window).animate({ scrollTop: 0 });
                }
            });

        });
    </script>
@stop

