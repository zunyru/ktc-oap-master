@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', $dataType->display_name_plural.' Ordering')

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{$dataType->display_name_plural.' Ordering' }}
    </h1>
    @if($dataTypeContent->trashed())
        <span class="badge badge-pill badge-danger">DELETED</span>
        <a href="{{ route('record.'.$dataType->slug.'.restore', $dataTypeContent->id) }}" type="button" class="btn btn-link">Restore</a>
    @endif
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <div class="panel-heading">
                        <p class="panel-title" style="color:#777">Drag and drop the Option Convenient Times Items below to re-arrange them.</p>
                    </div>
                    <div class="panel-body" style="padding:30px;">
                    <!-- form start -->
                    <form>
                        <ol class="dd-list" id="sortable">
                            @foreach ($info as $item)
                        <li class="dd-item" data-id="{{$item->id}}">
                                <div class="dd-handle">
                                    <span>{{$item->title}}</span>
                                </div>
                            </li>
                             @endforeach
                        </ol>
                    </form>

                    <!-- form end -->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
    <script src="{{customAsset('js/additional-methods.min.js')}}"></script>
    <script src="{{customAsset('js/jquery-sortable.js')}}"></script>
    <script>
        $( function() {
            $( "#sortable" ).sortable({
                delay: 150,
                stop: function() {
                    var selectedData = new Array();
                    $('.dd-item').each(function() {
                        selectedData.push($(this).attr("data-id"));
                    });
                    console.log(selectedData)
                    updateOrder(selectedData);
                }
            });

            function updateOrder(data) {
                $.ajax({
                    url:"{{ route('opt-convenient-times-order-list') }}",
                    type:'post',
                    data:{position:data},
                    _token: '{{ csrf_token() }}',
                    success:function(){
                        toastr.success("Successfully Order List");
                    }
                })
            }
            $( "#sortable" ).disableSelection();
        } );
    </script>
@stop

