@extends('voyager::master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
<h1 class="page-title">
	<i class="{{ $dataType->icon }}"></i>
	{{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
</h1>
@include('voyager::multilingual.language-selector')
@stop

@section('content')
<div class="page-content edit-add container-fluid">
	<div class="row">
		<div class="col-md-12">

			<div class="panel panel-bordered">
				<!-- form start -->

				<form
				id="ktc_form"
				role="form"
				{{-- class="form-edit-add" --}}
				action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
				method="POST" enctype="multipart/form-data">
				<!-- PUT Method if we are editing -->
				@if(!is_null($dataTypeContent->getKey()))
				{{ method_field("PUT") }}
				@endif

				<!-- CSRF TOKEN -->
				{{ csrf_field() }}

				<div class="panel-body">
					@if (count($errors) > 0)
					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif

					<!-- Adding / Editing -->
					@php
					$dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
					@endphp

					<input type="hidden" name="form_id" id="form_id" value="{{ $dataTypeContent->id }}">
					<div class="form-group col">
						<label for="title">Title <span class="red">*</span></label>
						<input  type="text" class="form-control" id="title" name="title" placeholder="Title" value="@if(isset($dataTypeContent->title)){{ trim($dataTypeContent->title) }}@endif">
						<label  id="title-error-dup" class="dup-error" for="title" style="display: none;">This title already exists</label>
					</div>

					<div class="form-group col">
						<label for="name">Description <span class="red">*</span></label>
						<textarea class="form-control" id="description" name="description" placeholder="Description" rows="5">@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
					</div>

					<div class="form-group col">
						<label for="reference">Product Code <span class="red">*</span></label>
						<input  type="text" class="form-control" id="reference" name="reference" placeholder="Code" value="@if(isset($dataTypeContent->sys_product_id)){{ $dataTypeContent->sys_product_id }}@endif">
					</div>

					<div class="form-group col">
						<label for="product-category">Product Category <span class="red">*</span></label>
						<select name="product_category" id="product-category" class="form-control select2">
							<option value=""></option>
							@foreach(\App\ProductCategory::all() as $category)
								<option value="{{ $category->id }}"{{ $dataTypeContent->product_category_id == $category->id ? " selected" : "" }}>{{ $category->name }}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group col">
						<label for="product_image">Product Image <span class="red">*</span></label>
						<img src="">
						<div class="drag-box">
							<input type="file" data-name="product_image" id="product_image" name="product_image" class="drag-file" accept="image/*" >
						</div>
					</div>

					{{-- masseges --}}

					<hr>
					<h1>Offer Management</h1>
					<div class="form-group col">
						<label for="offer_description">Offer Description</label>
						<textarea class="form-control richTextBox_ktc" id="offer-description" name="offer_description" placeholder="Offer Description" rows="5">@if(isset($dataTypeContent->offer_description)){{ $dataTypeContent->offer_description }}@endif</textarea>
					</div>

					<div class="form-group col">
						<label for="offer_privilege">Offer Privilege</label>
						<textarea class="form-control textarea richTextBox_ktc" id="offer-privilege" name="offer_privilege" placeholder="Offer Privilege" rows="5">@if(isset($dataTypeContent->offer_privilege)){{ $dataTypeContent->offer_privilege }}@endif</textarea>
					</div>

					<div class="form-group col">
						<label for="offer_contact">Offer Contact</label>
						<textarea class="form-control textarea richTextBox_ktc" id="offer-contact" name="offer_contact" placeholder="Offer Privilege" rows="5">@if(isset($dataTypeContent->offer_contact)){{ $dataTypeContent->offer_contact }}@endif</textarea>
					</div>

					<hr>
					<h1>Offer Management Thank You Page</h1>

					<div class="form-group col">
						<label for="offer_mes_privilege">Thank You Message</label>
						<textarea class="form-control textarea richTextBox_ktc" id="offer-mes-thank" name="offer_mes_thank" placeholder="Thank You Message" rows="5">@if(isset($dataTypeContent->offer_mes_thank)){{ $dataTypeContent->offer_mes_thank }}@endif</textarea>
					</div>

					<div class="form-group col">
						<label for="offer_mes_contact">Contact Thank You Message</label>
						<textarea class="form-control textarea richTextBox_ktc" id="offer-mes-thank-contact" name="offer_mes_thank_contact" placeholder="Offer Privilege" rows="5">@if(isset($dataTypeContent->offer_mes_thank_contact)){{ $dataTypeContent->offer_mes_thank_contact }}@endif</textarea>
					</div>
					
					<hr>
					<h1>Offer Management Duplicate Page</h1>
					<div class="form-group col">
						<label for="offer_mes_duplicate">Duplicate Message</label>
						<textarea class="form-control textarea richTextBox_ktc" id="offer-mes-duplicate" name="offer_mes_duplicate" placeholder="Offer Privilege" rows="5">@if(isset($dataTypeContent->offer_mes_duplicate)){{ $dataTypeContent->offer_mes_duplicate }}@endif</textarea>
					</div>

					<div class="form-group col">
						<label for="offer_mes_contact">Contact Duplicate Message</label>
						<textarea class="form-control textarea richTextBox_ktc" id="offer-mes-dup-contact" name="offer_mes_dup_contact" placeholder="Offer Privilege" rows="5">@if(isset($dataTypeContent->offer_mes_dup_contact)){{ $dataTypeContent->offer_mes_dup_contact }}@endif</textarea>
					</div>
					
					<hr>
					<h1>Redirect URL</h1>
					@php
					 if(isset($dataTypeContent->expired_date)){
					 	$dates   = explode("-",$dataTypeContent->expired_date);
					 }
					@endphp
					<div class="form-group col">
						<label for="expired_date">Expired Date</label>
						<input type="text" class="form-control" name="expired_date" value="@if(isset($dataTypeContent->expired_date)){{ $dates[2]."/".$dates[1]."/".$dates[0] }}@endif">
					</div>

					<div class="form-group col">
						<label for="redirect_url">Redirect URL</label>
						<input type="text" class="form-control" name="redirect_url" value="@if(isset($dataTypeContent->redirect_url)){{ $dataTypeContent->redirect_url }}@endif">
					</div>

					



					{{-- END masseges --}}



				</div>
				<!-- panel-body -->

				<div class="panel-footer">
					<button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
				</div>

			</form>

			<iframe id="form_target" name="form_target" style="display:none"></iframe>
			<form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
			enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
			<input name="image" id="upload_file" type="file"
			onchange="$('#my_form').submit();this.value='';">
			<input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
			{{ csrf_field() }}
		</form>


		<!-- form end -->
	</div>
</div>
</div>
</div>

<div class="modal fade modal-danger" id="confirm_delete_modal">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
				aria-hidden="true">&times;</button>
				<h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
			</div>

			<div class="modal-body">
				<h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
				<button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
			</div>
		</div>
	</div>
</div>
<!-- End Delete File Modal -->
@stop

@section('javascript')

<script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
<script src="{{customAsset('js/additional-methods.min.js')}}"></script>

{{-- fileinput --}}
<script src="{{customAsset('js/bootstrap-fileinput/piexif.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/sortable.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/purify.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/purify.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/popper.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/bootstrap.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/fileinput.min.js')}}"></script>

{{-- end fileinput --}}

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script type="text/javascript">

	$('document').ready(function () {

		@if(!is_null($dataTypeContent->getKey()))
		card_image = '{{ Voyager::image($dataTypeContent->pic) }}';
		@endif

		$("#product_image").fileinput({
            required:true,
			@if(!is_null($dataTypeContent->getKey()))
			initialPreview: [card_image],
			initialPreviewAsData: true,
			initialPreviewConfig: [
			{ downloadUrl: card_image  ,extra: {id: {{ $dataTypeContent->id }} },_token : '{{ csrf_token() }}'},
			],
			required:false,
			@endif

			browseClass: "btn btn-primary btn-block",
			showCaption: false,
			showRemove: true,
			showUpload: false,
			overwriteInitial: true,
			allowedFileExtensions: ["jpg", "png", "gif" ,"jpeg"],
			previewFileType: "image",
			browseLabel: "Card Image",
			browseIcon: "<i class=\"voyager-images\" style=\"font-size: 20px;position: relative;top: 4px;\"></i> ",
			removeClass: "btn btn-danger",
			dropZoneTitle :"Drag & drop image here …",
			layoutTemplates: {actionDelete: ''},
			maxFileSize: 1024,
		});

		//console.log($('#product_image').fileinput('getPreview'));

		$('input[name="expired_date"]').daterangepicker({
            singleDatePicker: true,
            locale: {
                format: 'DD/MM/YYYY'
            }
        });
	});

</script>

{{-- javascript froms --}}
@include('backend.javascript.jquery-product');

@stop
