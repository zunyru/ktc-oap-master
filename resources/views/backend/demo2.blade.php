<html lang="th" >
<head>
	<title>Viewing Page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="MgnXcBYwGq9XaT31dqXgLiq13sgqq1ZvLIzC07VF"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

	<!-- Favicon -->

	<link rel="shortcut icon" href="http://ktcoap.test/storage/settings/October2018/cwmAFmMdtly1Qf5shShl.png" type="image/x-icon">




	<!-- App CSS -->
	<link rel="stylesheet" href="http://ktcoap.test/vendor/tcg/voyager/assets/css/app.css">


	<link rel="stylesheet" href="http://ktcoap.test/css/custom_backend.css">

	<!-- In the header -->
	<link href="https://transloadit.edgly.net/releases/uppy/v0.27.5/dist/uppy.min.css" rel="stylesheet">


	<!-- Few Dynamic Styles -->
	<style type="text/css">
	.voyager .side-menu .navbar-header {
		background:#22A7F0;
		border-color:#22A7F0;
	}
	.widget .btn-primary{
		border-color:#22A7F0;
	}
	.widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
		background:#22A7F0;
	}
	.voyager .breadcrumb a{
		color:#22A7F0;
	}
</style>




</head>

<body class="voyager pageleaveform">

	<div class="app-container">

		<div class="page-content edit-add container-fluid">
			<div class="row">
				<div class="col-md-12">


					{{-- <form action="{{ url('/admin/upload') }}" enctype="multipart/form-data" class="dropzone" id="my-dropzone">
						{{ csrf_field() }}

						<div class="file-loading">
							<input id="input-24" name="input24[]" type="file" multiple>
						</div>
					</form> --}}

					<!-- Where the drag and drop will be -->
					<div id="drag-drop-area"></div>
					<input type="hidden" name="file_path" value="" id="file_path">

					<!-- form end -->
				</div>
			</div>
		</div>
	</div>

	<!-- End Delete File Modal -->
	<footer class="app-footer">
		<div class="site-footer-right">
			Made with <i class="voyager-heart"></i> by <a href="http://thecontrolgroup.com" target="_blank">The Control Group</a>
			- v1.1.7
		</div>
	</footer>

	<script type="text/javascript" src="http://ktcoap.test/vendor/tcg/voyager/assets/js/app.js"></script>
	<!-- Javascript Libs -->


	<script type="text/javascript" src="http://ktcoap.test/js/slug.js"></script>

	<!-- Bottom of the page -->
	<script src="https://transloadit.edgly.net/releases/uppy/v0.27.5/dist/uppy.min.js"></script>
	
	<script>
		jQuery(document).ready(function($) {
			var uppy = Uppy.Core({ autoProceed: false })
			uppy.use(Uppy.Dashboard, { target: '#drag-drop-area', inline: true })
			uppy.use(Uppy.XHRUpload, { endpoint: '/admin/upload',fieldName:'file' })
			uppy.setMeta({_token: '{{ csrf_token() }}' })
			uppy.on('upload-success', (file, resp, uploadURL) => {
				console.log(file.name, uploadURL,"resp :"+resp)
				  var data;
				  if($('#file_path').val() === ''){
					data = resp;
					$('#file_path').val(data);
				  }else{
				  	data = $('#file_path').val()+','+resp;
					$('#file_path').val(data);
				  }
			})
		});
		
	</script>
	
</body> 

