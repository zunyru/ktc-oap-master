@extends('voyager::master')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="{{customAsset('css/typeahead.css')}}">
@stop
@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)
@section('page_header')
<h1 class="page-title">
  <i class="{{ $dataType->icon }}"></i>
  {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
</h1>
@stop
@section('content')
<div class="page-content edit-add container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <!-- form start -->
                <form id="ktc_form" role="form" {{-- class="form-edit-add" --}} action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif" method="POST" enctype="multipart/form-data">
                    <!-- PUT Method if we are editing -->
                    @if(!is_null($dataTypeContent->getKey()))
                    {{ method_field("PUT") }}
                    @endif
                    <!-- CSRF TOKEN -->
                    {{ csrf_field() }}
                    <div class="panel-body">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <!-- Adding / Editing -->
                        @php
                        $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                        @endphp
                        <h3 class="form-sec-title"> General </h3>
                        <span class="form-sec-divider"></span>
                        <input type="hidden" name="form_id" id="form_id" value="{{ $dataTypeContent->id }}">
                        <div class="form-group col">
                            <label for="title">Page Title <span class="red">*</span></label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Title (TH)" value="@if(isset($dataTypeContent->title)){{ trim($dataTypeContent->title) }}@endif">
                            <label id="title-error-dup" class="dup-error" for="title" style="display: none;">This title already exists</label>
                        </div>
                        <hr>
                        <div class="form-group col">
                            <label for="title_page">Heading (TH) <span class="red"> *</span></label>
                            <input type="text" class="form-control" id="title_page" name="title_page" placeholder="Heading (TH)" value="@if(isset($dataTypeContent->title)){{ trim($dataTypeContent->title_page) }}@endif">
                            <label id="title-page-error-dup" class="dup-error" for="title_page" style="display: none;">This title already exists</label>
                        </div>
                        <div class="form-group col">
                            <label for="title_page-en">Heading (EN) <span class="red"> *</span></label>
                            <input type="text" class="form-control" id="title_page-en" name="title_page_en" placeholder="Heading (EN)" value="{{ $dataTypeContent->english()->title_page ?: "" }}">
                            <label id="title-page-en-error-dup" class="dup-error" for="title_page-en" style="display: none;">This title already exists</label>
                        </div>
                        <div class="form-group col">
                            <label for="slug">Slug <span class="red">*</span></label>
                            <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug"  value="@if(isset($dataTypeContent->title)){{ trim($dataTypeContent->slug) }}@endif">
                            <label id="slug-error-dup" class="dup-error" for="slug" style="display: none;">This slug already exists</label>
                        </div>
                        <div class="form-group col" id="description">
                            <label for="description">Description (TH)<span class="red"></span></label>
                            <textarea class="form-control"  name="description" placeholder="Description (TH)" rows="5">@if(isset($dataTypeContent->description)){{ $dataTypeContent->description }}@endif</textarea>
                        </div>
                        <div class="form-group col" id="description-en">
                            <label for="description-en">Description (EN)<span class="red"></span></label>
                            <textarea class="form-control"  name="description_en" placeholder="Description (EN)" rows="5">{{ $dataTypeContent->english()->description ?: "" }}</textarea>
                        </div>
                        <div class="form-group col">
                            <label for="product">Product <span class="red">*</span></label>
                            <select class="form-control select2" name="product" id="product">
                                <option data="0" value="">-- select --</option>
                                @foreach($products as $index=> $item)
                                <option data_img="{{ Voyager::image($item->pic) }}" value="{{ $item->id }}" @if($dataTypeContent->Product_id === $item->id) {{ 'selected' }} @endif>{{ $item->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="form">Leave Form <span class="red">*</span></label>
                            <select class="form-control select2" name="form" id="form">
                                <option value="">-- select --</option>
                                @foreach($leaveform as $index=> $item)
                                <option value="{{ $item->id }}" @if($dataTypeContent->LeaveForm_id === $item->id) {{ 'selected' }} @endif>{{ $item->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="layout">Layout <span class="red">*</span></label>
                            <select class="form-control select2" name="layout" id="layout">
                                <option value="">-- select --</option>
                                <option data_img="{{ URL('oap/img/Option_Form.png') }}" value="blank_form" @if($dataTypeContent->layout === "blank_form") {{ 'selected' }} @endif>Blank Layout</option>
                                <option data_img="{{ URL('oap/img/Option_Card.png') }}" value="card_hero" @if($dataTypeContent->layout === "card_hero") {{ 'selected' }} @endif>Card Hero Layout</option>
                                <option data_img="{{ URL('oap/img/Option_Slide.png') }}" value="slide_hero" @if($dataTypeContent->layout === "slide_hero") {{ 'selected' }} @endif>Slider Hero Layout</option>
                            </select>
                        </div>
                        {{-- Page Layout --}}
                        <div id="sub-layout">
                            {{-- normal layout --}}
                            <div id="layout_normal" style="display: none;">
                                <div class="panel panel panel-bordered panel-warning">
                                    <div class="panel-body">
                                        <div class="form-group col">
                                            <label for="setting">Use default card image <span class="red">*</span></label>
                                            <input type="checkbox" name="setting" id="setting" class="toggleswitch" data-on="yes" data-off="no" @if(isset($layouts->setting)) @if($layouts->setting === 'on') {{ 'checked'}}@endif @endif >
                                        </div>
                                        <div class="form-group col card_image">
                                            <label for="card_image">Card Image <span class="red">*</span></label>
                                            <img src="">
                                            <div class="drag-box">
                                                <input type="file" data-name="card_image" id="card_image" name="card_image" class="drag-file" accept="image/*">
                                            </div>
                                        </div>
                                        <div class="form-group col">
                                            <label for="bg_image">Cover Image <span class="red">*</span></label>
                                            <img src="">
                                            <div class="drag-box">
                                                <input type="file" data-name="bg_image" id="bg_image" name="bg_image" class="drag-file" accept="image/*">
                                            </div>
                                        </div>
                                        <div id="header_text">
                                            <div class="form-group col">
                                                <label for="header_text_card">Heading Title (TH) <span class="red">*</span></label>
                                                <input type="text" class="form-control" name="header_text_card" placeholder="Heading (TH)" value="@if($dataTypeContent->layout === "card_hero"){{ $layouts->heading }}@endif">
                                            </div>
                                            <div class="form-group col">
                                                <label for="header_text_card">Heading Title (EN) <span class="red">*</span></label>
                                                <input type="text" class="form-control" name="header_text_card_en" placeholder="Heading (EN)" value="@if($dataTypeContent->layout === "card_hero"){{ $layouts->english()->heading }}@endif">
                                            </div>
                                            <div class="form-group col">
                                                <label for="sub_header_text_card">Heading Description (TH)</label>
                                                <textarea class="form-control" id="sub_header_text_card" name="sub_header_text_card" placeholder="Description (TH)" rows="5">@if($dataTypeContent->layout === "card_hero"){{ $layouts->sub_heading }}@endif</textarea>
                                            </div>
                                            <div class="form-group col">
                                                <label for="sub_header_text_card">Heading Description (EN)</label>
                                                <textarea class="form-control" id="sub_header_text_card-en" name="sub_header_text_card_en" placeholder="Description (EN)" rows="5">@if($dataTypeContent->layout === "card_hero"){{ $layouts->english()->sub_heading }}@endif</textarea>
                                            </div>
                                        </div>
                                        @if(is_null($layouts))
                                        <div class="normal-data dynamic-row">
                                            <div class="form-group col ">
                                                <label for="text-feature-1"><span class="row-num">1.</span> Feature Heading(TH)</label>
                                                <input type="text" class="form-control" id="text-feature-1" name="feature[]" placeholder="Feature Heading (TH)" value="">
                                            </div>
                                            <div class="form-group col ">
                                                <label for="text-feature-en-1">Feature Heading (EN)</label>
                                                <input type="text" class="form-control" id="text-feature-en-1" name="feature_en[]" placeholder="Feature Heading (EN)" value="">
                                            </div>
                                            <div class="form-group col">
                                                <label for="text-feature_sub-1">Feature Sub-heading (TH)</label>
                                                <input class="form-control" id="text-feature_sub-1" name="feature_sub[]" placeholder="Feature Sub-heading (TH)">
                                            </div>
                                            <div class="form-group col">
                                                <label for="text-feature_sub-en-1">Feature Sub-heading (EN)</label>
                                                <input class="form-control" id="text-feature_sub-en-1" name="feature_sub_en[]" placeholder="Feature Sub-heading (EN)">
                                            </div>
                                            <div class="text-right">
                                                <button style="display: none;" class="btn btn btn-danger dynamic-rm-bt " data-target="normal-data"> Delete Item </button>
                                            </div>
                                        </div>
                                        <hr>
                                        @else
                                        @php
                                        $data = $layouts->features;
                                        $data_en = $layouts->english()->features;
                                        $features = json_decode($data);
                                        $features_en = json_decode($data_en);

                                        @endphp
                                        @if(!is_null($features))
                                        @php
                                            $count = sizeof($features->title);
                                        @endphp
                                        @for($i = 0; $i < $count; $i++)
                                        <div class="normal-data dynamic-row">
                                            @php
                                            $row_index = $i + 1;
                                            @endphp
                                            <div class="form-group col ">
                                                <label for="text-feature-{{ $row_index }}"><span class="row-num">{{ $row_index }}.</span> Feature Heading (TH)</label>
                                                <input type="text" class="form-control" id="text-feature-{{ $row_index }}" name="feature[]" placeholder="Feature Heading (TH)" value="{{ $features->title[$i] }}">
                                            </div>
                                            <div class="form-group col ">
                                                <label for="text-feature-en-{{ $row_index }}">Feature Heading (EN)</label>
                                                <input type="text" class="form-control" id="text-feature-en-{{ $row_index }}" name="feature_en[]" placeholder="Feature Heading (EN)" value="{{ $features_en->title[$i] }}">
                                            </div>
                                            <div class="form-group col">
                                                <label for="text-feature_sub-{{ $row_index }}">Feature Sub-heading (TH)</label>
                                                <input class="form-control" id="text-feature_sub-{{ $row_index }}" name="feature_sub[]" placeholder="Feature Sub-heading (TH)" value="{{ $features->sub[$i] }}">
                                            </div>
                                            <div class="form-group col">
                                                <label for="text-feature_sub-en-{{ $row_index }}">Feature Sub-heading (EN)</label>
                                                <input class="form-control" id="text-feature_sub-en-{{ $row_index }}" name="feature_sub_en[]" placeholder="Feature Sub-heading (EN)" value="{{ $features_en->sub[$i] }}">
                                            </div>
                                            <div class="text-right">
                                                <button class="btn btn btn-danger dynamic-rm-bt " data-target="normal-data"> Delete Item </button>
                                            </div>
                                        </div>
                                        <hr>
                                        @endfor
                                        @endif
                                        @endif
                                        <div class="form-group col">
                                            <button class="btn btn-info dynamic-add-bt " type="button" data-target="normal-data"> Add Item </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- End normal layout --}}
                            {{-- Slide layout --}}
                            <div id="layout_slide" style="display: none;">
                                <div class="form-group col">
                                    <label for="slide_image">Slides (TH) <span class="red">*</span></label>
                                    <img src="">
                                    <div class="drag-box">
                                        <input type="file" data-name="slide_image" name="slide_image[]" id="slide_image" class="drag-file" multiple accept="image/*">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label for="slide_image_en">Slides (EN) - leave blank if you want to use the same as Slide (TH)</label>
                                    <img src="">
                                    <div class="drag-box">
                                        <input type="file" data-name="slide_image_en" name="slide_image_en[]" id="slide_image_en" class="drag-file" multiple accept="image/*">
                                    </div>
                                </div>
                                <div class="form-group col">
                                    <label for="header-text">Heading (TH)<span class="red">*</span></label>
                                    <input type="text" class="form-control" id="header-text" name="header_text" placeholder="Heading (TH)" value="@if($dataTypeContent->layout === "slide_hero"){{ $layouts->heading }}@endif">
                                </div>
                                <div class="form-group col">
                                    <label for="header-text-en">Heading (EN) <span class="red">*</span></label>
                                    <input type="text" class="form-control" id="header-text-en" name="header_text_en" placeholder="Heading (EN)" value="@if($dataTypeContent->layout === "slide_hero"){{ $layouts->english()->heading }}@endif">
                                </div>
                                <div class="form-group col">
                                    <label for="sub_header_text">Content (TH)</label>
                                    <textarea class="form-control textarea richTextBox_ktc" id="sub_header_text" name="sub_header_text" placeholder="Content (TH)" rows="5">@if($dataTypeContent->layout === "slide_hero"){{ $layouts->sub_heading }}@endif</textarea>
                                </div>
                                <div class="form-group col">
                                    <label for="sub_header_text-en">Content (EN)</label>
                                    <textarea class="form-control textarea richTextBox_ktc" id="sub_header_text-en" name="sub_header_text_en" placeholder="Content (EN)" rows="5">@if($dataTypeContent->layout === "slide_hero"){{ $layouts->english()->sub_heading }}@endif</textarea>
                                </div>
                                <input type="hidden" id="slide_remove" name="slide_remove" value="">
                                <input type="hidden" id="slide_move" name="slide_move" value="">
                                <input type="hidden" id="slide_remove_en" name="slide_remove_en" value="">
                                <input type="hidden" id="slide_move_en" name="slide_move_en" value="">
                            </div>
                            {{-- End Slide layout --}}
                        </div>
                        {{-- End page Layout--}}
                        @php
                        $content = '<h2 class="content-title">สิทธิประโยชน์</h2>
                        <div class="content-wrap">
                        <div class="row section-service-detail">
                        <div class="col-lg-6">
                        <div class="service-desc">
                        <h5>ประกันการเดินทาง</h5>
                        <p>ประกันอุบัติเหตุการเดินทางทั้งในและต่างประเทศ ด้วยวงเงินประกันคุ้มครองสูงสุด 8,000,000 บาท
                        และประกันกระเป๋าเดินทางสูญเสียและสูญหายจากการเดินทางวงเงินสูงสุด 40,000 บาท/ครั้ง ในกรณี
                        ใช้บัตรเครดิตชำระค่าบัตรโดยสารยานพาหนะสาธารณะ</p>
                        </div>
                        <div class="service-desc">
                        <h5>รับคะแนนสะสมง่าย ๆ</h5>
                        <p>ทุก 25 บาท ที่ใช้จ่ายผ่านบัตรฯ รับ 1 คะแนนสะสม KTC FOREVER REWARDS เพื่อแลกสินค้า/
                        บริการ ณ ร้านค้าที่ร่วมรายการ และคะแนนสะสมไม่มีวันหมดอายุ</p>
                        </div>
                        <div class="service-desc">
                        <h5>เบิกถอนเงินสดได้ 100%</h5>
                        <p>เบิกถอนเงินสดด้วยบัตรเครดิตได้เต็มวงเงินของยอดคงเหลือในขณะนั้น หรือสูงสุดไม่เกิน 200,000
                        บาท/วัน หมายเหตุ จำนวนเงินที่เบิกถอนในแต่ละครั้งหรือต่อวันขึ้นอยู่กับเงื่อนไขและข้อจำกัดของเครื่อง
                        ATM แต่ละธนาคาร</p>
                        </div>
                        <div class="service-desc">
                        <h5>สะดวก คุ้มค่าทุกการเดินทางกับ KTC World Travel Service</h5>
                        <p>บริการข้อมูลเพื่อการเดินทางและท่องเที่ยว พร้อมรับส่วนลดตั๋วเครื่องบินภายในประเทศ 4% จากราคา
                        เต็มของตั๋วปกติ สอบถามรายละเอียด และเงื่อนไขเพิ่มเติม โทร. 02 123 5050 หรือ
                        www.ktcworld.co.th</p>
                        </div>
                        <div class="service-desc">
                        <h5>ค่าธรรมเนียมรายปี</h5>
                        <p>ไม่มีค่าธรรมเนียมแรกเข้าและรายปีตลอดชีพ</p>
                        </div>
                        </div>
                        <div class="col-lg-6 col-space-resp">
                        <div class="service-desc">
                        <h5>แบ่งชำระง่าย</h5>
                        <p>ชำระสินค้า/บริการด้วยอัตราดอกเบี้ยพิเศษ 0% สูงสุด 10 เดือน ผ่านบริการ
                        <a href="">KTC FLEXI เปลี่ยนยอดชำระเต็มเป็นแบ่งชำระได้</a>
                        แบ่งชำระรายการซื้อสินค้า/บริการของเดือนถัดไปได้ง่ายๆ ผ่านบริการ <a href="#">KTC FLEXI by PHONE</a>
                        ด้วยอัตราดอกเบี้ย 0.80% สูงสุด 10 เดือน พิเศษยิ่งขึ้น แบ่งชำระด้วยอัตราดอกเบี้ยพิเศษ 0.69%
                        สำหรับรายการใช้จ่ายที่ญี่ปุ่น
                        </p>
                        </div>
                        <div class="service-desc">
                        <h5>เงื่อนไขการใช้บริการ KTC FLEXI</h5>
                        <ul>
                        <li>สมาชิกบัตรเครดิต KTC JCB PLATINUM ทุกประเภท สามารถใช้บริการแบ่งชำระ KTC FLEXI
                        ด้วยอัตราดอกเบี้ย 0% เฉพาะสินค้าที่ระบุว่าร่วมรายการแบ่งชำระ 0% ตามที่กำหนด</li>
                        <li>สินค้าต้องมีราคาขั้นต่ำ 3,000 บาท โดยสามารถใช้บริการได้ตามวงเงินที่คงเหลืออยู่ในขณะที่
                        ซื้อสินค้า</li>
                        <li>สำหรับสินค้าที่ไม่ได้ร่วมรายการแบ่งชำระ KTC FLEXI 0% สมาชิกบัตรเครดิต KTC JCB
                        PLATINUM ทุกประเภท สามารถใช้บริการแบ่งชำระ KTC FLEXI ได้ในอัตราดอกเบี้ย 0.80%
                        โดยสามารถเลือกระยะเวลาแบ่งชำระได้ตั้งแต่ 3 – 10 เดือน ทั้งนี้สินค้าต้องมีราคาขั้นต่ำ
                        3,000 บาท</li>
                        <li>อัตราดอกเบี้ย 0.80% ต่อเดือน (Fixed Rate) เทียบเท่ากับอัตราดอกเบี้ยแบบลดต้นลดดอก
                        (Effective Rate) สำหรับระยะเวลา 6 เดือน เท่ากับ 16.27% ต่อปี, 9 เดือนเท่ากับ 16.96% ต่อปี,
                        10 เดือน เท่ากับ 17.09% ต่อปี เป็นต้น</li>
                        </ul>
                        </div>
                        </div>
                        </div>
                        </div>';
                        @endphp
                        <div class="form-group col" id="box_content" style="display:none">
                            <label for="heading">Content (TH)</label>
                            <textarea class="form-control richTextBox" id="content" name="content" placeholder="Content">@if(isset($dataTypeContent->content)){{ $dataTypeContent->content }}@else{!! $content !!}@endif</textarea>
                            <hr>
                            <label for="content-en">Content (EN)</label>
                            <textarea class="form-control richTextBox" id="content-en" name="content_en" placeholder="Content (EN)">{!! $dataTypeContent->english()->content ? e($dataTypeContent->english()->content) : $content !!}</textarea>
                        </div>
                        {{-- Custom Parameter from json to object --}}
                        <h3 class="form-sec-title"> Parameters </h3>
                        <span class="form-sec-divider"></span>
                        @php
                        $custom_param=json_decode($dataTypeContent->custom_param);
                        @endphp
                        <div class="form-group col" id="the-basics">
                            <label for="indicator">Indicator</label>
                            <input type="text" class="form-control typeahead" id="indicator" name="indicator" placeholder="Indicator" value="{{$custom_param->indicator ?? NULL}}">
                        </div>
                        <div class="form-group col">
                            <label for="sub-indicator">Sub-indicator</label>
                            <input type="text" class="form-control " id="sub-indicator" name="sub_indicator" placeholder="Sub-indicator" value="{{$custom_param->sub_indicator ?? NULL}}">
                        </div>
                        <div class="form-group col">
                            <label for="inbound">Inbound </label>
                            <textarea class="form-control " id="inbound" name="inbound" placeholder="Inbound" rows="5">@if(isset($custom_param->inbound)){{ $custom_param->inbound }}@endif</textarea>
                        </div>
                        <div class="form-group col">
                            <label for="outbound">Outbound</label>
                            <textarea class="form-control " id="outbound" name="outbound" placeholder="Outbound" rows="5">@if(isset($custom_param->outbound)){{ $custom_param->outbound }}@endif</textarea>
                        </div>
                        <div class="form-group col">
                            <label for="callback">Callback Link </label><br>
                            <small style="color: #999">ใช้ "{IDENTIFIER}" เป็นตัวแปรแทนค่า Lead ID บนระบบนี้</small>
                            <textarea class="form-control " id="callback" name="callback" placeholder="Callback Link" rows="5">@if(isset($custom_param->callback)){{ $custom_param->callback }}@endif</textarea>
                        </div>
                        <div class="form-group col">
                            <label for="branch-code"> Branch Code </label>
                            <select class="form-control select2" name="branch_code" id="branch-code">
                                <option data="0" value="">-- select --</option>
                                @foreach($branches as $key=> $branch)
                                <option value="{{ $branch->id }}" @if($dataTypeContent->branch_id === $branch->id) {{ 'selected' }} @endif>{{ $branch->title }} : {{ $branch->code }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="public">Public <span class="red"></span></label>
                            <input type="checkbox" name="public" id="public" class="toggleswitch" data-on="yes" data-off="no"  @if(isset($dataTypeContent->public)) @if($dataTypeContent->public === 1) {{''}}@else {{ 'checked' }}@endif @endif>
                        </div>
                    </div>
                    <br>
                    <hr>
                    {{-- panel-body --}}
                    <div class="panel-footer">
                        <p>Please save before preview.</p>
                        <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        @if(!is_null($dataTypeContent->getKey()))
                        <a target="_blank" class="btn btn-warning preview" href="{{route('page-index',['namepage'=>trim($dataTypeContent->slug),'preview'=>1])}}"> Preview </a>
                        @endif
                    </div>
                </form>
                <iframe id="form_target" name="form_target" style="display:none"></iframe>
                <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post" enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                    <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                    <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                    {{ csrf_field() }}
                </form>
                {{-- form end --}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-danger" id="confirm_delete_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
        aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
    </div>

    <div class="modal-body">
        <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
        <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
    </div>
</div>
</div>
</div>
{{-- End Delete File Modal  --}}
@stop

@section('javascript')

<script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
<script src="{{customAsset('js/additional-methods.min.js')}}"></script>
<script src="{{customAsset('js/slug.js')}}"></script>

{{-- fileinput --}}
<script src="{{customAsset('js/bootstrap-fileinput/piexif.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/sortable.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/purify.min.js')}}"></script>

<script src="{{customAsset('js/bootstrap-fileinput/fileinput.min.js')}}"></script>

{{-- end fileinput --}}

<script src="{{customAsset('js/typeahead.bundle.js')}}"></script>

<script>
    var PageLayout = Object.freeze({blank: "", card: "card_hero", slide: "slide_hero"});
    var page = {
        layout: PageLayout.blank
    };
    $('document').ready(function () {

        // $('#description').hide();
        // $('#description-en').hide();
        var updateLayout = function(layout) {
          if(layout === PageLayout.card) {
            $('#layout_normal').show();
            $('#layout_slide').hide();
            $('#box_content').show();
            $('#header_text').hide();
        } else if(layout === PageLayout.slide) {
            $('#layout_slide').show();
            $('#header_text').show();
            $('#layout_normal').hide();
            $('#box_content').hide();
            $('#box_content').hide();

        } else {
            $('#layout_slide').hide();
            $('#layout_normal').hide();
            $('#box_content').hide();
            $('#header_text').hide();
        }

        page.layout = layout;
    };

    @if(!is_null($layouts))
    @if(!is_null($dataTypeContent->getKey()))
    bg_image = '{{ Voyager::image($layouts->card_cover) }}';
    @endif
    @endif

    $("#bg_image").fileinput({
        @if(!is_null($layouts))
        @if(!is_null($dataTypeContent->getKey()))
        @if(Voyager::image($layouts->card_cover) != '')
        initialPreview: [bg_image],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: bg_image  ,extra: {id: {{ $layouts->id }} },_token : '{{ csrf_token() }}'},
        ],
        deleteUrl: "{{ route('file-delete') }}",
        @endif
        @endif
        @endif

        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif" ,"jpeg"],
        previewFileType: "image",
        browseLabel: "Card Image",
        browseIcon: "<i class=\"voyager-images\" style=\"font-size: 20px;position: relative;top: 4px;\"></i> ",
        removeClass: "btn btn-danger",
        dropZoneTitle :"Drag & drop image here …",
        required: true,
        maxFileSize: 1024,
        validateInitialCount: true
    });

    @if(!is_null($layouts))
    @if(!is_null($dataTypeContent->getKey()))
    card_image = '{{ Voyager::image($layouts->card_hero) }}';
    @endif
    @endif

    $("#card_image").fileinput({
        @if(!is_null($layouts))
        @if(!is_null($dataTypeContent->getKey()))
        @if(($layouts->setting !== 'on') && (Voyager::image($layouts->card_hero) != ''))
        initialPreview: [card_image ],
        initialPreviewAsData: true,
        initialPreviewConfig: [
        { downloadUrl: card_image  ,extra: {id: {{ $layouts->id }} },_token : '{{ csrf_token() }}'},
        ],
        deleteUrl: "{{ route('file-delete') }}",
        @endif
        @endif
        @endif

        browseClass: "btn btn-primary btn-block",
        showCaption: false,
        showRemove: true,
        showUpload: false,
        overwriteInitial: false,
        allowedFileExtensions: ["jpg", "png", "gif" ,"jpeg"],
        previewFileType: "image",
        browseLabel: "Card Image",
        browseIcon: "<i class=\"voyager-images\" style=\"font-size: 20px;position: relative;top: 4px;\"></i> ",
        removeClass: "btn btn-danger",
        dropZoneTitle :"Drag & drop image here …",
        required: true,
        maxFileSize: 1024,
        validateInitialCount: true
    });

   //file-muti-delete
   @if(!is_null($layouts))
   @if(!is_null($dataTypeContent->getKey()))


   var slide_img = [];
   var slide_img_en = [];

   @php
   $card_slides = json_decode($layouts->card_slide, true);
   $data = isset($card_slides) && isset($card_slides["default"]) ? $card_slides["default"] : null;
   $data_en = isset($card_slides) && isset($card_slides["en"]) ? $card_slides["en"] : null;
   $slide = $data;
   $slide_en = $data_en;

   if(isset($card_slides) && !isset($card_slides["default"]) && count($card_slides) > 0) {
        $data = $card_slides;
        $slide = $data;
   }
   @endphp
   @endif
   @endif

   $("#slide_image_en").fileinput({
       @if(isset($layouts) && isset($data_en) && !is_null($dataTypeContent->getKey()))
       initialPreview: [@foreach($slide_en as $item) '{{ \TCG\Voyager\Facades\Voyager::image($item) }}', @endforeach],
       initialPreviewAsData: true,
       initialPreviewConfig: [
       @foreach($slide_en as $key => $item)
           {downloadUrl : '{{ Voyager::image($item) }}', key: {{ $key }}, extra : { id: {{ $key }} ,image_url : '{{ Voyager::image($item) }}' },_token : '{{ csrf_token() }}'},
       @endforeach
       ],
       deleteUrl: "{{ route('file-delete') }}",
       @endif
       browseClass: "btn btn-primary btn-block",
       showCaption: false,
       showRemove: true,
       showUpload: false,
       overwriteInitial: false,
       allowedFileExtensions: ["jpg", "png", "gif" ,"jpeg"],
       previewFileType: "image",
       browseLabel: "Slide Image",
       browseIcon: "<i class=\"voyager-images\" style=\"font-size: 20px;position: relative;top: 4px;\"></i> ",
       removeClass: "btn btn-danger",
       dropZoneTitle :"Drag & drop image here …",
       required: true,
       maxFileSize: 1024,
       maxFileCount: 5,
       validateInitialCount: true
   })
       .on('filebeforedelete', function(event, key, data) {
           //console.log('key = ' + key + ', data = ' + data.id + 'imag =' + data.image_url);
           slide_img_en.push(data.image_url);
           //console.log(slide_img);
           $('#slide_remove_en').val(slide_img_en.toString());
       })
       .on('filesorted', function (event, params) {
           var data = params.stack;
           var slideMoved = [];
           for (var i = 0; i < data.length; i++) {
               slideMoved.push(data[i].extra.image_url);
           }
           $('#slide_move_en').val(slideMoved.toString());
       });

   $("#slide_image").fileinput({
    @if(!is_null($layouts))
    @if(!is_null($data))
    @if(!is_null($dataTypeContent->getKey()))
    initialPreview: [
    @foreach($slide as $key => $item)
    '{{ Voyager::image($item) }}',
    @endforeach
    ],
    initialPreviewAsData: true,
    initialPreviewConfig: [
    @foreach($slide as $key => $item)
    {downloadUrl : '{{ Voyager::image($item) }}', key: {{ $key }}, extra : { id: {{ $key }} ,image_url : '{{ Voyager::image($item) }}' },_token : '{{ csrf_token() }}'},
    @endforeach
    ],
    deleteUrl: "{{ route('file-delete') }}",
    @endif
    @endif
    @endif
    browseClass: "btn btn-primary btn-block",
    showCaption: false,
    showRemove: true,
    showUpload: false,
    overwriteInitial: false,
    allowedFileExtensions: ["jpg", "png", "gif" ,"jpeg"],
    previewFileType: "image",
    browseLabel: "Slide Image",
    browseIcon: "<i class=\"voyager-images\" style=\"font-size: 20px;position: relative;top: 4px;\"></i> ",
    removeClass: "btn btn-danger",
    dropZoneTitle :"Drag & drop image here …",
    required: true,
    maxFileSize: 1024,
    maxFileCount: 5,
    validateInitialCount: true
}).on('filebeforedelete', function(event, key, data) {
     //console.log('key = ' + key + ', data = ' + data.id + 'imag =' + data.image_url);
     slide_img.push(data.image_url);
     //console.log(slide_img);
     $('#slide_remove').val(slide_img.toString());

 });


$('#slide_image').on('filesorted', function(event, params) {

  var data = params.stack;
  var slide_move = [];
  for (var i = 0; i < data.length; i++) {
          //console.log(data[i].extra.image_url);
          slide_move.push(data[i].extra.image_url);
      }
      console.log(slide_move);

      $('#slide_move').val(slide_move.toString());
  });

   // console.log($('#slide_image').fileinput('getPreview'));



    //Selec2
    updateLayout($('#layout').val());

    $('#layout').on('select2:select', function (e) {

      var data = e.params.data;
      console.log(data.id);
      updateLayout(data.id);
  });


   //Dupicate div
   $(".dynamic-add-bt").click(function() {

    //console.log($(this).data("target"))

    var row = $("." + $(this).data("target") + ":first").clone(true);
    var numrow = $("." + $(this).data("target")).length;
    var target = $(this).parent();
    row.find(".row-num").html(String(numrow + 1) + ".");
    row.find("input").val("");

    row.find(".dynamic-rm-bt").show();
    row.insertBefore(target);

    if(numrow === 3){
      $(this).hide()
  }
});
   $(".dynamic-rm-bt").click(function() {
    var numrow = $("." + $(this).data("target")).length;
    if ($("." + $(this).data("target")).length > 1) {
      $(this)
      .closest(".dynamic-row")
      .remove();

      $.each($("." + $(this).data("target") + " .row-num"), function(key, value) {
        $(this).html(String(key + 1) + ".");
    });
  }
  if ($("." + $(this).data("target")).length == 1) {
      $("." + $(this).data("target") + " .dynamic-rm-bt").hide();
  }else{
      $("." + $(this).data("target") + " .dynamic-rm-bt").show();
  }
  if ($("input[name='" + $(this).data("target") + "']").hasClass("dup-group-count")) {
      dupGroupCount($(this).data("target"));
  }

  if(numrow <= 4){
      $(".dynamic-add-bt").show()
  }

  return false;
});

});

//typeahead
var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    matches = [];

    substrRegex = new RegExp(q, 'i');

    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

var states = {!! $indicator_values !!}

$('#the-basics .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 0
},
{
  name: 'states',
  source: substringMatcher(states)
});
</script>
{{-- javascript pages --}}
@include('backend.javascript.jquery-pages');

@if(is_null($dataTypeContent->getKey()))
@include('backend.javascript.validate-page-add');
@else
@include('backend.javascript.validate-page-edit');
@endif
@stop