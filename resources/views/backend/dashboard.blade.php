@extends('voyager::master')
@section('content')
<div class="page-content page-content container-fluid">
    <div class="dashboard Dashboard--full">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="panel-custom panel-primary-custom">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3 logo-col">
                                <i class="icon voyager-file-text fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{!! number_format($pages['count']) !!}</div>
                                <div>Pages</div>
                            </div>
                            <div class="col-xs-12 child-count-list">
                                {{date('F')}} : <span>{!! number_format($pages['count_month']) !!}</span>
                            </div>
                            <div class="col-xs-12 child-count-list">
                                {{date('Y')}} : <span>{!! number_format($pages['count_year']) !!}</span>
                            </div>
                        </div>
                    </div>
                    <!-- <a href="{{ $pages['link'] }}">
                        <div class="panel-footer">
                            <span class="pull-left">{!! $pages['text'] !!}</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a> -->
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="panel-custom panel-green-custom">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-3 logo-col">
                                <i class="icon voyager-people fa-5x"></i>
                            </div>
                            <div class="col-xs-9 text-right">
                                <div class="huge">{!! number_format($leads['count']) !!}</div>
                                <div>Leads</div>
                            </div>
                            <div class="col-xs-12 child-count-list">
                                {{date('F')}} : <span>{!! number_format($leads['count_month']) !!}</span>
                            </div>
                            <div class="col-xs-12  child-count-list">
                                {{date('Y')}} : <span>{!! number_format($leads['count_year']) !!}</span>
                            </div>
                        </div>
                    </div>
                    <!-- <a href="{{ $leads['link'] }}">
                        <div class="panel-footer">
                            <span class="pull-left">{!! $leads['text'] !!}</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a> -->
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3>Top 10 Leave Form Page</h3>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body-page">
                        <div class="list-group">

                            @foreach($top as $index => $item)
                                <a target="_blank" href="{{ route('page-index',$item->slug) }}" class="list-group-item" style="height: 60px;">
                                    {{ $index + 1 .". ".$item->title }}
                                    @if(isset($item->code))
                                    <br>
                                    <span class="text-muted small"><em>{{ $item->branches_name }}: {{ $item->code }}</em>
                                </span>
                                   @endif
                                </a>
                            @endforeach


                        </div>
                        <!-- /.list-group -->
                       <!--  <a href="{{ $pages['link'] }}" class="btn btn-default btn-block">View All</a> -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                @include('backend.partials.dashboard-report')
            </div>
        </div>
    </div>
</div>
@stop
