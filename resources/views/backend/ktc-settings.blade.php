@extends('voyager::master')
@section('page_title', "Settings")
@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="voyager-settings"></i> KTC Settings
        </h1>
    </div>
@stop
@section('css')
    <style>
        .panel-actions .voyager-trash {
            cursor: pointer;
        }
        .panel-actions .voyager-trash:hover {
            color: #e94542;
        }
        .settings .panel-actions{
            right:0px;
        }
        .panel hr {
            margin-bottom: 10px;
        }
        .panel {
            padding-bottom: 15px;
        }
        .sort-icons {
            font-size: 21px;
            color: #ccc;
            position: relative;
            cursor: pointer;
        }
        .sort-icons:hover {
            color: #37474F;
        }
        .voyager-sort-desc {
            margin-right: 10px;
        }
        .voyager-sort-asc {
            top: 10px;
        }
        .page-title {
            margin-bottom: 0;
        }
        .panel-title code {
            border-radius: 30px;
            padding: 5px 10px;
            font-size: 11px;
            border: 0;
            position: relative;
            top: -2px;
        }
        .modal-open .settings  .select2-container {
            z-index: 9!important;
            width: 100%!important;
        }
        .new-setting {
            text-align: center;
            width: 100%;
            margin-top: 20px;
        }
        .new-setting .panel-title {
            margin: 0 auto;
            display: inline-block;
            color: #999fac;
            font-weight: lighter;
            font-size: 13px;
            background: #fff;
            width: auto;
            height: auto;
            position: relative;
            padding-right: 15px;
        }
        .settings .panel-title{
            padding-left:0px;
            padding-right:0px;
        }
        .new-setting hr {
            margin-bottom: 0;
            position: absolute;
            top: 7px;
            width: 96%;
            margin-left: 2%;
        }
        .new-setting .panel-title i {
            position: relative;
            top: 2px;
        }
        .new-settings-options {
            display: none;
            padding-bottom: 10px;
        }
        .new-settings-options label {
            margin-top: 13px;
        }
        .new-settings-options .alert {
            margin-bottom: 0;
        }
        #toggle_options {
            clear: both;
            float: right;
            font-size: 12px;
            position: relative;
            margin-top: 15px;
            margin-right: 5px;
            margin-bottom: 10px;
            cursor: pointer;
            z-index: 9;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .new-setting-btn {
            margin-right: 15px;
            position: relative;
            margin-bottom: 0;
            top: 5px;
        }
        .new-setting-btn i {
            position: relative;
            top: 2px;
        }
        textarea {
            min-height: 120px;
        }
        textarea.hidden{
            display:none;
        }

        .voyager .settings .nav-tabs{
            background:none;
            border-bottom:0px;
        }

        .voyager .settings .nav-tabs .active a{
            border:0px;
        }

        .select2{
            width:100% !important;
            border: 1px solid #f1f1f1;
            border-radius: 3px;
        }

        .voyager .settings input[type=file]{
            width:100%;
        }

        .settings .select2-selection{
            height: 32px;
            padding: 2px;
        }

        .voyager .settings .nav-tabs > li{
            margin-bottom:-1px !important;
        }

        .voyager .settings .nav-tabs a{
            text-align: center;
            background: #f8f8f8;
            border: 1px solid #f1f1f1;
            position: relative;
            top: -1px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
        }

        .voyager .settings .nav-tabs a i{
            display: block;
            font-size: 22px;
        }

        .tab-content{
            background:#ffffff;
            border: 1px solid transparent;
        }

        .tab-content>div{
            padding:10px;
        }

        .settings .no-padding-left-right{
            padding-left:0px;
            padding-right:0px;
        }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
            background:#fff !important;
            color:#62a8ea !important;
            border-bottom:1px solid #fff !important;
            top:-1px !important;
        }

        .nav-tabs > li a{
            transition:all 0.3s ease;
        }


        .nav-tabs > li.active > a:focus{
            top:0px !important;
        }

        .voyager .settings .nav-tabs > li > a:hover{
            background-color:#fff !important;
        }
        .page-content.settings.container-fluid{
            padding-right:15px !important;
        }
        .custom-val-col{
            display:none;
        }
    </style>
@stop
@section('content')
    <div class="page-content">
        @include('voyager::alerts')
        {{-- @include('voyager::dimmers') --}}
        <div class="page-content browse container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form id="setting-form" action="{{route("admin.ktc-settings.update",'update')}}" method="POST" enctype="multipart/form-data">
                        <div class="panel">
                            <div class="page-content settings container-fluid">
                                <ul class="nav nav-tabs">
                                    <li class="active" >
                                        <a data-toggle="tab" href="#general">General</a>
                                    </li>
                            <li class="">
                                <a data-toggle="tab" href="#custom-script">Custom Script</a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#hide-header">Hide Header on Pages</a>
                            </li>
                                </ul>
                            @method('PATCH')
                            {{ csrf_field() }}
                                <div class="tab-content">
                                    <div id="general" class="tab-pane fade in active">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">
                                                Destination Page for Global Menu
                                            </h3>
                                        </div>
                                        <div class="panel-body no-padding-left-right">
                                            <div class="row">
                                                <div class="col-md-6 ">
                                                    <div class="form-group">
                                                        <label for="cc">Credit Card</label>
                                                        <select class="form-control select2" name="cc" id="cc">
                                                                <option value=""> -- select -- </option>
                                                            @foreach($pageleaveforms as $index => $pageleaveform)
                                                                <option value="{{$pageleaveform->slug}}" {{($global_dest->CC[0]==$pageleaveform->slug?"selected":NULL)}}>{{$pageleaveform->title_page}}</option>
                                                            @endforeach
                                                                <option value="custom_val" {{($global_dest->CC[0]=="custom_val"?"selected":NULL)}}> Custom URL ...  </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 custom-val-col">
                                                    <div class="form-group">
                                                        <label for="custom_cc" > Custom URL </label>
                                                        <input class="form-control" id="custom_cc" type="text" name="custom_cc" value="{{$global_dest->CC[1] ?? NULL }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 ">
                                                    <div class="form-group ">
                                                        <label for="index">Proud</label>
                                                        <select class="form-control select2" name="pl" id="index">
                                                                <option value=""> -- select -- </option>
                                                            @foreach($pageleaveforms as $index => $pageleaveform)
                                                                <option value="{{$pageleaveform->slug}}" {{($global_dest->PL[0]==$pageleaveform->slug?"selected":NULL)}}>{{$pageleaveform->title_page}}</option>
                                                            @endforeach
                                                                <option value="custom_val" {{($global_dest->PL[0]=="custom_val"?"selected":NULL)}}> Custom URL ...  </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 custom-val-col">
                                                    <div class="form-group ">
                                                        <label for="custom_pl" > Custom URL </label>
                                                        <input class="form-control" id="custom_pl" type="text" name="custom_pl" value="{{$global_dest->PL[1] ?? NULL }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fixed">Cash</label>
                                                        <select class="form-control select2" name="fixed" id="fixed">
                                                                 <option value=""> -- select -- </option>
                                                            @foreach($pageleaveforms as $index => $pageleaveform)
                                                                <option value="{{$pageleaveform->slug}}" {{($global_dest->FIXED[0]==$pageleaveform->slug?"selected":NULL)}}>{{$pageleaveform->title_page}}</option>
                                                            @endforeach
                                                                <option value="custom_val" {{($global_dest->FIXED[0]=="custom_val"?"selected":NULL)}}> Custom URL ... </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 custom-val-col">
                                                    <div class="form-group ">
                                                        <label for="custom_fixed" > Custom URL </label>
                                                        <input class="form-control" id="custom_fixed" type="text" name="custom_fixed" value="{{$global_dest->FIXED[1] ?? NULL }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="plcfc">PLCFC</label>
                                                        <select class="form-control select2" name="plcfc" id="plcfc">
                                                                 <option value=""> -- select -- </option>
                                                            @foreach($pageleaveforms as $index => $pageleaveform)
                                                                <option value="{{$pageleaveform->slug}}" {{($global_dest->PLCFC[0]==$pageleaveform->slug?"selected":NULL)??NULL}}>{{$pageleaveform->title_page}}</option>
                                                            @endforeach
                                                                <option value="custom_val" {{($global_dest->PLCFC[0]=="custom_val"?"selected":NULL)??NULL}}> Custom URL ... </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 custom-val-col">
                                                    <div class="form-group ">
                                                        <label for="custom_plcfc" > Custom URL </label>
                                                        <input class="form-control" id="custom_plcfc" type="text" name="custom_plcfc" value="{{$global_dest->PLCFC[1] ?? NULL }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            <hr>
                                    </div>
                                <div id="custom-script" class="tab-pane fade in ">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                        Custom Script for Header
                                        </h3>
                                    </div>
                                    <div class="panel-body no-padding-left-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="header-script">Insert contents/scripts before <&#47;header> tag. (please include all necessary codes.) </label>
<div id="header-script" class="editor" style="height:300px;">{{$custom_script->header ?? NULL }}</div>
                                                    <textarea class="form-control hidden" name="header_content" id="header-content" rows="5" cols="70" readonly="">{{$custom_script->header ?? NULL }}</textarea>
                                                </div>
                                </div>
                            </div>
                        </div>
                                    <hr>
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                        Custom Script for Footer
                                        </h3>
                                    </div>
                                    <div class="panel-body no-padding-left-right">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="footer-script">Insert contents/scripts before <&#47;body> tag. (please include all necessary codes.) </label>
<div id="footer-script" class="editor" style="height:300px;">{{$custom_script->footer ?? NULL }}</div>
                                                    <textarea class="form-control hidden" name="footer_content" id="footer-content" rows="5" cols="70" readonly="">{{$custom_script->footer ?? NULL }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div id="hide-header" class="tab-pane fade in">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">
                                            Hide Header on Pages
                                        </h3>
                                    </div>
                                    <div class="panel-body no-padding-left-right">
                                        <div class="row">
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label for="hide_header_on_pages">Fill the page slugs with comma (,) separate</label>
                                                    <input id="hide_header_on_pages" type="text" class="form-control" name="hide_header_on_pages" value="{{$hide_header_on_pages}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <hr>
                                </div>
                            </div>

                </div>
            </div>
                <button type="submit" class="btn btn-primary pull-right submit-bt">{{ __('voyager::settings.save') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
@section('javascript')
<script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
<script src="{{customAsset('js/additional-methods.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.4.2/ace.js"></script>
<script type="text/javascript">
    jQuery( document ).ready(function($) {
        var textarea_header = $('#header-content');
        var editor_header = ace.edit("header-script");
        editor_header.setTheme("ace/theme/twilight");
        editor_header.getSession().setMode("ace/mode/html");

        editor_header.getSession().on('change', function () {
            textarea_header.val(editor_header.getSession().getValue());
        });
        textarea_header.val(editor_header.getSession().getValue());

        var textarea_footer = $('#footer-content');
        var editor_footer = ace.edit("footer-script");
        editor_footer.setTheme("ace/theme/twilight");
        editor_footer.getSession().setMode("ace/mode/html");

        editor_footer.getSession().on('change', function () {
            textarea_footer.val(editor_footer.getSession().getValue());
        });
        textarea_footer.val(editor_footer.getSession().getValue());

        $("select").on("change",function(){
            if ($(this).val()=="custom_val") {
                $(this).closest(".row").find(".custom-val-col").show();
            }else{
                $(this).closest(".row").find(".custom-val-col").hide();
            }
        });
        $("select").change();

        $('#setting-form').validate({
            rules: {
                'cc': { required: true },
                'pl': { required: true },
                'fixed': { required: true },
                'custom_cc': { required: true,url: true },
                'custom_pl': { required: true,url: true },
                'custom_fixed': { required: true,url: true },
                'custom_plcfc': { required: true,url: true }
            },
            errorPlacement: function(error, element) {
                //Custom position: first name
                if (element.hasClass("select2")) {
                    error.appendTo($(element).closest(".form-group"));
                }else {
                    error.insertAfter(element);
                }
            },
        });
        // $(".submit-bt").on("click",function(){
        //     $("#setting-form")[0].submit();
        // });
    });
</script>
@stop