@extends('voyager::master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
<h1 class="page-title">
  <i class="{{ $dataType->icon }}"></i>
  {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
</h1>
@include('voyager::multilingual.language-selector')
@stop

@section('content')
<div class="page-content edit-add container-fluid">
  <div class="row">
    <div class="col-md-12">

      <div class="panel panel-bordered">
        <!-- form start -->

        <form
        id="ktc_form"
        role="form"
        {{-- class="form-edit-add" --}}
        action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
        method="POST" enctype="multipart/form-data">
        <!-- PUT Method if we are editing -->
        @if(!is_null($dataTypeContent->getKey()))
        {{ method_field("PUT") }}
        @endif

        <!-- CSRF TOKEN -->
        {{ csrf_field() }}

        <div class="panel-body">
          @if (count($errors) > 0)
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          <!-- Adding / Editing -->
          @php
          $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
          @endphp

          <h3 class="form-sec-title"> General </h3>
          <span class="form-sec-divider"></span>

          <input type="hidden" name="form_id" id="form_id" value="{{ $dataTypeContent->id }}">
          <div class="form-group col">
            <label for="title">First name <span class="red">*</span></label>
            <input  type="text" class="form-control" id="fname" name="fname" placeholder="First name" value="@if(isset($dataTypeContent->fname)){{ trim($dataTypeContent->fname) }}@endif">
            <label  id="fname-error-dup" class="dup-error" for="fname" style="display: none;">This title already exists</label>
          </div>

          <div class="form-group col">
            <label for="name">Last name <span class="red">*</span></label>
            <input  type="text" class="form-control" id="lname" name="lname" placeholder="Last name " value="@if(isset($dataTypeContent->lname)){{ $dataTypeContent->lname}}@endif">
          </div>

          <div class="form-group col">
            <label for="phone">Phone <span class="red">*</span></label>
            <input  type="text" class="form-control phone_with_ddd" id="phone" name="phone" placeholder="Phone" value="@if(isset($dataTypeContent->tel)){{ $dataTypeContent->tel }}@endif">
          </div>

          <div class="form-group col">
            <label for="email">E-mail <span class="red">*</span></label>
            <input type="text" class="form-control" id="txtEmail" name="email" placeholder="E-mail" value="@if(isset($dataTypeContent->email)){{ $dataTypeContent->email }}@endif">
          </div>

          <div class="form-group col">
            <label for="salary">Salary <span class="red">*</span></label>
            <select class="form-control" name="salary">
              @foreach($salaries as $index=> $salary)
              <option @if( $dataTypeContent->opt_Salary_id == $salary->id) {{ 'selected' }} @endif value="{{ $salary->id }}">{{ $salary->title }}</option>
              @endforeach
            </select>
          </div>

          <div class="form-group col">
            <label for="email">E-mail <span class="red">*</span></label>
            <input type="text" class="form-control" id="txtEmail" name="email" placeholder="E-mail" value="@if(isset($dataTypeContent->email)){{ $dataTypeContent->email }}@endif">
          </div>

          @php
            $cf_1 = json_decode($dataTypeContent->cf_1);
            $cf_2 = json_decode($dataTypeContent->cf_2);
          @endphp

          @if(!is_null($cf_1))
          <div class="form-group col">
            <label for="email">Custom filed 1 : @if(!is_null($cf_1)) {{ $cf_1->key_name }} @endif<span class="red"></span></label>
            <input type="hidden" class="form-control"  name="cf_key_1"  value="@if(!is_null($cf_1)){{ $cf_1->key_name }} @endif">
            <input type="text" class="form-control"  name="cf_1" placeholder="Custom filed 1" value="@if(!is_null($cf_1)){{ $cf_1->value }}@endif">
          </div>
          @endif
          @if(!is_null($cf_2))
          <div class="form-group col">
            <label for="email">Custom filed 2 : @if(!is_null($cf_2)) {{ $cf_2->key_name }} @endif<span class="red"></span></label>
            <input type="hidden" class="form-control"  name="cf_key_2"  value="@if(!is_null($cf_2)) {{ $cf_2->key_name }} @endif">
            <input type="text" class="form-control"  name="cf_2" placeholder="Custom filed 2" value="@if(!is_null($cf_2)) {{ $cf_2->value }}@endif">
          </div>
          @endif

          {{-- Custom Parameter from json to object --}}
          <h3 class="form-sec-title"> Parameters </h3>
          <span class="form-sec-divider"></span>
          @php
          $custom_param=json_decode($dataTypeContent->custom_param);
          @endphp
          <div class="form-group col">
              <label for="indicator">Indicator</label>
              <input type="text" class="form-control " id="indicator" name="indicator" placeholder="Indicator" value="{{$custom_param->indicator ?? NULL}}">
          </div>
          <div class="form-group col">
              <label for="sub-indicator">Sub-indicator</label>
              <input type="text" class="form-control " id="sub-indicator" name="sub_indicator" placeholder="Sub-indicator" value="{{$custom_param->sub_indicator ?? NULL}}">
          </div>
          <div class="form-group col">
              <label for="inbound">Inbound </label>
              <textarea class="form-control " id="inbound" name="inbound" placeholder="Inbound" rows="5">@if(isset($custom_param->inbound)){{ $custom_param->inbound }}@endif</textarea>
          </div>

          <div class="form-group col">
              <label for="outbound">Outbound</label>
              <textarea class="form-control " id="outbound" name="outbound" placeholder="Outbound" rows="5">@if(isset($custom_param->outbound)){{ $custom_param->outbound }}@endif</textarea>
          </div>
          <div class="form-group col">
              <label for="callback">Callback Link</label>
              <textarea class="form-control " id="callback" name="callback" placeholder="Callback Link" rows="5">@if(isset($custom_param->callback)){{ $custom_param->callback }}@endif</textarea>
          </div>

          <h3 class="form-sec-title"> Reference </h3>
          <span class="form-sec-divider"></span>
          <div class="form-group col">
            <label for="product">Product <span class="red">*</span></label>
            <select class="form-control" name="product" id="product">
              @foreach($products as $index=> $product)
              <option data_img="{{ Voyager::image($product->pic) }}" @if( $dataTypeContent->Product_id == $product->id) {{ 'selected' }} @endif value="{{ $product->id }}">{{ $product->title }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group col">
            <label for="pcode">Product Code </label>
            <input type="text" class="form-control" id="pcode" name="pcode" placeholder="Product Code" value="@if(isset($dataTypeContent->pcode)){{ $dataTypeContent->pcode }}@endif" disabled="">
          </div>
          <div class="form-group col">
            <label for="product">Branch Code </label>
            <input type="text" class="form-control" id="branch-code" name="branch_code" placeholder="Product Code" value="@if(isset($dataTypeContent->branch_code)){{ $dataTypeContent->branch_code }}@endif" disabled="">
          </div>
          <div class="form-group col">
            <label for="product">Submitted From: <span class="red"></span></label>

              @foreach($pageleaveforms as $index=> $page)
              @if( $dataTypeContent->PageLeaveForm_id == $page->id)
              <a target="_blank" href="{{ route('page-index',['namepage' => $page->slug]) }}">{{ $page->slug }}</a>
                @endif
              @endforeach

          </div>
           <div class="form-group col">
              <label for="callback">Full reference URL</label>
              <textarea readonly class="form-control " id="ref_url" name="ref_url" placeholder="Full reference URL" rows="5">@if(isset($custom_param->ref_url)){{ $custom_param->ref_url }}@endif</textarea>
          </div>

         @if($dataTypeContent->isDuplicated())
          <div class="form-group col">
            <label for="duplicated">Duplicated : <span class="red"></span></label>
            <span class="badge badge-pill badge-danger">ข้อมูลซ้ำ</span>
          </div>
         @endif


        </div>
        <!-- panel-body -->

        <div class="panel-footer">
          <button type="submit" class="btn btn-primary save" id="ktc_form">{{ __('voyager::generic.save') }}</button>
        </div>

      </form>

      <iframe id="form_target" name="form_target" style="display:none"></iframe>
      <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
      enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
      <input name="image" id="upload_file" type="file"
      onchange="$('#my_form').submit();this.value='';">
      <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
      {{ csrf_field() }}
    </form>


    <!-- form end -->
  </div>
</div>
</div>
</div>

<div class="modal fade modal-danger" id="confirm_delete_modal">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"
        aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
      </div>

      <div class="modal-body">
        <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
        <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
      </div>
    </div>
  </div>
</div>
<!-- End Delete File Modal -->
@stop

@section('javascript')

<script src="{{customAsset('js/jquery.validate.min.js')}}"></script>
<script src="{{customAsset('js/additional-methods.min.js')}}"></script>
<script type="text/javascript" src="{{customAsset('js/jquery.mask.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function(){
   $('.phone_with_ddd').mask('000 000 0000');

   $('#ktc_form').validate({
    rules: {
      fname: {
       required: true
     },
     lname: {
       required: true
     },
     phone:{
       required: true,
     },
     email: {
      required: true,
      email: true
    },
    salary:{
      required: true,
    },
    product: true,
  },
  messages: {
    fname: "Please specify First name",
    lname: "Please specify Last name",
    phone: "Please specify Phone",
    email: {
      required: "We need your Email address to contact you",
      email: "Your Email address must be in the format of name@domain.com"
    },

  }

});
 });
</script>
@stop