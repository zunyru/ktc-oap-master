<div id="table-dashboard-report" class="panel panel-primary">
    <div class="panel-heading">
        <h3>Report</h3>
    </div>
    <div class="panel-body-page">
        <div class="row">
            <div class="col-xs-12">
                <form method="post" id="filter-leads">
                    <div class="form-group col-md-6">
                        <label for="daterange">Submitted Date</label>
                        <input readonly="" id="daterange" type="text" name="report_date_range" class="form-control"
                               value=""/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="branches">Branch<span class="red"></span></label>
                        <select id="branches" name="branches[]" class="form-control filter" multiple="multiple">
                            @foreach($report['branches'] as $index=> $item)
                                <option value="{{ $item->id }}">{{ $item->code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="branch_categories">Source Type<span class="red"></span></label>
                        <select id="branch_categories" name="branch_categories[]" class="form-control filter"
                                multiple="multiple">
                            @foreach($report['branch_categories'] as $index=> $item)
                                <option value="{{ $item->id }}">{{ $item->code }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <button type="button" class="btn btn-primary btn-filter">Filter</button>
                        <a class="btn btn-warning btn-clear">Clear</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <table id="table-dashboard-leads-info" class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>No. of Leads</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>{{-- $type --}}</td>
                        <td>{{-- $amount --}}</td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th class="active">Total</th>
                        <th class="active text-total">{{-- $total --}}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

{{-- script --}}
@section('javascript')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
<script src="{{ customAsset('js/oap_dashboard_report.js') }}"></script>

<script>
    $(function () {
        var report = new DashboardReport({
            url: '{{ route('api.oap.dashboard_report') }}',
            form: '#filter-leads',
            success: function (data) {
                // console.log('data', data);

                // render
                var sum = 0;
                var leadsTable = $('#table-dashboard-leads-info');
                var tableBody = $('#table-dashboard-leads-info tbody');
                tableBody.children().remove();

                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    var typeCol = "<td>" + item.title + "</td>";
                    var numCol = "<td>" + item.TOTAL + "</td>";
                    var row = "<tr>" + typeCol + numCol + "</tr>";
                    tableBody.append(row);
                    sum += item.TOTAL;
                }

                leadsTable.find('tfoot .text-total').html(sum);
            }
        });

        $(".filter").select2({
            placeholder: "-- Select --",
            allowClear: true,
            tokenSeparators: [',']
        }).on('change', function(e) {
            var target = $(this).attr('id');
            if(target === "branches") {
                report.filters.branches = $(this).val();
            } else if(target === "branch_categories") {
                report.filters.categories = $(this).val();
            }
            // report.applyFilters();
        });

        $('.btn-filter').on('click', function() {
            report.applyFilters();
        });

        var defaultStartDate = moment().startOf('year');
        var now = moment();

        var reportDateRange = $('input[name="report_date_range"]');
        reportDateRange.daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear',
                format: 'DD/MM/YYYY'
            },
            startDate: defaultStartDate,
            endDate: now
        });

        reportDateRange.val(defaultStartDate.format('DD/MM/YYYY') + '-' + now.format('DD/MM/YYYY'));

        reportDateRange.on('apply.daterangepicker', function (ev, picker) {
            var startDate = picker.startDate.format('DD/MM/YYYY');
            var endDate = picker.endDate.format('DD/MM/YYYY');
            $(this).val(startDate + '-' + endDate);
            report.filters.dates = startDate + '-' + endDate;
            // report.applyFilters();
        });

        reportDateRange.on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            report.filters.dates = '';
            // report.applyFilters();
        });

        $(".btn-clear").on("click",function(e){
            e.preventDefault();
            $('input[name="report_date_range"]').val(moment().startOf('year').format('DD/MM/YYYY') + '-' + moment().format('DD/MM/YYYY'));
            $('.filter').val(null).trigger('change');
            report.applyFilters();
        });

        // initial
        report.applyFilters();
    });
</script>
@stop
