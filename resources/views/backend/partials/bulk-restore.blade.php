<a class="btn btn-restore" id="bulk_restore_btn"><i class="voyager-refresh"></i> <span>
{{ __('validation.restore')  }}
</span></a>

{{-- Bulk restore modal --}}
<div class="modal modal-info fade" tabindex="-1" id="bulk_restore_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <i class="voyager-refresh"></i> {{ __('validation.are_you_sure_restore') }} <span id="bulk_restore_count"></span> <span id="bulk_restore_display_name"></span>?
                </h4>
            </div>
            <div class="modal-body" id="bulk_restore_modal_body">
            </div>
            <div class="modal-footer">
                <form action="{{ route('record.'.$dataType->slug.'.restore',0) }}" id="bulk_restore_form" method="POST">
                    {{ method_field("put") }}
                    {{ csrf_field() }}
                    <input type="hidden" name="ids" id="bulk_restore_input" value="">
                    <input type="submit" class="btn btn-success pull-right restore-confirm"
                             value="{{ __('validation.bulk_restore_confirm') }} {{ strtolower($dataType->display_name_plural) }}">
                </form>
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal">
                    {{ __('voyager::generic.cancel') }}
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
window.onload = function () {
    // Bulk restore selectors
    var $bulkrestoreBtn = $('#bulk_restore_btn');
    var $bulkrestoreModal = $('#bulk_restore_modal');
    var $bulkrestoreCount = $('#bulk_restore_count');
    var $bulkrestoreDisplayName = $('#bulk_restore_display_name');
    var $bulkrestoreInput = $('#bulk_restore_input');
    // Reposition modal to prevent z-index issues
    $bulkrestoreModal.appendTo('body');
    // Bulk restore listener
    $bulkrestoreBtn.click(function () {
        var ids = [];
        var $checkedBoxes = $('#dataTable input[type=checkbox]:checked').not('.select_all');
        var count = $checkedBoxes.length;
        if (count) {
            // Reset input value
            $bulkrestoreInput.val('');
            // Deletion info
            var displayName = count > 1 ? '{{ $dataType->display_name_plural }}' : '{{ $dataType->display_name_singular }}';
            displayName = displayName.toLowerCase();
            $bulkrestoreCount.html(count);
            $bulkrestoreDisplayName.html(displayName);
            // Gather IDs
            $.each($checkedBoxes, function () {
                var value = $(this).val();
                ids.push(value);
            })
            // Set input value
            $bulkrestoreInput.val(ids);
            // Show modal
            $bulkrestoreModal.modal('show');
        } else {
            // No row selected
            toastr.warning('{{ __('validation.bulk_restore_nothing') }}');
        }
    });
}
</script>
