<script>
	var params = {};
	var $image;

	$('document').ready(function () {
		$('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
            	if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
            		elt.type = 'text';
            		$(elt).datetimepicker($(elt).data('datepicker'));
            	}
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif


            $('.side-body input[data-slug-origin]').each(function(i, el) {
            	$(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
            	e.preventDefault();
            	$image = $(this).siblings('img');

            	params = {
            		slug:   '{{ $dataType->slug }}',
            		image:  $image.data('image'),
            		id:     $image.data('id'),
            		field:  $image.parent().data('field-name'),
            		_token: '{{ csrf_token() }}'
            	}

            	$('.confirm_delete_name').text($image.data('image'));
            	$('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
            	$.post('{{ route('voyager.media.remove') }}', params, function (response) {
            		if ( response
            			&& response.data
            			&& response.data.status
            			&& response.data.status == 200 ) {

            			toastr.success(response.data.message);
            		$image.parent().fadeOut(300, function() { $(this).remove(); })
            	} else {
            		toastr.error("Error removing image.");
            	}
            });

            	$('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
          });
        </script>
        <script type="text/javascript">
         $(document).ready(function(){

          $.ajaxSetup({
           headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

          tinymce.init({
           menubar: false,
           selector:'textarea.richTextBox_ktc',
           skin: 'voyager',
           min_height: 200,
           resize: 'vertical',
           plugins: 'link, code, textcolor, lists',
           extended_valid_elements : 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
           file_browser_callback: function(field_name, url, type, win) {
            if(type =='image'){
             $('#upload_file').trigger('click');
           }
         },
         toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | code',
         convert_urls: false,
         image_caption: true,
         image_title: true,
         init_instance_callback: function (editor) {
          if (typeof tinymce_init_callback !== "undefined") {
           tinymce_init_callback(editor);
         }
       }
     });    

              var setting = true;
              //checkbox setting
              if($('#setting').prop("checked") == true){
                  $('.card_image').hide();
                  setting = true;
              }else{
                  $('.card_image').show();
                  setting = false;
              }
              $("#setting").change(function(){
                  if($(this).prop("checked") == true){
                    $('.card_image').hide();
                    setting = true;
                }else{
                    $('.card_image').show();
                    setting = false;
                }
              });

            var check_dup_title = true;
            var check_dup_title_page = true;
            var check_dup_slug = true;


            //setup before functions
            var typingTimer;                //timer identifier
            var doneTypingInterval = 300;  //time in ms, 5 second for example

            var $title_page = $("#title_page");
            var $slug = $("#slug");
            var $title = $("#title");

                $slug.on('keyup', function () {
                  clearTimeout(typingTimer);
                  typingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
                  $('#slug').val(slug($('#slug').val()));
                  $('#slug').removeClass('error');
                  $('#slug-error').hide();
                });
                //on keydown, clear the countdown
                $slug.on('keydown', function () {
                  clearTimeout(typingTimer);
                });

                $title.on('keyup', function () {
                  clearTimeout(typingTimer);
                  typingTimer = setTimeout(doneTyping, doneTypingInterval);
                  $('#title-page').removeClass('error');
                  $('#title-page-error').hide();
                });
                //on keydown, clear the countdown
                $title.on('keydown', function () {
                  clearTimeout(typingTimer);
                });

                $title_page.on('keyup', function () {
                  clearTimeout(typingTimer);
                  typingTimer = setTimeout(doneTypingTitlePage, doneTypingInterval);
                  $('#title_page').removeClass('error');
                  $('#title-page-error').hide();
                });
                //on keydown, clear the countdown
                $title_page.on('keydown', function () {
                  clearTimeout(typingTimer);
                });


            @if(is_null($dataTypeContent->getKey()))
                //on keyup, start the countdown
                $title_page.on('keyup', function () {
                  clearTimeout(typingTimer);
                  typingTimer = setTimeout(doneTypingTitlePage, doneTypingInterval);
                  typingTimer = setTimeout(doneTypingSlug, doneTypingInterval);
                  $('#slug').val(slug($title_page.val()));
                  $('#title_page').removeClass('error');
                  $('#title-page-error').hide();
                });
                //on keydown, clear the countdown
                $title_page.on('keydown', function () {
                  clearTimeout(typingTimer);
                });
            @endif
            //user is "finished typing," do something
            function doneTyping () {
              $.post('{{ route('check.check-dup-title') }}',{ _method:"POST" , title : $title.val(),form_id : $('#form_id').val()  , 
                _db : "Pageleaveform" , _token: '{{ csrf_token() }}' })
              .done(function(data){
               setTimeout(function(){

                if(data === '0'){
                  $('#title').removeClass('error');  
                  $('#title-error-dup').hide()
                  check_dup_title=true;

                }else{
                  $('#title-error-dup').show()
                  $('#title').addClass('error');  
                  check_dup_title=false;
                }
              }, 300);    
             });
            }

            function doneTypingSlug () {
              $.post('{{ route('check.check-dup-slug') }}',{ _method:"POST" , slug : $slug.val(),form_id : $('#form_id').val()  , 
                _db : "Pageleaveform" , _token: '{{ csrf_token() }}' })
              .done(function(data){
               setTimeout(function(){

                if(data === '0'){
                  $('#slug').removeClass('error');  
                  $('#slug-error-dup').hide()
                  check_dup_slug=true;

                }else{
                  $('#slug-error-dup').show()
                  $('#slug').addClass('error');  
                  check_dup_slug=false;
                }
              }, 300);    
             });
            }

            function doneTypingTitlePage () {
              $.post('{{ route('check.check-dup-titlepage') }}',{ _method:"POST" , title_page : $title_page.val(),form_id : $('#form_id').val()  , 
                _db : "Pageleaveform" , _token: '{{ csrf_token() }}' })
              .done(function(data){
               setTimeout(function(){

                if(data === '0'){
                  $('#title_page').removeClass('error');  
                  $('#title-page-error-dup').hide()
                  check_dup_title_page=true;

                }else{
                  //$('#title-page-error-dup').show()
                  //$('#title_page').addClass('error');  
                  //check_dup_title_page=false;
                  check_dup_title_page=true;
                }
              }, 300);    
             });
            }


            $('#ktc_form').on('submit',function(e) {
              e.preventDefault();

              if (check_dup_title === true && check_dup_title_page === true && check_dup_slug === true && $('#ktc_form').valid()) {

                  if(page) {

                    //$("#bg_image").fileinput('upload');
                      if(page.layout === PageLayout.card && $("#card_image").fileinput('getFilesCount') == 0 && setting == false) {
                          $("#card_image").fileinput('upload');
                          $('html, body').animate({
                            scrollTop: $("#card_image").offset().top -200
                          });
                      }else if(page.layout === PageLayout.card && $("#bg_image").fileinput('getFilesCount') == 0) {
                           $('html, body').animate({
                            scrollTop: $("#card_image").offset().top
                          });
                      } else if(page.layout === PageLayout.slide && $("#slide_image").fileinput('getFilesCount') == 0) {
                          $("#slide_image").fileinput('upload');
                          $('html, body').animate({
                              scrollTop: $("#layout_slide").offset().top
                          });
                      } else {
                          $('#ktc_form')[0].submit();
                      }
                  } else {
                      $('#ktc_form')[0].submit();
                  }
              }else{
                    //Dup
                    if (check_dup_title === false){
                      $('#title').addClass('error'); 
                       $('html, body').animate({
                            scrollTop: $("#title").offset().top -200
                        });
                    }
                    if (check_dup_title_page === false){
                      $('#title_page').addClass('error'); 
                      $('html, body').animate({
                            scrollTop: $("#title_page").offset().top -200
                        });
                    }
                    if (check_dup_slug === false){
                      $('#slug').addClass('error'); 
                      $('html, body').animate({
                            scrollTop: $("#slug").offset().top -200
                        });
                    }
                  }

              });

          });


          //select2
          $(document).ready(function(){
            $("#product").select2({
             templateResult: formatStateCard,
             formatSelection: formatStateCard
           });

            $("#layout").select2({
             templateResult: formatStateForm,
             formatSelection: formatStateForm
           });
          });

          function formatStateCard (state) {
            if (!state.id) { return state.text; }
            var $state = $(
             '<span ><img class="img-card" sytle="display: inline-block;" src="' + state.element.getAttribute('data_img') + '" /> ' + state.text + '</span>'
             );
            //console.log(state.element.getAttribute('data_img').toLowerCase())

            return $state;
          }

          function formatStateForm (state) {
            if (!state.id) { return state.text; }
            var $state = $(
             '<span ><img class="img-layout" sytle="display: inline-block;" src="' + state.element.getAttribute('data_img') + '" /> ' + state.text + '</span>'
             );
            //console.log(state.element.getAttribute('data_img').toLowerCase())

            return $state;
          }

        </script>