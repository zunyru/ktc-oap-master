<script>
    var params = {};
    var $image;

    $('document').ready(function () {
        $('.toggleswitch').bootstrapToggle();

        //Init datepicker for date fields if data-datepicker attribute defined
        //or if browser does not handle date inputs
        $('.form-group input[type=date]').each(function (idx, elt) {
            if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                elt.type = 'text';
                $(elt).datetimepicker($(elt).data('datepicker'));
            }
        });

        @if ($isModelTranslatable)
        $('.side-body').multilingual({"editing": true});
        @endif

        $('.side-body input[data-slug-origin]').each(function (i, el) {
            $(el).slugify();
        });

        $('.form-group').on('click', '.remove-multi-image', function (e) {
            e.preventDefault();
            $image = $(this).siblings('img');

            params = {
                slug: '{{ $dataType->slug }}',
                image: $image.data('image'),
                id: $image.data('id'),
                field: $image.parent().data('field-name'),
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text($image.data('image'));
            $('#confirm_delete_modal').modal('show');
        });

        $('#confirm_delete').on('click', function () {
            $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                if (response
                    && response.data
                    && response.data.status
                    && response.data.status == 200) {

                    toastr.success(response.data.message);
                    $image.parent().fadeOut(300, function () {
                        $(this).remove();
                    })
                } else {
                    toastr.error("Error removing image.");
                }
            });

            $('#confirm_delete_modal').modal('hide');
        });
        $('[data-toggle="tooltip"]').tooltip();


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        tinymce.init({
            menubar: false,
            selector: 'textarea.richTextBox_ktc',
            skin: 'voyager',
            min_height: 200,
            resize: 'vertical',
            plugins: 'link, code, textcolor, lists',
            extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
            file_browser_callback: function (field_name, url, type, win) {
                if (type == 'image') {
                    $('#upload_file').trigger('click');
                }
            },
            toolbar: 'styleselect bold italic underline | forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image table youtube giphy | code',
            convert_urls: false,
            image_caption: true,
            image_title: true,
            init_instance_callback: function (editor) {
                if (typeof tinymce_init_callback !== "undefined") {
                    tinymce_init_callback(editor);
                }
            }
        });

    });

</script>

<script>
    $(document).ready(function () {
        $('#ktc_form').validate({
            rules: {
                title: {
                    required: true
                },
                description: {
                    required: true
                },
                reference: {
                    required: true
                },
                product_category: { required: true }
            },
            messages: {
                title: "Please specify Title",
                description: "Please specify Description",
                reference: "Please specify Reference Code",
            }

        });

        var check_dup_title = true;

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 300;  //time in ms, 5 second for example
        var $input = $("#title");

        //on keyup, start the countdown
        $input.on('keyup', function () {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(doneTyping, doneTypingInterval);
        });
        //on keydown, clear the countdown
        $input.on('keydown', function () {
            clearTimeout(typingTimer);
        });

        //user is "finished typing," do something
        function doneTyping() {
            $.post('{{ route('check.check-dup-title') }}', {
                _method: "POST",
                title: $input.val(),
                form_id: $('#form_id').val(),
                _db: "Product",
                _token: '{{ csrf_token() }}'
            })
                .done(function (data) {
                    setTimeout(function () {

                        if (data === '0') {
                            $('#title').removeClass('error');
                            $('#title-error-dup').hide()
                            check_dup_title = true;
                        } else {
                            $('#title-error-dup').show()
                            $('#title').addClass('error');
                            check_dup_title = false;
                        }
                    }, 300);
                });
        }

        $('#ktc_form').on('submit', function (e) {
            e.preventDefault();
            if (check_dup_title === true && $('#ktc_form').valid()) {
                if (!$('#product_image').fileinput('getPreview')) {

                    $("#product_image").fileinput('upload');
                } else {
                    $('#ktc_form')[0].submit();
                }
            } else {
                //Dup
                if (check_dup_title === false) {
                    $('#title').addClass('error');
                }
                $('html, body').animate({
                    scrollTop: 0
                });
            }

        });


    });
</script>