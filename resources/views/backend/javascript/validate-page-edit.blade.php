<script type="text/javascript">
    $('#ktc_form').validate({
        rules: {
            title: {
                required: true
            },
            title_en: {
                required: true
            },
            title_page: {
                required: true
            },
            title_page_en: {
                required: true
            },
            slug: {
                required: true
            },
            product: {
                required: true
            },
            layout: {
                required: true
            },
            form: {
                required: true
            },
            header_text: {
                required: true
            },
            opt_group: {
                required: true
            }
        },
        messages: {
            title: "Please specify Title in Thai",
            title_en: "Please specify Title in English",
            title_page: "Please specify Title Content in Thai",
            title_page_en: "Please specify Title Content in English",
            slug: "Please specify Slug",
            product: "Please select Product Text",
            layout: "Please select Layout",
            form: "Please select Form",
            product: "Please select Product",
            bg_image: "Please chosen Background Hero ",
            bg_image: "Please chosen Card Hero ",
            header_text: "Please specify Heading Title",
            "slide_image[]": "Please add at least 1 slide image",
            "slide_image_en[]": "Please add at least 1 slide image",
            opt_group: "Please select salary options group",
        }

    });

</script>