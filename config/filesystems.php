<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/oap/storage',
            'visibility' => 'public',
        ],

        // 'public' => [
        //     'driver' => 's3',
        //     'key' => env('AWS_ACCESS_KEY_ID'),
        //     'secret' => env('AWS_SECRET_ACCESS_KEY'),
        //     'region' => env('AWS_DEFAULT_REGION'),
        //     'bucket' => env('AWS_BUCKET'),
        //     'url' => env('AWS_URL'),
        // ],

        /*'s3' => [
            'driver' => 's3',
            'key' => 'AKIAJZ5SI2Y72F6MPOBA',
            'secret' => '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',
            'region' => 'ap-southeast-1',
            'bucket' => 'ktc-oap',
            'url' => "https://s3-ap-southeast-1.amazonaws.com/ktc-oap/",
            // 'http'    => [
            //      'proxy' => 'http://172.20.32.44:8080'
            //  ],
        ],*/

        's3' => [
            'driver' => 's3',
            'region' => 'ap-southeast-1',
            'bucket' => 's3-ktc-oap',
            'url' => "https://s3-ap-southeast-1.amazonaws.com/s3-ktc-oap/",
            'credentials' => false
        ],
        'visa-sftp' => [
            'driver' => 'sftp',
            'host' => '172.29.8.25',
            'username' => 'oap_uat',
            'password' => 'New19new',
            'port' => 22,
            'root' => '/data01/UAT/WEB/KTC/OAP'
        ]

        // 'ftp' => [
        //     'driver' => 'ftp',
        //     'host' => '172.29.8.25',
        //     'username' => 'OAP_UAT',
        //     'password' => 'New19new'
        // ],

        /*'sftp' => [
            'driver' => 'sftp',
            'host' => '172.19.8.25',
            'username' => 'oap_prd',
            'password' => 'New19new',
            'port' => 22,
            'root' => '/data02/PRD/WEB/KTC/OAP'
            //'privateKey' => '~/.ssh/id_rsa.pub',
            //'password' => 'encryption-password',
        ],
        'cs-sftp' => [
            'driver' => 'sftp',
            'host' => '172.19.8.25',
            'username' => 'oap_prd',
            'password' => 'New19new',
            'port' => 22,
            'root' => '/data02/PRD/OAP/KTC/CS'
            //'privateKey' => '~/.ssh/id_rsa.pub',
            //'password' => 'encryption-password',
        ],
        'unionpay-sftp' => [
            'driver' => 'sftp',
            'host' => '172.19.8.25',
            'username' => 'oap_prd',
            'password' => 'New19new',
            'port' => 22,
            'root' => '/data02/PRD/OAP/KTC/CAU'
            //'privateKey' => '~/.ssh/id_rsa.pub',
            //'password' => 'encryption-password',
        ]*/

    ],

];